-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2020 at 05:13 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newgen`
--

-- --------------------------------------------------------

--
-- Table structure for table `addon_account`
--

CREATE TABLE `addon_account` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account`
--

INSERT INTO `addon_account` (`name`, `label`, `shared`) VALUES
('bank_savings', 'Bank Savings', 0),
('caution', 'Caution', 0),
('property_black_money', 'Black Money', 0),
('society_ambulance', 'EMS', 1),
('society_banker', 'Bank', 1),
('society_burgershot', 'Burgershot', 1),
('society_cardealer', 'Cardealer', 1),
('society_mechanic', 'Mechanic', 1),
('society_police', 'Police', 1),
('society_taxi', 'Taxi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_account_data`
--

CREATE TABLE `addon_account_data` (
  `id` int(11) NOT NULL,
  `account_name` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) NOT NULL,
  `owner` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_account_data`
--

INSERT INTO `addon_account_data` (`id`, `account_name`, `money`, `owner`) VALUES
(2, 'society_ambulance', 0, NULL),
(3, 'society_banker', 0, NULL),
(4, 'society_cardealer', 0, NULL),
(5, 'society_mechanic', 1000000, NULL),
(6, 'society_police', 1000000, NULL),
(7, 'society_taxi', 0, NULL),
(11, 'bank_savings', 0, 'steam:110000111d0b1aa'),
(12, 'caution', 0, 'steam:110000111d0b1aa'),
(13, 'property_black_money', 0, 'steam:110000111d0b1aa'),
(14, 'bank_savings', 0, 'steam:1100001081d1893'),
(15, 'caution', 0, 'steam:1100001081d1893'),
(16, 'property_black_money', 0, 'steam:1100001081d1893'),
(17, 'bank_savings', 0, 'steam:11000010b49a743'),
(18, 'caution', 0, 'steam:11000010b49a743'),
(19, 'property_black_money', 0, 'steam:11000010b49a743'),
(20, 'bank_savings', 0, 'steam:11000013e7edea3'),
(21, 'caution', 0, 'steam:11000013e7edea3'),
(22, 'property_black_money', 0, 'steam:11000013e7edea3'),
(23, 'bank_savings', 0, 'steam:110000111c332ed'),
(24, 'caution', 0, 'steam:110000111c332ed'),
(25, 'property_black_money', 0, 'steam:110000111c332ed'),
(26, 'bank_savings', 0, 'steam:11000010a01bdb9'),
(27, 'property_black_money', 0, 'steam:11000010a01bdb9'),
(28, 'caution', 0, 'steam:11000010a01bdb9'),
(29, 'bank_savings', 0, 'steam:11000013e9d4b99'),
(30, 'caution', 0, 'steam:11000013e9d4b99'),
(31, 'property_black_money', 0, 'steam:11000013e9d4b99'),
(32, 'bank_savings', 0, 'steam:110000107941620'),
(33, 'caution', 0, 'steam:110000107941620'),
(34, 'property_black_money', 0, 'steam:110000107941620'),
(35, 'bank_savings', 0, 'steam:11000010fe595d0'),
(36, 'caution', 0, 'steam:11000010fe595d0'),
(37, 'property_black_money', 0, 'steam:11000010fe595d0'),
(38, 'bank_savings', 0, 'steam:11000010e2a62e2'),
(39, 'caution', 0, 'steam:11000010e2a62e2'),
(40, 'property_black_money', 0, 'steam:11000010e2a62e2'),
(41, 'bank_savings', 0, 'steam:11000010b88b452'),
(42, 'caution', 0, 'steam:11000010b88b452'),
(43, 'property_black_money', 0, 'steam:11000010b88b452'),
(44, 'society_burgershot', 0, NULL),
(45, 'bank_savings', 0, 'steam:110000105ed368b'),
(46, 'caution', 0, 'steam:110000105ed368b'),
(47, 'property_black_money', 0, 'steam:110000105ed368b'),
(48, 'bank_savings', 0, 'steam:1100001183c7077'),
(49, 'caution', 0, 'steam:1100001183c7077'),
(50, 'property_black_money', 0, 'steam:1100001183c7077'),
(51, 'bank_savings', 0, 'steam:1100001139319c1'),
(52, 'property_black_money', 0, 'steam:1100001139319c1'),
(53, 'caution', 0, 'steam:1100001139319c1'),
(54, 'bank_savings', 0, 'steam:110000132580eb0'),
(55, 'caution', 0, 'steam:110000132580eb0'),
(56, 'property_black_money', 0, 'steam:110000132580eb0'),
(57, 'bank_savings', 0, 'steam:110000119e7384d'),
(58, 'caution', 0, 'steam:110000119e7384d'),
(59, 'property_black_money', 0, 'steam:110000119e7384d'),
(60, 'bank_savings', 0, 'steam:110000139d9adf9'),
(61, 'property_black_money', 0, 'steam:110000139d9adf9'),
(62, 'caution', 0, 'steam:110000139d9adf9'),
(63, 'bank_savings', 0, 'steam:11000013eaca305'),
(64, 'caution', 0, 'steam:11000013eaca305'),
(65, 'property_black_money', 0, 'steam:11000013eaca305'),
(66, 'bank_savings', 0, 'steam:11000013efa68e1'),
(67, 'caution', 0, 'steam:11000013efa68e1'),
(68, 'property_black_money', 0, 'steam:11000013efa68e1'),
(69, 'bank_savings', 0, 'steam:1100001176cddfb'),
(70, 'caution', 0, 'steam:1100001176cddfb'),
(71, 'property_black_money', 0, 'steam:1100001176cddfb'),
(72, 'bank_savings', 0, 'steam:11000013f6b85e1'),
(73, 'caution', 0, 'steam:11000013f6b85e1'),
(74, 'property_black_money', 0, 'steam:11000013f6b85e1'),
(75, 'bank_savings', 0, 'steam:11000013e33b934'),
(76, 'caution', 0, 'steam:11000013e33b934'),
(77, 'property_black_money', 0, 'steam:11000013e33b934'),
(78, 'bank_savings', 0, 'steam:11000013517b942'),
(79, 'caution', 0, 'steam:11000013517b942'),
(80, 'property_black_money', 0, 'steam:11000013517b942'),
(81, 'bank_savings', 0, 'steam:110000134e3ca1a'),
(82, 'property_black_money', 0, 'steam:110000134e3ca1a'),
(83, 'caution', 0, 'steam:110000134e3ca1a'),
(84, 'bank_savings', 0, 'steam:1100001013142e0'),
(85, 'caution', 0, 'steam:1100001013142e0'),
(86, 'property_black_money', 0, 'steam:1100001013142e0'),
(87, 'bank_savings', 0, 'steam:110000133d93ea2'),
(88, 'caution', 0, 'steam:110000133d93ea2'),
(89, 'property_black_money', 0, 'steam:110000133d93ea2'),
(90, 'bank_savings', 0, 'steam:11000011531f6b5'),
(91, 'caution', 0, 'steam:11000011531f6b5'),
(92, 'property_black_money', 0, 'steam:11000011531f6b5'),
(93, 'bank_savings', 0, 'steam:11000010cfb10c1'),
(94, 'property_black_money', 0, 'steam:11000010cfb10c1'),
(95, 'caution', 0, 'steam:11000010cfb10c1'),
(96, 'bank_savings', 0, 'steam:110000135e316b5'),
(97, 'caution', 0, 'steam:110000135e316b5'),
(98, 'property_black_money', 0, 'steam:110000135e316b5'),
(99, 'bank_savings', 0, 'steam:1100001156cfac9'),
(100, 'property_black_money', 0, 'steam:1100001156cfac9'),
(101, 'caution', 0, 'steam:1100001156cfac9'),
(102, 'bank_savings', 0, 'steam:110000136177a4e'),
(103, 'caution', 0, 'steam:110000136177a4e'),
(104, 'property_black_money', 0, 'steam:110000136177a4e'),
(105, 'bank_savings', 0, 'steam:110000116e48a9b'),
(106, 'property_black_money', 0, 'steam:110000116e48a9b'),
(107, 'caution', 0, 'steam:110000116e48a9b'),
(108, 'bank_savings', 0, 'steam:110000106197c1a'),
(109, 'caution', 0, 'steam:110000106197c1a'),
(110, 'property_black_money', 0, 'steam:110000106197c1a');

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory`
--

CREATE TABLE `addon_inventory` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory`
--

INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
('property', 'Property', 0),
('society_ambulance', 'EMS', 1),
('society_burgershot', 'Burgershot', 1),
('society_burgershot_fridge', 'Burgershot(frigorifico)', 1),
('society_cardealer', 'Cardealer', 1),
('society_mechanic', 'Mechanic', 1),
('society_police', 'Police', 1),
('society_taxi', 'Taxi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addon_inventory_items`
--

CREATE TABLE `addon_inventory_items` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `addon_inventory_items`
--

INSERT INTO `addon_inventory_items` (`id`, `inventory_name`, `name`, `count`, `owner`) VALUES
(1, 'society_mechanic', 'cannabis', 26, NULL),
(2, 'society_taxi', 'bread', 0, NULL),
(3, 'society_taxi', 'water', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sender` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `target_type` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`id`, `identifier`, `sender`, `target_type`, `target`, `label`, `amount`) VALUES
(4, 'steam:110000107941620', 'steam:11000010b49a743', 'player', 'steam:11000010b49a743', 'Fine: Driving an illegal Vehicle', 100),
(5, 'steam:110000107941620', 'steam:11000010b49a743', 'player', 'steam:11000010b49a743', 'Fine: Driving without a License', 1500),
(6, 'steam:110000107941620', 'steam:11000010b49a743', 'player', 'steam:11000010b49a743', 'Fine: Disorderly conduct', 90),
(7, 'steam:110000134e3ca1a', 'steam:110000111c332ed', 'player', 'steam:110000111c332ed', 'Fine: Illegal Passing', 100),
(8, 'steam:110000107941620', 'steam:1100001081d1893', 'player', 'steam:1100001081d1893', 'Fine: Illegal Passing', 100);

-- --------------------------------------------------------

--
-- Table structure for table `cardealer_vehicles`
--

CREATE TABLE `cardealer_vehicles` (
  `id` int(11) NOT NULL,
  `vehicle` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE `characters` (
  `id` int(11) NOT NULL,
  `identifier` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `dateofbirth` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `sex` varchar(1) COLLATE utf8mb4_bin NOT NULL DEFAULT 'M',
  `height` varchar(128) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `identifier`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`) VALUES
(2, 'steam:110000111d0b1aa', 'Michelle', 'Tester', '1994-09-17', 'f', '164'),
(3, 'steam:1100001081d1893', 'Godrick', 'Twinkletoes', '1994-09-12', 'm', '200'),
(4, 'steam:11000010b49a743', 'Killian', 'Oconnell', '19960529', 'm', '155'),
(5, 'steam:11000013e7edea3', 'Steve', 'Erwin', '', 'm', '200'),
(6, 'steam:110000111c332ed', 'Darryl', 'Normil', '1987/08/29', 'm', '140'),
(7, 'steam:11000010a01bdb9', 'Tommie', 'Pickles', '1988-12-28', 'm', '170'),
(8, 'steam:11000013e9d4b99', 'Jon', 'Arm', '1995 12 1', 'm', '140'),
(9, 'steam:110000107941620', 'Jon', 'Smtih', '7/23/1993', 'm', '200'),
(10, 'steam:11000010fe595d0', 'Dylan', 'Fernandez', '1998', 'm', '180'),
(11, 'steam:11000010e2a62e2', 'Anthony', 'Smith', '1990-05-24', 'm', '184'),
(12, 'steam:11000010b88b452', 'Sashka', 'Gbbrg', '20000101', 'm', '190'),
(13, 'steam:11000010fe595d0', 'Dylan', 'Fernandez', '19980530', 'm', '200'),
(14, 'steam:110000111d0b1aa', 'Melaficent', 'Spikes', '1994-109-17', 'f', '170'),
(15, 'steam:110000105ed368b', 'Dean', 'Conners', '1985-07-14', 'm', '160'),
(16, 'steam:1100001183c7077', 'Jesse', 'Spikes', '1992-02-25', 'm', '200'),
(17, 'steam:1100001139319c1', 'Michael', 'Jameson', '1994-04-16', 'm', '187'),
(18, 'steam:110000132580eb0', 'Jak', 'Fulton', '1988-10-10', 'm', '174'),
(19, 'steam:110000119e7384d', 'Mohamed', 'Salm', '1998-12-19', 'm', '194'),
(20, 'steam:1100001176cddfb', 'Jesse', 'Anderson', '1985/04/01', 'm', '195'),
(21, 'steam:11000013f6b85e1', 'Gregory', 'Stevens', '1950-09-09', 'm', '200'),
(22, 'steam:11000013e33b934', 'ANGEL', 'HERNANDEZ', '1989/07/06', 'm', '170'),
(23, 'steam:110000134e3ca1a', 'Jones', 'Smih', '04-17-1996', 'm', '140'),
(24, 'steam:110000111c332ed', 'Sara', 'Jones', '1987/08/29', 'f', '140'),
(25, 'steam:1100001013142e0', 'Roy', 'Moshkovich', '1996-07-04', 'm', '180'),
(26, 'steam:110000133d93ea2', 'Nicholas ', 'Silver', '3/17/1999', 'm', '200'),
(27, 'steam:11000011531f6b5', 'Tony', 'Montana', '19902712', 'm', '150'),
(28, 'steam:11000010cfb10c1', 'Tommy', 'Callahan', '1994-06-08', 'm', '165'),
(29, 'steam:110000135e316b5', 'Aaron', 'Freeman', '1993-10-26', 'm', '155'),
(30, 'steam:1100001156cfac9', 'Joey ', 'Mcboby', '08/12/99', 'm', '200'),
(31, 'steam:110000136177a4e', 'Nick', 'Stromlund', '20002505', 'm', '150'),
(32, 'steam:110000116e48a9b', 'Nate', 'Redmond', '19970429', 'm', '160'),
(33, 'steam:110000106197c1a', 'Austin', 'Nocera', '1995-06-09', 'm', '200');

-- --------------------------------------------------------

--
-- Table structure for table `datastore`
--

CREATE TABLE `datastore` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `shared` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore`
--

INSERT INTO `datastore` (`name`, `label`, `shared`) VALUES
('property', 'Property', 0),
('society_ambulance', 'EMS', 1),
('society_burgershot', 'Burgershot', 1),
('society_police', 'Police', 1),
('society_taxi', 'Taxi', 1),
('user_ears', 'Ears', 0),
('user_glasses', 'Glasses', 0),
('user_helmet', 'Helmet', 0),
('user_mask', 'Mask', 0);

-- --------------------------------------------------------

--
-- Table structure for table `datastore_data`
--

CREATE TABLE `datastore_data` (
  `id` int(11) NOT NULL,
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL,
  `data` longtext COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `datastore_data`
--

INSERT INTO `datastore_data` (`id`, `name`, `owner`, `data`) VALUES
(2, 'society_ambulance', NULL, '{}'),
(3, 'society_police', NULL, '{}'),
(4, 'society_taxi', NULL, '{}'),
(10, 'property', 'steam:110000111d0b1aa', '{}'),
(11, 'user_ears', 'steam:110000111d0b1aa', '{}'),
(12, 'user_glasses', 'steam:110000111d0b1aa', '{}'),
(13, 'user_helmet', 'steam:110000111d0b1aa', '{}'),
(14, 'user_mask', 'steam:110000111d0b1aa', '{}'),
(15, 'property', 'steam:1100001081d1893', '{}'),
(16, 'user_ears', 'steam:1100001081d1893', '{}'),
(17, 'user_glasses', 'steam:1100001081d1893', '{}'),
(18, 'user_helmet', 'steam:1100001081d1893', '{}'),
(19, 'user_mask', 'steam:1100001081d1893', '{}'),
(20, 'property', 'steam:11000010b49a743', '{}'),
(21, 'user_ears', 'steam:11000010b49a743', '{}'),
(22, 'user_glasses', 'steam:11000010b49a743', '{}'),
(23, 'user_helmet', 'steam:11000010b49a743', '{}'),
(24, 'user_mask', 'steam:11000010b49a743', '{}'),
(25, 'property', 'steam:11000013e7edea3', '{}'),
(26, 'user_ears', 'steam:11000013e7edea3', '{}'),
(27, 'user_glasses', 'steam:11000013e7edea3', '{}'),
(28, 'user_helmet', 'steam:11000013e7edea3', '{}'),
(29, 'user_mask', 'steam:11000013e7edea3', '{}'),
(30, 'property', 'steam:110000111c332ed', '{}'),
(31, 'user_ears', 'steam:110000111c332ed', '{}'),
(32, 'user_glasses', 'steam:110000111c332ed', '{}'),
(33, 'user_helmet', 'steam:110000111c332ed', '{}'),
(34, 'user_mask', 'steam:110000111c332ed', '{}'),
(35, 'user_ears', 'steam:11000010a01bdb9', '{}'),
(36, 'property', 'steam:11000010a01bdb9', '{}'),
(37, 'user_glasses', 'steam:11000010a01bdb9', '{}'),
(38, 'user_helmet', 'steam:11000010a01bdb9', '{}'),
(39, 'user_mask', 'steam:11000010a01bdb9', '{}'),
(40, 'property', 'steam:11000013e9d4b99', '{}'),
(41, 'user_ears', 'steam:11000013e9d4b99', '{}'),
(42, 'user_glasses', 'steam:11000013e9d4b99', '{}'),
(43, 'user_helmet', 'steam:11000013e9d4b99', '{}'),
(44, 'user_mask', 'steam:11000013e9d4b99', '{}'),
(45, 'user_ears', 'steam:110000107941620', '{}'),
(46, 'property', 'steam:110000107941620', '{}'),
(47, 'user_glasses', 'steam:110000107941620', '{}'),
(48, 'user_mask', 'steam:110000107941620', '{}'),
(49, 'user_helmet', 'steam:110000107941620', '{}'),
(50, 'property', 'steam:11000010fe595d0', '{}'),
(51, 'user_ears', 'steam:11000010fe595d0', '{}'),
(52, 'user_glasses', 'steam:11000010fe595d0', '{}'),
(53, 'user_mask', 'steam:11000010fe595d0', '{}'),
(54, 'user_helmet', 'steam:11000010fe595d0', '{}'),
(55, 'property', 'steam:11000010e2a62e2', '{}'),
(56, 'user_ears', 'steam:11000010e2a62e2', '{}'),
(57, 'user_glasses', 'steam:11000010e2a62e2', '{}'),
(58, 'user_helmet', 'steam:11000010e2a62e2', '{}'),
(59, 'user_mask', 'steam:11000010e2a62e2', '{}'),
(60, 'property', 'steam:11000010b88b452', '{}'),
(61, 'user_ears', 'steam:11000010b88b452', '{}'),
(62, 'user_glasses', 'steam:11000010b88b452', '{}'),
(63, 'user_helmet', 'steam:11000010b88b452', '{}'),
(64, 'user_mask', 'steam:11000010b88b452', '{}'),
(65, 'society_burgershot', NULL, '{}'),
(66, 'property', 'steam:110000105ed368b', '{}'),
(67, 'user_ears', 'steam:110000105ed368b', '{}'),
(68, 'user_glasses', 'steam:110000105ed368b', '{}'),
(69, 'user_helmet', 'steam:110000105ed368b', '{}'),
(70, 'user_mask', 'steam:110000105ed368b', '{}'),
(71, 'property', 'steam:1100001183c7077', '{}'),
(72, 'user_ears', 'steam:1100001183c7077', '{}'),
(73, 'user_glasses', 'steam:1100001183c7077', '{}'),
(74, 'user_helmet', 'steam:1100001183c7077', '{}'),
(75, 'user_mask', 'steam:1100001183c7077', '{}'),
(76, 'property', 'steam:1100001139319c1', '{}'),
(77, 'user_ears', 'steam:1100001139319c1', '{}'),
(78, 'user_helmet', 'steam:1100001139319c1', '{}'),
(79, 'user_mask', 'steam:1100001139319c1', '{}'),
(80, 'user_glasses', 'steam:1100001139319c1', '{}'),
(81, 'property', 'steam:110000132580eb0', '{}'),
(82, 'user_ears', 'steam:110000132580eb0', '{}'),
(83, 'user_glasses', 'steam:110000132580eb0', '{}'),
(84, 'user_helmet', 'steam:110000132580eb0', '{}'),
(85, 'user_mask', 'steam:110000132580eb0', '{}'),
(86, 'user_helmet', 'steam:110000119e7384d', '{}'),
(87, 'user_mask', 'steam:110000119e7384d', '{}'),
(88, 'property', 'steam:110000119e7384d', '{}'),
(89, 'user_glasses', 'steam:110000119e7384d', '{}'),
(90, 'user_ears', 'steam:110000119e7384d', '{}'),
(91, 'property', 'steam:110000139d9adf9', '{}'),
(92, 'user_glasses', 'steam:110000139d9adf9', '{}'),
(93, 'user_mask', 'steam:110000139d9adf9', '{}'),
(94, 'user_ears', 'steam:110000139d9adf9', '{}'),
(95, 'user_helmet', 'steam:110000139d9adf9', '{}'),
(96, 'property', 'steam:11000013eaca305', '{}'),
(97, 'user_ears', 'steam:11000013eaca305', '{}'),
(98, 'user_glasses', 'steam:11000013eaca305', '{}'),
(99, 'user_helmet', 'steam:11000013eaca305', '{}'),
(100, 'user_mask', 'steam:11000013eaca305', '{}'),
(101, 'property', 'steam:11000013efa68e1', '{}'),
(102, 'user_ears', 'steam:11000013efa68e1', '{}'),
(103, 'user_glasses', 'steam:11000013efa68e1', '{}'),
(104, 'user_helmet', 'steam:11000013efa68e1', '{}'),
(105, 'user_mask', 'steam:11000013efa68e1', '{}'),
(106, 'property', 'steam:1100001176cddfb', '{}'),
(107, 'user_glasses', 'steam:1100001176cddfb', '{}'),
(108, 'user_ears', 'steam:1100001176cddfb', '{}'),
(109, 'user_helmet', 'steam:1100001176cddfb', '{}'),
(110, 'user_mask', 'steam:1100001176cddfb', '{}'),
(111, 'property', 'steam:11000013f6b85e1', '{}'),
(112, 'user_ears', 'steam:11000013f6b85e1', '{}'),
(113, 'user_glasses', 'steam:11000013f6b85e1', '{}'),
(114, 'user_helmet', 'steam:11000013f6b85e1', '{}'),
(115, 'user_mask', 'steam:11000013f6b85e1', '{}'),
(116, 'user_helmet', 'steam:11000013e33b934', '{}'),
(117, 'user_mask', 'steam:11000013e33b934', '{}'),
(118, 'user_ears', 'steam:11000013e33b934', '{}'),
(119, 'property', 'steam:11000013e33b934', '{}'),
(120, 'user_glasses', 'steam:11000013e33b934', '{}'),
(121, 'property', 'steam:11000013517b942', '{}'),
(122, 'user_ears', 'steam:11000013517b942', '{}'),
(123, 'user_glasses', 'steam:11000013517b942', '{}'),
(124, 'user_helmet', 'steam:11000013517b942', '{}'),
(125, 'user_mask', 'steam:11000013517b942', '{}'),
(126, 'property', 'steam:110000134e3ca1a', '{}'),
(127, 'user_ears', 'steam:110000134e3ca1a', '{}'),
(128, 'user_helmet', 'steam:110000134e3ca1a', '{}'),
(129, 'user_mask', 'steam:110000134e3ca1a', '{}'),
(130, 'user_glasses', 'steam:110000134e3ca1a', '{}'),
(131, 'user_helmet', 'steam:1100001013142e0', '{}'),
(132, 'user_mask', 'steam:1100001013142e0', '{}'),
(133, 'property', 'steam:1100001013142e0', '{}'),
(134, 'user_ears', 'steam:1100001013142e0', '{}'),
(135, 'user_glasses', 'steam:1100001013142e0', '{}'),
(136, 'user_helmet', 'steam:110000133d93ea2', '{}'),
(137, 'user_mask', 'steam:110000133d93ea2', '{}'),
(138, 'property', 'steam:110000133d93ea2', '{}'),
(139, 'user_ears', 'steam:110000133d93ea2', '{}'),
(140, 'user_glasses', 'steam:110000133d93ea2', '{}'),
(141, 'property', 'steam:11000011531f6b5', '{}'),
(142, 'user_ears', 'steam:11000011531f6b5', '{}'),
(143, 'user_glasses', 'steam:11000011531f6b5', '{}'),
(144, 'user_mask', 'steam:11000011531f6b5', '{}'),
(145, 'user_helmet', 'steam:11000011531f6b5', '{}'),
(146, 'property', 'steam:11000010cfb10c1', '{}'),
(147, 'user_ears', 'steam:11000010cfb10c1', '{}'),
(148, 'user_glasses', 'steam:11000010cfb10c1', '{}'),
(149, 'user_mask', 'steam:11000010cfb10c1', '{}'),
(150, 'user_helmet', 'steam:11000010cfb10c1', '{}'),
(151, 'property', 'steam:110000135e316b5', '{}'),
(152, 'user_glasses', 'steam:110000135e316b5', '{}'),
(153, 'user_ears', 'steam:110000135e316b5', '{}'),
(154, 'user_mask', 'steam:110000135e316b5', '{}'),
(155, 'user_helmet', 'steam:110000135e316b5', '{}'),
(156, 'user_helmet', 'steam:1100001156cfac9', '{}'),
(157, 'user_mask', 'steam:1100001156cfac9', '{}'),
(158, 'property', 'steam:1100001156cfac9', '{}'),
(159, 'user_ears', 'steam:1100001156cfac9', '{}'),
(160, 'user_glasses', 'steam:1100001156cfac9', '{}'),
(161, 'property', 'steam:110000136177a4e', '{}'),
(162, 'user_ears', 'steam:110000136177a4e', '{}'),
(163, 'user_glasses', 'steam:110000136177a4e', '{}'),
(164, 'user_helmet', 'steam:110000136177a4e', '{}'),
(165, 'user_mask', 'steam:110000136177a4e', '{}'),
(166, 'property', 'steam:110000116e48a9b', '{}'),
(167, 'user_ears', 'steam:110000116e48a9b', '{}'),
(168, 'user_glasses', 'steam:110000116e48a9b', '{}'),
(169, 'user_helmet', 'steam:110000116e48a9b', '{}'),
(170, 'user_mask', 'steam:110000116e48a9b', '{}'),
(171, 'user_helmet', 'steam:110000106197c1a', '{}'),
(172, 'property', 'steam:110000106197c1a', '{}'),
(173, 'user_ears', 'steam:110000106197c1a', '{}'),
(174, 'user_glasses', 'steam:110000106197c1a', '{}'),
(175, 'user_mask', 'steam:110000106197c1a', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `dpkeybinds`
--

CREATE TABLE `dpkeybinds` (
  `id` varchar(50) DEFAULT NULL,
  `keybind1` varchar(50) DEFAULT 'num4',
  `emote1` varchar(255) DEFAULT '',
  `keybind2` varchar(50) DEFAULT 'num5',
  `emote2` varchar(255) DEFAULT '',
  `keybind3` varchar(50) DEFAULT 'num6',
  `emote3` varchar(255) DEFAULT '',
  `keybind4` varchar(50) DEFAULT 'num7',
  `emote4` varchar(255) DEFAULT '',
  `keybind5` varchar(50) DEFAULT 'num8',
  `emote5` varchar(255) DEFAULT '',
  `keybind6` varchar(50) DEFAULT 'num9',
  `emote6` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dpkeybinds`
--

INSERT INTO `dpkeybinds` (`id`, `keybind1`, `emote1`, `keybind2`, `emote2`, `keybind3`, `emote3`, `keybind4`, `emote4`, `keybind5`, `emote5`, `keybind6`, `emote6`) VALUES
('steam:110000111d0b1aa', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000111c332ed', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000105ed368b', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010e2a62e2', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010b49a743', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001081d1893', 'num4', 'medic2', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000119e7384d', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000139d9adf9', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013eaca305', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013efa68e1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000132580eb0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001139319c1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001183c7077', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013e7edea3', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000107941620', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001176cddfb', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010fe595d0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013f6b85e1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013e33b934', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000013517b942', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000134e3ca1a', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001013142e0', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000133d93ea2', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000011531f6b5', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010cfb10c1', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000135e316b5', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:1100001156cfac9', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000136177a4e', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000116e48a9b', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:110000106197c1a', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', ''),
('steam:11000010a01bdb9', 'num4', '', 'num5', '', 'num6', '', 'num7', '', 'num8', '', 'num9', '');

-- --------------------------------------------------------

--
-- Table structure for table `fine_types`
--

CREATE TABLE `fine_types` (
  `id` int(11) NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `fine_types`
--

INSERT INTO `fine_types` (`id`, `label`, `amount`, `category`) VALUES
(1, 'Misuse of a horn', 30, 0),
(2, 'Illegally Crossing a continuous Line', 40, 0),
(3, 'Driving on the wrong side of the road', 250, 0),
(4, 'Illegal U-Turn', 250, 0),
(5, 'Illegally Driving Off-road', 170, 0),
(6, 'Refusing a Lawful Command', 30, 0),
(7, 'Illegally Stopping a Vehicle', 150, 0),
(8, 'Illegal Parking', 70, 0),
(9, 'Failing to Yield to the right', 70, 0),
(10, 'Failure to comply with Vehicle Information', 90, 0),
(11, 'Failing to stop at a Stop Sign ', 105, 0),
(12, 'Failing to stop at a Red Light', 130, 0),
(13, 'Illegal Passing', 100, 0),
(14, 'Driving an illegal Vehicle', 100, 0),
(15, 'Driving without a License', 1500, 0),
(16, 'Hit and Run', 800, 0),
(17, 'Exceeding Speeds Over < 5 mph', 90, 0),
(18, 'Exceeding Speeds Over 5-15 mph', 120, 0),
(19, 'Exceeding Speeds Over 15-30 mph', 180, 0),
(20, 'Exceeding Speeds Over > 30 mph', 300, 0),
(21, 'Impeding traffic flow', 110, 1),
(22, 'Public Intoxication', 90, 1),
(23, 'Disorderly conduct', 90, 1),
(24, 'Obstruction of Justice', 130, 1),
(25, 'Insults towards Civilans', 75, 1),
(26, 'Disrespecting of an LEO', 110, 1),
(27, 'Verbal Threat towards a Civilan', 90, 1),
(28, 'Verbal Threat towards an LEO', 150, 1),
(29, 'Providing False Information', 250, 1),
(30, 'Attempt of Corruption', 1500, 1),
(31, 'Brandishing a weapon in city Limits', 120, 2),
(32, 'Brandishing a Lethal Weapon in city Limits', 300, 2),
(33, 'No Firearms License', 600, 2),
(34, 'Possession of an Illegal Weapon', 700, 2),
(35, 'Possession of Burglary Tools', 300, 2),
(36, 'Grand Theft Auto', 1800, 2),
(37, 'Intent to Sell/Distrube of an illegal Substance', 1500, 2),
(38, 'Frabrication of an Illegal Substance', 1500, 2),
(39, 'Possession of an Illegal Substance ', 650, 2),
(40, 'Kidnapping of a Civilan', 1500, 2),
(41, 'Kidnapping of an LEO', 2000, 2),
(42, 'Robbery', 650, 2),
(43, 'Armed Robbery of a Store', 650, 2),
(44, 'Armed Robbery of a Bank', 1500, 2),
(45, 'Assault on a Civilian', 2000, 3),
(46, 'Assault of an LEO', 2500, 3),
(47, 'Attempt of Murder of a Civilian', 3000, 3),
(48, 'Attempt of Murder of an LEO', 5000, 3),
(49, 'Murder of a Civilian', 10000, 3),
(50, 'Murder of an LEO', 30000, 3),
(51, 'Involuntary manslaughter', 1800, 3),
(52, 'Fraud', 2000, 2);

-- --------------------------------------------------------

--
-- Table structure for table `glovebox_inventory`
--

CREATE TABLE `glovebox_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `weight` int(11) NOT NULL DEFAULT 1,
  `limit` int(10) NOT NULL DEFAULT 0,
  `rare` tinyint(1) NOT NULL DEFAULT 0,
  `can_remove` tinyint(1) NOT NULL DEFAULT 1,
  `price` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`name`, `label`, `weight`, `limit`, `rare`, `can_remove`, `price`) VALUES
('9mm_rounds', '9mm Rounds', 1, 20, 0, 1, 10),
('WEAPON_BAT', 'Baseball Bat', 1, 1, 0, 1, 50),
('WEAPON_FLASHLIGHT', 'Flashlight', 1, 1, 0, 1, 100),
('WEAPON_KNIFE', 'Knife', 1, 100, 1, 1, 75),
('WEAPON_PISTOL', 'Pistol', 1, 100, 1, 1, 250),
('WEAPON_PUMPSHOTGUN', 'Pump Shotgun', 1, 1, 0, 1, 500),
('WEAPON_STUNGUN', 'Taser', 1, 100, 1, 1, 150),
('alive_chicken', 'Live Chicken', 1, 20, 0, 1, 0),
('bandage', 'Bandage', 2, 0, 0, 1, 0),
('beer', 'Beer', 1, 30, 0, 1, 0),
('blowpipe', 'Blowtorch', 2, 0, 0, 1, 0),
('bread', 'Bread', 1, 10, 0, 1, 0),
('breathalyzer', 'Breathalyzer', 1, 10, 0, 1, 0),
('burger', 'Bacon Burgare', 1, -1, 0, 1, 0),
('cannabis', 'Cannabis', 3, 0, 0, 1, 0),
('carokit', 'Body Kit', 3, 0, 0, 1, 0),
('carotool', 'Tools', 2, 0, 0, 1, 0),
('ccheese', 'Feta de queso', 1, 40, 0, 1, 0),
('cheese', 'Queso', 1, 10, 0, 1, 0),
('cheesebows', 'OLW Ostbågar', 1, -1, 0, 1, 0),
('chips', 'OLW 3xLök Chips', 1, -1, 0, 1, 0),
('cigarett', 'Cigarett', 1, -1, 0, 1, 0),
('clettuce', 'Lechuga cortada', 1, 40, 0, 1, 0),
('clothe', 'Cloth', 1, 40, 0, 1, 0),
('coca', 'CocaPlant', 1, 150, 0, 1, 0),
('cocacola', 'Coca Cola', 1, -1, 0, 1, 0),
('coke', 'Coke (1G)', 1, 0, 0, 1, 0),
('coke_pooch', 'Bag of coke (28G)', 2, 0, 0, 1, 0),
('copper', 'Copper', 1, 56, 0, 1, 0),
('crack', 'Crack', 1, 25, 0, 1, 0),
('ctomato', 'Tomate cortado', 1, 40, 0, 1, 0),
('cutted_wood', 'Cut Wood', 1, 20, 0, 1, 0),
('dabs', 'Dabs', 1, 50, 0, 1, 0),
('diamond', 'Diamond', 1, 50, 0, 1, 0),
('drugtest', 'DrugTest', 1, 10, 0, 1, 0),
('ephedra', 'Ephedra', 1, 100, 0, 1, 0),
('ephedrine', 'Ephedrine', 1, 100, 0, 1, 0),
('essence', 'Essence', 1, 24, 0, 1, 0),
('fabric', 'Fabric', 1, 80, 0, 1, 0),
('fakepee', 'Fake Pee', 1, 5, 0, 1, 0),
('fanta', 'Fanta Exotic', 1, -1, 0, 1, 0),
('fburger', 'Hamburguesa congelada', 1, 20, 0, 1, 0),
('fish', 'Fish', 1, 100, 0, 1, 0),
('fixkit', 'Repair Kit', 3, 0, 0, 1, 0),
('fixtool', 'Repair Tools', 2, 0, 0, 1, 0),
('fvburger', 'Hamburguesa veggie congelada', 1, 20, 0, 1, 0),
('gazbottle', 'Gas Bottle', 2, 0, 0, 1, 0),
('gold', 'Gold', 1, 21, 0, 1, 0),
('heroine', 'Heroine', 1, 10, 0, 1, 0),
('iron', 'Iron', 1, 42, 0, 1, 0),
('lettuce', 'Lechuga', 1, 10, 0, 1, 0),
('lighter', 'Tändare', 1, -1, 0, 1, 0),
('loka', 'Loka Crush', 1, -1, 0, 1, 0),
('lotteryticket', 'Trisslott', 1, -1, 0, 1, 0),
('macka', 'Skinkmacka', 1, -1, 0, 1, 0),
('marabou', 'Marabou Mjölkchoklad', 1, -1, 0, 1, 0),
('marijuana', 'Marijuana', 2, 0, 0, 1, 0),
('medikit', 'Medikit', 2, 0, 0, 1, 0),
('meth', 'Meth (1G)', 1, 0, 0, 1, 0),
('meth_pooch', 'Bag of meth (28G)', 2, 0, 0, 1, 0),
('narcan', 'Narcan', 1, 10, 0, 1, 0),
('nugget', 'Nugget', 1, 40, 0, 1, 0),
('nuggets10', 'Nuggets x10', 1, 4, 0, 1, 0),
('nuggets4', 'Nuggets x4', 1, 10, 0, 1, 0),
('opium', 'Opium (1G)', 1, 0, 0, 1, 0),
('opium_pooch', 'Bag of opium (28G)', 2, 0, 0, 1, 0),
('packaged_chicken', 'Packaged Chicken', 1, 100, 0, 1, 0),
('packaged_plank', 'Packaged Plank', 1, 100, 0, 1, 0),
('painkiller', 'Painkiller', 1, 10, 0, 1, 0),
('pastacarbonara', 'Pasta Carbonara', 1, -1, 0, 1, 0),
('pcp', 'PCP', 1, 25, 0, 1, 0),
('petrol', 'Petrol', 1, 24, 0, 1, 0),
('petrol_raffin', 'Refined Petrol', 1, 24, 0, 1, 0),
('pizza', 'Kebab Pizza', 1, -1, 0, 1, 0),
('poppy', 'Poppy', 1, 100, 0, 1, 0),
('potato', 'Papa', 1, 10, 0, 1, 0),
('radio', 'Radio', 1, 1, 0, 0, 0),
('shamburger', 'Hamburguesa simple', 1, 5, 0, 1, 0),
('shotgun_shells', 'Shotgun Shells', 1, 20, 0, 1, 0),
('slaughtered_chicken', 'Slaughtered Chicken', 1, 20, 0, 1, 0),
('sprite', 'Sprite', 1, -1, 0, 1, 0),
('stone', 'Stone', 1, 7, 0, 1, 0),
('tequila', 'Tequila', 1, 10, 0, 1, 0),
('tomato', 'Tomate', 1, 10, 0, 1, 0),
('vbread', 'Pan veggie', 1, 20, 0, 1, 0),
('vhamburger', 'hamburguesa veggie', 1, 5, 0, 1, 0),
('vodka', 'Vodka', 1, 10, 0, 1, 0),
('washed_stone', 'Washed Stone', 1, 7, 0, 1, 0),
('water', 'Water', 1, 5, 0, 1, 0),
('weed', 'Weed (1G)', 1, 0, 0, 1, 0),
('weed_pooch', 'Bag of weed (28G)', 2, 0, 0, 1, 0),
('whiskey', 'Whiskey', 1, 10, 0, 1, 0),
('wood', 'Wood', 1, 20, 0, 1, 0),
('wool', 'Wool', 1, 40, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `whitelisted` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('ambulance', 'EMS', 1),
('banker', 'Banker', 1),
('burgershot', 'Burgershot', 1),
('cardealer', 'Cardealer', 0),
('fisherman', 'Fisherman', 0),
('fueler', 'Fueler', 0),
('lumberjack', 'Lumberjack', 0),
('mechanic', 'Mechanic', 0),
('miner', 'Miner', 0),
('police', 'Police', 1),
('reporter', 'Journalist', 0),
('slaughterer', 'Slaughterer', 0),
('tailor', 'Tailor', 0),
('taxi', 'Taxi', 0),
('unemployed', 'Unemployed', 0);

-- --------------------------------------------------------

--
-- Table structure for table `job_grades`
--

CREATE TABLE `job_grades` (
  `id` int(11) NOT NULL,
  `job_name` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `grade` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `salary` int(11) NOT NULL,
  `skin_male` longtext COLLATE utf8mb4_bin NOT NULL,
  `skin_female` longtext COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `job_grades`
--

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(1, 'unemployed', 0, 'unemployed', 'Unemployed', 250, '{}', '{}'),
(2, 'banker', 0, 'advisor', 'Advisor', 10, '{}', '{}'),
(3, 'banker', 1, 'banker', 'Banker', 20, '{}', '{}'),
(4, 'banker', 2, 'business_banker', 'Business Banker', 30, '{}', '{}'),
(5, 'banker', 3, 'trader', 'Trader', 40, '{}', '{}'),
(6, 'banker', 4, 'boss', 'Boss', 0, '{}', '{}'),
(7, 'lumberjack', 0, 'employee', 'Employee', 0, '{}', '{}'),
(8, 'fisherman', 0, 'employee', 'Employee', 0, '{}', '{}'),
(9, 'fueler', 0, 'employee', 'Employee', 0, '{}', '{}'),
(10, 'reporter', 0, 'employee', 'Employee', 0, '{}', '{}'),
(11, 'tailor', 0, 'employee', 'Employee', 0, '{\"mask_1\":0,\"arms\":1,\"glasses_1\":0,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":0,\"torso_1\":24,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":0,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":36,\"tshirt_2\":0,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":48,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}', '{\"mask_1\":0,\"arms\":5,\"glasses_1\":5,\"hair_color_2\":4,\"makeup_1\":0,\"face\":19,\"glasses\":0,\"mask_2\":0,\"makeup_3\":0,\"skin\":29,\"helmet_2\":0,\"lipstick_4\":0,\"sex\":1,\"torso_1\":52,\"makeup_2\":0,\"bags_2\":0,\"chain_2\":0,\"ears_1\":-1,\"bags_1\":0,\"bproof_1\":0,\"shoes_2\":1,\"lipstick_2\":0,\"chain_1\":0,\"tshirt_1\":23,\"eyebrows_3\":0,\"pants_2\":0,\"beard_4\":0,\"torso_2\":0,\"beard_2\":6,\"ears_2\":0,\"hair_2\":0,\"shoes_1\":42,\"tshirt_2\":4,\"beard_3\":0,\"hair_1\":2,\"hair_color_1\":0,\"pants_1\":36,\"helmet_1\":-1,\"bproof_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"decals_1\":0,\"age_2\":0,\"beard_1\":5,\"shoes\":10,\"lipstick_1\":0,\"eyebrows_1\":0,\"glasses_2\":0,\"makeup_4\":0,\"decals_2\":0,\"lipstick_3\":0,\"age_1\":0}'),
(12, 'miner', 0, 'employee', 'Employee', 0, '{\"tshirt_2\":1,\"ears_1\":8,\"glasses_1\":15,\"torso_2\":0,\"ears_2\":2,\"glasses_2\":3,\"shoes_2\":1,\"pants_1\":75,\"shoes_1\":51,\"bags_1\":0,\"helmet_2\":0,\"pants_2\":7,\"torso_1\":71,\"tshirt_1\":59,\"arms\":2,\"bags_2\":0,\"helmet_1\":0}', '{}'),
(13, 'slaughterer', 0, 'employee', 'Employee', 0, '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":67,\"pants_1\":36,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":0,\"torso_1\":56,\"beard_2\":6,\"shoes_1\":12,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":15,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}', '{\"age_1\":0,\"glasses_2\":0,\"beard_1\":5,\"decals_2\":0,\"beard_4\":0,\"shoes_2\":0,\"tshirt_2\":0,\"lipstick_2\":0,\"hair_2\":0,\"arms\":72,\"pants_1\":45,\"skin\":29,\"eyebrows_2\":0,\"shoes\":10,\"helmet_1\":-1,\"lipstick_1\":0,\"helmet_2\":0,\"hair_color_1\":0,\"glasses\":0,\"makeup_4\":0,\"makeup_1\":0,\"hair_1\":2,\"bproof_1\":0,\"bags_1\":0,\"mask_1\":0,\"lipstick_3\":0,\"chain_1\":0,\"eyebrows_4\":0,\"sex\":1,\"torso_1\":49,\"beard_2\":6,\"shoes_1\":24,\"decals_1\":0,\"face\":19,\"lipstick_4\":0,\"tshirt_1\":9,\"mask_2\":0,\"age_2\":0,\"eyebrows_3\":0,\"chain_2\":0,\"glasses_1\":5,\"ears_1\":-1,\"bags_2\":0,\"ears_2\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"eyebrows_1\":0,\"makeup_3\":0,\"pants_2\":0,\"beard_3\":0,\"hair_color_2\":4}'),
(14, 'mechanic', 0, 'recrue', 'Recruit', 150, '{}', '{}'),
(15, 'mechanic', 1, 'novice', 'Novice', 300, '{}', '{}'),
(16, 'mechanic', 2, 'experimente', 'Experienced', 400, '{}', '{}'),
(17, 'mechanic', 3, 'chief', 'Leader', 500, '{}', '{}'),
(18, 'mechanic', 4, 'boss', 'Boss', 0, '{}', '{}'),
(19, 'taxi', 0, 'recrue', 'Recruit', 650, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":32,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":0,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":27,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(20, 'taxi', 1, 'novice', 'Cabby', 850, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":32,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":0,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":27,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(21, 'taxi', 2, 'experimente', 'Experienced', 1000, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":26,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":57,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":11,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(22, 'taxi', 3, 'uber', 'Uber Cabby', 1250, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":26,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":57,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":11,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(23, 'taxi', 4, 'boss', 'Lead Cabby', 1500, '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":29,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":31,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":4,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":1,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":0,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":0,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":4,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":10,\"pants_1\":24}', '{\"hair_2\":0,\"hair_color_2\":0,\"torso_1\":57,\"bags_1\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_3\":0,\"makeup_3\":0,\"makeup_2\":0,\"tshirt_1\":38,\"makeup_1\":0,\"bags_2\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"chain_1\":0,\"lipstick_4\":0,\"bproof_2\":0,\"hair_color_1\":0,\"decals_2\":0,\"pants_2\":1,\"age_2\":0,\"glasses_2\":0,\"ears_2\":0,\"arms\":21,\"lipstick_1\":0,\"ears_1\":-1,\"mask_2\":0,\"sex\":1,\"lipstick_3\":0,\"helmet_1\":-1,\"shoes_2\":0,\"beard_2\":0,\"beard_1\":0,\"lipstick_2\":0,\"beard_4\":0,\"glasses_1\":5,\"bproof_1\":0,\"mask_1\":0,\"decals_1\":1,\"hair_1\":0,\"eyebrows_2\":0,\"beard_3\":0,\"age_1\":0,\"tshirt_2\":0,\"skin\":0,\"torso_2\":0,\"eyebrows_1\":0,\"face\":0,\"shoes_1\":49,\"pants_1\":11}'),
(24, 'cardealer', 0, 'recruit', 'Recruit', 10, '{}', '{}'),
(25, 'cardealer', 1, 'novice', 'Novice', 25, '{}', '{}'),
(26, 'cardealer', 2, 'experienced', 'Experienced', 40, '{}', '{}'),
(27, 'cardealer', 3, 'boss', 'Boss', 0, '{}', '{}'),
(28, 'ambulance', 0, 'ambulance', 'Jr. EMT', 20, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(29, 'ambulance', 1, 'doctor', 'EMT', 40, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(30, 'ambulance', 2, 'chief_doctor', 'Sr. EMT', 60, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(31, 'ambulance', 3, 'boss', 'EMT Supervisor', 80, '{\"tshirt_2\":0,\"hair_color_1\":5,\"glasses_2\":3,\"shoes\":9,\"torso_2\":3,\"hair_color_2\":0,\"pants_1\":24,\"glasses_1\":4,\"hair_1\":2,\"sex\":0,\"decals_2\":0,\"tshirt_1\":15,\"helmet_1\":8,\"helmet_2\":0,\"arms\":92,\"face\":19,\"decals_1\":60,\"torso_1\":13,\"hair_2\":0,\"skin\":34,\"pants_2\":5}', '{\"tshirt_2\":3,\"decals_2\":0,\"glasses\":0,\"hair_1\":2,\"torso_1\":73,\"shoes\":1,\"hair_color_2\":0,\"glasses_1\":19,\"skin\":13,\"face\":6,\"pants_2\":5,\"tshirt_1\":75,\"pants_1\":37,\"helmet_1\":57,\"torso_2\":0,\"arms\":14,\"sex\":1,\"glasses_2\":0,\"decals_1\":0,\"hair_2\":0,\"helmet_2\":0,\"hair_color_1\":0}'),
(32, 'police', 0, 'recruit', 'Recruit', 500, '{}', '{}'),
(33, 'police', 1, 'officer', 'Officer', 750, '{}', '{}'),
(34, 'police', 2, 'sergeant', 'Sergeant', 1000, '{}', '{}'),
(35, 'police', 3, 'lieutenant', 'Lieutenant', 2000, '{}', '{}'),
(36, 'police', 4, 'boss', 'Chief', 3000, '{}', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `licenses`
--

CREATE TABLE `licenses` (
  `type` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `licenses`
--

INSERT INTO `licenses` (`type`, `label`) VALUES
('boat', 'Boat License'),
('dmv', 'Driving Permit'),
('drive', 'Drivers License'),
('drive_bike', 'Motorcycle License'),
('drive_truck', 'Commercial Drivers License'),
('weapon', 'Weapon Permit'),
('weed_processing', 'Weed Processing License');

-- --------------------------------------------------------

--
-- Table structure for table `old_owned_vehicles`
--

CREATE TABLE `old_owned_vehicles` (
  `owner` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `vehicle` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `type` varchar(20) COLLATE utf8mb4_bin NOT NULL DEFAULT 'car',
  `job` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL,
  `stored` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `old_owned_vehicles`
--

INSERT INTO `old_owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('steam:1100001081d1893', 'AFA 021', '{\"modFrontBumper\":-1,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"modTurbo\":false,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"AFA 021\",\"dirtLevel\":0.0,\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"color1\":134,\"modRightFender\":-1,\"pearlescentColor\":156,\"modHorns\":-1,\"fuelLevel\":75.3,\"modTrimB\":-1,\"modFender\":-1,\"modWindows\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modAirFilter\":-1,\"plateIndex\":4,\"modLivery\":3,\"extras\":{\"2\":false,\"1\":false,\"12\":false,\"11\":false,\"8\":false,\"7\":false,\"10\":true,\"9\":false,\"4\":false,\"3\":false,\"6\":true,\"5\":false},\"neonColor\":[255,0,255],\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modRearBumper\":-1,\"modTank\":-1,\"modFrame\":-1,\"tyreSmokeColor\":[255,255,255],\"modSuspension\":-1,\"modEngineBlock\":-1,\"wheelColor\":156,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modGrille\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1745872177,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modArmor\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"modSideSkirt\":-1,\"modArchCover\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"modStruts\":-1,\"modPlateHolder\":-1,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'AJB 215', '{\"neonColor\":[255,0,255],\"modLivery\":3,\"modTrunk\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modFender\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"modAerials\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"dirtLevel\":0.0,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"plate\":\"AJB 215\",\"extras\":{\"6\":false,\"5\":false,\"10\":false,\"7\":false,\"2\":true,\"1\":false,\"4\":false,\"3\":false,\"11\":false,\"12\":false,\"9\":false,\"8\":false},\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modBackWheels\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"modFrame\":-1,\"pearlescentColor\":156,\"color1\":134,\"engineHealth\":1000.0,\"model\":-1616359240,\"modEngine\":-1,\"modArmor\":-1,\"windowTint\":-1,\"modTank\":-1,\"modSuspension\":-1,\"modSideSkirt\":-1,\"modTrimA\":-1,\"modBrakes\":-1,\"plateIndex\":4,\"modTurbo\":false,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"wheels\":1,\"modOrnaments\":-1,\"wheelColor\":156,\"modDoorSpeaker\":-1,\"modRoof\":-1,\"modHydrolic\":-1,\"modStruts\":-1,\"fuelLevel\":87.6,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"modHorns\":-1,\"color2\":134,\"modWindows\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'BRL 379', '{\"modDoorSpeaker\":-1,\"modFrontWheels\":-1,\"modTransmission\":-1,\"plateIndex\":4,\"plate\":\"BRL 379\",\"tyreSmokeColor\":[255,255,255],\"modSuspension\":-1,\"modWindows\":-1,\"modRoof\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"fuelLevel\":76.4,\"modSteeringWheel\":-1,\"modOrnaments\":-1,\"modSmokeEnabled\":false,\"model\":1357997983,\"modEngine\":-1,\"modXenon\":false,\"modArchCover\":-1,\"modArmor\":-1,\"color2\":134,\"color1\":134,\"neonEnabled\":[false,false,false,false],\"modBrakes\":-1,\"modTank\":-1,\"pearlescentColor\":156,\"modFender\":-1,\"modLivery\":1,\"windowTint\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"modTrimB\":-1,\"modDashboard\":-1,\"modBackWheels\":-1,\"modSpeakers\":-1,\"modGrille\":-1,\"modTrimA\":-1,\"modTurbo\":false,\"modSeats\":-1,\"dirtLevel\":0.0,\"modEngineBlock\":-1,\"modHorns\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modRearBumper\":-1,\"neonColor\":[255,0,255],\"modHood\":-1,\"extras\":{\"12\":false,\"8\":false,\"11\":false,\"10\":false,\"2\":false,\"3\":false,\"9\":false,\"1\":false,\"6\":false,\"7\":false,\"4\":false,\"5\":true},\"modExhaust\":-1,\"modTrunk\":-1,\"engineHealth\":1000.0,\"modPlateHolder\":-1,\"modStruts\":-1,\"modAirFilter\":-1,\"modSpoilers\":-1,\"wheelColor\":156,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modRightFender\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"wheels\":1}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'BRT 622', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":false,\"6\":false,\"7\":true,\"9\":false,\"1\":false,\"8\":false,\"11\":false,\"4\":false,\"5\":false,\"2\":false,\"3\":false},\"modRightFender\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":-1616359240,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modHorns\":-1,\"modTank\":-1,\"modShifterLeavers\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"plate\":\"BRT 622\",\"modArchCover\":-1,\"plateIndex\":4,\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"color1\":134,\"neonColor\":[255,0,255],\"wheels\":1,\"modFrame\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modExhaust\":-1,\"modLivery\":0,\"modArmor\":-1,\"modSeats\":-1,\"modWindows\":-1,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"fuelLevel\":81.4,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modRearBumper\":-1,\"pearlescentColor\":156}', 'car', 'police', 1),
('steam:11000010b49a743', 'CFJ 046', '{\"modWindows\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"modDashboard\":-1,\"modFrame\":-1,\"modXenon\":false,\"modLivery\":1,\"modSmokeEnabled\":false,\"modFender\":-1,\"modBackWheels\":-1,\"modSideSkirt\":-1,\"modTrunk\":-1,\"modGrille\":-1,\"windowTint\":-1,\"modAerials\":-1,\"neonColor\":[255,0,255],\"modTurbo\":false,\"modAirFilter\":-1,\"dirtLevel\":1.0,\"color2\":134,\"modSuspension\":-1,\"modSpeakers\":-1,\"model\":161178935,\"modFrontWheels\":-1,\"modHood\":-1,\"plateIndex\":4,\"modFrontBumper\":-1,\"engineHealth\":1000.0,\"modSteeringWheel\":-1,\"extras\":{\"12\":false,\"9\":false,\"10\":false,\"11\":false,\"3\":false,\"2\":false,\"1\":true,\"8\":false,\"7\":false,\"6\":false,\"5\":false,\"4\":false},\"pearlescentColor\":156,\"wheelColor\":156,\"modBrakes\":-1,\"wheels\":1,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"modOrnaments\":-1,\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modSeats\":-1,\"plate\":\"CFJ 046\",\"modTank\":-1,\"fuelLevel\":78.4,\"modAPlate\":-1,\"modEngine\":-1,\"modRightFender\":-1,\"modTransmission\":-1,\"color1\":134,\"modArmor\":-1,\"modDoorSpeaker\":-1,\"modTrimB\":-1,\"modHydrolic\":-1,\"modVanityPlate\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"modSpoilers\":-1,\"modEngineBlock\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modRearBumper\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'CXP 038', '{\"modBrakes\":-1,\"modFrontBumper\":-1,\"modEngineBlock\":-1,\"modPlateHolder\":-1,\"modAirFilter\":-1,\"wheelColor\":156,\"modArmor\":-1,\"modFender\":-1,\"modWindows\":-1,\"modBackWheels\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"plate\":\"CXP 038\",\"wheels\":0,\"modGrille\":-1,\"modHood\":-1,\"modSteeringWheel\":-1,\"modStruts\":-1,\"modHydrolic\":-1,\"modDashboard\":-1,\"modDial\":-1,\"pearlescentColor\":1,\"tyreSmokeColor\":[255,255,255],\"color1\":0,\"modTurbo\":false,\"modRightFender\":-1,\"modExhaust\":-1,\"modSuspension\":-1,\"modArchCover\":-1,\"modOrnaments\":-1,\"modHorns\":-1,\"fuelLevel\":64.3,\"modTransmission\":-1,\"modSpoilers\":-1,\"color2\":0,\"modEngine\":1,\"modAerials\":-1,\"extras\":[],\"modSmokeEnabled\":false,\"bodyHealth\":1000.0,\"neonEnabled\":[false,false,false,false],\"modXenon\":1,\"windowTint\":1,\"engineHealth\":1000.0,\"modSpeakers\":-1,\"modVanityPlate\":-1,\"dirtLevel\":0.0,\"plateIndex\":1,\"modRoof\":-1,\"modTank\":-1,\"modSeats\":-1,\"modLivery\":2,\"modRearBumper\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"modTrimB\":-1,\"neonColor\":[255,0,255],\"modTrunk\":-1,\"model\":-1513691047,\"modShifterLeavers\":-1,\"modFrontWheels\":-1,\"modTrimA\":-1}', 'car', NULL, 1),
('steam:110000133d93ea2', 'DAZ 673', '{\"modWindows\":-1,\"modHydrolic\":-1,\"modTurbo\":false,\"modArchCover\":-1,\"modRearBumper\":-1,\"windowTint\":-1,\"modFrontWheels\":-1,\"modAPlate\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modDoorSpeaker\":-1,\"modAerials\":-1,\"modShifterLeavers\":-1,\"modSmokeEnabled\":false,\"modTrunk\":-1,\"modFender\":-1,\"modSuspension\":-1,\"fuelLevel\":65.0,\"modTransmission\":-1,\"modSideSkirt\":-1,\"engineHealth\":1000.0,\"plate\":\"DAZ 673\",\"modStruts\":-1,\"modBackWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modXenon\":false,\"modHood\":-1,\"modFrontBumper\":-1,\"modTank\":-1,\"modDial\":-1,\"modLivery\":-1,\"neonColor\":[255,0,255],\"modBrakes\":-1,\"modRoof\":-1,\"modEngine\":-1,\"modSpoilers\":-1,\"modGrille\":-1,\"modTrimB\":-1,\"modVanityPlate\":-1,\"neonEnabled\":[false,false,false,false],\"modDashboard\":-1,\"modHorns\":-1,\"modExhaust\":-1,\"pearlescentColor\":5,\"color1\":0,\"plateIndex\":0,\"wheelColor\":112,\"modEngineBlock\":-1,\"modArmor\":-1,\"modFrame\":-1,\"modRightFender\":-1,\"modOrnaments\":-1,\"modAirFilter\":-1,\"modSeats\":-1,\"extras\":[],\"wheels\":5,\"bodyHealth\":1000.0,\"model\":1549126457,\"modSpeakers\":-1,\"color2\":4,\"modSteeringWheel\":-1}', 'car', NULL, 1),
('steam:11000010fe595d0', 'DDU 263', '{\"modXenon\":false,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"wheelColor\":156,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"DDU 263\",\"modTrimB\":-1,\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"bodyHealth\":1000.0,\"modHorns\":-1,\"fuelLevel\":81.1,\"modWindows\":-1,\"modFender\":-1,\"modTank\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modArmor\":-1,\"plateIndex\":4,\"modLivery\":0,\"extras\":{\"2\":false,\"1\":false,\"12\":false,\"11\":false,\"8\":true,\"7\":true,\"10\":false,\"9\":false,\"4\":false,\"3\":false,\"6\":false,\"5\":false},\"neonColor\":[255,0,255],\"modExhaust\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modGrille\":-1,\"modAirFilter\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1027958099,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modRightFender\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"color1\":134,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"dirtLevel\":6.0,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:11000013e33b934', 'DPN 185', '{\"modTrimA\":-1,\"modBrakes\":-1,\"modFender\":-1,\"modHood\":-1,\"modSmokeEnabled\":false,\"color1\":29,\"modTrunk\":-1,\"modAirFilter\":-1,\"modGrille\":-1,\"modPlateHolder\":-1,\"plate\":\"DPN 185\",\"modLivery\":-1,\"modRoof\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modSideSkirt\":-1,\"modTurbo\":false,\"fuelLevel\":65.0,\"wheels\":3,\"modAerials\":-1,\"model\":-301519603,\"modXenon\":false,\"dirtLevel\":9.0,\"modRightFender\":-1,\"modBackWheels\":-1,\"modFrontWheels\":-1,\"modSpoilers\":-1,\"pearlescentColor\":29,\"modDial\":-1,\"modDoorSpeaker\":-1,\"tyreSmokeColor\":[255,255,255],\"color2\":0,\"bodyHealth\":1000.0,\"neonEnabled\":[false,false,false,false],\"modSteeringWheel\":-1,\"modSpeakers\":-1,\"modTransmission\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modRearBumper\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"modOrnaments\":-1,\"plateIndex\":4,\"modShifterLeavers\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modArchCover\":-1,\"modArmor\":-1,\"modDashboard\":-1,\"modHorns\":-1,\"modEngine\":-1,\"modHydrolic\":-1,\"windowTint\":-1,\"extras\":{\"3\":true,\"2\":true,\"1\":true},\"modFrame\":-1,\"neonColor\":[255,0,255],\"modEngineBlock\":-1,\"modTank\":-1,\"engineHealth\":1000.0,\"wheelColor\":0,\"modWindows\":-1}', 'car', NULL, 1),
('steam:1100001081d1893', 'DUH 545', '{\"modSideSkirt\":-1,\"wheelColor\":156,\"modSteeringWheel\":-1,\"bodyHealth\":1000.0,\"modGrille\":-1,\"modSuspension\":-1,\"modFrame\":-1,\"windowTint\":-1,\"plate\":\"DUH 545\",\"dirtLevel\":0.0,\"modSmokeEnabled\":false,\"modAirFilter\":-1,\"modAerials\":-1,\"modTrimA\":-1,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modSeats\":-1,\"plateIndex\":4,\"extras\":{\"1\":false,\"12\":false,\"11\":true,\"10\":false,\"5\":true,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false,\"2\":false,\"3\":false},\"modSpeakers\":-1,\"modHorns\":-1,\"modTransmission\":-1,\"modEngineBlock\":-1,\"model\":1745872177,\"modWindows\":-1,\"engineHealth\":1000.0,\"modAPlate\":-1,\"modPlateHolder\":-1,\"modEngine\":-1,\"fuelLevel\":84.8,\"modVanityPlate\":-1,\"modTurbo\":false,\"modRearBumper\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"modRightFender\":-1,\"modTank\":-1,\"color2\":134,\"modDashboard\":-1,\"color1\":134,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"neonColor\":[255,0,255],\"modXenon\":false,\"modSpoilers\":-1,\"modShifterLeavers\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modHood\":-1,\"modFender\":-1,\"modLivery\":0,\"modTrimB\":-1,\"tyreSmokeColor\":[255,255,255],\"modBackWheels\":-1,\"modTrunk\":-1,\"modHydrolic\":-1,\"pearlescentColor\":156,\"modArchCover\":-1,\"wheels\":1,\"modStruts\":-1,\"modOrnaments\":-1,\"modFrontWheels\":-1}', 'car', 'police', 1),
('steam:11000010fe595d0', 'FCJ 140', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"color1\":134,\"modTransmission\":-1,\"extras\":{\"10\":false,\"11\":true,\"6\":false,\"7\":false,\"9\":false,\"1\":false,\"8\":false,\"12\":false,\"4\":false,\"5\":false,\"2\":true,\"3\":false},\"modOrnaments\":-1,\"engineHealth\":1000.0,\"modArchCover\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modDashboard\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"modTrunk\":-1,\"modRearBumper\":-1,\"modStruts\":-1,\"modTrimB\":-1,\"fuelLevel\":84.1,\"modLivery\":1,\"modShifterLeavers\":-1,\"neonColor\":[255,0,255],\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modVanityPlate\":-1,\"modDoorSpeaker\":-1,\"modTrimA\":-1,\"modGrille\":-1,\"modSideSkirt\":-1,\"modRoof\":-1,\"modBackWheels\":-1,\"modWindows\":-1,\"modHorns\":-1,\"wheels\":1,\"modSpeakers\":-1,\"modXenon\":false,\"model\":1357997983,\"modFrontBumper\":-1,\"modSeats\":-1,\"tyreSmokeColor\":[255,255,255],\"wheelColor\":156,\"plateIndex\":4,\"modEngine\":-1,\"modArmor\":-1,\"modBrakes\":-1,\"pearlescentColor\":156,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modExhaust\":-1,\"modSuspension\":-1,\"modFender\":-1,\"neonEnabled\":[false,false,false,false],\"modRightFender\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modFrame\":-1,\"plate\":\"FCJ 140\"}', 'car', 'police', 1),
('steam:11000010b49a743', 'FHQ 779', '{\"modSteeringWheel\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modTrimB\":-1,\"modWindows\":-1,\"modTransmission\":-1,\"modSpeakers\":-1,\"color2\":134,\"modEngine\":-1,\"modAerials\":-1,\"modSeats\":-1,\"pearlescentColor\":156,\"modDoorSpeaker\":-1,\"modFrontBumper\":-1,\"wheels\":0,\"plateIndex\":4,\"extras\":{\"5\":false,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false,\"12\":true,\"10\":false,\"11\":false,\"1\":false,\"3\":false,\"2\":false},\"modFender\":-1,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modHood\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modTurbo\":false,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"dirtLevel\":0.0,\"fuelLevel\":89.6,\"modArchCover\":-1,\"modXenon\":false,\"modGrille\":-1,\"plate\":\"FHQ 779\",\"modStruts\":-1,\"modPlateHolder\":-1,\"neonEnabled\":[false,false,false,false],\"engineHealth\":1000.0,\"modAPlate\":-1,\"modRoof\":-1,\"model\":-1059115956,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modHydrolic\":-1,\"color1\":134,\"modTrimA\":-1,\"modLivery\":3,\"modDashboard\":-1,\"windowTint\":-1,\"modArmor\":-1,\"wheelColor\":156}', 'car', 'police', 1),
('steam:1100001081d1893', 'GJV 215', '{\"modSeats\":-1,\"modTrimA\":-1,\"modHorns\":-1,\"fuelLevel\":73.2,\"modHydrolic\":-1,\"plateIndex\":3,\"tyreSmokeColor\":[255,255,255],\"modDashboard\":-1,\"modBackWheels\":-1,\"modGrille\":-1,\"modTrimB\":-1,\"modArchCover\":-1,\"modSideSkirt\":-1,\"modSteeringWheel\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modFender\":-1,\"modSpeakers\":-1,\"modDial\":-1,\"modRearBumper\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":998.3,\"modRightFender\":-1,\"modTransmission\":-1,\"modAirFilter\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"pearlescentColor\":23,\"modAerials\":-1,\"modExhaust\":-1,\"extras\":[],\"color1\":3,\"color2\":3,\"modEngine\":-1,\"modLivery\":-1,\"modWindows\":-1,\"modRoof\":-1,\"modDoorSpeaker\":-1,\"windowTint\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modHood\":-1,\"modBrakes\":-1,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"wheelColor\":156,\"dirtLevel\":7.2,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modTrunk\":-1,\"modOrnaments\":-1,\"modTurbo\":false,\"modSpoilers\":-1,\"wheels\":5,\"engineHealth\":997.4,\"modStruts\":-1,\"modArmor\":-1,\"plate\":\"GJV 215\",\"modPlateHolder\":-1,\"model\":1373123368,\"modEngineBlock\":-1,\"modXenon\":false}', 'car', NULL, 1),
('steam:11000010fe595d0', 'GNN 441', '{\"modXenon\":false,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"wheelColor\":156,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"GNN 441\",\"modTrimB\":-1,\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"bodyHealth\":1000.0,\"modHorns\":-1,\"fuelLevel\":87.2,\"modWindows\":-1,\"modFender\":-1,\"modTank\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modArmor\":-1,\"plateIndex\":4,\"modLivery\":1,\"extras\":{\"2\":false,\"1\":true,\"12\":false,\"11\":true,\"8\":false,\"7\":false,\"10\":false,\"9\":false,\"4\":false,\"3\":false,\"6\":false,\"5\":false},\"neonColor\":[255,0,255],\"modExhaust\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modTurbo\":false,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modGrille\":-1,\"modAirFilter\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1357997983,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modRightFender\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"color1\":134,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"dirtLevel\":0.0,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:11000010e2a62e2', 'HEZ 969', '{\"modPlateHolder\":-1,\"modArchCover\":-1,\"bodyHealth\":1000.0,\"model\":1032823388,\"modAPlate\":-1,\"modBrakes\":-1,\"color1\":31,\"modXenon\":false,\"modDoorSpeaker\":-1,\"plate\":\"HEZ 969\",\"windowTint\":-1,\"modTurbo\":false,\"modHorns\":-1,\"modTransmission\":-1,\"modExhaust\":-1,\"modFrame\":-1,\"modSmokeEnabled\":false,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modSpeakers\":-1,\"modTank\":-1,\"modRightFender\":-1,\"modEngineBlock\":-1,\"pearlescentColor\":32,\"modStruts\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modDial\":-1,\"modFender\":-1,\"modSpoilers\":-1,\"wheels\":7,\"modAerials\":-1,\"modArmor\":-1,\"modSeats\":-1,\"modGrille\":-1,\"tyreSmokeColor\":[255,255,255],\"modBackWheels\":-1,\"modTrimA\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"fuelLevel\":65.0,\"modSteeringWheel\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"dirtLevel\":9.0,\"modShifterLeavers\":-1,\"neonEnabled\":[false,false,false,false],\"modHood\":-1,\"modAirFilter\":-1,\"engineHealth\":1000.0,\"color2\":0,\"wheelColor\":156,\"modLivery\":-1,\"extras\":{\"10\":false,\"12\":true},\"plateIndex\":0,\"modTrunk\":-1,\"modTrimB\":-1,\"modDashboard\":-1}', 'car', NULL, 1),
('steam:110000133d93ea2', 'HHT 377', '{\"modExhaust\":-1,\"modHydrolic\":-1,\"modTurbo\":false,\"modArchCover\":-1,\"modRearBumper\":-1,\"windowTint\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modDoorSpeaker\":-1,\"modRightFender\":-1,\"tyreSmokeColor\":[255,255,255],\"modHood\":-1,\"modFender\":-1,\"color2\":88,\"modGrille\":-1,\"modLivery\":-1,\"modWindows\":-1,\"modSideSkirt\":-1,\"engineHealth\":1000.0,\"modOrnaments\":-1,\"modStruts\":-1,\"fuelLevel\":65.0,\"modDial\":-1,\"modXenon\":false,\"modTank\":-1,\"modFrontBumper\":-1,\"plate\":\"HHT 377\",\"modBackWheels\":-1,\"modAirFilter\":-1,\"neonColor\":[255,0,255],\"modDashboard\":-1,\"modSmokeEnabled\":false,\"modEngine\":-1,\"modSpoilers\":-1,\"modTransmission\":-1,\"modArmor\":-1,\"modVanityPlate\":-1,\"neonEnabled\":[false,false,false,false],\"modShifterLeavers\":-1,\"modHorns\":-1,\"modBrakes\":-1,\"pearlescentColor\":88,\"color1\":88,\"plateIndex\":3,\"wheelColor\":156,\"modSuspension\":-1,\"modFrontWheels\":-1,\"modFrame\":-1,\"modTrunk\":-1,\"modRoof\":-1,\"modEngineBlock\":-1,\"modSeats\":-1,\"modSpeakers\":-1,\"wheels\":6,\"bodyHealth\":1000.0,\"model\":1672195559,\"extras\":{\"1\":true,\"4\":true,\"9\":true},\"modAerials\":-1,\"modSteeringWheel\":-1}', 'car', NULL, 1),
('steam:11000010b49a743', 'HKZ 176', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":-1616359240,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":88.2,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":0,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"HKZ 176\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"7\":true,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":false,\"8\":true,\"10\":true},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'HQR 873', '{\"modStruts\":-1,\"modTank\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrunk\":-1,\"modXenon\":false,\"modSeats\":-1,\"color1\":134,\"modDoorSpeaker\":-1,\"plate\":\"HQR 873\",\"modTransmission\":-1,\"modAirFilter\":-1,\"modTrimB\":-1,\"modArmor\":-1,\"engineHealth\":1000.0,\"wheels\":1,\"modShifterLeavers\":-1,\"color2\":134,\"modOrnaments\":-1,\"modArchCover\":-1,\"extras\":{\"12\":false,\"1\":false,\"4\":false,\"10\":false,\"5\":false,\"11\":true,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":true},\"modDial\":-1,\"modExhaust\":-1,\"modGrille\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modHydrolic\":-1,\"plateIndex\":4,\"modTurbo\":false,\"modRearBumper\":-1,\"modFender\":-1,\"windowTint\":-1,\"modSpeakers\":-1,\"modHood\":-1,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modPlateHolder\":-1,\"modSuspension\":-1,\"modBackWheels\":-1,\"modRightFender\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modFrontWheels\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modEngineBlock\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"pearlescentColor\":156,\"modSteeringWheel\":-1,\"dirtLevel\":0.0,\"fuelLevel\":88.8,\"modSideSkirt\":-1,\"wheelColor\":156,\"modAerials\":-1,\"model\":1745872177,\"modRoof\":-1,\"modLivery\":2,\"modFrame\":-1,\"modTrimA\":-1,\"modSpoilers\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'IRW 235', '{\"modSeats\":-1,\"modTrimA\":-1,\"modHorns\":-1,\"fuelLevel\":75.2,\"modHydrolic\":-1,\"plateIndex\":0,\"tyreSmokeColor\":[255,255,255],\"modDashboard\":-1,\"modBackWheels\":-1,\"modGrille\":-1,\"modTrimB\":-1,\"modArchCover\":-1,\"modSideSkirt\":-1,\"modSteeringWheel\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modFender\":-1,\"modSpeakers\":-1,\"modDial\":-1,\"modRearBumper\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"modRightFender\":-1,\"modTransmission\":-1,\"modAirFilter\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"pearlescentColor\":64,\"modAerials\":-1,\"modExhaust\":-1,\"extras\":[],\"color1\":64,\"color2\":0,\"modEngine\":-1,\"modLivery\":-1,\"modWindows\":-1,\"modRoof\":-1,\"modDoorSpeaker\":-1,\"windowTint\":-1,\"modVanityPlate\":-1,\"modSmokeEnabled\":false,\"modHood\":-1,\"modBrakes\":-1,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"wheelColor\":156,\"dirtLevel\":7.0,\"neonColor\":[255,255,255],\"modSuspension\":-1,\"modTrunk\":-1,\"modOrnaments\":-1,\"modTurbo\":false,\"modSpoilers\":-1,\"wheels\":6,\"engineHealth\":1000.0,\"modStruts\":-1,\"modArmor\":-1,\"plate\":\"IRW 235\",\"modPlateHolder\":-1,\"model\":-114291515,\"modEngineBlock\":-1,\"modXenon\":false}', 'car', NULL, 1),
('steam:11000010b49a743', 'ISR 383', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":1418298348,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":75.7,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":3,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"ISR 383\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":true,\"1\":false,\"4\":false,\"3\":false,\"7\":false,\"9\":false,\"12\":true,\"11\":false,\"6\":false,\"5\":false,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'IVL 836', '{\"model\":1027958099,\"modBrakes\":-1,\"bodyHealth\":1000.0,\"modStruts\":-1,\"modTrimA\":-1,\"extras\":{\"1\":true,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":false,\"8\":false,\"9\":false,\"10\":false,\"11\":true,\"12\":false},\"plate\":\"IVL 836\",\"dirtLevel\":4.0,\"neonColor\":[255,0,255],\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modAirFilter\":-1,\"fuelLevel\":75.3,\"modSpeakers\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"modBackWheels\":-1,\"modTrimB\":-1,\"modEngine\":-1,\"modFender\":-1,\"modTurbo\":false,\"modTank\":-1,\"engineHealth\":1000.0,\"color1\":134,\"modRightFender\":-1,\"color2\":134,\"modHydrolic\":-1,\"modExhaust\":-1,\"modHorns\":-1,\"modXenon\":false,\"modWindows\":-1,\"wheelColor\":156,\"modVanityPlate\":-1,\"modTrunk\":-1,\"modSeats\":-1,\"neonEnabled\":[false,false,false,false],\"modAerials\":-1,\"modAPlate\":-1,\"modTransmission\":-1,\"modGrille\":-1,\"modDashboard\":-1,\"modFrontBumper\":-1,\"modLivery\":2,\"modSuspension\":-1,\"modEngineBlock\":-1,\"windowTint\":-1,\"modSmokeEnabled\":false,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modFrame\":-1,\"wheels\":1,\"modSpoilers\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"modDoorSpeaker\":-1,\"modRearBumper\":-1,\"modOrnaments\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modHood\":-1,\"modRoof\":-1,\"plateIndex\":4}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'IXW 287', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":false,\"6\":false,\"7\":false,\"9\":false,\"1\":true,\"8\":false,\"11\":true,\"4\":false,\"5\":false,\"2\":false,\"3\":false},\"modRightFender\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":1027958099,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modHorns\":-1,\"modTank\":-1,\"modShifterLeavers\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"plate\":\"IXW 287\",\"modArchCover\":-1,\"plateIndex\":4,\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"color1\":134,\"neonColor\":[255,0,255],\"wheels\":1,\"modFrame\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modExhaust\":-1,\"modLivery\":3,\"modArmor\":-1,\"modSeats\":-1,\"modWindows\":-1,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"fuelLevel\":76.5,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modRearBumper\":-1,\"pearlescentColor\":156}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'JDP 890', '{\"modStruts\":-1,\"modTank\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrunk\":-1,\"modXenon\":false,\"modSeats\":-1,\"color1\":134,\"modDoorSpeaker\":-1,\"plate\":\"JDP 890\",\"modTransmission\":-1,\"modAirFilter\":-1,\"modTrimB\":-1,\"modArmor\":-1,\"engineHealth\":1000.0,\"wheels\":1,\"modShifterLeavers\":-1,\"color2\":134,\"modOrnaments\":-1,\"modArchCover\":-1,\"extras\":{\"12\":false,\"1\":false,\"4\":false,\"10\":false,\"5\":false,\"11\":true,\"3\":false,\"2\":false,\"9\":false,\"8\":false,\"7\":true,\"6\":false},\"modDial\":-1,\"modExhaust\":-1,\"modGrille\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modHydrolic\":-1,\"plateIndex\":4,\"modTurbo\":false,\"modRearBumper\":-1,\"modFender\":-1,\"windowTint\":-1,\"modSpeakers\":-1,\"modHood\":-1,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modPlateHolder\":-1,\"modSuspension\":-1,\"modBackWheels\":-1,\"modRightFender\":-1,\"modBrakes\":-1,\"modDashboard\":-1,\"modFrontWheels\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modEngineBlock\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"modWindows\":-1,\"pearlescentColor\":156,\"modSteeringWheel\":-1,\"dirtLevel\":0.0,\"fuelLevel\":78.0,\"modSideSkirt\":-1,\"wheelColor\":156,\"modAerials\":-1,\"model\":-1616359240,\"modRoof\":-1,\"modLivery\":2,\"modFrame\":-1,\"modTrimA\":-1,\"modSpoilers\":-1}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'KGN 356', '{\"modXenon\":false,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"KGN 356\",\"neonColor\":[255,0,255],\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"color1\":134,\"modFender\":-1,\"pearlescentColor\":156,\"modHorns\":-1,\"fuelLevel\":94.5,\"modTrimB\":-1,\"modTurbo\":false,\"modWindows\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"plateIndex\":4,\"modLivery\":1,\"modArmor\":-1,\"extras\":{\"2\":false,\"1\":false,\"12\":false,\"11\":false,\"8\":false,\"7\":false,\"10\":true,\"9\":false,\"4\":false,\"3\":false,\"6\":false,\"5\":true},\"modExhaust\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modGrille\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modTank\":-1,\"modFrontBumper\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1027958099,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"modPlateHolder\":-1,\"modArchCover\":-1,\"dirtLevel\":0.0,\"wheelColor\":156,\"modStruts\":-1,\"bodyHealth\":1000.0,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:110000105ed368b', 'LRR 177', '{\"modTank\":-1,\"neonColor\":[255,0,255],\"modEngineBlock\":-1,\"dirtLevel\":3.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":161178935,\"windowTint\":-1,\"modSpeakers\":-1,\"neonEnabled\":[false,false,false,false],\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"modArchCover\":-1,\"modAerials\":-1,\"modSpoilers\":-1,\"fuelLevel\":89.2,\"modFender\":-1,\"modHorns\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"modBackWheels\":-1,\"modLivery\":0,\"modOrnaments\":-1,\"modTurbo\":false,\"modXenon\":false,\"modSuspension\":-1,\"modSmokeEnabled\":false,\"color1\":134,\"wheelColor\":156,\"modRearBumper\":-1,\"modPlateHolder\":-1,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"plate\":\"LRR 177\",\"modAPlate\":-1,\"modEngine\":-1,\"modFrontBumper\":-1,\"modSideSkirt\":-1,\"modDial\":-1,\"color2\":134,\"modDashboard\":-1,\"tyreSmokeColor\":[255,255,255],\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":true,\"4\":false,\"3\":false,\"10\":false,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":false,\"8\":false,\"7\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'MGL 230', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":2.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":0,\"model\":-1059115956,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":90.5,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":3,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"MGL 230\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":true,\"1\":false,\"4\":false,\"3\":false,\"7\":false,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":false,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'MPV 719', '{\"modSteeringWheel\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"modFrame\":-1,\"modShifterLeavers\":-1,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modTrimB\":-1,\"modWindows\":-1,\"modTransmission\":-1,\"modSpeakers\":-1,\"color2\":134,\"modEngine\":-1,\"modAerials\":-1,\"modSeats\":-1,\"pearlescentColor\":156,\"modDoorSpeaker\":-1,\"modFrontBumper\":-1,\"wheels\":0,\"plateIndex\":4,\"extras\":{\"5\":false,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false,\"12\":true,\"10\":false,\"11\":false,\"1\":false,\"3\":false,\"2\":false},\"modFender\":-1,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modBrakes\":-1,\"modHorns\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modHood\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modTurbo\":false,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"dirtLevel\":0.0,\"fuelLevel\":76.2,\"modArchCover\":-1,\"modXenon\":false,\"modGrille\":-1,\"plate\":\"MPV 719\",\"modStruts\":-1,\"modPlateHolder\":-1,\"neonEnabled\":[false,false,false,false],\"engineHealth\":1000.0,\"modAPlate\":-1,\"modRoof\":-1,\"model\":-1059115956,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"modHydrolic\":-1,\"color1\":134,\"modTrimA\":-1,\"modLivery\":2,\"modDashboard\":-1,\"windowTint\":-1,\"modArmor\":-1,\"wheelColor\":156}', 'car', 'police', 1),
('steam:11000010b49a743', 'OIL 252', '{\"modDoorSpeaker\":-1,\"modFrontWheels\":-1,\"modTransmission\":-1,\"plateIndex\":4,\"plate\":\"OIL 252\",\"tyreSmokeColor\":[255,255,255],\"modSuspension\":-1,\"modWindows\":-1,\"modRoof\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"fuelLevel\":87.7,\"modSteeringWheel\":-1,\"modOrnaments\":-1,\"modSmokeEnabled\":false,\"model\":1745872177,\"modEngine\":-1,\"modXenon\":false,\"modArchCover\":-1,\"modArmor\":-1,\"color2\":134,\"color1\":134,\"neonEnabled\":[false,false,false,false],\"modBrakes\":-1,\"modTank\":-1,\"pearlescentColor\":156,\"modFender\":-1,\"modLivery\":2,\"windowTint\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"modTrimB\":-1,\"modDashboard\":-1,\"modBackWheels\":-1,\"modSpeakers\":-1,\"modGrille\":-1,\"modTrimA\":-1,\"modTurbo\":false,\"modSeats\":-1,\"dirtLevel\":0.0,\"modEngineBlock\":-1,\"modHorns\":-1,\"modDial\":-1,\"modHydrolic\":-1,\"modRearBumper\":-1,\"neonColor\":[255,0,255],\"modHood\":-1,\"extras\":{\"12\":false,\"8\":false,\"11\":false,\"10\":true,\"2\":false,\"3\":false,\"9\":false,\"1\":false,\"6\":false,\"7\":false,\"4\":true,\"5\":false},\"modExhaust\":-1,\"modTrunk\":-1,\"engineHealth\":1000.0,\"modPlateHolder\":-1,\"modStruts\":-1,\"modAirFilter\":-1,\"modSpoilers\":-1,\"wheelColor\":156,\"modFrontBumper\":-1,\"modVanityPlate\":-1,\"modRightFender\":-1,\"modFrame\":-1,\"modAPlate\":-1,\"wheels\":1}', 'car', 'police', 1),
('steam:1100001139319c1', 'ORM 791', '{\"neonColor\":[255,0,255],\"modLivery\":0,\"modTrunk\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modFender\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modArchCover\":-1,\"color2\":134,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modSeats\":-1,\"model\":598074270,\"modVanityPlate\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"plate\":\"ORM 791\",\"extras\":{\"6\":true,\"5\":false,\"10\":true,\"7\":false,\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"11\":false,\"9\":false,\"8\":false},\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modBackWheels\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"color1\":134,\"modXenon\":false,\"modEngine\":-1,\"modTank\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modBrakes\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"tyreSmokeColor\":[255,255,255],\"modDial\":-1,\"wheels\":1,\"modOrnaments\":-1,\"wheelColor\":156,\"modRoof\":-1,\"bodyHealth\":1000.0,\"modHydrolic\":-1,\"modStruts\":-1,\"fuelLevel\":76.8,\"modShifterLeavers\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"modHorns\":-1,\"modTrimA\":-1,\"dirtLevel\":0.0}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'QIK 955', '{\"modXenon\":false,\"modTrunk\":-1,\"modDashboard\":-1,\"wheels\":1,\"modShifterLeavers\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"neonEnabled\":[false,false,false,false],\"plate\":\"QIK 955\",\"neonColor\":[255,0,255],\"modAPlate\":-1,\"color2\":134,\"modDial\":-1,\"color1\":134,\"modFender\":-1,\"pearlescentColor\":156,\"modHorns\":-1,\"fuelLevel\":93.5,\"modTrimB\":-1,\"modTurbo\":false,\"modWindows\":-1,\"modDoorSpeaker\":-1,\"modSeats\":-1,\"modVanityPlate\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"plateIndex\":4,\"modLivery\":0,\"modArmor\":-1,\"extras\":{\"2\":false,\"1\":false,\"12\":true,\"11\":false,\"8\":false,\"7\":false,\"10\":false,\"9\":false,\"4\":true,\"3\":false,\"6\":false,\"5\":false},\"modExhaust\":-1,\"modSpeakers\":-1,\"modRearBumper\":-1,\"tyreSmokeColor\":[255,255,255],\"modFrame\":-1,\"modGrille\":-1,\"modSuspension\":-1,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modTransmission\":-1,\"modTank\":-1,\"modFrontBumper\":-1,\"modRoof\":-1,\"modBrakes\":-1,\"modTrimA\":-1,\"model\":1357997983,\"engineHealth\":1000.0,\"modHood\":-1,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"modPlateHolder\":-1,\"modArchCover\":-1,\"dirtLevel\":0.0,\"wheelColor\":156,\"modStruts\":-1,\"bodyHealth\":1000.0,\"modEngine\":-1,\"modSpoilers\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'QSX 486', '{\"plate\":\"QSX 486\",\"pearlescentColor\":156,\"modTransmission\":-1,\"plateIndex\":4,\"neonEnabled\":[false,false,false,false],\"color1\":134,\"modPlateHolder\":-1,\"bodyHealth\":1000.0,\"modArchCover\":-1,\"engineHealth\":1000.0,\"modTurbo\":false,\"modRearBumper\":-1,\"modSuspension\":-1,\"modHood\":-1,\"modEngineBlock\":-1,\"modFrame\":-1,\"modGrille\":-1,\"modHydrolic\":-1,\"tyreSmokeColor\":[255,255,255],\"modOrnaments\":-1,\"modBrakes\":-1,\"wheelColor\":156,\"modHorns\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modRightFender\":-1,\"neonColor\":[255,0,255],\"modRoof\":-1,\"modWindows\":-1,\"color2\":134,\"model\":1745872177,\"modTrimA\":-1,\"modSpoilers\":-1,\"modVanityPlate\":-1,\"wheels\":1,\"modAerials\":-1,\"modSideSkirt\":-1,\"modStruts\":-1,\"modFrontBumper\":-1,\"modBackWheels\":-1,\"modFender\":-1,\"modTrunk\":-1,\"modFrontWheels\":-1,\"modAirFilter\":-1,\"extras\":{\"11\":true,\"10\":false,\"1\":false,\"12\":false,\"4\":false,\"5\":false,\"2\":false,\"3\":false,\"8\":false,\"9\":false,\"6\":false,\"7\":true},\"modSteeringWheel\":-1,\"modDashboard\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"fuelLevel\":86.4,\"modLivery\":0,\"windowTint\":-1,\"modExhaust\":-1,\"dirtLevel\":0.0,\"modTank\":-1,\"modSmokeEnabled\":false,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modSpeakers\":-1,\"modEngine\":-1,\"modSeats\":-1,\"modXenon\":false}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'RNC 414', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":1.0,\"modOrnaments\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":false,\"6\":true,\"7\":false,\"9\":false,\"1\":false,\"8\":false,\"11\":false,\"4\":false,\"5\":false,\"2\":false,\"3\":false},\"modRightFender\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":-1059115956,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modHorns\":-1,\"modTank\":-1,\"modShifterLeavers\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"plate\":\"RNC 414\",\"modArchCover\":-1,\"plateIndex\":4,\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"color1\":134,\"neonColor\":[255,0,255],\"wheels\":0,\"modFrame\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modExhaust\":-1,\"modLivery\":3,\"modArmor\":-1,\"modSeats\":-1,\"modWindows\":-1,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"fuelLevel\":75.1,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modRearBumper\":-1,\"pearlescentColor\":156}', 'car', 'police', 1),
('steam:1100001081d1893', 'RPC 234', '{\"modSteeringWheel\":-1,\"modRightFender\":-1,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"model\":-1059115956,\"modFrame\":-1,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"modSuspension\":-1,\"modTrimB\":-1,\"modWindows\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"color2\":134,\"modEngine\":-1,\"modAerials\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"modDoorSpeaker\":-1,\"modArmor\":-1,\"modAPlate\":-1,\"modTransmission\":-1,\"extras\":{\"5\":false,\"4\":false,\"7\":false,\"6\":false,\"9\":false,\"8\":false,\"12\":false,\"10\":false,\"11\":false,\"1\":false,\"3\":false,\"2\":true},\"wheels\":0,\"modAirFilter\":-1,\"modEngineBlock\":-1,\"modFrontWheels\":-1,\"tyreSmokeColor\":[255,255,255],\"modSpoilers\":-1,\"modFender\":-1,\"modHorns\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modHood\":-1,\"bodyHealth\":1000.0,\"modTank\":-1,\"modTurbo\":false,\"modOrnaments\":-1,\"modVanityPlate\":-1,\"dirtLevel\":3.0,\"fuelLevel\":78.3,\"modArchCover\":-1,\"modHydrolic\":-1,\"modBrakes\":-1,\"plate\":\"RPC 234\",\"modSeats\":-1,\"modPlateHolder\":-1,\"modGrille\":-1,\"modStruts\":-1,\"modShifterLeavers\":-1,\"modRoof\":-1,\"neonEnabled\":[false,false,false,false],\"modSideSkirt\":-1,\"modFrontBumper\":-1,\"modDashboard\":-1,\"color1\":134,\"plateIndex\":4,\"modTrimA\":-1,\"modBackWheels\":-1,\"windowTint\":-1,\"modLivery\":3,\"wheelColor\":156}', 'car', 'police', 1),
('steam:110000111c332ed', 'RRJ 481', '{\"modSideSkirt\":-1,\"wheelColor\":156,\"modSteeringWheel\":-1,\"modAPlate\":-1,\"modGrille\":-1,\"modSuspension\":-1,\"modFrame\":-1,\"plateIndex\":4,\"plate\":\"RRJ 481\",\"dirtLevel\":0.0,\"modWindows\":-1,\"modXenon\":false,\"modAerials\":-1,\"bodyHealth\":1000.0,\"modSmokeEnabled\":false,\"modArmor\":-1,\"modSeats\":-1,\"modEngineBlock\":-1,\"extras\":{\"1\":false,\"12\":false,\"11\":false,\"10\":true,\"5\":false,\"4\":false,\"7\":true,\"6\":false,\"9\":false,\"8\":false,\"2\":false,\"3\":false},\"modTrimA\":-1,\"modHorns\":-1,\"modBrakes\":-1,\"model\":-1616359240,\"modTransmission\":-1,\"fuelLevel\":76.8,\"engineHealth\":1000.0,\"modRearBumper\":-1,\"modTurbo\":false,\"modEngine\":-1,\"modDoorSpeaker\":-1,\"modVanityPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modRightFender\":-1,\"modFrontBumper\":-1,\"modExhaust\":-1,\"windowTint\":-1,\"modDashboard\":-1,\"color2\":134,\"modLivery\":0,\"color1\":134,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"neonColor\":[255,0,255],\"modTrimB\":-1,\"modRoof\":-1,\"modShifterLeavers\":-1,\"modHood\":-1,\"modSpeakers\":-1,\"modFender\":-1,\"modTank\":-1,\"modSpoilers\":-1,\"modPlateHolder\":-1,\"modArchCover\":-1,\"modAirFilter\":-1,\"modTrunk\":-1,\"modHydrolic\":-1,\"pearlescentColor\":156,\"modBackWheels\":-1,\"wheels\":1,\"modStruts\":-1,\"modOrnaments\":-1,\"modFrontWheels\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'RVK 344', '{\"neonColor\":[255,0,255],\"modLivery\":0,\"modTrunk\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modFender\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modArchCover\":-1,\"color2\":134,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modSeats\":-1,\"model\":1745872177,\"modVanityPlate\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"plate\":\"RVK 344\",\"extras\":{\"6\":false,\"5\":false,\"10\":true,\"7\":true,\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"11\":false,\"12\":false,\"9\":false,\"8\":true},\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modBackWheels\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"color1\":134,\"modXenon\":false,\"modEngine\":-1,\"modTank\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modBrakes\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"tyreSmokeColor\":[255,255,255],\"modDial\":-1,\"wheels\":1,\"modOrnaments\":-1,\"wheelColor\":156,\"modRoof\":-1,\"bodyHealth\":1000.0,\"modHydrolic\":-1,\"modStruts\":-1,\"fuelLevel\":80.5,\"modShifterLeavers\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"modHorns\":-1,\"modTrimA\":-1,\"dirtLevel\":0.0}', 'car', 'police', 1);
INSERT INTO `old_owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('steam:110000133d93ea2', 'RWS 405', '{\"dirtLevel\":0.0,\"engineHealth\":1000.0,\"modOrnaments\":-1,\"modSpoilers\":-1,\"pearlescentColor\":88,\"model\":1672195559,\"modDashboard\":-1,\"modFrontBumper\":-1,\"modAerials\":-1,\"wheels\":6,\"modStruts\":-1,\"modDial\":-1,\"modTransmission\":-1,\"modTrunk\":-1,\"windowTint\":-1,\"modSpeakers\":-1,\"modWindows\":-1,\"modRightFender\":-1,\"modDoorSpeaker\":-1,\"modLivery\":-1,\"modTrimA\":-1,\"modGrille\":-1,\"modEngine\":-1,\"neonColor\":[255,0,255],\"modTrimB\":-1,\"modVanityPlate\":-1,\"modFender\":-1,\"modSteeringWheel\":-1,\"modArmor\":-1,\"modExhaust\":-1,\"modEngineBlock\":-1,\"modXenon\":false,\"modFrame\":-1,\"bodyHealth\":1000.0,\"modArchCover\":-1,\"plate\":\"RWS 405\",\"tyreSmokeColor\":[255,255,255],\"modBrakes\":-1,\"modTurbo\":false,\"plateIndex\":3,\"modShifterLeavers\":-1,\"modHorns\":-1,\"color2\":88,\"modHood\":-1,\"modSuspension\":-1,\"modAPlate\":-1,\"modTank\":-1,\"modAirFilter\":-1,\"modFrontWheels\":-1,\"modRoof\":-1,\"modBackWheels\":-1,\"neonEnabled\":[false,false,false,false],\"modRearBumper\":-1,\"extras\":{\"9\":true,\"4\":true,\"1\":true},\"modSmokeEnabled\":false,\"color1\":88,\"modPlateHolder\":-1,\"fuelLevel\":65.0,\"modSideSkirt\":-1,\"modSeats\":-1,\"modHydrolic\":-1,\"wheelColor\":156}', 'car', NULL, 1),
('steam:110000133d93ea2', 'TDI 236', '{\"modPlateHolder\":-1,\"modShifterLeavers\":-1,\"bodyHealth\":1000.0,\"pearlescentColor\":38,\"modAPlate\":-1,\"modGrille\":-1,\"color1\":29,\"modExhaust\":-1,\"modDoorSpeaker\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modTurbo\":false,\"plateIndex\":4,\"modTransmission\":-1,\"modFender\":-1,\"modAerials\":-1,\"modSmokeEnabled\":false,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"modFrontWheels\":-1,\"modTank\":-1,\"modRightFender\":-1,\"modHorns\":-1,\"modHood\":-1,\"modStruts\":-1,\"modFrontBumper\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"model\":1131912276,\"modSuspension\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modSpoilers\":-1,\"wheelColor\":156,\"modEngineBlock\":-1,\"modWindows\":-1,\"modSeats\":-1,\"extras\":[],\"tyreSmokeColor\":[255,255,255],\"engineHealth\":1000.0,\"modBackWheels\":-1,\"modXenon\":false,\"modSideSkirt\":-1,\"fuelLevel\":0.0,\"modSteeringWheel\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modDial\":-1,\"plate\":\"TDI 236\",\"neonEnabled\":[false,false,false,false],\"modLivery\":-1,\"modAirFilter\":-1,\"dirtLevel\":0.0,\"color2\":0,\"modTrimB\":-1,\"wheels\":6,\"modArmor\":-1,\"modTrimA\":-1,\"modTrunk\":-1,\"modSpeakers\":-1,\"modDashboard\":-1}', 'car', NULL, 1),
('steam:110000106197c1a', 'TJK 073', '{\"modBackWheels\":-1,\"color2\":0,\"modSideSkirt\":-1,\"modTransmission\":-1,\"modSmokeEnabled\":false,\"modEngineBlock\":-1,\"modBrakes\":-1,\"modEngine\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"wheelColor\":6,\"modFrontWheels\":-1,\"modAirFilter\":-1,\"modDial\":-1,\"modVanityPlate\":-1,\"modSteeringWheel\":-1,\"color1\":5,\"engineHealth\":1000.0,\"modArchCover\":-1,\"windowTint\":-1,\"modLivery\":-1,\"neonEnabled\":[false,false,false,false],\"modArmor\":-1,\"modFender\":-1,\"modFrontBumper\":-1,\"modXenon\":false,\"modTrimA\":-1,\"modTurbo\":false,\"neonColor\":[255,0,255],\"modPlateHolder\":-1,\"modTrunk\":-1,\"fuelLevel\":65.0,\"modDoorSpeaker\":-1,\"modTrimB\":-1,\"modHydrolic\":-1,\"modHood\":-1,\"modRearBumper\":-1,\"modAerials\":-1,\"modSuspension\":-1,\"model\":2067820283,\"modAPlate\":-1,\"plateIndex\":0,\"modHorns\":-1,\"modGrille\":-1,\"modTank\":-1,\"modFrame\":-1,\"plate\":\"TJK 073\",\"modExhaust\":-1,\"modSpeakers\":-1,\"modWindows\":-1,\"modDashboard\":-1,\"pearlescentColor\":5,\"modSpoilers\":-1,\"modRightFender\":-1,\"dirtLevel\":4.0,\"modShifterLeavers\":-1,\"modSeats\":-1,\"extras\":[],\"tyreSmokeColor\":[255,255,255],\"wheels\":7,\"modOrnaments\":-1,\"modStruts\":-1}', 'car', NULL, 1),
('steam:11000010b49a743', 'TJZ 019', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":598074270,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":94.9,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":1,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"TJZ 019\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":true,\"9\":false,\"7\":false,\"11\":true,\"6\":false,\"5\":false,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010b49a743', 'TPN 884', '{\"modWindows\":-1,\"modHorns\":-1,\"bodyHealth\":1000.0,\"modRoof\":-1,\"modDashboard\":-1,\"modFrame\":-1,\"modXenon\":false,\"modLivery\":0,\"modSmokeEnabled\":false,\"modFender\":-1,\"modBackWheels\":-1,\"modSideSkirt\":-1,\"modTrunk\":-1,\"modGrille\":-1,\"windowTint\":-1,\"modAerials\":-1,\"neonColor\":[255,0,255],\"modTurbo\":false,\"modAirFilter\":-1,\"dirtLevel\":0.0,\"color2\":134,\"modSuspension\":-1,\"modSpeakers\":-1,\"model\":1745872177,\"modFrontWheels\":-1,\"modHood\":-1,\"plateIndex\":4,\"modFrontBumper\":-1,\"engineHealth\":1000.0,\"modSteeringWheel\":-1,\"extras\":{\"12\":false,\"9\":false,\"10\":false,\"11\":false,\"3\":false,\"2\":false,\"1\":false,\"8\":true,\"7\":true,\"6\":false,\"5\":false,\"4\":false},\"pearlescentColor\":156,\"wheelColor\":156,\"modBrakes\":-1,\"wheels\":1,\"modDial\":-1,\"neonEnabled\":[false,false,false,false],\"modOrnaments\":-1,\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modSeats\":-1,\"plate\":\"TPN 884\",\"modTank\":-1,\"fuelLevel\":84.2,\"modAPlate\":-1,\"modEngine\":-1,\"modRightFender\":-1,\"modTransmission\":-1,\"color1\":134,\"modArmor\":-1,\"modDoorSpeaker\":-1,\"modTrimB\":-1,\"modHydrolic\":-1,\"modVanityPlate\":-1,\"modTrimA\":-1,\"modPlateHolder\":-1,\"modSpoilers\":-1,\"modEngineBlock\":-1,\"modStruts\":-1,\"modExhaust\":-1,\"modShifterLeavers\":-1,\"modRearBumper\":-1}', 'car', 'police', 1),
('steam:11000010fe595d0', 'TQV 422', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modRightFender\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":false,\"6\":false,\"7\":false,\"9\":false,\"1\":false,\"8\":false,\"11\":true,\"4\":false,\"5\":false,\"2\":false,\"3\":true},\"modOrnaments\":-1,\"engineHealth\":1000.0,\"modFrame\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modVanityPlate\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":1357997983,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"modTank\":-1,\"fuelLevel\":75.3,\"pearlescentColor\":156,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modTrunk\":-1,\"plate\":\"TQV 422\",\"modLivery\":0,\"neonColor\":[255,0,255],\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"modWindows\":-1,\"modHorns\":-1,\"wheels\":1,\"plateIndex\":4,\"modXenon\":false,\"wheelColor\":156,\"modFrontBumper\":-1,\"modShifterLeavers\":-1,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modExhaust\":-1,\"modRoof\":-1,\"modArmor\":-1,\"modBrakes\":-1,\"color1\":134,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modTrimA\":-1,\"modArchCover\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'UCU 505', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":1745872177,\"windowTint\":-1,\"modSpeakers\":-1,\"modAirFilter\":-1,\"plateIndex\":4,\"modHydrolic\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"neonColor\":[255,0,255],\"engineHealth\":1000.0,\"wheelColor\":156,\"modAerials\":-1,\"modShifterLeavers\":-1,\"modEngine\":-1,\"modFender\":-1,\"modSpoilers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modVanityPlate\":-1,\"neonEnabled\":[false,false,false,false],\"modOrnaments\":-1,\"modBackWheels\":-1,\"modXenon\":false,\"modSuspension\":-1,\"modSmokeEnabled\":false,\"modLivery\":1,\"color1\":134,\"modPlateHolder\":-1,\"fuelLevel\":82.2,\"modTrimA\":-1,\"modArchCover\":-1,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modDial\":-1,\"plate\":\"UCU 505\",\"modAPlate\":-1,\"modDashboard\":-1,\"modRearBumper\":-1,\"modSideSkirt\":-1,\"modTurbo\":false,\"color2\":134,\"pearlescentColor\":156,\"tyreSmokeColor\":[255,255,255],\"modHood\":-1,\"modFrontBumper\":-1,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"7\":false,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":true,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'UJT 115', '{\"modAerials\":-1,\"color2\":0,\"modTrimA\":-1,\"modGrille\":-1,\"engineHealth\":1000.0,\"modExhaust\":-1,\"modRoof\":-1,\"modWindows\":-1,\"modTrunk\":-1,\"modSideSkirt\":-1,\"windowTint\":-1,\"dirtLevel\":5.0,\"pearlescentColor\":38,\"modSuspension\":-1,\"modVanityPlate\":-1,\"modTransmission\":-1,\"modOrnaments\":-1,\"modEngine\":-1,\"modFrame\":-1,\"model\":1131912276,\"modSpoilers\":-1,\"color1\":29,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modStruts\":-1,\"tyreSmokeColor\":[255,255,255],\"modRightFender\":-1,\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"bodyHealth\":1000.0,\"plateIndex\":4,\"plate\":\"UJT 115\",\"modArchCover\":-1,\"modBrakes\":-1,\"wheels\":6,\"modSteeringWheel\":-1,\"modLivery\":-1,\"modAPlate\":-1,\"modArmor\":-1,\"modBackWheels\":-1,\"fuelLevel\":0.0,\"modRearBumper\":-1,\"modShifterLeavers\":-1,\"modHood\":-1,\"extras\":[],\"modSeats\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"modFrontWheels\":-1,\"modXenon\":false,\"wheelColor\":156,\"modDashboard\":-1,\"modFender\":-1,\"modHydrolic\":-1,\"modTrimB\":-1,\"modFrontBumper\":-1,\"modTank\":-1,\"modHorns\":-1,\"neonColor\":[255,0,255],\"neonEnabled\":[false,false,false,false],\"modTurbo\":false}', 'car', NULL, 1),
('steam:11000010b49a743', 'VAV 851', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":4.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":1027958099,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":87.0,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":0,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"VAV 851\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"7\":true,\"9\":false,\"12\":false,\"11\":true,\"6\":false,\"5\":false,\"8\":false,\"10\":false},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:110000133d93ea2', 'WUX 779', '{\"modAerials\":-1,\"color2\":0,\"modTrimA\":-1,\"modShifterLeavers\":-1,\"modWindows\":-1,\"modExhaust\":-1,\"modRoof\":-1,\"modGrille\":-1,\"modTransmission\":-1,\"bodyHealth\":1000.0,\"modFender\":-1,\"dirtLevel\":7.0,\"pearlescentColor\":38,\"modSuspension\":-1,\"modVanityPlate\":-1,\"modStruts\":-1,\"modOrnaments\":-1,\"modSteeringWheel\":-1,\"modPlateHolder\":-1,\"neonColor\":[255,0,255],\"modFrontBumper\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"modTurbo\":false,\"model\":1131912276,\"modSideSkirt\":-1,\"tyreSmokeColor\":[255,255,255],\"neonEnabled\":[false,false,false,false],\"modDoorSpeaker\":-1,\"modEngineBlock\":-1,\"modRightFender\":-1,\"plateIndex\":4,\"plate\":\"WUX 779\",\"modTrunk\":-1,\"modBrakes\":-1,\"modArchCover\":-1,\"modEngine\":-1,\"wheelColor\":156,\"modFrame\":-1,\"modArmor\":-1,\"modBackWheels\":-1,\"fuelLevel\":86.0,\"modRearBumper\":-1,\"modSmokeEnabled\":false,\"modHood\":-1,\"modAPlate\":-1,\"modSeats\":-1,\"modHydrolic\":-1,\"color1\":29,\"modFrontWheels\":-1,\"modSpoilers\":-1,\"modDial\":-1,\"modDashboard\":-1,\"extras\":[],\"modAirFilter\":-1,\"modTrimB\":-1,\"modLivery\":-1,\"engineHealth\":1000.0,\"modHorns\":-1,\"windowTint\":-1,\"wheels\":6,\"modTank\":-1}', 'car', NULL, 1),
('steam:110000111c332ed', 'XFW 709', '{\"wheelColor\":156,\"modRoof\":-1,\"fuelLevel\":75.1,\"modFrontWheels\":-1,\"modSteeringWheel\":-1,\"bodyHealth\":1000.0,\"modDashboard\":-1,\"plateIndex\":4,\"wheels\":0,\"modXenon\":false,\"modSpoilers\":-1,\"modEngine\":-1,\"tyreSmokeColor\":[255,255,255],\"model\":-1059115956,\"modVanityPlate\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modGrille\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRightFender\":-1,\"modHorns\":-1,\"windowTint\":-1,\"modFrontBumper\":-1,\"modPlateHolder\":-1,\"modTransmission\":-1,\"modBrakes\":-1,\"modEngineBlock\":-1,\"modFender\":-1,\"modExhaust\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"modBackWheels\":-1,\"modAPlate\":-1,\"dirtLevel\":0.0,\"modWindows\":-1,\"modHydrolic\":-1,\"modDial\":-1,\"modTrunk\":-1,\"modDoorSpeaker\":-1,\"modSideSkirt\":-1,\"modAirFilter\":-1,\"modRearBumper\":-1,\"color2\":134,\"modOrnaments\":-1,\"extras\":{\"4\":false,\"3\":false,\"2\":false,\"1\":false,\"12\":false,\"11\":false,\"10\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":true,\"5\":false},\"modTrimA\":-1,\"modTank\":-1,\"plate\":\"XFW 709\",\"modArchCover\":-1,\"modAerials\":-1,\"neonColor\":[255,0,255],\"modSmokeEnabled\":false,\"modFrame\":-1,\"neonEnabled\":[false,false,false,false],\"color1\":134,\"modStruts\":-1,\"modLivery\":0,\"modSuspension\":-1,\"modSeats\":-1,\"modHood\":-1,\"modSpeakers\":-1}', 'car', 'police', 1),
('steam:11000013f6b85e1', 'XJS 557', '{\"windowTint\":-1,\"modSmokeEnabled\":false,\"modDial\":-1,\"modPlateHolder\":-1,\"dirtLevel\":0.0,\"modOrnaments\":-1,\"modTransmission\":-1,\"extras\":{\"10\":false,\"12\":true,\"6\":false,\"7\":false,\"9\":false,\"1\":false,\"8\":false,\"11\":false,\"4\":true,\"5\":false,\"2\":false,\"3\":false},\"modRightFender\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modAPlate\":-1,\"modAerials\":-1,\"modTrunk\":-1,\"modTurbo\":false,\"modEngineBlock\":-1,\"modAirFilter\":-1,\"model\":1357997983,\"neonEnabled\":[false,false,false,false],\"modStruts\":-1,\"modTrimB\":-1,\"modHorns\":-1,\"modTank\":-1,\"modShifterLeavers\":-1,\"modDashboard\":-1,\"modHydrolic\":-1,\"modSpoilers\":-1,\"modSteeringWheel\":-1,\"modRoof\":-1,\"plate\":\"XJS 557\",\"modArchCover\":-1,\"plateIndex\":4,\"modSideSkirt\":-1,\"modGrille\":-1,\"modBackWheels\":-1,\"color1\":134,\"neonColor\":[255,0,255],\"wheels\":1,\"modFrame\":-1,\"modXenon\":false,\"modBrakes\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"tyreSmokeColor\":[255,255,255],\"modTrimA\":-1,\"modExhaust\":-1,\"modLivery\":1,\"modArmor\":-1,\"modSeats\":-1,\"modWindows\":-1,\"color2\":134,\"modHood\":-1,\"modFrontWheels\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"modFender\":-1,\"modEngine\":-1,\"fuelLevel\":82.2,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"modRearBumper\":-1,\"pearlescentColor\":156}', 'car', 'police', 1),
('steam:11000010b49a743', 'YCM 152', '{\"modTank\":-1,\"modHorns\":-1,\"modEngineBlock\":-1,\"dirtLevel\":0.0,\"modSeats\":-1,\"bodyHealth\":1000.0,\"modArmor\":-1,\"wheels\":1,\"model\":1745872177,\"windowTint\":-1,\"modSpeakers\":-1,\"modArchCover\":-1,\"plateIndex\":4,\"modAirFilter\":-1,\"modBrakes\":-1,\"modExhaust\":-1,\"modHydrolic\":-1,\"modFrontBumper\":-1,\"wheelColor\":156,\"modAerials\":-1,\"modBackWheels\":-1,\"fuelLevel\":85.4,\"modFender\":-1,\"modShifterLeavers\":-1,\"modTrimB\":-1,\"modRoof\":-1,\"modSpoilers\":-1,\"neonColor\":[255,0,255],\"modOrnaments\":-1,\"neonEnabled\":[false,false,false,false],\"modXenon\":false,\"modDial\":-1,\"modSmokeEnabled\":false,\"modSuspension\":-1,\"modLivery\":2,\"modDashboard\":-1,\"color1\":134,\"modTrimA\":-1,\"pearlescentColor\":156,\"modRightFender\":-1,\"modGrille\":-1,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modTurbo\":false,\"plate\":\"YCM 152\",\"modAPlate\":-1,\"modEngine\":-1,\"modVanityPlate\":-1,\"modPlateHolder\":-1,\"modSideSkirt\":-1,\"color2\":134,\"tyreSmokeColor\":[255,255,255],\"modRearBumper\":-1,\"modHood\":-1,\"engineHealth\":1000.0,\"modDoorSpeaker\":-1,\"modStruts\":-1,\"modFrontWheels\":-1,\"modWindows\":-1,\"extras\":{\"2\":false,\"1\":false,\"4\":false,\"3\":true,\"7\":false,\"9\":false,\"12\":false,\"11\":false,\"6\":false,\"5\":false,\"8\":false,\"10\":true},\"modFrame\":-1,\"modTransmission\":-1}', 'car', 'police', 1),
('steam:11000010fe595d0', 'YMJ 060', '{\"modStruts\":-1,\"modTank\":-1,\"modAPlate\":-1,\"tyreSmokeColor\":[255,255,255],\"modTrunk\":-1,\"extras\":{\"12\":false,\"1\":false,\"4\":false,\"10\":false,\"5\":false,\"11\":false,\"3\":true,\"2\":false,\"9\":false,\"8\":false,\"7\":false,\"6\":false},\"modSeats\":-1,\"color1\":134,\"modDoorSpeaker\":-1,\"plate\":\"YMJ 060\",\"modTransmission\":-1,\"modAirFilter\":-1,\"modFrontBumper\":-1,\"modArmor\":-1,\"engineHealth\":1000.0,\"wheels\":1,\"modTrimB\":-1,\"color2\":134,\"modOrnaments\":-1,\"modArchCover\":-1,\"modShifterLeavers\":-1,\"windowTint\":-1,\"modDial\":-1,\"modGrille\":-1,\"modVanityPlate\":-1,\"modFender\":-1,\"modHydrolic\":-1,\"plateIndex\":4,\"modLivery\":0,\"modRearBumper\":-1,\"modFrontWheels\":-1,\"modXenon\":false,\"modSpeakers\":-1,\"modHood\":-1,\"modDashboard\":-1,\"modEngine\":-1,\"modPlateHolder\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modRightFender\":-1,\"modBrakes\":-1,\"modBackWheels\":-1,\"bodyHealth\":1000.0,\"modHorns\":-1,\"modWindows\":-1,\"modEngineBlock\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"pearlescentColor\":156,\"neonColor\":[255,0,255],\"modSteeringWheel\":-1,\"dirtLevel\":0.0,\"fuelLevel\":88.9,\"modSideSkirt\":-1,\"wheelColor\":156,\"modAerials\":-1,\"model\":161178935,\"modRoof\":-1,\"modExhaust\":-1,\"modFrame\":-1,\"modTrimA\":-1,\"modSpoilers\":-1}', 'car', 'police', 1),
('steam:1100001081d1893', 'ZCK 658', '{\"plate\":\"ZCK 658\",\"pearlescentColor\":156,\"modTransmission\":-1,\"plateIndex\":4,\"modXenon\":false,\"color1\":134,\"modPlateHolder\":-1,\"bodyHealth\":1000.0,\"tyreSmokeColor\":[255,255,255],\"engineHealth\":1000.0,\"modArmor\":-1,\"modSideSkirt\":-1,\"modSuspension\":-1,\"modRightFender\":-1,\"modEngineBlock\":-1,\"modFrame\":-1,\"modRearBumper\":-1,\"modHydrolic\":-1,\"modTrimA\":-1,\"modBrakes\":-1,\"modAerials\":-1,\"wheelColor\":156,\"modHorns\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modGrille\":-1,\"neonColor\":[255,0,255],\"modRoof\":-1,\"modWindows\":-1,\"modHood\":-1,\"model\":1745872177,\"fuelLevel\":81.1,\"modSpoilers\":-1,\"modVanityPlate\":-1,\"wheels\":1,\"modFender\":-1,\"modFrontBumper\":-1,\"modStruts\":-1,\"modBackWheels\":-1,\"modFrontWheels\":-1,\"dirtLevel\":0.0,\"modTrunk\":-1,\"modSteeringWheel\":-1,\"modAirFilter\":-1,\"extras\":{\"11\":false,\"10\":false,\"1\":false,\"12\":false,\"4\":false,\"5\":false,\"2\":true,\"3\":false,\"8\":false,\"9\":false,\"6\":false,\"7\":false},\"color2\":134,\"modDashboard\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"modArchCover\":-1,\"modLivery\":1,\"windowTint\":-1,\"modExhaust\":-1,\"modTank\":-1,\"neonEnabled\":[false,false,false,false],\"modSmokeEnabled\":false,\"modDoorSpeaker\":-1,\"modTurbo\":false,\"modSpeakers\":-1,\"modEngine\":-1,\"modSeats\":-1,\"modOrnaments\":-1}', 'car', 'police', 1),
('steam:1100001139319c1', 'ZFQ 624', '{\"neonColor\":[255,0,255],\"modLivery\":1,\"modTrunk\":-1,\"modAPlate\":-1,\"modHood\":-1,\"modFender\":-1,\"modGrille\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modArchCover\":-1,\"color2\":134,\"modFrontBumper\":-1,\"modFrontWheels\":-1,\"modSeats\":-1,\"model\":598074270,\"modVanityPlate\":-1,\"modAirFilter\":-1,\"modPlateHolder\":-1,\"plate\":\"ZFQ 624\",\"extras\":{\"6\":false,\"5\":true,\"10\":true,\"7\":false,\"2\":false,\"1\":false,\"4\":false,\"3\":false,\"11\":false,\"9\":false,\"8\":false},\"modEngineBlock\":-1,\"modRearBumper\":-1,\"modBackWheels\":-1,\"modSteeringWheel\":-1,\"modTrimB\":-1,\"modDashboard\":-1,\"engineHealth\":1000.0,\"pearlescentColor\":156,\"color1\":134,\"modXenon\":false,\"modEngine\":-1,\"modTank\":-1,\"modFrame\":-1,\"windowTint\":-1,\"modWindows\":-1,\"modSuspension\":-1,\"modTurbo\":false,\"modArmor\":-1,\"modBrakes\":-1,\"plateIndex\":4,\"modDoorSpeaker\":-1,\"modSpeakers\":-1,\"modSmokeEnabled\":false,\"neonEnabled\":[false,false,false,false],\"tyreSmokeColor\":[255,255,255],\"modDial\":-1,\"wheels\":1,\"modOrnaments\":-1,\"wheelColor\":156,\"modRoof\":-1,\"bodyHealth\":1000.0,\"modHydrolic\":-1,\"modStruts\":-1,\"fuelLevel\":81.5,\"modShifterLeavers\":-1,\"modAerials\":-1,\"modSideSkirt\":-1,\"modHorns\":-1,\"modTrimA\":-1,\"dirtLevel\":0.0}', 'car', 'police', 1),
('steam:1100001081d1893', 'ZQO 189', '{\"modBackWheels\":-1,\"modSmokeEnabled\":false,\"modHydrolic\":-1,\"model\":-344943009,\"modSideSkirt\":-1,\"modBrakes\":-1,\"modSpeakers\":-1,\"plate\":\"ZQO 189\",\"modExhaust\":-1,\"wheelColor\":156,\"modFrame\":-1,\"engineHealth\":999.6,\"modPlateHolder\":-1,\"modHood\":-1,\"modDoorSpeaker\":-1,\"modShifterLeavers\":-1,\"modTurbo\":false,\"modRoof\":-1,\"wheels\":0,\"modEngine\":-1,\"modTrunk\":-1,\"pearlescentColor\":111,\"modVanityPlate\":-1,\"modTransmission\":-1,\"modRearBumper\":-1,\"modAerials\":-1,\"modWindows\":-1,\"color1\":6,\"modRightFender\":-1,\"modTrimA\":-1,\"neonEnabled\":[false,false,false,false],\"color2\":0,\"modDial\":-1,\"dirtLevel\":11.2,\"modOrnaments\":-1,\"modEngineBlock\":-1,\"modSteeringWheel\":-1,\"modArchCover\":-1,\"modXenon\":false,\"bodyHealth\":999.8,\"plateIndex\":0,\"modGrille\":-1,\"extras\":{\"12\":false,\"10\":true},\"modTank\":-1,\"tyreSmokeColor\":[255,255,255],\"modFender\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modSeats\":-1,\"modArmor\":-1,\"modTrimB\":-1,\"modAPlate\":-1,\"modHorns\":-1,\"modFrontBumper\":-1,\"fuelLevel\":77.4,\"modDashboard\":-1,\"modAirFilter\":-1,\"modStruts\":-1,\"modSuspension\":-1,\"neonColor\":[255,0,255],\"modSpoilers\":-1,\"modLivery\":-1}', 'car', NULL, 1),
('steam:11000010b49a743', 'ZSR 928', '{\"model\":-1059115956,\"modBrakes\":-1,\"bodyHealth\":1000.0,\"modStruts\":-1,\"modTrimA\":-1,\"extras\":{\"1\":false,\"2\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"7\":true,\"8\":false,\"9\":false,\"10\":false,\"11\":false,\"12\":false},\"plate\":\"ZSR 928\",\"dirtLevel\":1.0,\"neonColor\":[255,0,255],\"modArchCover\":-1,\"tyreSmokeColor\":[255,255,255],\"modAirFilter\":-1,\"fuelLevel\":77.8,\"modSpeakers\":-1,\"modPlateHolder\":-1,\"pearlescentColor\":156,\"modBackWheels\":-1,\"modTrimB\":-1,\"modEngine\":-1,\"modFender\":-1,\"modTurbo\":false,\"modTank\":-1,\"engineHealth\":1000.0,\"color1\":134,\"modRightFender\":-1,\"color2\":134,\"modHydrolic\":-1,\"modExhaust\":-1,\"modHorns\":-1,\"modXenon\":false,\"modWindows\":-1,\"wheelColor\":156,\"modVanityPlate\":-1,\"modTrunk\":-1,\"modSeats\":-1,\"neonEnabled\":[false,false,false,false],\"modAerials\":-1,\"modAPlate\":-1,\"modTransmission\":-1,\"modGrille\":-1,\"modDashboard\":-1,\"modFrontBumper\":-1,\"modLivery\":3,\"modSuspension\":-1,\"modEngineBlock\":-1,\"windowTint\":-1,\"modSmokeEnabled\":false,\"modFrontWheels\":-1,\"modSideSkirt\":-1,\"modFrame\":-1,\"wheels\":0,\"modSpoilers\":-1,\"modDial\":-1,\"modShifterLeavers\":-1,\"modDoorSpeaker\":-1,\"modRearBumper\":-1,\"modOrnaments\":-1,\"modArmor\":-1,\"modSteeringWheel\":-1,\"modHood\":-1,\"modRoof\":-1,\"plateIndex\":4}', 'car', 'police', 1);

-- --------------------------------------------------------

--
-- Table structure for table `owned_properties`
--

CREATE TABLE `owned_properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` double NOT NULL,
  `rented` int(11) NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `owned_properties`
--

INSERT INTO `owned_properties` (`id`, `name`, `price`, `rented`, `owner`) VALUES
(1, 'TinselTowersApt12', 1700000, 0, 'steam:110000111c332ed'),
(2, 'RichardMajesticApt2', 8500, 1, 'steam:110000134e3ca1a');

-- --------------------------------------------------------

--
-- Table structure for table `owned_vehicles`
--

CREATE TABLE `owned_vehicles` (
  `owner` varchar(22) NOT NULL,
  `plate` varchar(12) NOT NULL,
  `vehicle` longtext DEFAULT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'car',
  `job` varchar(20) NOT NULL DEFAULT '',
  `stored` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `owned_vehicles`
--

INSERT INTO `owned_vehicles` (`owner`, `plate`, `vehicle`, `type`, `job`, `stored`) VALUES
('steam:11000010a01bdb9', 'BDD 799', '{\"modTransmission\":-1,\"modDial\":-1,\"modEngineBlock\":-1,\"modHood\":-1,\"modRoof\":-1,\"color2\":0,\"extras\":[],\"tyreSmokeColor\":[255,255,255],\"modGrille\":-1,\"modArmor\":-1,\"modTank\":-1,\"modAPlate\":-1,\"modBackWheels\":-1,\"modDashboard\":-1,\"modRightFender\":-1,\"fuelLevel\":93.4,\"modOrnaments\":-1,\"modFender\":-1,\"engineHealth\":1000.0,\"modShifterLeavers\":-1,\"dirtLevel\":7.3,\"modLivery\":-1,\"modFrame\":-1,\"modHorns\":-1,\"modEngine\":-1,\"bodyHealth\":1000.0,\"modSpeakers\":-1,\"wheelColor\":156,\"modExhaust\":-1,\"pearlescentColor\":38,\"neonEnabled\":[false,false,false,false],\"modSeats\":-1,\"plateIndex\":4,\"modHydrolic\":-1,\"modSideSkirt\":-1,\"modBrakes\":-1,\"modFrontWheels\":-1,\"windowTint\":-1,\"modWindows\":-1,\"neonColor\":[255,0,255],\"modDoorSpeaker\":-1,\"modTrimA\":-1,\"modStruts\":-1,\"wheels\":6,\"modFrontBumper\":-1,\"modSuspension\":-1,\"modAerials\":-1,\"modRearBumper\":-1,\"modArchCover\":-1,\"modSmokeEnabled\":false,\"modTrunk\":-1,\"plate\":\"BDD 799\",\"modSpoilers\":-1,\"modTrimB\":-1,\"modPlateHolder\":-1,\"modSteeringWheel\":-1,\"model\":1131912276,\"modAirFilter\":-1,\"modVanityPlate\":-1,\"color1\":29,\"modXenon\":false,\"modTurbo\":false}', 'car', '', 1),
('steam:1100001081d1893', 'GHX 382', '{\"modDial\":-1,\"dirtLevel\":8.0,\"modSeats\":-1,\"engineHealth\":1000.0,\"modVanityPlate\":-1,\"modDoorSpeaker\":-1,\"modSuspension\":-1,\"neonEnabled\":[false,false,false,false],\"modTurbo\":false,\"modTrimB\":-1,\"modRearBumper\":-1,\"modDashboard\":-1,\"modShifterLeavers\":-1,\"modFrame\":-1,\"modSpoilers\":-1,\"modRightFender\":-1,\"modXenon\":false,\"modWindows\":-1,\"pearlescentColor\":89,\"modSpeakers\":-1,\"plate\":\"GHX 382\",\"modFrontWheels\":-1,\"modAerials\":-1,\"modOrnaments\":-1,\"plateIndex\":4,\"fuelLevel\":0.0,\"modTrunk\":-1,\"color1\":88,\"neonColor\":[255,0,255],\"modFender\":-1,\"modRoof\":-1,\"wheelColor\":156,\"extras\":[],\"wheels\":6,\"bodyHealth\":1000.0,\"modEngine\":-1,\"model\":1131912276,\"modHorns\":-1,\"modAirFilter\":-1,\"modHood\":-1,\"modAPlate\":-1,\"windowTint\":-1,\"modEngineBlock\":-1,\"modGrille\":-1,\"modTrimA\":-1,\"modBrakes\":-1,\"modTank\":-1,\"color2\":0,\"modArchCover\":-1,\"modHydrolic\":-1,\"modSteeringWheel\":-1,\"modPlateHolder\":-1,\"tyreSmokeColor\":[255,255,255],\"modLivery\":-1,\"modStruts\":-1,\"modBackWheels\":-1,\"modTransmission\":-1,\"modSmokeEnabled\":false,\"modFrontBumper\":-1,\"modExhaust\":-1,\"modArmor\":-1,\"modSideSkirt\":-1}', 'car', '', 0),
('steam:1100001081d1893', 'HWQ 197', '{\"modDoorSpeaker\":-1,\"modHood\":-1,\"modHorns\":-1,\"modSteeringWheel\":-1,\"modRightFender\":-1,\"modSideSkirt\":-1,\"windowTint\":-1,\"wheelColor\":156,\"engineHealth\":1000.0,\"modArmor\":-1,\"modFender\":-1,\"modEngineBlock\":-1,\"modAPlate\":-1,\"modTank\":-1,\"modPlateHolder\":-1,\"color1\":6,\"modOrnaments\":-1,\"modBackWheels\":-1,\"modAirFilter\":-1,\"modFrame\":-1,\"pearlescentColor\":111,\"modSpeakers\":-1,\"modSuspension\":-1,\"modWindows\":-1,\"neonEnabled\":[false,false,false,false],\"modSmokeEnabled\":false,\"color2\":0,\"dirtLevel\":11.1,\"modDial\":-1,\"modTrimB\":-1,\"plate\":\"HWQ 197\",\"tyreSmokeColor\":[255,255,255],\"neonColor\":[255,0,255],\"modGrille\":-1,\"modVanityPlate\":-1,\"modRoof\":-1,\"modTrunk\":-1,\"modStruts\":-1,\"plateIndex\":0,\"modSeats\":-1,\"modArchCover\":-1,\"modFrontBumper\":-1,\"modBrakes\":-1,\"modSpoilers\":-1,\"model\":-344943009,\"bodyHealth\":1000.0,\"extras\":{\"10\":false,\"12\":true},\"modShifterLeavers\":-1,\"modRearBumper\":-1,\"modFrontWheels\":-1,\"modTurbo\":false,\"modHydrolic\":-1,\"modXenon\":false,\"modEngine\":-1,\"modTrimA\":-1,\"modExhaust\":-1,\"modAerials\":-1,\"fuelLevel\":92.6,\"modTransmission\":-1,\"modDashboard\":-1,\"wheels\":0,\"modLivery\":-1}', 'car', '', 1),
('steam:1100001081d1893', 'UED 687', '{\"modShifterLeavers\":-1,\"windowTint\":-1,\"plate\":\"UED 687\",\"modFender\":-1,\"modHydrolic\":-1,\"bodyHealth\":1000.0,\"modVanityPlate\":-1,\"neonColor\":[255,0,255],\"modXenon\":false,\"modStruts\":-1,\"wheels\":1,\"modBackWheels\":-1,\"modFrontBumper\":-1,\"modAerials\":-1,\"modRightFender\":-1,\"neonEnabled\":[false,false,false,false],\"modFrame\":-1,\"modRearBumper\":-1,\"modEngineBlock\":-1,\"modDoorSpeaker\":-1,\"modAirFilter\":-1,\"modSmokeEnabled\":false,\"modGrille\":-1,\"dirtLevel\":5.0,\"modDial\":-1,\"modEngine\":-1,\"modSuspension\":-1,\"modExhaust\":-1,\"modTransmission\":-1,\"modTrimA\":-1,\"tyreSmokeColor\":[255,255,255],\"modSteeringWheel\":-1,\"modHood\":-1,\"model\":1027958099,\"modBrakes\":-1,\"extras\":{\"7\":false,\"8\":false,\"9\":false,\"12\":false,\"3\":false,\"4\":false,\"5\":false,\"6\":false,\"1\":false,\"2\":true,\"10\":false,\"11\":true},\"modSeats\":-1,\"modSideSkirt\":-1,\"modSpeakers\":-1,\"modTrunk\":-1,\"modArmor\":-1,\"modRoof\":-1,\"modTurbo\":false,\"color2\":134,\"modPlateHolder\":-1,\"modLivery\":3,\"engineHealth\":1000.0,\"wheelColor\":156,\"modAPlate\":-1,\"modSpoilers\":-1,\"modWindows\":-1,\"plateIndex\":4,\"modTank\":-1,\"modFrontWheels\":-1,\"pearlescentColor\":156,\"modArchCover\":-1,\"color1\":134,\"modHorns\":-1,\"modOrnaments\":-1,\"fuelLevel\":82.2,\"modDashboard\":-1,\"modTrimB\":-1}', 'car', 'police', 1);

-- --------------------------------------------------------

--
-- Table structure for table `phone_app_chat`
--

CREATE TABLE `phone_app_chat` (
  `id` int(11) NOT NULL,
  `channel` varchar(20) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_app_chat`
--

INSERT INTO `phone_app_chat` (`id`, `channel`, `message`, `time`) VALUES
(28, 'drugs', 'Test', '2020-03-14 18:45:59'),
(29, 'drugs', 'Hi anyone looking for coke', '2020-03-14 18:46:27'),
(30, 'killkillian', 'yesssssss', '2020-03-14 18:47:23'),
(31, 'killkillian', 'I agree', '2020-03-14 18:47:34'),
(32, 'killkillian', 'When can we set this up?', '2020-03-14 18:47:58'),
(33, 'killkillian', 'like now', '2020-03-14 18:48:07');

-- --------------------------------------------------------

--
-- Table structure for table `phone_calls`
--

CREATE TABLE `phone_calls` (
  `id` int(11) NOT NULL,
  `owner` varchar(10) NOT NULL COMMENT 'Name',
  `num` varchar(10) NOT NULL COMMENT 'Phone Number',
  `incoming` int(11) NOT NULL COMMENT 'Define Incoming Call',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `accepts` int(11) NOT NULL COMMENT 'Accept Call'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_calls`
--

INSERT INTO `phone_calls` (`id`, `owner`, `num`, `incoming`, `time`, `accepts`) VALUES
(122, '827-5849', '1130914', 1, '2020-02-06 04:46:45', 0),
(125, '827-5849', '1130914', 1, '2020-02-06 04:49:29', 0),
(127, '113-0914', '827-5849', 1, '2020-02-06 04:53:06', 1),
(128, '827-5849', '113-0914', 0, '2020-02-06 04:53:06', 1),
(129, '827-5849', '113-0914', 1, '2020-02-06 04:54:49', 0),
(130, '113-0914', '827-5849', 0, '2020-02-06 04:54:49', 0),
(131, '440-5316', '7711741', 1, '2020-02-08 02:23:58', 0),
(132, '104-0089', '827-5849', 0, '2020-02-08 02:24:39', 1),
(133, '827-5849', '104-0089', 1, '2020-02-08 02:24:39', 1),
(134, '104-0089', '827-5849', 1, '2020-02-08 03:55:28', 1),
(135, '827-5849', '104-0089', 0, '2020-02-08 03:55:28', 1),
(136, '104-0089', '827-5849', 1, '2020-02-08 04:39:40', 1),
(137, '827-5849', '104-0089', 0, '2020-02-08 04:39:40', 1),
(138, '104-0089', '827-5849', 1, '2020-02-08 04:41:08', 1),
(139, '827-5849', '104-0089', 0, '2020-02-08 04:41:08', 1),
(140, '104-0089', '827-5849', 1, '2020-02-08 04:52:23', 1),
(141, '827-5849', '104-0089', 0, '2020-02-08 04:52:23', 1),
(142, '104-0089', '827-5849', 1, '2020-02-08 04:54:47', 1),
(143, '827-5849', '104-0089', 0, '2020-02-08 04:54:47', 1),
(144, '827-5849', '104-0089', 0, '2020-02-08 04:55:01', 1),
(145, '104-0089', '827-5849', 1, '2020-02-08 04:55:01', 1),
(146, '104-0089', '827-5849', 1, '2020-02-08 04:56:51', 1),
(147, '827-5849', '104-0089', 0, '2020-02-08 04:56:51', 1),
(148, '104-0089', '827-5849', 1, '2020-02-08 04:59:32', 0),
(149, '827-5849', '104-0089', 0, '2020-02-08 04:59:32', 0),
(150, '104-0089', '827-5849', 1, '2020-02-08 04:59:44', 1),
(151, '827-5849', '104-0089', 0, '2020-02-08 04:59:44', 1),
(152, '104-0089', '827-5849', 1, '2020-02-08 05:02:04', 0),
(153, '827-5849', '104-0089', 0, '2020-02-08 05:02:04', 0),
(154, '104-0089', '827-5849', 0, '2020-02-08 05:05:00', 1),
(155, '827-5849', '104-0089', 1, '2020-02-08 05:05:00', 1),
(156, '104-0089', '827-5849', 1, '2020-02-08 05:06:01', 1),
(157, '827-5849', '104-0089', 0, '2020-02-08 05:06:01', 1),
(158, '104-0089', '827-5849', 1, '2020-02-08 05:06:17', 1),
(159, '827-5849', '104-0089', 0, '2020-02-08 05:06:17', 1),
(160, '822-5198', '911', 1, '2020-02-08 05:24:12', 0),
(161, '822-5198', '911', 1, '2020-02-08 05:26:49', 0),
(162, '827-5849', '104-0089', 1, '2020-02-10 00:07:02', 0),
(163, '104-0089', '827-5849', 0, '2020-02-10 00:07:02', 0),
(164, '104-0089', '753-9558', 1, '2020-02-10 01:24:50', 0),
(165, '753-9558', '104-0089', 0, '2020-02-10 01:24:50', 0),
(166, '753-9558', '104-0089', 1, '2020-02-10 01:25:05', 1),
(167, '104-0089', '753-9558', 0, '2020-02-10 01:25:05', 1),
(168, '113-0914', '121-3758', 1, '2020-02-10 01:49:20', 1),
(169, '121-3758', '113-0914', 0, '2020-02-10 01:49:20', 1),
(170, '104-0089', '753-9558', 1, '2020-02-10 02:32:56', 1),
(171, '753-9558', '104-0089', 0, '2020-02-10 02:32:56', 1),
(172, '753-9558', '104-0089', 1, '2020-02-10 02:40:02', 1),
(173, '104-0089', '753-9558', 0, '2020-02-10 02:40:02', 1),
(174, '753-9558', '104-0089', 1, '2020-02-10 02:42:31', 1),
(175, '104-0089', '753-9558', 0, '2020-02-10 02:42:31', 1),
(176, '440-5316', '771-1741', 1, '2020-02-23 03:43:38', 0),
(177, '771-1741', '440-5316', 0, '2020-02-23 03:43:38', 0),
(178, '440-5316', '771-1741', 1, '2020-02-23 03:44:12', 0),
(179, '771-1741', '440-5316', 0, '2020-02-23 03:44:12', 0),
(180, '531-3502', '873-7104', 1, '2020-03-14 18:42:20', 1),
(181, '873-7104', '531-3502', 0, '2020-03-14 18:42:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `phone_messages`
--

CREATE TABLE `phone_messages` (
  `id` int(11) NOT NULL,
  `transmitter` varchar(10) NOT NULL,
  `receiver` varchar(10) NOT NULL,
  `message` varchar(255) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `isRead` int(11) NOT NULL DEFAULT 0,
  `owner` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_messages`
--

INSERT INTO `phone_messages` (`id`, `transmitter`, `receiver`, `message`, `time`, `isRead`, `owner`) VALUES
(158, 'police', '873-7104', 'GPS: 428.07467651367, -978.43487548828', '2020-03-14 18:54:34', 1, 0),
(159, 'police', '531-3502', 'Alert (873-7104): Test', '2020-03-14 18:55:39', 1, 0),
(160, 'police', '873-7104', 'GPS: 428.07467651367, -978.43487548828', '2020-03-14 18:55:39', 1, 0),
(161, 'police', '531-3502', 'GPS: 428.07467651367, -978.43487548828', '2020-03-14 18:55:39', 1, 0),
(162, 'police', '873-7104', 'Alert (873-7104): Test', '2020-03-14 18:55:39', 1, 0),
(163, 'police', '531-3502', 'on my way', '2020-03-14 18:56:05', 1, 1),
(164, 'police', '531-3502', 'Alert (531-3502): i am being kidnapped help', '2020-03-14 18:57:03', 0, 0),
(165, 'police', '873-7104', 'Alert (531-3502): i am being kidnapped help', '2020-03-14 18:57:03', 0, 0),
(166, 'police', '873-7104', 'GPS: 420.88244628906, -944.7138671875', '2020-03-14 18:57:03', 0, 0),
(167, 'police', '531-3502', 'GPS: 420.88244628906, -944.7138671875', '2020-03-14 18:57:03', 0, 0),
(168, 'police', '873-7104', 'Alert (873-7104): Dispatch* 911 call came from Officer Killians house about a bomb in the toilet', '2020-03-14 18:57:54', 0, 0),
(169, 'police', '873-7104', 'GPS: 428.07467651367, -978.43487548828', '2020-03-14 18:57:54', 0, 0),
(175, 'ambulance', '873-7104', 'GPS: 1969.4251708984, 3697.4235839844', '2020-03-15 00:55:01', 0, 0),
(174, 'ambulance', '873-7104', 'Alert (113-0914): Medical attention required: unconscious citizen!', '2020-03-15 00:55:01', 0, 0),
(172, 'ambulance', '873-7104', 'Alert (531-3502): Medical attention required: unconscious citizen!', '2020-03-14 19:00:57', 1, 0),
(171, 'police', '531-3502', 'Alert (873-7104): Dispatch* 911 call came from Officer Killians house about a bomb in the toilet', '2020-03-14 18:57:54', 0, 0),
(170, 'police', '531-3502', 'GPS: 428.07467651367, -978.43487548828', '2020-03-14 18:57:54', 0, 0),
(173, 'ambulance', '873-7104', 'GPS: 434.24554443359, -975.66943359375', '2020-03-14 19:00:57', 1, 0),
(157, 'police', '873-7104', 'Alert (873-7104): Help', '2020-03-14 18:54:34', 1, 0),
(156, 'police', '531-3502', 'GPS: 428.07467651367, -978.43487548828', '2020-03-14 18:54:34', 1, 0),
(155, 'police', '531-3502', 'Alert (873-7104): Help', '2020-03-14 18:54:34', 1, 0),
(176, 'ambulance', '859-9967', 'Alert (873-7104): Medical attention required: unconscious citizen!', '2020-03-15 01:06:49', 1, 0),
(177, 'ambulance', '859-9967', 'GPS: 1854.1694335938, 3687.6745605469', '2020-03-15 01:06:49', 1, 0),
(178, 'ambulance', '873-7104', 'Alert (873-7104): Medical attention required: unconscious citizen!', '2020-03-15 01:06:49', 0, 0),
(179, 'ambulance', '873-7104', 'GPS: 1854.1694335938, 3687.6745605469', '2020-03-15 01:06:49', 0, 0),
(180, 'ambulance', '873-7104', 'GPS: 864.12097167969, -1706.9594726563', '2020-03-15 01:27:35', 0, 0),
(181, 'ambulance', '859-9967', 'Alert (873-7104): Medical attention required: unconscious citizen!', '2020-03-15 01:27:35', 0, 0),
(182, 'ambulance', '873-7104', 'Alert (873-7104): Medical attention required: unconscious citizen!', '2020-03-15 01:27:35', 0, 0),
(183, 'ambulance', '859-9967', 'GPS: 864.12097167969, -1706.9594726563', '2020-03-15 01:27:35', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `phone_users_contacts`
--

CREATE TABLE `phone_users_contacts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) CHARACTER SET utf8mb4 DEFAULT NULL,
  `number` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `display` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT '-1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phone_users_contacts`
--

INSERT INTO `phone_users_contacts` (`id`, `identifier`, `number`, `display`) VALUES
(7, 'steam:110000111d0b1aa', '113-0914', 'Darryl'),
(9, 'steam:110000111c332ed', '827-5849', 'Melody'),
(11, 'steam:110000105ed368b', '531-3502', 'AC'),
(13, 'steam:110000105ed368b', '440-5316', 'Sen Corporal Miek'),
(14, 'steam:110000111d0b1aa', '104-0089', 'Jessi'),
(15, 'steam:1100001183c7077', '827-5849', 'Mel'),
(16, 'steam:1100001139319c1', '771-1741', 'Shane SASP'),
(17, 'steam:1100001183c7077', '753-9558', 'Jones'),
(18, 'steam:110000111c332ed', '121-3758', 'metalhead'),
(19, 'steam:11000011531f6b5', '2887796', 'Doe aka T Callahan'),
(20, 'steam:1100001081d1893', '873-7104', 'Sticky'),
(21, 'steam:11000010a01bdb9', '531-3502', 'TwinkToes');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `entering` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `exit` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `inside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `outside` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `ipls` varchar(255) COLLATE utf8mb4_bin DEFAULT '[]',
  `gateway` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_single` int(11) DEFAULT NULL,
  `is_room` int(11) DEFAULT NULL,
  `is_gateway` int(11) DEFAULT NULL,
  `room_menu` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `label`, `entering`, `exit`, `inside`, `outside`, `ipls`, `gateway`, `is_single`, `is_room`, `is_gateway`, `room_menu`, `price`) VALUES
(1, 'WhispymoundDrive', '2677 Whispymound Drive', '{\"y\":564.89,\"z\":182.959,\"x\":119.384}', '{\"x\":117.347,\"y\":559.506,\"z\":183.304}', '{\"y\":557.032,\"z\":183.301,\"x\":118.037}', '{\"y\":567.798,\"z\":182.131,\"x\":119.249}', '[]', NULL, 1, 1, 0, '{\"x\":118.748,\"y\":566.573,\"z\":175.697}', 1500000),
(2, 'NorthConkerAvenue2045', '2045 North Conker Avenue', '{\"x\":372.796,\"y\":428.327,\"z\":144.685}', '{\"x\":373.548,\"y\":422.982,\"z\":144.907},', '{\"y\":420.075,\"z\":145.904,\"x\":372.161}', '{\"x\":372.454,\"y\":432.886,\"z\":143.443}', '[]', NULL, 1, 1, 0, '{\"x\":377.349,\"y\":429.422,\"z\":137.3}', 1500000),
(3, 'RichardMajesticApt2', 'Richard Majestic, Apt 2', '{\"y\":-379.165,\"z\":37.961,\"x\":-936.363}', '{\"y\":-365.476,\"z\":113.274,\"x\":-913.097}', '{\"y\":-367.637,\"z\":113.274,\"x\":-918.022}', '{\"y\":-382.023,\"z\":37.961,\"x\":-943.626}', '[]', NULL, 1, 1, 0, '{\"x\":-927.554,\"y\":-377.744,\"z\":112.674}', 1700000),
(4, 'NorthConkerAvenue2044', '2044 North Conker Avenue', '{\"y\":440.8,\"z\":146.702,\"x\":346.964}', '{\"y\":437.456,\"z\":148.394,\"x\":341.683}', '{\"y\":435.626,\"z\":148.394,\"x\":339.595}', '{\"x\":350.535,\"y\":443.329,\"z\":145.764}', '[]', NULL, 1, 1, 0, '{\"x\":337.726,\"y\":436.985,\"z\":140.77}', 1500000),
(5, 'WildOatsDrive', '3655 Wild Oats Drive', '{\"y\":502.696,\"z\":136.421,\"x\":-176.003}', '{\"y\":497.817,\"z\":136.653,\"x\":-174.349}', '{\"y\":495.069,\"z\":136.666,\"x\":-173.331}', '{\"y\":506.412,\"z\":135.0664,\"x\":-177.927}', '[]', NULL, 1, 1, 0, '{\"x\":-174.725,\"y\":493.095,\"z\":129.043}', 1500000),
(6, 'HillcrestAvenue2862', '2862 Hillcrest Avenue', '{\"y\":596.58,\"z\":142.641,\"x\":-686.554}', '{\"y\":591.988,\"z\":144.392,\"x\":-681.728}', '{\"y\":590.608,\"z\":144.392,\"x\":-680.124}', '{\"y\":599.019,\"z\":142.059,\"x\":-689.492}', '[]', NULL, 1, 1, 0, '{\"x\":-680.46,\"y\":588.6,\"z\":136.769}', 1500000),
(7, 'LowEndApartment', 'Appartement de base', '{\"y\":-1078.735,\"z\":28.4031,\"x\":292.528}', '{\"y\":-1007.152,\"z\":-102.002,\"x\":265.845}', '{\"y\":-1002.802,\"z\":-100.008,\"x\":265.307}', '{\"y\":-1078.669,\"z\":28.401,\"x\":296.738}', '[]', NULL, 1, 1, 0, '{\"x\":265.916,\"y\":-999.38,\"z\":-100.008}', 562500),
(8, 'MadWayneThunder', '2113 Mad Wayne Thunder', '{\"y\":454.955,\"z\":96.462,\"x\":-1294.433}', '{\"x\":-1289.917,\"y\":449.541,\"z\":96.902}', '{\"y\":446.322,\"z\":96.899,\"x\":-1289.642}', '{\"y\":455.453,\"z\":96.517,\"x\":-1298.851}', '[]', NULL, 1, 1, 0, '{\"x\":-1287.306,\"y\":455.901,\"z\":89.294}', 1500000),
(9, 'HillcrestAvenue2874', '2874 Hillcrest Avenue', '{\"x\":-853.346,\"y\":696.678,\"z\":147.782}', '{\"y\":690.875,\"z\":151.86,\"x\":-859.961}', '{\"y\":688.361,\"z\":151.857,\"x\":-859.395}', '{\"y\":701.628,\"z\":147.773,\"x\":-855.007}', '[]', NULL, 1, 1, 0, '{\"x\":-858.543,\"y\":697.514,\"z\":144.253}', 1500000),
(10, 'HillcrestAvenue2868', '2868 Hillcrest Avenue', '{\"y\":620.494,\"z\":141.588,\"x\":-752.82}', '{\"y\":618.62,\"z\":143.153,\"x\":-759.317}', '{\"y\":617.629,\"z\":143.153,\"x\":-760.789}', '{\"y\":621.281,\"z\":141.254,\"x\":-750.919}', '[]', NULL, 1, 1, 0, '{\"x\":-762.504,\"y\":618.992,\"z\":135.53}', 1500000),
(11, 'TinselTowersApt12', 'Tinsel Towers, Apt 42', '{\"y\":37.025,\"z\":42.58,\"x\":-618.299}', '{\"y\":58.898,\"z\":97.2,\"x\":-603.301}', '{\"y\":58.941,\"z\":97.2,\"x\":-608.741}', '{\"y\":30.603,\"z\":42.524,\"x\":-620.017}', '[]', NULL, 1, 1, 0, '{\"x\":-622.173,\"y\":54.585,\"z\":96.599}', 1700000),
(12, 'MiltonDrive', 'Milton Drive', '{\"x\":-775.17,\"y\":312.01,\"z\":84.658}', NULL, NULL, '{\"x\":-775.346,\"y\":306.776,\"z\":84.7}', '[]', NULL, 0, 0, 1, NULL, 0),
(13, 'Modern1Apartment', 'Appartement Moderne 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_01_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.661,\"y\":327.672,\"z\":210.396}', 1300000),
(14, 'Modern2Apartment', 'Appartement Moderne 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_01_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.735,\"y\":326.757,\"z\":186.313}', 1300000),
(15, 'Modern3Apartment', 'Appartement Moderne 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_01_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.386,\"y\":330.782,\"z\":195.08}', 1300000),
(16, 'Mody1Apartment', 'Appartement Mode 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_02_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.615,\"y\":327.878,\"z\":210.396}', 1300000),
(17, 'Mody2Apartment', 'Appartement Mode 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_02_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.297,\"y\":327.092,\"z\":186.313}', 1300000),
(18, 'Mody3Apartment', 'Appartement Mode 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_02_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.303,\"y\":330.932,\"z\":195.085}', 1300000),
(19, 'Vibrant1Apartment', 'Appartement Vibrant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_03_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.885,\"y\":327.641,\"z\":210.396}', 1300000),
(20, 'Vibrant2Apartment', 'Appartement Vibrant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_03_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.607,\"y\":327.344,\"z\":186.313}', 1300000),
(21, 'Vibrant3Apartment', 'Appartement Vibrant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_03_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.525,\"y\":330.851,\"z\":195.085}', 1300000),
(22, 'Sharp1Apartment', 'Appartement Persan 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_04_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.527,\"y\":327.89,\"z\":210.396}', 1300000),
(23, 'Sharp2Apartment', 'Appartement Persan 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_04_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.642,\"y\":326.497,\"z\":186.313}', 1300000),
(24, 'Sharp3Apartment', 'Appartement Persan 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_04_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.503,\"y\":331.318,\"z\":195.085}', 1300000),
(25, 'Monochrome1Apartment', 'Appartement Monochrome 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_05_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.289,\"y\":328.086,\"z\":210.396}', 1300000),
(26, 'Monochrome2Apartment', 'Appartement Monochrome 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_05_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.692,\"y\":326.762,\"z\":186.313}', 1300000),
(27, 'Monochrome3Apartment', 'Appartement Monochrome 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_05_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.094,\"y\":330.976,\"z\":195.085}', 1300000),
(28, 'Seductive1Apartment', 'Appartement Séduisant 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_06_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.263,\"y\":328.104,\"z\":210.396}', 1300000),
(29, 'Seductive2Apartment', 'Appartement Séduisant 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_06_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.655,\"y\":326.611,\"z\":186.313}', 1300000),
(30, 'Seductive3Apartment', 'Appartement Séduisant 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_06_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.3,\"y\":331.414,\"z\":195.085}', 1300000),
(31, 'Regal1Apartment', 'Appartement Régal 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_07_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.956,\"y\":328.257,\"z\":210.396}', 1300000),
(32, 'Regal2Apartment', 'Appartement Régal 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_07_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.545,\"y\":326.659,\"z\":186.313}', 1300000),
(33, 'Regal3Apartment', 'Appartement Régal 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_07_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.087,\"y\":331.429,\"z\":195.123}', 1300000),
(34, 'Aqua1Apartment', 'Appartement Aqua 1', NULL, '{\"x\":-784.194,\"y\":323.636,\"z\":210.997}', '{\"x\":-779.751,\"y\":323.385,\"z\":210.997}', NULL, '[\"apa_v_mp_h_08_a\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-766.187,\"y\":328.47,\"z\":210.396}', 1300000),
(35, 'Aqua2Apartment', 'Appartement Aqua 2', NULL, '{\"x\":-786.8663,\"y\":315.764,\"z\":186.913}', '{\"x\":-781.808,\"y\":315.866,\"z\":186.913}', NULL, '[\"apa_v_mp_h_08_c\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-795.658,\"y\":326.563,\"z\":186.313}', 1300000),
(36, 'Aqua3Apartment', 'Appartement Aqua 3', NULL, '{\"x\":-774.012,\"y\":342.042,\"z\":195.686}', '{\"x\":-779.057,\"y\":342.063,\"z\":195.686}', NULL, '[\"apa_v_mp_h_08_b\"]', 'MiltonDrive', 0, 1, 0, '{\"x\":-765.287,\"y\":331.084,\"z\":195.086}', 1300000),
(37, 'IntegrityWay', '4 Integrity Way', '{\"x\":-47.804,\"y\":-585.867,\"z\":36.956}', NULL, NULL, '{\"x\":-54.178,\"y\":-583.762,\"z\":35.798}', '[]', NULL, 0, 0, 1, NULL, 0),
(38, 'IntegrityWay28', '4 Integrity Way - Apt 28', NULL, '{\"x\":-31.409,\"y\":-594.927,\"z\":79.03}', '{\"x\":-26.098,\"y\":-596.909,\"z\":79.03}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-11.923,\"y\":-597.083,\"z\":78.43}', 1700000),
(39, 'IntegrityWay30', '4 Integrity Way - Apt 30', NULL, '{\"x\":-17.702,\"y\":-588.524,\"z\":89.114}', '{\"x\":-16.21,\"y\":-582.569,\"z\":89.114}', NULL, '[]', 'IntegrityWay', 0, 1, 0, '{\"x\":-26.327,\"y\":-588.384,\"z\":89.123}', 1700000),
(40, 'DellPerroHeights', 'Dell Perro Heights', '{\"x\":-1447.06,\"y\":-538.28,\"z\":33.74}', NULL, NULL, '{\"x\":-1440.022,\"y\":-548.696,\"z\":33.74}', '[]', NULL, 0, 0, 1, NULL, 0),
(41, 'DellPerroHeightst4', 'Dell Perro Heights - Apt 28', NULL, '{\"x\":-1452.125,\"y\":-540.591,\"z\":73.044}', '{\"x\":-1455.435,\"y\":-535.79,\"z\":73.044}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1467.058,\"y\":-527.571,\"z\":72.443}', 1700000),
(42, 'DellPerroHeightst7', 'Dell Perro Heights - Apt 30', NULL, '{\"x\":-1451.562,\"y\":-523.535,\"z\":55.928}', '{\"x\":-1456.02,\"y\":-519.209,\"z\":55.929}', NULL, '[]', 'DellPerroHeights', 0, 1, 0, '{\"x\":-1457.026,\"y\":-530.219,\"z\":55.937}', 1700000);

-- --------------------------------------------------------

--
-- Table structure for table `rented_vehicles`
--

CREATE TABLE `rented_vehicles` (
  `vehicle` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(12) COLLATE utf8mb4_bin NOT NULL,
  `player_name` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `base_price` int(11) NOT NULL,
  `rent_price` int(11) NOT NULL,
  `owner` varchar(22) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `id` int(11) NOT NULL,
  `store` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `store`, `item`, `price`) VALUES
(1, 'TwentyFourSeven', 'bread', 30),
(2, 'TwentyFourSeven', 'water', 15),
(3, 'RobsLiquor', 'bread', 30),
(4, 'RobsLiquor', 'water', 15),
(5, 'LTDgasoline', 'bread', 30),
(6, 'LTDgasoline', 'water', 15);

-- --------------------------------------------------------

--
-- Table structure for table `society_moneywash`
--

CREATE TABLE `society_moneywash` (
  `id` int(11) NOT NULL,
  `identifier` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `society` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `trunk_inventory`
--

CREATE TABLE `trunk_inventory` (
  `id` int(11) NOT NULL,
  `plate` varchar(8) NOT NULL,
  `data` text NOT NULL,
  `owned` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `trunk_inventory`
--

INSERT INTO `trunk_inventory` (`id`, `plate`, `data`, `owned`) VALUES
(3, 'OIL 252 ', '{\"weapons\":[{\"ammo\":24,\"label\":\"Sticky bomb\",\"name\":\"WEAPON_STICKYBOMB\"}],\"coffre\":[{\"name\":\"marijuana\",\"count\":95}]}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_accounts`
--

CREATE TABLE `twitter_accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '0',
  `password` varchar(50) COLLATE utf8mb4_bin NOT NULL DEFAULT '0',
  `avatar_url` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_accounts`
--

INSERT INTO `twitter_accounts` (`id`, `username`, `password`, `avatar_url`) VALUES
(38, 'JamesonSASP', 'Hagan1100', 'https://townsquare.media/site/704/files/2019/10/state-trooper.jpg?w=980&q=75'),
(39, '22pcross', 'Prince87', 'https://i.imgur.com/'),
(40, 'Lord Godrick', 'charlesd46san', NULL),
(41, 'sticky', 'stoner420', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_likes`
--

CREATE TABLE `twitter_likes` (
  `id` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `tweetId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `twitter_likes`
--

INSERT INTO `twitter_likes` (`id`, `authorId`, `tweetId`) VALUES
(137, 41, 172);

-- --------------------------------------------------------

--
-- Table structure for table `twitter_tweets`
--

CREATE TABLE `twitter_tweets` (
  `id` int(11) NOT NULL,
  `authorId` int(11) NOT NULL,
  `realUser` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp(),
  `likes` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `twitter_tweets`
--

INSERT INTO `twitter_tweets` (`id`, `authorId`, `realUser`, `message`, `time`, `likes`) VALUES
(170, 38, 'steam:1100001139319c1', 'Ready to Work!', '2020-02-08 02:22:51', 0),
(171, 39, 'steam:110000111c332ed', 'out on patrol', '2020-02-10 01:46:10', 0),
(172, 40, 'steam:1100001081d1893', 'I am contemplating going after someone.', '2020-03-14 18:50:06', 1),
(173, 41, 'steam:11000010a01bdb9', '@Lord Godrick Ill assist', '2020-03-14 18:50:54', 0),
(174, 40, 'steam:1100001081d1893', '@sticky Yessssssss!', '2020-03-14 18:51:14', 0),
(175, 41, 'steam:11000010a01bdb9', '@Lord Godrick Let me know when and where', '2020-03-14 18:51:45', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `identifier` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `license` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `money` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `skin` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `job` varchar(50) COLLATE utf8mb4_bin DEFAULT 'unemployed',
  `job_grade` int(11) DEFAULT 0,
  `loadout` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `bank` int(11) DEFAULT NULL,
  `permission_level` int(11) DEFAULT NULL,
  `group` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `lastname` varchar(50) COLLATE utf8mb4_bin DEFAULT '',
  `dateofbirth` varchar(25) COLLATE utf8mb4_bin DEFAULT '',
  `sex` varchar(10) COLLATE utf8mb4_bin DEFAULT '',
  `height` varchar(5) COLLATE utf8mb4_bin DEFAULT '',
  `status` longtext COLLATE utf8mb4_bin DEFAULT NULL,
  `last_property` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `is_dead` tinyint(1) DEFAULT 0,
  `phone_number` varchar(10) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `status`, `last_property`, `is_dead`, `phone_number`) VALUES
('steam:1100001013142e0', 'license:e30e0bd16a8b01da8b6e91ce04e69827aa98aed0', 0, 'ThombStone', '{\"blush_3\":0,\"beard_3\":0,\"bags_1\":0,\"makeup_2\":0,\"makeup_3\":0,\"mask_2\":0,\"arms_2\":0,\"glasses_2\":0,\"decals_1\":0,\"moles_2\":0,\"sun_2\":0,\"shoes_2\":0,\"tshirt_2\":0,\"hair_2\":0,\"lipstick_2\":0,\"pants_1\":0,\"makeup_4\":0,\"chest_2\":0,\"chain_2\":0,\"lipstick_3\":0,\"pants_2\":0,\"torso_2\":0,\"bracelets_2\":0,\"blemishes_1\":0,\"bproof_1\":0,\"bags_2\":0,\"eyebrows_1\":0,\"bodyb_2\":0,\"beard_4\":0,\"chain_1\":0,\"sun_1\":0,\"hair_color_2\":0,\"blush_2\":0,\"chest_3\":0,\"face\":0,\"blemishes_2\":0,\"chest_1\":0,\"makeup_1\":0,\"shoes_1\":0,\"moles_1\":0,\"beard_2\":0,\"decals_2\":0,\"age_1\":0,\"hair_color_1\":0,\"bproof_2\":0,\"complexion_2\":0,\"torso_1\":0,\"lipstick_4\":0,\"watches_2\":0,\"bodyb_1\":0,\"arms\":0,\"eyebrows_2\":0,\"ears_1\":-1,\"eye_color\":0,\"ears_2\":0,\"blush_1\":0,\"sex\":0,\"age_2\":0,\"bracelets_1\":-1,\"glasses_1\":0,\"hair_1\":0,\"lipstick_1\":0,\"eyebrows_3\":0,\"mask_1\":0,\"complexion_1\":0,\"watches_1\":-1,\"helmet_1\":-1,\"beard_1\":0,\"eyebrows_4\":0,\"skin\":0,\"tshirt_1\":0,\"helmet_2\":0}', 'unemployed', 0, '[]', '{\"x\":-838.1,\"z\":215.0,\"y\":4181.6}', 0, 0, 'user', 'Roy', 'Moshkovich', '1996-07-04', 'm', '180', '[{\"name\":\"hunger\",\"percent\":98.4,\"val\":984000},{\"name\":\"thirst\",\"percent\":98.8,\"val\":988000},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '933-5393'),
('steam:110000105ed368b', 'license:468b43146dc337352c6c210541d2fe1a4794c7e4', 14971, 'Joker', '{\"blush_2\":0,\"torso_2\":0,\"chest_2\":0,\"bproof_1\":34,\"bracelets_1\":-1,\"complexion_2\":0,\"skin\":13,\"pants_2\":1,\"watches_2\":0,\"hair_color_1\":18,\"decals_1\":0,\"chest_3\":0,\"arms_2\":0,\"hair_1\":19,\"beard_4\":0,\"watches_1\":-1,\"hair_2\":4,\"bags_2\":0,\"complexion_1\":0,\"lipstick_1\":0,\"bags_1\":0,\"makeup_2\":0,\"chest_1\":0,\"blush_1\":0,\"bodyb_2\":0,\"makeup_3\":0,\"sex\":0,\"bodyb_1\":0,\"chain_1\":28,\"ears_1\":2,\"shoes_2\":0,\"age_1\":0,\"tshirt_1\":96,\"age_2\":0,\"mask_1\":0,\"decals_2\":0,\"beard_1\":18,\"lipstick_2\":0,\"blemishes_1\":0,\"torso_1\":115,\"lipstick_4\":0,\"eyebrows_1\":3,\"bproof_2\":0,\"blemishes_2\":0,\"mask_2\":0,\"blush_3\":0,\"eyebrows_2\":10,\"bracelets_2\":0,\"moles_1\":0,\"lipstick_3\":0,\"makeup_1\":0,\"eyebrows_4\":0,\"helmet_1\":-1,\"shoes_1\":2,\"glasses_1\":3,\"tshirt_2\":17,\"moles_2\":0,\"ears_2\":0,\"helmet_2\":0,\"beard_2\":10,\"eyebrows_3\":26,\"eye_color\":0,\"face\":12,\"hair_color_2\":0,\"sun_2\":0,\"pants_1\":1,\"chain_2\":9,\"makeup_4\":0,\"glasses_2\":0,\"arms\":6,\"sun_1\":0,\"beard_3\":0}', 'unemployed', 0, '[{\"label\":\"Knife\",\"name\":\"WEAPON_KNIFE\",\"components\":[],\"ammo\":0},{\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[],\"ammo\":0},{\"label\":\"Hammer\",\"name\":\"WEAPON_HAMMER\",\"components\":[],\"ammo\":0},{\"label\":\"Bat\",\"name\":\"WEAPON_BAT\",\"components\":[],\"ammo\":0},{\"label\":\"Golf club\",\"name\":\"WEAPON_GOLFCLUB\",\"components\":[],\"ammo\":0},{\"label\":\"Crow bar\",\"name\":\"WEAPON_CROWBAR\",\"components\":[],\"ammo\":0},{\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\",\"flashlight\"],\"ammo\":1000},{\"label\":\"AP pistol\",\"name\":\"WEAPON_APPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Heavy revolver\",\"name\":\"WEAPON_REVOLVER\",\"components\":[],\"ammo\":1000},{\"label\":\"Sns pistol\",\"name\":\"WEAPON_SNSPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Heavy pistol\",\"name\":\"WEAPON_HEAVYPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Vintage pistol\",\"name\":\"WEAPON_VINTAGEPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Micro SMG\",\"name\":\"WEAPON_MICROSMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"SMG\",\"name\":\"WEAPON_SMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Assault SMG\",\"name\":\"WEAPON_ASSAULTSMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Mini smg\",\"name\":\"WEAPON_MINISMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Machine pistol\",\"name\":\"WEAPON_MACHINEPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat pdw\",\"name\":\"WEAPON_COMBATPDW\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Sawed off shotgun\",\"name\":\"WEAPON_SAWNOFFSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Assault shotgun\",\"name\":\"WEAPON_ASSAULTSHOTGUN\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Bullpup shotgun\",\"name\":\"WEAPON_BULLPUPSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Heavy shotgun\",\"name\":\"WEAPON_HEAVYSHOTGUN\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Assault rifle\",\"name\":\"WEAPON_ASSAULTRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Advanced rifle\",\"name\":\"WEAPON_ADVANCEDRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Special carbine\",\"name\":\"WEAPON_SPECIALCARBINE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Bullpup rifle\",\"name\":\"WEAPON_BULLPUPRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Compact rifle\",\"name\":\"WEAPON_COMPACTRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"MG\",\"name\":\"WEAPON_MG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat MG\",\"name\":\"WEAPON_COMBATMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Gusenberg sweeper\",\"name\":\"WEAPON_GUSENBERG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Sniper rifle\",\"name\":\"WEAPON_SNIPERRIFLE\",\"components\":[\"scope\"],\"ammo\":994},{\"label\":\"Heavy sniper\",\"name\":\"WEAPON_HEAVYSNIPER\",\"components\":[\"scope_advanced\"],\"ammo\":994},{\"label\":\"Marksman rifle\",\"name\":\"WEAPON_MARKSMANRIFLE\",\"components\":[\"clip_default\",\"scope\"],\"ammo\":994},{\"label\":\"Grenade launcher\",\"name\":\"WEAPON_GRENADELAUNCHER\",\"components\":[],\"ammo\":20},{\"label\":\"Rocket launcher\",\"name\":\"WEAPON_RPG\",\"components\":[],\"ammo\":20},{\"label\":\"Minigun\",\"name\":\"WEAPON_MINIGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Grenade\",\"name\":\"WEAPON_GRENADE\",\"components\":[],\"ammo\":24},{\"label\":\"Sticky bomb\",\"name\":\"WEAPON_STICKYBOMB\",\"components\":[],\"ammo\":25},{\"label\":\"Smoke grenade\",\"name\":\"WEAPON_SMOKEGRENADE\",\"components\":[],\"ammo\":25},{\"label\":\"Bz gas\",\"name\":\"WEAPON_BZGAS\",\"components\":[],\"ammo\":25},{\"label\":\"Molotov cocktail\",\"name\":\"WEAPON_MOLOTOV\",\"components\":[],\"ammo\":25},{\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[],\"ammo\":2000},{\"label\":\"Jerrycan\",\"name\":\"WEAPON_PETROLCAN\",\"components\":[],\"ammo\":3279},{\"label\":\"Ball\",\"name\":\"WEAPON_BALL\",\"components\":[],\"ammo\":1},{\"label\":\"Bottle\",\"name\":\"WEAPON_BOTTLE\",\"components\":[],\"ammo\":0},{\"label\":\"Dagger\",\"name\":\"WEAPON_DAGGER\",\"components\":[],\"ammo\":0},{\"label\":\"Firework\",\"name\":\"WEAPON_FIREWORK\",\"components\":[],\"ammo\":20},{\"label\":\"Musket\",\"name\":\"WEAPON_MUSKET\",\"components\":[],\"ammo\":1000},{\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Homing launcher\",\"name\":\"WEAPON_HOMINGLAUNCHER\",\"components\":[],\"ammo\":10},{\"label\":\"Snow ball\",\"name\":\"WEAPON_SNOWBALL\",\"components\":[],\"ammo\":10},{\"label\":\"Flaregun\",\"name\":\"WEAPON_FLAREGUN\",\"components\":[],\"ammo\":15},{\"label\":\"Marksman pistol\",\"name\":\"WEAPON_MARKSMANPISTOL\",\"components\":[],\"ammo\":1000},{\"label\":\"Knuckledusters\",\"name\":\"WEAPON_KNUCKLE\",\"components\":[],\"ammo\":0},{\"label\":\"Hatchet\",\"name\":\"WEAPON_HATCHET\",\"components\":[],\"ammo\":0},{\"label\":\"Railgun\",\"name\":\"WEAPON_RAILGUN\",\"components\":[],\"ammo\":20},{\"label\":\"Machete\",\"name\":\"WEAPON_MACHETE\",\"components\":[],\"ammo\":0},{\"label\":\"Switchblade\",\"name\":\"WEAPON_SWITCHBLADE\",\"components\":[],\"ammo\":0},{\"label\":\"Double barrel shotgun\",\"name\":\"WEAPON_DBSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Auto shotgun\",\"name\":\"WEAPON_AUTOSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Battle axe\",\"name\":\"WEAPON_BATTLEAXE\",\"components\":[],\"ammo\":0},{\"label\":\"Compact launcher\",\"name\":\"WEAPON_COMPACTLAUNCHER\",\"components\":[],\"ammo\":20},{\"label\":\"Pipe bomb\",\"name\":\"WEAPON_PIPEBOMB\",\"components\":[],\"ammo\":10},{\"label\":\"Pool cue\",\"name\":\"WEAPON_POOLCUE\",\"components\":[],\"ammo\":0},{\"label\":\"Pipe wrench\",\"name\":\"WEAPON_WRENCH\",\"components\":[],\"ammo\":0},{\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[],\"ammo\":0},{\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\",\"components\":[],\"ammo\":25},{\"label\":\"Double-Action Revolver\",\"name\":\"WEAPON_DOUBLEACTION\",\"components\":[],\"ammo\":1000}]', '{\"z\":38.8,\"x\":1597.2,\"y\":3596.0}', 270875, 1, 'superadmin', 'Dean', 'Conners', '1985-07-14', 'm', '160', '[{\"name\":\"hunger\",\"val\":709500,\"percent\":70.95},{\"name\":\"thirst\",\"val\":557125,\"percent\":55.7125},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '501-1190'),
('steam:110000106197c1a', 'license:a8f78a1b293e00697b6d5296a0cbbe4cef9b08c0', 362500, 'billsrock35', '{\"ears_2\":0,\"torso_2\":0,\"eyebrows_2\":0,\"hair_color_2\":0,\"skin\":0,\"makeup_3\":0,\"glasses_2\":0,\"hair_color_1\":0,\"sun_1\":0,\"blush_1\":0,\"sex\":0,\"eyebrows_1\":0,\"blush_2\":0,\"beard_3\":0,\"beard_2\":0,\"chest_2\":0,\"bodyb_1\":0,\"mask_1\":0,\"beard_1\":0,\"hair_1\":0,\"complexion_1\":0,\"makeup_4\":0,\"moles_1\":0,\"ears_1\":-1,\"bproof_2\":0,\"pants_1\":0,\"moles_2\":0,\"age_2\":0,\"tshirt_1\":0,\"shoes_1\":0,\"helmet_2\":0,\"eyebrows_4\":0,\"glasses_1\":0,\"arms\":0,\"bracelets_2\":0,\"chain_2\":0,\"mask_2\":0,\"makeup_2\":0,\"bracelets_1\":-1,\"face\":0,\"complexion_2\":0,\"lipstick_4\":0,\"lipstick_2\":0,\"chain_1\":0,\"pants_2\":0,\"decals_1\":0,\"age_1\":0,\"eyebrows_3\":0,\"bags_2\":0,\"bags_1\":0,\"shoes_2\":0,\"lipstick_1\":0,\"bodyb_2\":0,\"tshirt_2\":0,\"helmet_1\":-1,\"sun_2\":0,\"makeup_1\":0,\"watches_1\":-1,\"bproof_1\":0,\"blush_3\":0,\"torso_1\":0,\"lipstick_3\":0,\"blemishes_1\":0,\"decals_2\":0,\"chest_3\":0,\"blemishes_2\":0,\"hair_2\":0,\"beard_4\":0,\"eye_color\":0,\"chest_1\":0,\"arms_2\":0,\"watches_2\":0}', 'ambulance', 2, '[{\"ammo\":1000,\"name\":\"WEAPON_MICROSMG\",\"components\":[\"clip_extended\",\"suppressor\",\"luxary_finish\"],\"label\":\"Micro SMG\"},{\"ammo\":1000,\"name\":\"WEAPON_ASSAULTSMG\",\"components\":[\"clip_default\"],\"label\":\"Assault SMG\"},{\"ammo\":1000,\"name\":\"WEAPON_MINISMG\",\"components\":[\"clip_default\"],\"label\":\"Mini smg\"},{\"ammo\":985,\"name\":\"WEAPON_SAWNOFFSHOTGUN\",\"components\":[\"luxary_finish\"],\"label\":\"Sawed off shotgun\"},{\"ammo\":1000,\"name\":\"WEAPON_COMPACTRIFLE\",\"components\":[\"clip_drum\"],\"label\":\"Compact rifle\"},{\"ammo\":1000,\"name\":\"WEAPON_COMBATMG\",\"components\":[\"clip_default\"],\"label\":\"Combat MG\"}]', '{\"x\":2205.9,\"y\":3736.9,\"z\":37.7}', 452290, 0, 'user', 'Austin', 'Nocera', '1995-06-09', 'm', '200', '[{\"val\":316900,\"percent\":31.69,\"name\":\"hunger\"},{\"val\":362675,\"percent\":36.2675,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '367-8401'),
('steam:110000107941620', 'license:c5d34e7a8849335a40f53f3d2612c6d801a8fdac', 800, 'Jedi', '{\"decals_2\":0,\"eyebrows_4\":0,\"makeup_4\":0,\"hair_1\":0,\"tshirt_2\":0,\"hair_2\":0,\"bracelets_1\":-1,\"torso_1\":0,\"pants_2\":0,\"chest_1\":0,\"decals_1\":0,\"hair_color_2\":0,\"chest_3\":0,\"shoes_2\":0,\"complexion_1\":0,\"blemishes_2\":0,\"arms_2\":0,\"blush_1\":0,\"sun_1\":0,\"sex\":0,\"bproof_1\":0,\"helmet_1\":-1,\"hair_color_1\":0,\"bags_2\":0,\"lipstick_2\":0,\"blush_3\":0,\"moles_1\":0,\"arms\":0,\"moles_2\":0,\"skin\":0,\"sun_2\":0,\"shoes_1\":0,\"blush_2\":0,\"ears_1\":-1,\"blemishes_1\":0,\"mask_2\":0,\"torso_2\":0,\"eyebrows_2\":0,\"face\":0,\"glasses_2\":0,\"glasses_1\":0,\"makeup_3\":0,\"helmet_2\":0,\"chain_2\":0,\"eyebrows_1\":0,\"pants_1\":0,\"beard_4\":0,\"chain_1\":0,\"beard_1\":0,\"mask_1\":0,\"tshirt_1\":0,\"watches_2\":0,\"makeup_2\":0,\"bracelets_2\":0,\"bodyb_1\":0,\"age_1\":0,\"complexion_2\":0,\"bags_1\":0,\"bodyb_2\":0,\"eyebrows_3\":0,\"lipstick_4\":0,\"ears_2\":0,\"chest_2\":0,\"makeup_1\":0,\"watches_1\":-1,\"beard_2\":0,\"lipstick_1\":0,\"bproof_2\":0,\"eye_color\":0,\"lipstick_3\":0,\"beard_3\":0,\"age_2\":0}', 'taxi', 0, '[{\"ammo\":985,\"label\":\"Sns pistol\",\"name\":\"WEAPON_SNSPISTOL\",\"components\":[\"clip_default\"]},{\"ammo\":4375,\"label\":\"Jerrycan\",\"name\":\"WEAPON_PETROLCAN\",\"components\":[]},{\"ammo\":1000,\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[]}]', '{\"y\":3318.9,\"x\":1783.2,\"z\":40.9}', 139510, 0, 'user', 'Jon', 'Smtih', '7/23/1993', 'm', '200', '[{\"name\":\"hunger\",\"percent\":66.11,\"val\":661100},{\"name\":\"thirst\",\"percent\":45.7425,\"val\":457425},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '427-1473'),
('steam:1100001081d1893', 'license:335b97a11f294fa23e5ab4446ddf5f9d9452b275', 483838, '456', '{\"chest_3\":0,\"bodyb_1\":0,\"arms_2\":0,\"sex\":0,\"bproof_2\":0,\"chest_1\":0,\"bags_1\":0,\"ears_1\":-1,\"torso_1\":0,\"ears_2\":0,\"eyebrows_3\":0,\"skin\":0,\"moles_2\":0,\"chain_2\":0,\"helmet_1\":-1,\"sun_1\":0,\"watches_1\":-1,\"chest_2\":0,\"decals_2\":0,\"makeup_3\":0,\"arms\":0,\"eyebrows_4\":0,\"beard_1\":0,\"hair_color_2\":0,\"hair_color_1\":0,\"eye_color\":0,\"bracelets_2\":0,\"makeup_2\":0,\"blemishes_2\":0,\"torso_2\":0,\"watches_2\":0,\"tshirt_2\":0,\"bproof_1\":0,\"shoes_1\":0,\"lipstick_2\":0,\"blush_3\":0,\"beard_3\":0,\"eyebrows_1\":0,\"blush_1\":0,\"sun_2\":0,\"decals_1\":0,\"mask_2\":0,\"complexion_2\":0,\"age_2\":0,\"blush_2\":0,\"tshirt_1\":0,\"bracelets_1\":-1,\"beard_2\":0,\"glasses_1\":0,\"helmet_2\":0,\"beard_4\":0,\"shoes_2\":0,\"blemishes_1\":0,\"eyebrows_2\":0,\"pants_1\":0,\"chain_1\":0,\"mask_1\":0,\"bags_2\":0,\"pants_2\":0,\"bodyb_2\":0,\"lipstick_3\":0,\"lipstick_1\":0,\"glasses_2\":0,\"complexion_1\":0,\"age_1\":0,\"hair_2\":0,\"lipstick_4\":0,\"moles_1\":0,\"hair_1\":2,\"makeup_1\":0,\"makeup_4\":0,\"face\":0}', 'police', 4, '[{\"components\":[\"clip_default\",\"flashlight\"],\"ammo\":995,\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\"}]', '{\"y\":-870.8,\"z\":29.2,\"x\":301.8}', 478900, 1, 'superadmin', 'Godrick', 'Twinkletoes', '1994-09-12', 'm', '200', '[{\"percent\":75.59,\"val\":755900,\"name\":\"hunger\"},{\"percent\":84.1925,\"val\":841925,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 0, '531-3502'),
('steam:11000010a01bdb9', 'license:cb5765d5073314f0d95e9e69ab51cf5cc5e16d7d', 9825, 'stickybombz', '{\"lipstick_1\":0,\"glasses_2\":0,\"age_1\":0,\"bodyb_2\":0,\"beard_4\":0,\"sun_1\":0,\"moles_2\":0,\"torso_1\":0,\"skin\":0,\"lipstick_4\":0,\"sun_2\":0,\"face\":0,\"eyebrows_4\":0,\"eyebrows_3\":0,\"decals_2\":0,\"glasses_1\":0,\"beard_1\":0,\"hair_color_1\":0,\"mask_2\":0,\"beard_3\":0,\"lipstick_3\":0,\"blemishes_1\":0,\"blush_1\":0,\"helmet_1\":-1,\"bracelets_1\":-1,\"moles_1\":0,\"blemishes_2\":0,\"chest_2\":0,\"arms_2\":0,\"shoes_2\":0,\"blush_2\":0,\"watches_2\":0,\"age_2\":0,\"makeup_1\":0,\"bags_1\":0,\"watches_1\":-1,\"makeup_2\":0,\"makeup_4\":0,\"sex\":0,\"torso_2\":0,\"complexion_2\":0,\"blush_3\":0,\"hair_2\":0,\"chest_3\":0,\"bproof_2\":0,\"tshirt_2\":0,\"ears_2\":0,\"eye_color\":0,\"helmet_2\":0,\"chest_1\":0,\"arms\":0,\"decals_1\":0,\"pants_1\":0,\"mask_1\":0,\"eyebrows_1\":0,\"chain_2\":0,\"beard_2\":0,\"pants_2\":0,\"lipstick_2\":0,\"ears_1\":-1,\"bags_2\":0,\"complexion_1\":0,\"eyebrows_2\":0,\"bproof_1\":0,\"bodyb_1\":0,\"makeup_3\":0,\"tshirt_1\":0,\"shoes_1\":0,\"chain_1\":0,\"hair_1\":0,\"hair_color_2\":0,\"bracelets_2\":0}', 'ambulance', 1, '[{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\"}]', '{\"y\":-804.6,\"z\":30.5,\"x\":230.4}', 5540, 1, 'superadmin', 'Tommie', 'Pickles', '1988-12-28', 'm', '170', '[{\"percent\":44.95,\"val\":449500,\"name\":\"hunger\"},{\"percent\":46.2125,\"val\":462125,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 0, '873-7104'),
('steam:11000010b49a743', 'license:e5d2acf163c043923bfb9d0a3117fb456b890766', 199936, 'killian2134', '{\"chest_3\":0,\"bodyb_1\":0,\"face\":0,\"sex\":0,\"bproof_2\":0,\"chest_1\":0,\"bags_1\":0,\"ears_1\":-1,\"torso_1\":0,\"ears_2\":0,\"eyebrows_2\":0,\"skin\":0,\"moles_2\":0,\"chain_2\":0,\"helmet_1\":-1,\"sun_1\":0,\"watches_1\":-1,\"chest_2\":0,\"decals_2\":0,\"makeup_3\":0,\"arms\":0,\"eyebrows_4\":0,\"beard_1\":0,\"hair_color_2\":0,\"hair_color_1\":0,\"eye_color\":0,\"bracelets_2\":0,\"makeup_2\":0,\"blemishes_2\":0,\"torso_2\":0,\"watches_2\":0,\"tshirt_2\":0,\"bproof_1\":0,\"shoes_1\":24,\"lipstick_2\":0,\"blush_3\":0,\"beard_3\":0,\"eyebrows_1\":0,\"blush_1\":0,\"sun_2\":0,\"decals_1\":0,\"mask_2\":0,\"hair_2\":0,\"age_2\":0,\"blush_2\":0,\"tshirt_1\":0,\"bracelets_1\":-1,\"bags_2\":0,\"glasses_1\":0,\"helmet_2\":0,\"bodyb_2\":0,\"shoes_2\":0,\"blemishes_1\":0,\"beard_4\":0,\"pants_1\":0,\"beard_2\":0,\"mask_1\":0,\"glasses_2\":0,\"arms_2\":0,\"pants_2\":0,\"eyebrows_3\":0,\"makeup_4\":0,\"lipstick_3\":0,\"complexion_1\":0,\"age_1\":0,\"makeup_1\":0,\"complexion_2\":0,\"lipstick_4\":0,\"chain_1\":0,\"lipstick_1\":0,\"moles_1\":0,\"hair_1\":4}', 'police', 4, '[{\"ammo\":0,\"label\":\"Knife\",\"components\":[],\"name\":\"WEAPON_KNIFE\"},{\"ammo\":0,\"label\":\"Nightstick\",\"components\":[],\"name\":\"WEAPON_NIGHTSTICK\"},{\"ammo\":1000,\"label\":\"Pistol\",\"components\":[\"clip_default\"],\"name\":\"WEAPON_PISTOL\"},{\"ammo\":1000,\"label\":\"Combat pistol\",\"components\":[\"clip_default\",\"flashlight\"],\"name\":\"WEAPON_COMBATPISTOL\"},{\"ammo\":1000,\"label\":\"SMG\",\"components\":[\"clip_extended\",\"flashlight\",\"scope\"],\"name\":\"WEAPON_SMG\"},{\"ammo\":1000,\"label\":\"Pump shotgun\",\"components\":[\"flashlight\"],\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"ammo\":1000,\"label\":\"Carbine rifle\",\"components\":[\"clip_default\",\"flashlight\",\"scope\",\"grip\"],\"name\":\"WEAPON_CARBINERIFLE\"},{\"ammo\":1000,\"label\":\"Heavy sniper\",\"components\":[\"scope_advanced\"],\"name\":\"WEAPON_HEAVYSNIPER\"},{\"ammo\":25,\"label\":\"Bz gas\",\"components\":[],\"name\":\"WEAPON_BZGAS\"},{\"ammo\":2000,\"label\":\"Fire extinguisher\",\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\"},{\"ammo\":1000,\"label\":\"Taser\",\"components\":[],\"name\":\"WEAPON_STUNGUN\"},{\"ammo\":0,\"label\":\"Flashlight\",\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\"},{\"ammo\":25,\"label\":\"Flare gun\",\"components\":[],\"name\":\"WEAPON_FLARE\"}]', '{\"x\":406.2,\"y\":-1005.0,\"z\":29.3}', 961435, 1, 'superadmin', 'Killian', 'Oconnell', '19960529', 'm', '155', '[{\"name\":\"hunger\",\"val\":539900,\"percent\":53.99},{\"name\":\"thirst\",\"val\":654250,\"percent\":65.425},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '906-8228'),
('steam:11000010b88b452', 'license:45a5d3fb073a103ebd2dfbe1b39779508c21967a', 0, 'Ebaklak', NULL, 'unemployed', 0, '[]', '{\"x\":-2057.8,\"z\":32.8,\"y\":2835.4}', 200, 0, 'user', 'Sashka', 'Gbbrg', '20000101', 'm', '190', '[{\"percent\":93.35,\"val\":933500,\"name\":\"hunger\"},{\"percent\":95.0125,\"val\":950125,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 1, '183-6771'),
('steam:11000010cfb10c1', 'license:1d01db020b3634fa219da748f05a60bf6ce33e88', 994, 'Knightrider164', NULL, 'fueler', 0, '[{\"ammo\":39,\"label\":\"Pistol\",\"components\":[\"clip_default\"],\"name\":\"WEAPON_PISTOL\"},{\"ammo\":4494,\"label\":\"Jerrycan\",\"components\":[],\"name\":\"WEAPON_PETROLCAN\"}]', '{\"x\":1651.4,\"y\":2856.3,\"z\":41.0}', 28000, 0, 'user', 'Tommy', 'Callahan', '1994-06-08', 'm', '165', '[{\"val\":445100,\"name\":\"hunger\",\"percent\":44.51},{\"val\":308825,\"name\":\"thirst\",\"percent\":30.8825},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 1, '288-7796'),
('steam:11000010e2a62e2', 'license:4bd8348aff04ce40e48df509991d5f1d7a857ceb', 0, 'Metalhead434', '{\"blush_1\":0,\"eyebrows_3\":0,\"hair_2\":0,\"bodyb_2\":0,\"bproof_2\":1,\"makeup_2\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"helmet_1\":58,\"pants_2\":7,\"age_2\":0,\"torso_1\":94,\"lipstick_2\":0,\"chain_2\":0,\"torso_2\":0,\"decals_2\":0,\"bracelets_2\":0,\"mask_1\":0,\"skin\":0,\"face\":0,\"age_1\":0,\"sun_1\":0,\"makeup_4\":0,\"blemishes_1\":0,\"bags_1\":0,\"bracelets_1\":-1,\"complexion_2\":0,\"sex\":0,\"hair_color_2\":0,\"decals_1\":0,\"lipstick_3\":0,\"chest_2\":0,\"tshirt_1\":15,\"chest_3\":0,\"mask_2\":0,\"blemishes_2\":0,\"beard_2\":10,\"chain_1\":0,\"beard_4\":0,\"blush_3\":0,\"moles_1\":0,\"glasses_1\":9,\"complexion_1\":0,\"helmet_2\":2,\"lipstick_1\":0,\"ears_2\":0,\"tshirt_2\":0,\"makeup_1\":0,\"shoes_2\":0,\"eye_color\":0,\"bproof_1\":7,\"glasses_2\":0,\"shoes_1\":25,\"eyebrows_2\":0,\"bags_2\":0,\"watches_1\":-1,\"ears_1\":-1,\"lipstick_4\":0,\"eyebrows_1\":0,\"sun_2\":0,\"arms_2\":0,\"moles_2\":0,\"watches_2\":0,\"chest_1\":0,\"arms\":30,\"beard_1\":2,\"hair_1\":19,\"blush_2\":0,\"bodyb_1\":0,\"beard_3\":5,\"pants_1\":9,\"hair_color_1\":5}', 'police', 3, '[{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_KNIFE\",\"label\":\"Knife\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_NIGHTSTICK\",\"label\":\"Nightstick\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_HAMMER\",\"label\":\"Hammer\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_BAT\",\"label\":\"Bat\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_GOLFCLUB\",\"label\":\"Golf club\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_CROWBAR\",\"label\":\"Crow bar\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_PISTOL\",\"label\":\"Pistol\"},{\"components\":[\"clip_default\",\"flashlight\"],\"ammo\":1000,\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_APPISTOL\",\"label\":\"AP pistol\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_PISTOL50\",\"label\":\"Pistol .50\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_REVOLVER\",\"label\":\"Heavy revolver\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_SNSPISTOL\",\"label\":\"Sns pistol\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_HEAVYPISTOL\",\"label\":\"Heavy pistol\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_VINTAGEPISTOL\",\"label\":\"Vintage pistol\"},{\"components\":[\"clip_default\"],\"ammo\":974,\"name\":\"WEAPON_MICROSMG\",\"label\":\"Micro SMG\"},{\"components\":[\"clip_default\",\"flashlight\",\"scope\"],\"ammo\":974,\"name\":\"WEAPON_SMG\",\"label\":\"SMG\"},{\"components\":[\"clip_default\"],\"ammo\":974,\"name\":\"WEAPON_ASSAULTSMG\",\"label\":\"Assault SMG\"},{\"components\":[\"clip_default\"],\"ammo\":974,\"name\":\"WEAPON_MINISMG\",\"label\":\"Mini smg\"},{\"components\":[\"clip_default\"],\"ammo\":974,\"name\":\"WEAPON_MACHINEPISTOL\",\"label\":\"Machine pistol\"},{\"components\":[\"clip_default\"],\"ammo\":974,\"name\":\"WEAPON_COMBATPDW\",\"label\":\"Combat pdw\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_SAWNOFFSHOTGUN\",\"label\":\"Sawed off shotgun\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_ASSAULTSHOTGUN\",\"label\":\"Assault shotgun\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_BULLPUPSHOTGUN\",\"label\":\"Bullpup shotgun\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_HEAVYSHOTGUN\",\"label\":\"Heavy shotgun\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_ASSAULTRIFLE\",\"label\":\"Assault rifle\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_CARBINERIFLE\",\"label\":\"Carbine rifle\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_ADVANCEDRIFLE\",\"label\":\"Advanced rifle\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_SPECIALCARBINE\",\"label\":\"Special carbine\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_BULLPUPRIFLE\",\"label\":\"Bullpup rifle\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_COMPACTRIFLE\",\"label\":\"Compact rifle\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_MG\",\"label\":\"MG\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_COMBATMG\",\"label\":\"Combat MG\"},{\"components\":[\"clip_default\"],\"ammo\":1000,\"name\":\"WEAPON_GUSENBERG\",\"label\":\"Gusenberg sweeper\"},{\"components\":[\"scope\"],\"ammo\":1000,\"name\":\"WEAPON_SNIPERRIFLE\",\"label\":\"Sniper rifle\"},{\"components\":[\"scope_advanced\"],\"ammo\":1000,\"name\":\"WEAPON_HEAVYSNIPER\",\"label\":\"Heavy sniper\"},{\"components\":[\"clip_default\",\"scope\"],\"ammo\":1000,\"name\":\"WEAPON_MARKSMANRIFLE\",\"label\":\"Marksman rifle\"},{\"components\":[],\"ammo\":20,\"name\":\"WEAPON_GRENADELAUNCHER\",\"label\":\"Grenade launcher\"},{\"components\":[],\"ammo\":20,\"name\":\"WEAPON_RPG\",\"label\":\"Rocket launcher\"},{\"components\":[],\"ammo\":31,\"name\":\"WEAPON_MINIGUN\",\"label\":\"Minigun\"},{\"components\":[],\"ammo\":25,\"name\":\"WEAPON_GRENADE\",\"label\":\"Grenade\"},{\"components\":[],\"ammo\":25,\"name\":\"WEAPON_STICKYBOMB\",\"label\":\"Sticky bomb\"},{\"components\":[],\"ammo\":25,\"name\":\"WEAPON_SMOKEGRENADE\",\"label\":\"Smoke grenade\"},{\"components\":[],\"ammo\":25,\"name\":\"WEAPON_BZGAS\",\"label\":\"Bz gas\"},{\"components\":[],\"ammo\":25,\"name\":\"WEAPON_MOLOTOV\",\"label\":\"Molotov cocktail\"},{\"components\":[],\"ammo\":2000,\"name\":\"WEAPON_FIREEXTINGUISHER\",\"label\":\"Fire extinguisher\"},{\"components\":[],\"ammo\":3163,\"name\":\"WEAPON_PETROLCAN\",\"label\":\"Jerrycan\"},{\"components\":[],\"ammo\":1,\"name\":\"WEAPON_BALL\",\"label\":\"Ball\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_BOTTLE\",\"label\":\"Bottle\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_DAGGER\",\"label\":\"Dagger\"},{\"components\":[],\"ammo\":20,\"name\":\"WEAPON_FIREWORK\",\"label\":\"Firework\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_MUSKET\",\"label\":\"Musket\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\"},{\"components\":[],\"ammo\":10,\"name\":\"WEAPON_HOMINGLAUNCHER\",\"label\":\"Homing launcher\"},{\"components\":[],\"ammo\":5,\"name\":\"WEAPON_PROXMINE\",\"label\":\"Proximity mine\"},{\"components\":[],\"ammo\":20,\"name\":\"WEAPON_FLAREGUN\",\"label\":\"Flaregun\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_MARKSMANPISTOL\",\"label\":\"Marksman pistol\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_KNUCKLE\",\"label\":\"Knuckledusters\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_HATCHET\",\"label\":\"Hatchet\"},{\"components\":[],\"ammo\":18,\"name\":\"WEAPON_RAILGUN\",\"label\":\"Railgun\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_MACHETE\",\"label\":\"Machete\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_SWITCHBLADE\",\"label\":\"Switchblade\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_DBSHOTGUN\",\"label\":\"Double barrel shotgun\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_AUTOSHOTGUN\",\"label\":\"Auto shotgun\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_BATTLEAXE\",\"label\":\"Battle axe\"},{\"components\":[],\"ammo\":20,\"name\":\"WEAPON_COMPACTLAUNCHER\",\"label\":\"Compact launcher\"},{\"components\":[],\"ammo\":10,\"name\":\"WEAPON_PIPEBOMB\",\"label\":\"Pipe bomb\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_POOLCUE\",\"label\":\"Pool cue\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_WRENCH\",\"label\":\"Pipe wrench\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_FLASHLIGHT\",\"label\":\"Flashlight\"},{\"components\":[],\"ammo\":23,\"name\":\"WEAPON_FLARE\",\"label\":\"Flare gun\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_DOUBLEACTION\",\"label\":\"Double-Action Revolver\"}]', '{\"y\":-1579.8,\"z\":-1.1,\"x\":-921.0}', 372949, 1, 'superadmin', 'Anthony', 'Smith', '1990-05-24', 'm', '184', '[{\"percent\":24.48,\"val\":244800,\"name\":\"hunger\"},{\"percent\":30.86,\"val\":308600,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 0, '121-3758'),
('steam:11000010fe595d0', 'license:af8d774357cd4d1bca63d28ab84c0f418d353fb7', 0, 'DylanFernandez', '{\"blush_1\":0,\"eyebrows_3\":0,\"hair_2\":0,\"bodyb_2\":0,\"bproof_2\":0,\"makeup_2\":0,\"makeup_3\":0,\"eyebrows_4\":0,\"helmet_1\":-1,\"pants_2\":0,\"age_2\":0,\"torso_1\":0,\"lipstick_2\":0,\"chain_2\":0,\"torso_2\":0,\"decals_2\":0,\"bracelets_2\":0,\"eye_color\":0,\"skin\":0,\"face\":0,\"age_1\":0,\"sun_1\":0,\"makeup_4\":0,\"blemishes_1\":0,\"bags_1\":0,\"bracelets_1\":-1,\"complexion_2\":0,\"sex\":0,\"watches_1\":-1,\"decals_1\":0,\"lipstick_3\":0,\"chest_2\":0,\"tshirt_1\":0,\"chest_3\":0,\"mask_2\":0,\"hair_1\":4,\"beard_2\":0,\"chain_1\":0,\"beard_4\":0,\"blush_3\":0,\"beard_1\":0,\"glasses_1\":0,\"complexion_1\":0,\"pants_1\":0,\"lipstick_1\":0,\"glasses_2\":0,\"tshirt_2\":0,\"makeup_1\":0,\"shoes_2\":0,\"beard_3\":0,\"hair_color_2\":0,\"arms\":0,\"bproof_1\":0,\"blemishes_2\":0,\"bags_2\":0,\"ears_2\":0,\"ears_1\":-1,\"lipstick_4\":0,\"eyebrows_2\":0,\"sun_2\":0,\"arms_2\":0,\"moles_2\":0,\"watches_2\":0,\"chest_1\":0,\"shoes_1\":0,\"mask_1\":0,\"helmet_2\":0,\"moles_1\":0,\"bodyb_1\":0,\"eyebrows_1\":0,\"blush_2\":0,\"hair_color_1\":0}', 'police', 2, '[]', '{\"x\":454.4,\"y\":-1018.2,\"z\":28.4}', 86300, 0, 'user', 'Dylan', 'Fernandez', '1998', 'm', '180', '[{\"name\":\"hunger\",\"percent\":34.94,\"val\":349400},{\"name\":\"thirst\",\"percent\":33.705,\"val\":337050},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '249-9884'),
('steam:110000111c332ed', 'license:c7b5c1f2e80fc912bddb70417074f3b389c003b6', 6742908, 'DarrylTV', '{\"arms_2\":0,\"complexion_2\":0,\"chain_2\":0,\"bodyb_2\":0,\"bproof_1\":0,\"hair_2\":0,\"blush_1\":0,\"ears_2\":0,\"mask_1\":0,\"pants_1\":0,\"blush_3\":0,\"glasses_2\":0,\"helmet_1\":-1,\"bags_2\":0,\"blush_2\":0,\"chain_1\":0,\"eye_color\":0,\"chest_3\":0,\"shoes_1\":0,\"lipstick_4\":0,\"bodyb_1\":0,\"complexion_1\":0,\"torso_2\":0,\"chest_1\":0,\"makeup_4\":0,\"eyebrows_4\":0,\"tshirt_2\":0,\"decals_2\":0,\"sun_2\":0,\"eyebrows_2\":0,\"beard_2\":0,\"torso_1\":0,\"lipstick_1\":0,\"bags_1\":0,\"makeup_3\":0,\"moles_2\":0,\"hair_color_1\":0,\"bproof_2\":0,\"beard_1\":0,\"eyebrows_1\":0,\"watches_1\":-1,\"ears_1\":-1,\"pants_2\":0,\"chest_2\":0,\"hair_color_2\":0,\"sex\":0,\"beard_4\":0,\"makeup_1\":0,\"beard_3\":0,\"decals_1\":0,\"makeup_2\":0,\"helmet_2\":0,\"skin\":0,\"lipstick_3\":0,\"mask_2\":0,\"arms\":0,\"eyebrows_3\":0,\"blemishes_1\":0,\"age_2\":0,\"tshirt_1\":0,\"hair_1\":0,\"shoes_2\":0,\"lipstick_2\":0,\"age_1\":0,\"bracelets_2\":0,\"blemishes_2\":0,\"watches_2\":0,\"face\":0,\"moles_1\":0,\"bracelets_1\":-1,\"sun_1\":0,\"glasses_1\":0}', 'police', 3, '[{\"components\":[\"clip_extended\",\"flashlight\"],\"ammo\":-1,\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\"},{\"components\":[],\"ammo\":-1,\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\"},{\"components\":[],\"ammo\":-1,\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\"}]', '{\"y\":3723.9,\"z\":32.3,\"x\":1975.9}', 44670420, 1, 'superadmin', 'Sara', 'Jones', '1987/08/29', 'f', '140', '[{\"percent\":49.71,\"val\":497100,\"name\":\"hunger\"},{\"percent\":49.7825,\"val\":497825,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 0, '113-0914'),
('steam:110000111d0b1aa', 'license:4c35a379d3f41843a3774eaf606ad2c998c89fd3', 4000000, 'babyfacemel', '{\"beard_4\":0,\"chest_3\":0,\"beard_2\":0,\"shoes_2\":1,\"chain_1\":0,\"blemishes_1\":0,\"watches_2\":0,\"hair_color_2\":19,\"shoes_1\":44,\"tshirt_2\":0,\"moles_2\":3,\"pants_1\":109,\"bodyb_2\":0,\"eye_color\":3,\"lipstick_2\":6,\"makeup_1\":4,\"bproof_2\":0,\"hair_1\":5,\"bodyb_1\":0,\"moles_1\":2,\"ears_2\":0,\"helmet_2\":0,\"mask_2\":0,\"bags_2\":0,\"age_2\":0,\"helmet_1\":-1,\"bags_1\":0,\"bproof_1\":39,\"complexion_1\":0,\"sex\":1,\"watches_1\":-1,\"bracelets_2\":0,\"lipstick_4\":6,\"bracelets_1\":-1,\"eyebrows_1\":12,\"decals_1\":0,\"arms\":68,\"chest_2\":0,\"sun_1\":0,\"pants_2\":0,\"hair_color_1\":35,\"complexion_2\":0,\"age_1\":0,\"torso_1\":118,\"beard_1\":0,\"eyebrows_2\":4,\"hair_2\":2,\"mask_1\":0,\"eyebrows_3\":61,\"decals_2\":0,\"blush_3\":10,\"torso_2\":1,\"eyebrows_4\":0,\"lipstick_1\":3,\"arms_2\":0,\"tshirt_1\":14,\"ears_1\":-1,\"blush_1\":1,\"beard_3\":0,\"chest_1\":0,\"chain_2\":0,\"makeup_2\":6,\"sun_2\":0,\"blemishes_2\":3,\"lipstick_3\":53,\"makeup_3\":19,\"glasses_1\":19,\"glasses_2\":0,\"skin\":28,\"face\":21,\"makeup_4\":13,\"blush_2\":5}', 'taxi', 4, '[{\"name\":\"WEAPON_APPISTOL\",\"components\":[\"clip_extended\",\"suppressor\"],\"label\":\"AP pistol\",\"ammo\":995},{\"name\":\"WEAPON_GRENADE\",\"components\":[],\"label\":\"Grenade\",\"ammo\":25},{\"name\":\"WEAPON_MOLOTOV\",\"components\":[],\"label\":\"Molotov cocktail\",\"ammo\":25},{\"name\":\"WEAPON_PETROLCAN\",\"components\":[],\"label\":\"Jerrycan\",\"ammo\":4292},{\"name\":\"WEAPON_COMPACTLAUNCHER\",\"components\":[],\"label\":\"Compact launcher\",\"ammo\":16},{\"name\":\"WEAPON_FLARE\",\"components\":[],\"label\":\"Flare gun\",\"ammo\":25}]', '{\"x\":94.1,\"y\":6395.5,\"z\":31.2}', 77880, 1, 'superadmin', 'Melaficent', 'Spikes', '1994-109-17', 'f', '170', '[{\"name\":\"hunger\",\"percent\":86.21,\"val\":862100},{\"name\":\"thirst\",\"percent\":89.6875,\"val\":896875},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '827-5849'),
('steam:1100001139319c1', 'license:fe4031d6bbcd8ce2770862c34c19038ca32d88f0', 376, 'Mito Jr', '{\"makeup_2\":0,\"arms_2\":0,\"eye_color\":0,\"lipstick_1\":0,\"makeup_3\":0,\"age_1\":0,\"face\":4,\"bracelets_2\":0,\"blush_2\":0,\"makeup_4\":0,\"helmet_2\":0,\"arms\":0,\"beard_3\":0,\"pants_1\":0,\"eyebrows_1\":0,\"blemishes_1\":0,\"shoes_2\":3,\"decals_1\":0,\"hair_2\":5,\"hair_color_1\":5,\"bproof_2\":0,\"moles_1\":0,\"bags_1\":0,\"tshirt_2\":0,\"sun_1\":0,\"age_2\":0,\"sex\":0,\"chest_3\":0,\"makeup_1\":0,\"moles_2\":0,\"ears_1\":-1,\"complexion_2\":0,\"chain_2\":0,\"glasses_2\":0,\"hair_1\":2,\"lipstick_2\":0,\"helmet_1\":-1,\"bracelets_1\":-1,\"mask_2\":0,\"bproof_1\":0,\"watches_2\":0,\"bodyb_2\":0,\"hair_color_2\":0,\"chain_1\":0,\"eyebrows_2\":0,\"beard_2\":0,\"mask_1\":0,\"bodyb_1\":0,\"watches_1\":-1,\"complexion_1\":0,\"glasses_1\":0,\"blush_1\":0,\"beard_4\":0,\"bags_2\":0,\"shoes_1\":3,\"skin\":4,\"decals_2\":0,\"ears_2\":0,\"eyebrows_4\":0,\"eyebrows_3\":0,\"lipstick_4\":0,\"chest_2\":0,\"sun_2\":0,\"beard_1\":0,\"lipstick_3\":0,\"blemishes_2\":0,\"torso_1\":0,\"pants_2\":0,\"blush_3\":0,\"torso_2\":0,\"tshirt_1\":0,\"chest_1\":0}', 'police', 2, '[{\"label\":\"Combat pistol\",\"components\":[\"clip_default\",\"flashlight\"],\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":964},{\"label\":\"Pump shotgun\",\"components\":[],\"name\":\"WEAPON_PUMPSHOTGUN\",\"ammo\":1000},{\"label\":\"Carbine rifle\",\"components\":[\"clip_default\",\"flashlight\",\"grip\"],\"name\":\"WEAPON_CARBINERIFLE\",\"ammo\":980},{\"label\":\"Taser\",\"components\":[],\"name\":\"WEAPON_STUNGUN\",\"ammo\":1000}]', '{\"z\":34.2,\"x\":1567.7,\"y\":3794.1}', 290076, 0, 'user', 'Michael', 'Jameson', '1994-04-16', 'm', '187', '[{\"name\":\"hunger\",\"val\":671100,\"percent\":67.11},{\"name\":\"thirst\",\"val\":901600,\"percent\":90.16},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '440-5316'),
('steam:11000011531f6b5', 'license:09c4006f557a0c94e7d4f08a5dc23c1914c6737e', 0, 'Don Tony', '{\"chest_3\":0,\"makeup_4\":0,\"blush_3\":0,\"torso_1\":0,\"blemishes_1\":0,\"moles_2\":0,\"complexion_2\":0,\"watches_2\":0,\"bproof_2\":0,\"eyebrows_3\":0,\"beard_1\":0,\"makeup_1\":0,\"tshirt_2\":0,\"lipstick_1\":0,\"glasses_2\":0,\"arms\":5,\"bodyb_1\":0,\"blush_2\":0,\"eye_color\":0,\"face\":0,\"torso_2\":0,\"hair_2\":0,\"pants_2\":0,\"moles_1\":0,\"blemishes_2\":0,\"chain_1\":0,\"lipstick_2\":0,\"pants_1\":0,\"bracelets_2\":0,\"decals_1\":0,\"chain_2\":0,\"eyebrows_4\":0,\"eyebrows_2\":0,\"beard_2\":0,\"sun_2\":0,\"mask_1\":0,\"bodyb_2\":0,\"age_1\":0,\"bproof_1\":0,\"lipstick_3\":0,\"ears_2\":0,\"helmet_1\":-1,\"hair_color_1\":0,\"mask_2\":0,\"beard_4\":0,\"chest_2\":0,\"makeup_3\":0,\"decals_2\":0,\"bracelets_1\":-1,\"lipstick_4\":0,\"age_2\":0,\"hair_color_2\":0,\"arms_2\":0,\"sun_1\":0,\"ears_1\":-1,\"blush_1\":0,\"makeup_2\":0,\"skin\":45,\"complexion_1\":0,\"bags_1\":9,\"beard_3\":0,\"tshirt_1\":5,\"watches_1\":1,\"helmet_2\":0,\"shoes_1\":0,\"eyebrows_1\":10,\"chest_1\":0,\"sex\":0,\"bags_2\":0,\"hair_1\":3,\"glasses_1\":0,\"shoes_2\":6}', 'mechanic', 4, '[{\"ammo\":9,\"label\":\"Pump shotgun\",\"components\":[],\"name\":\"WEAPON_PUMPSHOTGUN\"},{\"ammo\":0,\"label\":\"Parachute\",\"components\":[],\"name\":\"GADGET_PARACHUTE\"}]', '{\"x\":-2594.0,\"y\":2609.7,\"z\":-0.5}', 9340, 0, 'user', 'Tony', 'Montana', '19902712', 'm', '150', '[{\"val\":133800,\"name\":\"hunger\",\"percent\":13.38},{\"val\":225350,\"name\":\"thirst\",\"percent\":22.535},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 1, '311-8622'),
('steam:1100001156cfac9', 'license:5d9ba217761fbe2cb970db3c8c32ac212826e369', 700, 'Joe', '{\"decals_1\":0,\"lipstick_2\":0,\"torso_2\":0,\"bracelets_2\":0,\"blush_1\":0,\"beard_4\":0,\"chain_2\":0,\"lipstick_4\":0,\"chest_2\":0,\"makeup_1\":0,\"complexion_1\":0,\"bags_1\":0,\"shoes_1\":0,\"sun_1\":0,\"lipstick_1\":0,\"torso_1\":0,\"eyebrows_1\":0,\"tshirt_2\":0,\"blush_2\":0,\"hair_color_1\":0,\"bproof_2\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"face\":0,\"makeup_2\":0,\"arms\":0,\"ears_1\":-1,\"mask_1\":0,\"bags_2\":0,\"decals_2\":0,\"pants_1\":0,\"watches_1\":-1,\"glasses_1\":0,\"age_1\":0,\"ears_2\":0,\"beard_3\":0,\"chest_1\":0,\"chest_3\":0,\"skin\":0,\"eyebrows_4\":0,\"hair_2\":0,\"moles_2\":0,\"beard_2\":0,\"watches_2\":0,\"bodyb_2\":0,\"helmet_2\":0,\"eyebrows_2\":0,\"tshirt_1\":0,\"mask_2\":0,\"complexion_2\":0,\"makeup_4\":0,\"bodyb_1\":0,\"arms_2\":0,\"chain_1\":0,\"blemishes_2\":0,\"shoes_2\":0,\"eye_color\":0,\"bproof_1\":0,\"pants_2\":0,\"eyebrows_3\":0,\"age_2\":0,\"sun_2\":0,\"hair_1\":4,\"glasses_2\":0,\"sex\":0,\"makeup_3\":0,\"beard_1\":0,\"hair_color_2\":0,\"moles_1\":0,\"blemishes_1\":0,\"blush_3\":0,\"bracelets_1\":-1}', 'ambulance', 1, '[]', '{\"y\":20.4,\"z\":72.8,\"x\":170.1}', 4019, 0, 'user', 'Joey ', 'Mcboby', '08/12/99', 'm', '200', '[{\"percent\":67.41,\"val\":674100,\"name\":\"hunger\"},{\"percent\":33.0575,\"val\":330575,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 0, '859-9967'),
('steam:110000116e48a9b', 'license:afeca7162eb4179bd704399b08706ed34e67f640', 12500, 'FireLieutenant184', '{\"age_2\":0,\"lipstick_2\":0,\"torso_2\":0,\"bracelets_2\":0,\"blush_1\":0,\"beard_4\":0,\"chain_2\":0,\"lipstick_4\":0,\"chest_2\":0,\"makeup_1\":0,\"complexion_1\":0,\"eye_color\":0,\"chain_1\":0,\"sun_1\":0,\"lipstick_1\":0,\"torso_1\":4,\"blush_3\":0,\"tshirt_2\":0,\"blush_2\":0,\"hair_color_1\":0,\"bproof_2\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"face\":10,\"makeup_2\":0,\"arms\":0,\"ears_1\":-1,\"mask_1\":0,\"bags_2\":0,\"decals_2\":0,\"pants_1\":1,\"watches_1\":-1,\"glasses_1\":2,\"age_1\":0,\"sex\":0,\"beard_3\":0,\"chest_1\":0,\"chest_3\":0,\"skin\":0,\"eyebrows_4\":0,\"hair_2\":0,\"moles_2\":0,\"beard_2\":0,\"watches_2\":0,\"bodyb_2\":0,\"helmet_2\":0,\"eyebrows_2\":0,\"tshirt_1\":0,\"mask_2\":0,\"complexion_2\":0,\"makeup_4\":0,\"bodyb_1\":0,\"arms_2\":0,\"decals_1\":0,\"blemishes_2\":0,\"bracelets_1\":-1,\"eyebrows_1\":0,\"eyebrows_3\":0,\"pants_2\":0,\"sun_2\":0,\"ears_2\":0,\"shoes_2\":0,\"hair_1\":2,\"glasses_2\":0,\"shoes_1\":1,\"makeup_3\":0,\"beard_1\":0,\"bproof_1\":0,\"moles_1\":0,\"blemishes_1\":0,\"bags_1\":0,\"hair_color_2\":0}', 'police', 3, '[{\"components\":[],\"name\":\"WEAPON_NIGHTSTICK\",\"ammo\":0,\"label\":\"Nightstick\"},{\"components\":[\"clip_default\"],\"name\":\"WEAPON_PISTOL\",\"ammo\":1000,\"label\":\"Pistol\"},{\"components\":[\"clip_extended\",\"flashlight\"],\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":1000,\"label\":\"Combat pistol\"},{\"components\":[\"flashlight\"],\"name\":\"WEAPON_PUMPSHOTGUN\",\"ammo\":931,\"label\":\"Pump shotgun\"},{\"components\":[\"clip_default\",\"flashlight\",\"scope\"],\"name\":\"WEAPON_CARBINERIFLE\",\"ammo\":1000,\"label\":\"Carbine rifle\"},{\"components\":[\"clip_extended\",\"flashlight\",\"scope\",\"grip\"],\"name\":\"WEAPON_SPECIALCARBINE\",\"ammo\":1000,\"label\":\"Special carbine\"},{\"components\":[],\"name\":\"WEAPON_SMOKEGRENADE\",\"ammo\":17,\"label\":\"Smoke grenade\"},{\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\",\"ammo\":577,\"label\":\"Fire extinguisher\"},{\"components\":[],\"name\":\"WEAPON_STUNGUN\",\"ammo\":1000,\"label\":\"Taser\"},{\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\",\"ammo\":0,\"label\":\"Flashlight\"},{\"components\":[],\"name\":\"WEAPON_FLARE\",\"ammo\":25,\"label\":\"Flare gun\"}]', '{\"x\":2044.2,\"y\":2599.3,\"z\":53.8}', 123750, 0, 'user', 'Nate', 'Redmond', '19970429', 'm', '160', '[{\"val\":673400,\"percent\":67.34,\"name\":\"hunger\"},{\"val\":730050,\"percent\":73.005,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '453-2662'),
('steam:1100001176cddfb', 'license:afa0ca33eb47c7444aadff124cbe6e62acb38b35', 7000, 'HeliDriverFF', '{\"watches_1\":4,\"sex\":0,\"hair_2\":5,\"makeup_3\":0,\"beard_2\":0,\"helmet_2\":0,\"bodyb_2\":0,\"eyebrows_3\":0,\"skin\":0,\"eye_color\":2,\"eyebrows_2\":0,\"torso_1\":44,\"ears_2\":0,\"bracelets_1\":-1,\"beard_3\":0,\"blush_3\":0,\"makeup_1\":0,\"chain_1\":0,\"tshirt_1\":16,\"arms\":0,\"bproof_1\":0,\"glasses_2\":0,\"hair_color_2\":6,\"face\":0,\"bags_2\":0,\"mask_1\":0,\"lipstick_3\":0,\"beard_4\":0,\"age_1\":0,\"lipstick_4\":0,\"chest_1\":0,\"blush_2\":0,\"eyebrows_1\":0,\"decals_1\":0,\"shoes_2\":0,\"helmet_1\":-1,\"blemishes_1\":0,\"bproof_2\":0,\"chest_2\":0,\"tshirt_2\":0,\"bags_1\":0,\"ears_1\":-1,\"makeup_2\":0,\"moles_2\":0,\"chest_3\":0,\"eyebrows_4\":0,\"sun_2\":0,\"hair_color_1\":18,\"pants_2\":0,\"watches_2\":0,\"hair_1\":11,\"lipstick_2\":0,\"mask_2\":0,\"blush_1\":0,\"moles_1\":0,\"shoes_1\":25,\"pants_1\":47,\"torso_2\":0,\"complexion_1\":0,\"age_2\":0,\"sun_1\":0,\"complexion_2\":0,\"chain_2\":0,\"arms_2\":0,\"glasses_1\":6,\"bracelets_2\":0,\"makeup_4\":0,\"lipstick_1\":0,\"bodyb_1\":0,\"decals_2\":0,\"blemishes_2\":0,\"beard_1\":0}', 'unemployed', 0, '[{\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_box\",\"luxary_finish\"],\"label\":\"Carbine rifle\",\"ammo\":738}]', '{\"x\":-734.8,\"z\":51.2,\"y\":5418.6}', 4000, 0, 'user', 'Jesse', 'Anderson', '1985/04/01', 'm', '195', '[{\"percent\":21.0,\"name\":\"hunger\",\"val\":210000},{\"percent\":40.75,\"name\":\"thirst\",\"val\":407500},{\"percent\":0.0,\"name\":\"drunk\",\"val\":0}]', NULL, 0, '822-5198'),
('steam:1100001183c7077', 'license:80638247cf95b34b74ffefd30b13a6226b77b3f3', 25000, 'KillerMcFatass', '{\"beard_4\":0,\"chest_3\":0,\"beard_2\":10,\"shoes_2\":0,\"chain_1\":0,\"blemishes_1\":0,\"watches_2\":0,\"hair_color_2\":0,\"shoes_1\":24,\"tshirt_2\":0,\"moles_2\":0,\"pants_1\":76,\"bodyb_2\":0,\"eye_color\":3,\"lipstick_2\":0,\"makeup_1\":0,\"bproof_2\":0,\"hair_1\":46,\"bodyb_1\":0,\"chain_2\":0,\"ears_2\":0,\"beard_3\":0,\"mask_2\":0,\"bags_2\":0,\"age_2\":0,\"sun_2\":0,\"bags_1\":0,\"bproof_1\":0,\"complexion_1\":0,\"sex\":0,\"watches_1\":-1,\"chest_1\":0,\"lipstick_4\":0,\"bracelets_1\":-1,\"eyebrows_1\":23,\"decals_1\":0,\"arms\":1,\"chest_2\":0,\"sun_1\":0,\"pants_2\":0,\"hair_color_1\":0,\"complexion_2\":0,\"age_1\":0,\"torso_1\":6,\"beard_1\":2,\"eyebrows_2\":10,\"ears_1\":24,\"arms_2\":0,\"eyebrows_3\":26,\"decals_2\":0,\"helmet_1\":-1,\"mask_1\":0,\"eyebrows_4\":0,\"hair_2\":0,\"tshirt_1\":14,\"blush_3\":0,\"makeup_2\":0,\"blush_1\":0,\"helmet_2\":0,\"torso_2\":0,\"skin\":0,\"glasses_2\":1,\"moles_1\":0,\"blemishes_2\":0,\"lipstick_3\":0,\"makeup_3\":0,\"glasses_1\":15,\"lipstick_1\":0,\"face\":43,\"bracelets_2\":0,\"makeup_4\":0,\"blush_2\":0}', 'unemployed', 0, '[{\"name\":\"WEAPON_BAT\",\"ammo\":0,\"label\":\"Bat\",\"components\":[]},{\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":980,\"label\":\"Combat pistol\",\"components\":[\"clip_default\"]},{\"name\":\"WEAPON_PUMPSHOTGUN\",\"ammo\":7,\"label\":\"Pump shotgun\",\"components\":[]}]', '{\"x\":321.4,\"y\":-1389.0,\"z\":31.9}', 17000, 1, 'superadmin', 'Jesse', 'Spikes', '1992-02-25', 'm', '200', '[{\"name\":\"hunger\",\"percent\":49.28,\"val\":492800},{\"name\":\"thirst\",\"percent\":49.46,\"val\":494600},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '104-0089'),
('steam:110000119e7384d', 'license:792f68387d8c28da436769f43ef9c77a671ff2d7', 0, 'Pro Styx', '{\"watches_1\":-1,\"ears_2\":0,\"blush_2\":0,\"glasses_2\":0,\"helmet_1\":-1,\"beard_4\":0,\"bracelets_1\":-1,\"tshirt_1\":0,\"beard_3\":0,\"bproof_2\":0,\"sun_1\":0,\"decals_1\":0,\"bags_2\":0,\"blush_1\":0,\"eyebrows_1\":0,\"moles_2\":0,\"chain_1\":0,\"lipstick_4\":0,\"makeup_1\":0,\"hair_color_1\":0,\"age_1\":0,\"chain_2\":0,\"lipstick_3\":0,\"moles_1\":0,\"mask_1\":0,\"complexion_2\":0,\"mask_2\":0,\"eye_color\":0,\"ears_1\":-1,\"bproof_1\":0,\"beard_1\":0,\"blemishes_2\":0,\"eyebrows_2\":0,\"age_2\":0,\"sex\":0,\"glasses_1\":0,\"shoes_2\":0,\"shoes_1\":0,\"bracelets_2\":0,\"pants_1\":0,\"blush_3\":0,\"torso_2\":0,\"helmet_2\":0,\"arms_2\":0,\"torso_1\":0,\"skin\":0,\"eyebrows_3\":0,\"bags_1\":0,\"bodyb_2\":0,\"pants_2\":0,\"hair_color_2\":0,\"watches_2\":0,\"hair_2\":0,\"chest_2\":0,\"chest_3\":0,\"lipstick_1\":0,\"beard_2\":0,\"tshirt_2\":0,\"makeup_2\":0,\"makeup_3\":0,\"sun_2\":0,\"decals_2\":0,\"chest_1\":0,\"blemishes_1\":0,\"makeup_4\":0,\"face\":0,\"complexion_1\":0,\"arms\":0,\"bodyb_1\":0,\"eyebrows_4\":0,\"lipstick_2\":0,\"hair_1\":0}', 'unemployed', 0, '[]', '{\"z\":105.5,\"y\":205.4,\"x\":227.1}', 400, 0, 'user', 'Mohamed', 'Salm', '1998-12-19', 'm', '194', '[{\"name\":\"hunger\",\"val\":896600,\"percent\":89.66},{\"name\":\"thirst\",\"val\":922450,\"percent\":92.245},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '646-1662');
INSERT INTO `users` (`identifier`, `license`, `money`, `name`, `skin`, `job`, `job_grade`, `loadout`, `position`, `bank`, `permission_level`, `group`, `firstname`, `lastname`, `dateofbirth`, `sex`, `height`, `status`, `last_property`, `is_dead`, `phone_number`) VALUES
('steam:110000132580eb0', 'license:66ad9907077ce29ebeb7234ef771915368ae5d6e', 683, 'K9Marine', NULL, 'miner', 0, '[{\"label\":\"Knife\",\"name\":\"WEAPON_KNIFE\",\"components\":[],\"ammo\":0},{\"label\":\"Nightstick\",\"name\":\"WEAPON_NIGHTSTICK\",\"components\":[],\"ammo\":0},{\"label\":\"Hammer\",\"name\":\"WEAPON_HAMMER\",\"components\":[],\"ammo\":0},{\"label\":\"Bat\",\"name\":\"WEAPON_BAT\",\"components\":[],\"ammo\":0},{\"label\":\"Golf club\",\"name\":\"WEAPON_GOLFCLUB\",\"components\":[],\"ammo\":0},{\"label\":\"Crow bar\",\"name\":\"WEAPON_CROWBAR\",\"components\":[],\"ammo\":0},{\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"AP pistol\",\"name\":\"WEAPON_APPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Heavy revolver\",\"name\":\"WEAPON_REVOLVER\",\"components\":[],\"ammo\":1000},{\"label\":\"Sns pistol\",\"name\":\"WEAPON_SNSPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Heavy pistol\",\"name\":\"WEAPON_HEAVYPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Vintage pistol\",\"name\":\"WEAPON_VINTAGEPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Micro SMG\",\"name\":\"WEAPON_MICROSMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"SMG\",\"name\":\"WEAPON_SMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Assault SMG\",\"name\":\"WEAPON_ASSAULTSMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Mini smg\",\"name\":\"WEAPON_MINISMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Machine pistol\",\"name\":\"WEAPON_MACHINEPISTOL\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat pdw\",\"name\":\"WEAPON_COMBATPDW\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Pump shotgun\",\"name\":\"WEAPON_PUMPSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Sawed off shotgun\",\"name\":\"WEAPON_SAWNOFFSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Assault shotgun\",\"name\":\"WEAPON_ASSAULTSHOTGUN\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Bullpup shotgun\",\"name\":\"WEAPON_BULLPUPSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Heavy shotgun\",\"name\":\"WEAPON_HEAVYSHOTGUN\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Assault rifle\",\"name\":\"WEAPON_ASSAULTRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Carbine rifle\",\"name\":\"WEAPON_CARBINERIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Advanced rifle\",\"name\":\"WEAPON_ADVANCEDRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Special carbine\",\"name\":\"WEAPON_SPECIALCARBINE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Bullpup rifle\",\"name\":\"WEAPON_BULLPUPRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Compact rifle\",\"name\":\"WEAPON_COMPACTRIFLE\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"MG\",\"name\":\"WEAPON_MG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Combat MG\",\"name\":\"WEAPON_COMBATMG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Gusenberg sweeper\",\"name\":\"WEAPON_GUSENBERG\",\"components\":[\"clip_default\"],\"ammo\":1000},{\"label\":\"Sniper rifle\",\"name\":\"WEAPON_SNIPERRIFLE\",\"components\":[\"scope\"],\"ammo\":1000},{\"label\":\"Heavy sniper\",\"name\":\"WEAPON_HEAVYSNIPER\",\"components\":[\"scope_advanced\"],\"ammo\":1000},{\"label\":\"Marksman rifle\",\"name\":\"WEAPON_MARKSMANRIFLE\",\"components\":[\"clip_default\",\"scope\"],\"ammo\":1000},{\"label\":\"Grenade launcher\",\"name\":\"WEAPON_GRENADELAUNCHER\",\"components\":[],\"ammo\":20},{\"label\":\"Rocket launcher\",\"name\":\"WEAPON_RPG\",\"components\":[],\"ammo\":20},{\"label\":\"Minigun\",\"name\":\"WEAPON_MINIGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Grenade\",\"name\":\"WEAPON_GRENADE\",\"components\":[],\"ammo\":25},{\"label\":\"Sticky bomb\",\"name\":\"WEAPON_STICKYBOMB\",\"components\":[],\"ammo\":25},{\"label\":\"Smoke grenade\",\"name\":\"WEAPON_SMOKEGRENADE\",\"components\":[],\"ammo\":25},{\"label\":\"Bz gas\",\"name\":\"WEAPON_BZGAS\",\"components\":[],\"ammo\":25},{\"label\":\"Molotov cocktail\",\"name\":\"WEAPON_MOLOTOV\",\"components\":[],\"ammo\":25},{\"label\":\"Fire extinguisher\",\"name\":\"WEAPON_FIREEXTINGUISHER\",\"components\":[],\"ammo\":2000},{\"label\":\"Jerrycan\",\"name\":\"WEAPON_PETROLCAN\",\"components\":[],\"ammo\":4500},{\"label\":\"Ball\",\"name\":\"WEAPON_BALL\",\"components\":[],\"ammo\":1},{\"label\":\"Bottle\",\"name\":\"WEAPON_BOTTLE\",\"components\":[],\"ammo\":0},{\"label\":\"Dagger\",\"name\":\"WEAPON_DAGGER\",\"components\":[],\"ammo\":0},{\"label\":\"Firework\",\"name\":\"WEAPON_FIREWORK\",\"components\":[],\"ammo\":20},{\"label\":\"Musket\",\"name\":\"WEAPON_MUSKET\",\"components\":[],\"ammo\":1000},{\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Homing launcher\",\"name\":\"WEAPON_HOMINGLAUNCHER\",\"components\":[],\"ammo\":10},{\"label\":\"Proximity mine\",\"name\":\"WEAPON_PROXMINE\",\"components\":[],\"ammo\":5},{\"label\":\"Snow ball\",\"name\":\"WEAPON_SNOWBALL\",\"components\":[],\"ammo\":10},{\"label\":\"Flaregun\",\"name\":\"WEAPON_FLAREGUN\",\"components\":[],\"ammo\":20},{\"label\":\"Marksman pistol\",\"name\":\"WEAPON_MARKSMANPISTOL\",\"components\":[],\"ammo\":1000},{\"label\":\"Knuckledusters\",\"name\":\"WEAPON_KNUCKLE\",\"components\":[],\"ammo\":0},{\"label\":\"Hatchet\",\"name\":\"WEAPON_HATCHET\",\"components\":[],\"ammo\":0},{\"label\":\"Railgun\",\"name\":\"WEAPON_RAILGUN\",\"components\":[],\"ammo\":20},{\"label\":\"Machete\",\"name\":\"WEAPON_MACHETE\",\"components\":[],\"ammo\":0},{\"label\":\"Switchblade\",\"name\":\"WEAPON_SWITCHBLADE\",\"components\":[],\"ammo\":0},{\"label\":\"Double barrel shotgun\",\"name\":\"WEAPON_DBSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Auto shotgun\",\"name\":\"WEAPON_AUTOSHOTGUN\",\"components\":[],\"ammo\":1000},{\"label\":\"Battle axe\",\"name\":\"WEAPON_BATTLEAXE\",\"components\":[],\"ammo\":0},{\"label\":\"Compact launcher\",\"name\":\"WEAPON_COMPACTLAUNCHER\",\"components\":[],\"ammo\":20},{\"label\":\"Pipe bomb\",\"name\":\"WEAPON_PIPEBOMB\",\"components\":[],\"ammo\":10},{\"label\":\"Pool cue\",\"name\":\"WEAPON_POOLCUE\",\"components\":[],\"ammo\":0},{\"label\":\"Pipe wrench\",\"name\":\"WEAPON_WRENCH\",\"components\":[],\"ammo\":0},{\"label\":\"Flashlight\",\"name\":\"WEAPON_FLASHLIGHT\",\"components\":[],\"ammo\":0},{\"label\":\"Flare gun\",\"name\":\"WEAPON_FLARE\",\"components\":[],\"ammo\":25},{\"label\":\"Double-Action Revolver\",\"name\":\"WEAPON_DOUBLEACTION\",\"components\":[],\"ammo\":1000}]', '{\"z\":29.1,\"y\":-1758.1,\"x\":-55.1}', 1500, 0, 'user', 'Jak', 'Fulton', '1988-10-10', 'm', '174', '[{\"name\":\"hunger\",\"val\":999000,\"percent\":99.9},{\"name\":\"thirst\",\"val\":999400,\"percent\":99.94},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '614-6017'),
('steam:110000133d93ea2', 'license:bb1b09de182bf81a18e4eca7a372134cad269c76', 63, 'Venom8797', '{\"age_2\":0,\"tshirt_1\":0,\"lipstick_2\":0,\"tshirt_2\":0,\"moles_2\":0,\"makeup_1\":0,\"blush_1\":0,\"bracelets_2\":0,\"complexion_1\":0,\"hair_1\":0,\"bags_2\":0,\"sun_2\":0,\"chain_2\":0,\"pants_2\":0,\"bproof_2\":0,\"pants_1\":0,\"arms\":0,\"blemishes_2\":0,\"lipstick_3\":0,\"helmet_1\":-1,\"face\":0,\"bproof_1\":0,\"shoes_1\":0,\"hair_color_1\":0,\"glasses_2\":0,\"decals_2\":0,\"moles_1\":0,\"complexion_2\":0,\"sex\":0,\"sun_1\":0,\"hair_color_2\":0,\"eyebrows_4\":0,\"chest_2\":0,\"lipstick_4\":0,\"chest_1\":0,\"bodyb_2\":0,\"hair_2\":0,\"chest_3\":0,\"ears_2\":0,\"eyebrows_2\":0,\"blush_2\":0,\"arms_2\":0,\"ears_1\":-1,\"eye_color\":0,\"torso_1\":0,\"torso_2\":0,\"eyebrows_1\":0,\"makeup_4\":0,\"glasses_1\":0,\"helmet_2\":0,\"bodyb_1\":0,\"beard_1\":0,\"beard_3\":0,\"chain_1\":0,\"mask_2\":0,\"makeup_2\":0,\"blemishes_1\":0,\"makeup_3\":0,\"decals_1\":0,\"blush_3\":0,\"beard_4\":0,\"age_1\":0,\"eyebrows_3\":0,\"skin\":0,\"watches_1\":-1,\"watches_2\":0,\"bags_1\":0,\"shoes_2\":0,\"mask_1\":0,\"lipstick_1\":0,\"bracelets_1\":-1,\"beard_2\":0}', 'ambulance', 1, '[{\"label\":\"Crow bar\",\"components\":[],\"name\":\"WEAPON_CROWBAR\",\"ammo\":0},{\"label\":\"Fire extinguisher\",\"components\":[],\"name\":\"WEAPON_FIREEXTINGUISHER\",\"ammo\":1564},{\"label\":\"Flashlight\",\"components\":[],\"name\":\"WEAPON_FLASHLIGHT\",\"ammo\":0},{\"label\":\"Flare gun\",\"components\":[],\"name\":\"WEAPON_FLARE\",\"ammo\":25}]', '{\"z\":42.5,\"x\":1701.0,\"y\":4946.8}', 2040, 0, 'user', 'Nicholas ', 'Silver', '3/17/1999', 'm', '200', '[{\"name\":\"hunger\",\"val\":380300,\"percent\":38.03},{\"name\":\"thirst\",\"val\":410225,\"percent\":41.0225},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '228-4620'),
('steam:110000134e3ca1a', 'license:18b98386826174336f17731ee73f01c954c03029', 197, 'ZG', '{\"bracelets_2\":0,\"beard_1\":0,\"bodyb_2\":0,\"blush_2\":0,\"bags_1\":0,\"bracelets_1\":-1,\"skin\":0,\"hair_1\":0,\"sun_2\":0,\"blush_1\":0,\"chain_2\":0,\"helmet_2\":0,\"sun_1\":0,\"torso_1\":0,\"shoes_1\":0,\"bodyb_1\":0,\"age_1\":0,\"glasses_1\":0,\"lipstick_1\":0,\"complexion_1\":0,\"hair_color_1\":0,\"blush_3\":0,\"chest_2\":0,\"bags_2\":0,\"beard_2\":0,\"makeup_2\":0,\"moles_1\":0,\"chain_1\":0,\"blemishes_2\":0,\"eyebrows_4\":0,\"watches_2\":0,\"watches_1\":-1,\"pants_1\":0,\"mask_1\":0,\"ears_2\":0,\"eyebrows_3\":0,\"ears_1\":-1,\"shoes_2\":0,\"lipstick_4\":0,\"eyebrows_2\":0,\"mask_2\":0,\"eye_color\":0,\"arms_2\":0,\"hair_2\":0,\"age_2\":0,\"arms\":0,\"torso_2\":0,\"bproof_2\":0,\"makeup_1\":0,\"chest_1\":0,\"helmet_1\":-1,\"face\":0,\"moles_2\":0,\"makeup_4\":0,\"glasses_2\":0,\"blemishes_1\":0,\"decals_1\":0,\"beard_4\":0,\"eyebrows_1\":0,\"sex\":0,\"tshirt_1\":0,\"chest_3\":0,\"complexion_2\":0,\"lipstick_3\":0,\"tshirt_2\":0,\"bproof_1\":0,\"decals_2\":0,\"makeup_3\":0,\"pants_2\":0,\"hair_color_2\":0,\"beard_3\":0,\"lipstick_2\":0}', 'miner', 0, '[]', '{\"y\":3550.9,\"x\":1280.5,\"z\":35.1}', -177000, 0, 'user', 'Jones', 'Smih', '04-17-1996', 'm', '140', '[{\"name\":\"hunger\",\"percent\":91.78,\"val\":917800},{\"name\":\"thirst\",\"percent\":93.73,\"val\":937300},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '753-9558'),
('steam:11000013517b942', 'license:72243fe74f8979354934205ba21e3e6d2303bb6f', 0, 'Detective Monkeyy', NULL, 'unemployed', 0, '[]', '{\"y\":5174.8,\"z\":18.2,\"x\":3337.8}', 0, 0, 'user', '', '', '', '', '', '[{\"val\":981200,\"percent\":98.12,\"name\":\"hunger\"},{\"val\":985900,\"percent\":98.59,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '183-7756'),
('steam:110000135e316b5', 'license:f5e5df53388c1d09abec13151fbb8be5a75f8ba2', 0, 'FreezyOW', '{\"torso_2\":0,\"bracelets_1\":-1,\"beard_3\":0,\"beard_4\":0,\"complexion_2\":0,\"eyebrows_4\":0,\"shoes_1\":0,\"tshirt_1\":0,\"eye_color\":0,\"tshirt_2\":0,\"face\":0,\"torso_1\":0,\"age_2\":0,\"bags_2\":0,\"skin\":0,\"makeup_1\":0,\"watches_1\":-1,\"bracelets_2\":0,\"bproof_1\":0,\"beard_1\":0,\"chain_2\":0,\"watches_2\":0,\"bodyb_1\":0,\"beard_2\":0,\"glasses_1\":0,\"ears_1\":-1,\"chain_1\":0,\"hair_2\":0,\"makeup_2\":0,\"sun_1\":0,\"sex\":0,\"blush_1\":0,\"bags_1\":0,\"eyebrows_1\":0,\"pants_2\":0,\"blemishes_2\":0,\"eyebrows_2\":0,\"moles_2\":0,\"age_1\":0,\"makeup_3\":0,\"shoes_2\":0,\"hair_1\":0,\"chest_1\":0,\"chest_2\":0,\"sun_2\":0,\"glasses_2\":0,\"lipstick_3\":0,\"mask_2\":0,\"hair_color_1\":0,\"blush_2\":0,\"bproof_2\":0,\"helmet_1\":-1,\"complexion_1\":0,\"hair_color_2\":0,\"moles_1\":0,\"eyebrows_3\":0,\"lipstick_2\":0,\"arms\":0,\"lipstick_4\":0,\"decals_2\":0,\"blemishes_1\":0,\"pants_1\":0,\"ears_2\":0,\"lipstick_1\":0,\"blush_3\":0,\"mask_1\":0,\"arms_2\":0,\"chest_3\":0,\"helmet_2\":0,\"decals_1\":0,\"bodyb_2\":0,\"makeup_4\":0}', 'fisherman', 0, '[]', '{\"x\":897.4,\"y\":-1661.6,\"z\":30.2}', 800, 0, 'user', 'Aaron', 'Freeman', '1993-10-26', 'm', '155', '[{\"val\":346700,\"name\":\"hunger\",\"percent\":34.67},{\"val\":385025,\"name\":\"thirst\",\"percent\":38.5025},{\"val\":0,\"name\":\"drunk\",\"percent\":0.0}]', NULL, 0, '207-0046'),
('steam:110000136177a4e', 'license:cc1fb4f4f7ddadf9ac11a23477ceca023b131e15', 0, 'nickbaseball1', NULL, 'unemployed', 0, '[]', '{\"y\":1753.7,\"z\":25.2,\"x\":2550.4}', 750, 0, 'user', 'Nick', 'Stromlund', '20002505', 'm', '150', '[{\"val\":877600,\"percent\":87.76,\"name\":\"hunger\"},{\"val\":908200,\"percent\":90.82,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '999-6114'),
('steam:110000139d9adf9', 'license:ae561a5668e725731bdcf693c98d7fa63be00a59', 0, 'нℓ | C4', NULL, 'unemployed', 0, '[]', '{\"z\":31.6,\"y\":6301.6,\"x\":-45.2}', 200, 0, 'user', '', '', '', '', '', '[{\"name\":\"hunger\",\"val\":978100,\"percent\":97.81},{\"name\":\"thirst\",\"val\":983575,\"percent\":98.3575},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '696-4507'),
('steam:11000013e33b934', 'license:9631ace593394b20479e2e471799e2099fe5135b', 0, 'Hernandez', NULL, 'mechanic', 0, '[]', '{\"x\":340.8,\"y\":-1394.2,\"z\":32.5}', 65897, 0, 'user', 'ANGEL', 'HERNANDEZ', '1989/07/06', 'm', '170', '[{\"val\":493200,\"percent\":49.32,\"name\":\"hunger\"},{\"val\":494900,\"percent\":49.49,\"name\":\"thirst\"},{\"val\":0,\"percent\":0.0,\"name\":\"drunk\"}]', NULL, 0, '517-1172'),
('steam:11000013e7edea3', 'license:713f44ae43ca9c36f67bed492533b040e379ae7f', 0, 'RICHS05', '{\"makeup_3\":0,\"lipstick_2\":0,\"beard_3\":0,\"blush_2\":0,\"watches_1\":-1,\"chain_2\":0,\"sun_1\":0,\"makeup_1\":0,\"blush_3\":0,\"lipstick_1\":0,\"helmet_1\":-1,\"age_2\":0,\"ears_2\":0,\"tshirt_1\":0,\"arms\":0,\"beard_1\":0,\"beard_4\":0,\"sex\":0,\"glasses_1\":0,\"tshirt_2\":0,\"lipstick_4\":0,\"bags_1\":0,\"glasses_2\":0,\"complexion_2\":0,\"hair_2\":0,\"ears_1\":-1,\"pants_2\":0,\"eyebrows_1\":0,\"eyebrows_3\":0,\"beard_2\":0,\"moles_2\":0,\"makeup_4\":0,\"torso_2\":0,\"chest_2\":0,\"torso_1\":0,\"chain_1\":0,\"decals_2\":0,\"bags_2\":0,\"lipstick_3\":0,\"eye_color\":0,\"bproof_2\":0,\"hair_color_2\":0,\"face\":0,\"arms_2\":0,\"blush_1\":0,\"complexion_1\":0,\"bproof_1\":0,\"chest_1\":0,\"sun_2\":0,\"age_1\":0,\"mask_2\":0,\"bracelets_1\":-1,\"blemishes_2\":0,\"shoes_2\":0,\"helmet_2\":0,\"makeup_2\":0,\"hair_color_1\":0,\"chest_3\":0,\"skin\":0,\"hair_1\":0,\"shoes_1\":0,\"eyebrows_2\":0,\"eyebrows_4\":0,\"blemishes_1\":0,\"moles_1\":0,\"decals_1\":0,\"pants_1\":0,\"bodyb_2\":0,\"mask_1\":0,\"bracelets_2\":0,\"bodyb_1\":0,\"watches_2\":0}', 'police', 4, '[{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_CROWBAR\",\"label\":\"Crow bar\"},{\"components\":[\"clip_default\",\"flashlight\"],\"ammo\":1000,\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\"},{\"components\":[\"clip_extended\",\"flashlight\",\"scope\",\"suppressor\"],\"ammo\":1000,\"name\":\"WEAPON_SMG\",\"label\":\"SMG\"},{\"components\":[\"flashlight\"],\"ammo\":997,\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\"},{\"components\":[\"clip_default\"],\"ammo\":965,\"name\":\"WEAPON_CARBINERIFLE\",\"label\":\"Carbine rifle\"},{\"components\":[\"scope_advanced\"],\"ammo\":1000,\"name\":\"WEAPON_HEAVYSNIPER\",\"label\":\"Heavy sniper\"},{\"components\":[],\"ammo\":25,\"name\":\"WEAPON_SMOKEGRENADE\",\"label\":\"Smoke grenade\"},{\"components\":[],\"ammo\":2000,\"name\":\"WEAPON_FIREEXTINGUISHER\",\"label\":\"Fire extinguisher\"},{\"components\":[],\"ammo\":1000,\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\"},{\"components\":[],\"ammo\":0,\"name\":\"WEAPON_FLASHLIGHT\",\"label\":\"Flashlight\"},{\"components\":[],\"ammo\":25,\"name\":\"WEAPON_FLARE\",\"label\":\"Flare gun\"}]', '{\"y\":-872.7,\"z\":29.2,\"x\":300.6}', 354771, 1, 'superadmin', 'Steve', 'Erwin', '', 'm', '200', '[{\"percent\":36.39,\"val\":363900,\"name\":\"hunger\"},{\"percent\":39.7925,\"val\":397925,\"name\":\"thirst\"},{\"percent\":0.0,\"val\":0,\"name\":\"drunk\"}]', NULL, 0, '771-1741'),
('steam:11000013e9d4b99', 'license:aa05f9d538a874cc7279f02b086b3e3aab1f6016', 0, 'beebo', NULL, 'unemployed', 0, '[{\"label\":\"Pistol\",\"name\":\"WEAPON_PISTOL\",\"ammo\":1000,\"components\":[\"clip_default\"]},{\"label\":\"Combat pistol\",\"name\":\"WEAPON_COMBATPISTOL\",\"ammo\":1000,\"components\":[\"clip_default\"]},{\"label\":\"Pistol .50\",\"name\":\"WEAPON_PISTOL50\",\"ammo\":1000,\"components\":[\"clip_default\"]},{\"label\":\"Taser\",\"name\":\"WEAPON_STUNGUN\",\"ammo\":1000,\"components\":[]}]', '{\"z\":47.1,\"x\":2795.6,\"y\":4483.0}', 1400, 0, 'user', 'Jon', 'Arm', '1995 12 1', 'm', '140', '[{\"name\":\"hunger\",\"percent\":70.43,\"val\":704300},{\"name\":\"thirst\",\"percent\":77.8225,\"val\":778225},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 0, '562-3039'),
('steam:11000013eaca305', 'license:6ce7fb07728002f3205689fee3303e863d40b7e9', 0, 'gameon.center4', NULL, 'unemployed', 0, '[]', '{\"z\":13.1,\"y\":-1082.9,\"x\":-1712.4}', 200, 0, 'user', '', '', '', '', '', '[{\"name\":\"hunger\",\"val\":959200,\"percent\":95.92},{\"name\":\"thirst\",\"val\":969400,\"percent\":96.94},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '416-0570'),
('steam:11000013efa68e1', 'license:6bbd64eed21b1d92dd602318cb2a595f787771c2', 0, 'mt3bx77', NULL, 'unemployed', 0, '[]', '{\"z\":131.4,\"y\":2092.2,\"x\":-597.2}', 200, 0, 'user', '', '', '', '', '', '[{\"name\":\"hunger\",\"val\":969200,\"percent\":96.92},{\"name\":\"thirst\",\"val\":976900,\"percent\":97.69},{\"name\":\"drunk\",\"val\":0,\"percent\":0.0}]', NULL, 0, '646-7832'),
('steam:11000013f6b85e1', 'license:20f178f554669182ff41d9912a7008092d0ab19c', 366, 'antoniocolello', '{\"watches_1\":-1,\"sex\":0,\"hair_2\":0,\"makeup_3\":0,\"beard_2\":0,\"moles_1\":0,\"arms_2\":0,\"eyebrows_3\":0,\"skin\":0,\"eye_color\":0,\"watches_2\":0,\"torso_1\":0,\"ears_2\":0,\"bracelets_1\":-1,\"beard_3\":0,\"blush_3\":0,\"makeup_1\":0,\"chain_1\":0,\"tshirt_1\":0,\"arms\":0,\"bproof_1\":0,\"glasses_2\":0,\"hair_color_2\":0,\"face\":0,\"bags_2\":0,\"sun_2\":0,\"lipstick_3\":0,\"beard_4\":0,\"age_1\":0,\"lipstick_4\":0,\"chest_1\":0,\"blush_2\":0,\"eyebrows_1\":0,\"decals_1\":0,\"shoes_2\":0,\"helmet_1\":-1,\"blemishes_1\":0,\"makeup_4\":0,\"chest_2\":0,\"pants_2\":0,\"bags_1\":0,\"ears_1\":-1,\"blush_1\":0,\"moles_2\":0,\"chest_3\":0,\"eyebrows_4\":0,\"hair_1\":0,\"hair_color_1\":0,\"complexion_2\":0,\"bodyb_1\":0,\"beard_1\":0,\"lipstick_2\":0,\"mask_2\":0,\"mask_1\":0,\"bodyb_2\":0,\"shoes_1\":0,\"helmet_2\":0,\"age_2\":0,\"complexion_1\":0,\"bracelets_2\":0,\"sun_1\":0,\"tshirt_2\":0,\"chain_2\":0,\"torso_2\":0,\"glasses_1\":0,\"decals_2\":0,\"bproof_2\":0,\"lipstick_1\":0,\"pants_1\":0,\"makeup_2\":0,\"blemishes_2\":0,\"eyebrows_2\":0}', 'police', 1, '[{\"name\":\"WEAPON_COMBATPISTOL\",\"label\":\"Combat pistol\",\"ammo\":983,\"components\":[\"clip_default\"]},{\"name\":\"WEAPON_PUMPSHOTGUN\",\"label\":\"Pump shotgun\",\"ammo\":10,\"components\":[]},{\"name\":\"WEAPON_CARBINERIFLE\",\"label\":\"Carbine rifle\",\"ammo\":1000,\"components\":[\"clip_default\"]},{\"name\":\"WEAPON_FIREEXTINGUISHER\",\"label\":\"Fire extinguisher\",\"ammo\":2000,\"components\":[]},{\"name\":\"WEAPON_STUNGUN\",\"label\":\"Taser\",\"ammo\":1000,\"components\":[]},{\"name\":\"WEAPON_FLARE\",\"label\":\"Flare gun\",\"ammo\":25,\"components\":[]}]', '{\"x\":500.0,\"y\":-852.6,\"z\":38.2}', 39000, 0, 'user', 'Gregory', 'Stevens', '1950-09-09', 'm', '200', '[{\"name\":\"hunger\",\"percent\":71.43,\"val\":714300},{\"name\":\"thirst\",\"percent\":73.5725,\"val\":735725},{\"name\":\"drunk\",\"percent\":0.0,\"val\":0}]', NULL, 1, '371-8759');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `id` int(11) NOT NULL,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `money` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`id`, `identifier`, `name`, `money`) VALUES
(2, 'steam:110000111d0b1aa', 'black_money', 0),
(3, 'steam:1100001081d1893', 'black_money', 0),
(4, 'steam:11000010b49a743', 'black_money', 0),
(5, 'steam:11000013e7edea3', 'black_money', 0),
(6, 'steam:110000111c332ed', 'black_money', 0),
(7, 'steam:11000010a01bdb9', 'black_money', 0),
(8, 'steam:11000013e9d4b99', 'black_money', 0),
(9, 'steam:110000107941620', 'black_money', 0),
(10, 'steam:11000010fe595d0', 'black_money', 0),
(11, 'steam:11000010e2a62e2', 'black_money', 0),
(12, 'steam:11000010b88b452', 'black_money', 0),
(13, 'steam:110000105ed368b', 'black_money', 0),
(14, 'steam:1100001183c7077', 'black_money', 0),
(15, 'steam:1100001139319c1', 'black_money', 0),
(16, 'steam:110000132580eb0', 'black_money', 0),
(17, 'steam:110000119e7384d', 'black_money', 0),
(18, 'steam:110000139d9adf9', 'black_money', 0),
(19, 'steam:11000013eaca305', 'black_money', 0),
(20, 'steam:11000013efa68e1', 'black_money', 0),
(21, 'steam:1100001176cddfb', 'black_money', 0),
(22, 'steam:11000013f6b85e1', 'black_money', 0),
(23, 'steam:11000013e33b934', 'black_money', 0),
(24, 'steam:11000013517b942', 'black_money', 0),
(25, 'steam:110000134e3ca1a', 'black_money', 0),
(26, 'steam:1100001013142e0', 'black_money', 0),
(27, 'steam:110000133d93ea2', 'black_money', 10108),
(28, 'steam:11000011531f6b5', 'black_money', 0),
(29, 'steam:11000010cfb10c1', 'black_money', 0),
(30, 'steam:110000135e316b5', 'black_money', 0),
(31, 'steam:1100001156cfac9', 'black_money', 0),
(32, 'steam:110000136177a4e', 'black_money', 0),
(33, 'steam:110000116e48a9b', 'black_money', 0),
(34, 'steam:110000106197c1a', 'black_money', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `id` int(11) NOT NULL,
  `identifier` varchar(22) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_inventory`
--

INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(32, 'steam:110000111d0b1aa', 'cannabis', 100),
(33, 'steam:110000111d0b1aa', 'wood', 0),
(34, 'steam:110000111d0b1aa', 'fabric', 0),
(35, 'steam:110000111d0b1aa', 'essence', 0),
(36, 'steam:110000111d0b1aa', 'iron', 0),
(37, 'steam:110000111d0b1aa', 'gazbottle', 0),
(38, 'steam:110000111d0b1aa', 'wool', 0),
(39, 'steam:110000111d0b1aa', 'carotool', 0),
(40, 'steam:110000111d0b1aa', 'clothe', 0),
(41, 'steam:110000111d0b1aa', 'slaughtered_chicken', 0),
(42, 'steam:110000111d0b1aa', 'packaged_plank', 0),
(43, 'steam:110000111d0b1aa', 'water', 15),
(44, 'steam:110000111d0b1aa', 'fish', 0),
(45, 'steam:110000111d0b1aa', 'stone', 0),
(46, 'steam:110000111d0b1aa', 'blowpipe', 0),
(47, 'steam:110000111d0b1aa', 'gold', 0),
(48, 'steam:110000111d0b1aa', 'petrol', 0),
(49, 'steam:110000111d0b1aa', 'cutted_wood', 0),
(50, 'steam:110000111d0b1aa', 'medikit', 0),
(51, 'steam:110000111d0b1aa', 'packaged_chicken', 0),
(52, 'steam:110000111d0b1aa', 'bandage', 0),
(53, 'steam:110000111d0b1aa', 'petrol_raffin', 0),
(54, 'steam:110000111d0b1aa', 'fixtool', 0),
(55, 'steam:110000111d0b1aa', 'washed_stone', 0),
(56, 'steam:110000111d0b1aa', 'bread', 12),
(57, 'steam:110000111d0b1aa', 'diamond', 100),
(58, 'steam:110000111d0b1aa', 'fixkit', 0),
(59, 'steam:110000111d0b1aa', 'alive_chicken', 0),
(60, 'steam:110000111d0b1aa', 'carokit', 0),
(61, 'steam:110000111d0b1aa', 'copper', 0),
(62, 'steam:110000111d0b1aa', 'marijuana', 0),
(63, 'steam:110000111d0b1aa', 'shotgun_shells', 0),
(64, 'steam:110000111d0b1aa', 'WEAPON_KNIFE', 0),
(65, 'steam:110000111d0b1aa', 'WEAPON_PUMPSHOTGUN', 0),
(66, 'steam:110000111d0b1aa', 'WEAPON_PISTOL', 0),
(67, 'steam:110000111d0b1aa', '9mm_rounds', 0),
(68, 'steam:110000111d0b1aa', 'WEAPON_FLASHLIGHT', 0),
(69, 'steam:110000111d0b1aa', 'WEAPON_BAT', 0),
(70, 'steam:110000111d0b1aa', 'WEAPON_STUNGUN', 0),
(71, 'steam:1100001081d1893', 'copper', 0),
(72, 'steam:1100001081d1893', 'marijuana', 0),
(73, 'steam:1100001081d1893', 'alive_chicken', 0),
(74, 'steam:1100001081d1893', 'fixtool', 0),
(75, 'steam:1100001081d1893', 'wood', 0),
(76, 'steam:1100001081d1893', 'gazbottle', 0),
(77, 'steam:1100001081d1893', 'cannabis', 0),
(78, 'steam:1100001081d1893', 'clothe', 0),
(79, 'steam:1100001081d1893', 'stone', 0),
(80, 'steam:1100001081d1893', 'iron', 0),
(81, 'steam:1100001081d1893', 'fish', 0),
(82, 'steam:1100001081d1893', 'shotgun_shells', 0),
(83, 'steam:1100001081d1893', 'blowpipe', 0),
(84, 'steam:1100001081d1893', 'packaged_chicken', 0),
(85, 'steam:1100001081d1893', 'fixkit', 0),
(86, 'steam:1100001081d1893', 'cutted_wood', 0),
(87, 'steam:1100001081d1893', 'slaughtered_chicken', 0),
(88, 'steam:1100001081d1893', 'bread', 6),
(89, 'steam:1100001081d1893', 'diamond', 0),
(90, 'steam:1100001081d1893', 'essence', 0),
(91, 'steam:1100001081d1893', 'fabric', 0),
(92, 'steam:1100001081d1893', 'WEAPON_KNIFE', 0),
(93, 'steam:1100001081d1893', 'petrol_raffin', 0),
(94, 'steam:1100001081d1893', 'wool', 0),
(95, 'steam:1100001081d1893', 'water', 0),
(96, 'steam:1100001081d1893', 'WEAPON_PUMPSHOTGUN', 0),
(97, 'steam:1100001081d1893', 'washed_stone', 0),
(98, 'steam:1100001081d1893', 'gold', 0),
(99, 'steam:1100001081d1893', 'WEAPON_PISTOL', 0),
(100, 'steam:1100001081d1893', '9mm_rounds', 0),
(101, 'steam:1100001081d1893', 'WEAPON_FLASHLIGHT', 0),
(102, 'steam:1100001081d1893', 'bandage', 0),
(103, 'steam:1100001081d1893', 'petrol', 0),
(104, 'steam:1100001081d1893', 'packaged_plank', 0),
(105, 'steam:1100001081d1893', 'WEAPON_BAT', 0),
(106, 'steam:1100001081d1893', 'carotool', 0),
(107, 'steam:1100001081d1893', 'medikit', 0),
(108, 'steam:1100001081d1893', 'carokit', 0),
(109, 'steam:1100001081d1893', 'WEAPON_STUNGUN', 0),
(110, 'steam:11000010b49a743', 'copper', 0),
(111, 'steam:11000010b49a743', 'marijuana', 0),
(112, 'steam:11000010b49a743', 'alive_chicken', 0),
(113, 'steam:11000010b49a743', 'fixtool', 0),
(114, 'steam:11000010b49a743', 'wood', 0),
(115, 'steam:11000010b49a743', 'gazbottle', 0),
(116, 'steam:11000010b49a743', 'cannabis', 0),
(117, 'steam:11000010b49a743', 'stone', 0),
(118, 'steam:11000010b49a743', 'clothe', 0),
(119, 'steam:11000010b49a743', 'iron', 0),
(120, 'steam:11000010b49a743', 'fish', 0),
(121, 'steam:11000010b49a743', 'shotgun_shells', 0),
(122, 'steam:11000010b49a743', 'blowpipe', 0),
(123, 'steam:11000010b49a743', 'packaged_chicken', 0),
(124, 'steam:11000010b49a743', 'fixkit', 0),
(125, 'steam:11000010b49a743', 'cutted_wood', 0),
(126, 'steam:11000010b49a743', 'slaughtered_chicken', 0),
(127, 'steam:11000010b49a743', 'bread', 39),
(128, 'steam:11000010b49a743', 'diamond', 0),
(129, 'steam:11000010b49a743', 'essence', 0),
(130, 'steam:11000010b49a743', 'fabric', 0),
(131, 'steam:11000010b49a743', 'WEAPON_KNIFE', 0),
(132, 'steam:11000010b49a743', 'petrol_raffin', 0),
(133, 'steam:11000010b49a743', 'wool', 0),
(134, 'steam:11000010b49a743', 'water', 79),
(135, 'steam:11000010b49a743', 'washed_stone', 0),
(136, 'steam:11000010b49a743', 'WEAPON_PUMPSHOTGUN', 0),
(137, 'steam:11000010b49a743', 'gold', 0),
(138, 'steam:11000010b49a743', 'WEAPON_PISTOL', 0),
(139, 'steam:11000010b49a743', '9mm_rounds', 0),
(140, 'steam:11000010b49a743', 'WEAPON_FLASHLIGHT', 0),
(141, 'steam:11000010b49a743', 'bandage', 0),
(142, 'steam:11000010b49a743', 'petrol', 0),
(143, 'steam:11000010b49a743', 'packaged_plank', 0),
(144, 'steam:11000010b49a743', 'WEAPON_BAT', 0),
(145, 'steam:11000010b49a743', 'carotool', 0),
(146, 'steam:11000010b49a743', 'medikit', 0),
(147, 'steam:11000010b49a743', 'carokit', 0),
(148, 'steam:11000010b49a743', 'WEAPON_STUNGUN', 0),
(149, 'steam:11000013e7edea3', 'WEAPON_BAT', 0),
(150, 'steam:11000013e7edea3', 'cutted_wood', 0),
(151, 'steam:11000013e7edea3', 'fixkit', 0),
(152, 'steam:11000013e7edea3', '9mm_rounds', 0),
(153, 'steam:11000013e7edea3', 'packaged_plank', 0),
(154, 'steam:11000013e7edea3', 'WEAPON_STUNGUN', 0),
(155, 'steam:11000013e7edea3', 'stone', 0),
(156, 'steam:11000013e7edea3', 'carokit', 0),
(157, 'steam:11000013e7edea3', 'petrol_raffin', 0),
(158, 'steam:11000013e7edea3', 'WEAPON_KNIFE', 0),
(159, 'steam:11000013e7edea3', 'diamond', 0),
(160, 'steam:11000013e7edea3', 'carotool', 0),
(161, 'steam:11000013e7edea3', 'copper', 0),
(162, 'steam:11000013e7edea3', 'bandage', 0),
(163, 'steam:11000013e7edea3', 'essence', 0),
(164, 'steam:11000013e7edea3', 'fabric', 0),
(165, 'steam:11000013e7edea3', 'blowpipe', 0),
(166, 'steam:11000013e7edea3', 'WEAPON_PUMPSHOTGUN', 0),
(167, 'steam:11000013e7edea3', 'gazbottle', 0),
(168, 'steam:11000013e7edea3', 'slaughtered_chicken', 0),
(169, 'steam:11000013e7edea3', 'petrol', 0),
(170, 'steam:11000013e7edea3', 'shotgun_shells', 0),
(171, 'steam:11000013e7edea3', 'medikit', 0),
(172, 'steam:11000013e7edea3', 'washed_stone', 0),
(173, 'steam:11000013e7edea3', 'WEAPON_PISTOL', 0),
(174, 'steam:11000013e7edea3', 'wool', 0),
(175, 'steam:11000013e7edea3', 'wood', 0),
(176, 'steam:11000013e7edea3', 'WEAPON_FLASHLIGHT', 0),
(177, 'steam:11000013e7edea3', 'water', 49),
(178, 'steam:11000013e7edea3', 'bread', 18),
(179, 'steam:11000013e7edea3', 'cannabis', 0),
(180, 'steam:11000013e7edea3', 'gold', 0),
(181, 'steam:11000013e7edea3', 'packaged_chicken', 0),
(182, 'steam:11000013e7edea3', 'fixtool', 0),
(183, 'steam:11000013e7edea3', 'fish', 0),
(184, 'steam:11000013e7edea3', 'marijuana', 0),
(185, 'steam:11000013e7edea3', 'iron', 0),
(186, 'steam:11000013e7edea3', 'clothe', 0),
(187, 'steam:11000013e7edea3', 'alive_chicken', 0),
(188, 'steam:110000111c332ed', 'blowpipe', 0),
(189, 'steam:110000111c332ed', 'alive_chicken', 0),
(190, 'steam:110000111c332ed', 'washed_stone', 0),
(191, 'steam:110000111c332ed', 'water', 36),
(192, 'steam:110000111c332ed', 'diamond', 0),
(193, 'steam:110000111c332ed', 'WEAPON_KNIFE', 0),
(194, 'steam:110000111c332ed', 'WEAPON_BAT', 0),
(195, 'steam:110000111c332ed', 'WEAPON_STUNGUN', 0),
(196, 'steam:110000111c332ed', '9mm_rounds', 0),
(197, 'steam:110000111c332ed', 'fish', 0),
(198, 'steam:110000111c332ed', 'marijuana', 0),
(199, 'steam:110000111c332ed', 'carotool', 0),
(200, 'steam:110000111c332ed', 'bandage', 0),
(201, 'steam:110000111c332ed', 'petrol', 0),
(202, 'steam:110000111c332ed', 'fabric', 0),
(203, 'steam:110000111c332ed', 'cannabis', 0),
(204, 'steam:110000111c332ed', 'wood', 0),
(205, 'steam:110000111c332ed', 'carokit', 0),
(206, 'steam:110000111c332ed', 'shotgun_shells', 0),
(207, 'steam:110000111c332ed', 'medikit', 0),
(208, 'steam:110000111c332ed', 'fixkit', 0),
(209, 'steam:110000111c332ed', 'wool', 0),
(210, 'steam:110000111c332ed', 'WEAPON_PUMPSHOTGUN', 0),
(211, 'steam:110000111c332ed', 'clothe', 0),
(212, 'steam:110000111c332ed', 'bread', 35),
(213, 'steam:110000111c332ed', 'slaughtered_chicken', 0),
(214, 'steam:110000111c332ed', 'iron', 0),
(215, 'steam:110000111c332ed', 'WEAPON_FLASHLIGHT', 0),
(216, 'steam:110000111c332ed', 'WEAPON_PISTOL', 0),
(217, 'steam:110000111c332ed', 'cutted_wood', 0),
(218, 'steam:110000111c332ed', 'stone', 0),
(219, 'steam:110000111c332ed', 'gazbottle', 0),
(220, 'steam:110000111c332ed', 'petrol_raffin', 0),
(221, 'steam:110000111c332ed', 'packaged_plank', 0),
(222, 'steam:110000111c332ed', 'essence', 0),
(223, 'steam:110000111c332ed', 'packaged_chicken', 0),
(224, 'steam:110000111c332ed', 'gold', 0),
(225, 'steam:110000111c332ed', 'fixtool', 0),
(226, 'steam:110000111c332ed', 'copper', 0),
(227, 'steam:11000010a01bdb9', 'petrol_raffin', 0),
(228, 'steam:11000010a01bdb9', 'iron', 0),
(229, 'steam:11000010a01bdb9', 'fish', 0),
(230, 'steam:11000010a01bdb9', 'stone', 0),
(231, 'steam:11000010a01bdb9', 'packaged_plank', 0),
(232, 'steam:11000010a01bdb9', 'slaughtered_chicken', 0),
(233, 'steam:11000010a01bdb9', 'gold', 0),
(234, 'steam:11000010a01bdb9', 'clothe', 0),
(235, 'steam:11000010a01bdb9', 'WEAPON_PISTOL', 0),
(236, 'steam:11000010a01bdb9', 'carokit', 0),
(237, 'steam:11000010a01bdb9', 'fabric', 0),
(238, 'steam:11000010a01bdb9', 'WEAPON_BAT', 0),
(239, 'steam:11000010a01bdb9', 'alive_chicken', 0),
(240, 'steam:11000010a01bdb9', 'WEAPON_STUNGUN', 0),
(241, 'steam:11000010a01bdb9', 'bread', 0),
(242, 'steam:11000010a01bdb9', 'petrol', 0),
(243, 'steam:11000010a01bdb9', 'WEAPON_KNIFE', 0),
(244, 'steam:11000010a01bdb9', 'medikit', 0),
(245, 'steam:11000010a01bdb9', '9mm_rounds', 0),
(246, 'steam:11000010a01bdb9', 'packaged_chicken', 0),
(247, 'steam:11000010a01bdb9', 'fixkit', 0),
(248, 'steam:11000010a01bdb9', 'fixtool', 0),
(249, 'steam:11000010a01bdb9', 'carotool', 0),
(250, 'steam:11000010a01bdb9', 'water', 0),
(251, 'steam:11000010a01bdb9', 'bandage', 0),
(252, 'steam:11000010a01bdb9', 'wood', 0),
(253, 'steam:11000010a01bdb9', 'wool', 0),
(254, 'steam:11000010a01bdb9', 'gazbottle', 0),
(255, 'steam:11000010a01bdb9', 'washed_stone', 0),
(256, 'steam:11000010a01bdb9', 'shotgun_shells', 0),
(257, 'steam:11000010a01bdb9', 'copper', 0),
(258, 'steam:11000010a01bdb9', 'cutted_wood', 0),
(259, 'steam:11000010a01bdb9', 'marijuana', 0),
(260, 'steam:11000010a01bdb9', 'blowpipe', 0),
(261, 'steam:11000010a01bdb9', 'cannabis', 0),
(262, 'steam:11000010a01bdb9', 'WEAPON_FLASHLIGHT', 0),
(263, 'steam:11000010a01bdb9', 'WEAPON_PUMPSHOTGUN', 0),
(264, 'steam:11000010a01bdb9', 'diamond', 0),
(265, 'steam:11000010a01bdb9', 'essence', 0),
(266, 'steam:11000013e9d4b99', 'fabric', 0),
(267, 'steam:11000013e9d4b99', 'fish', 0),
(268, 'steam:11000013e9d4b99', 'WEAPON_PISTOL', 0),
(269, 'steam:11000013e9d4b99', 'WEAPON_STUNGUN', 0),
(270, 'steam:11000013e9d4b99', 'blowpipe', 0),
(271, 'steam:11000013e9d4b99', 'clothe', 0),
(272, 'steam:11000013e9d4b99', 'bread', 0),
(273, 'steam:11000013e9d4b99', 'marijuana', 0),
(274, 'steam:11000013e9d4b99', 'alive_chicken', 0),
(275, 'steam:11000013e9d4b99', 'iron', 0),
(276, 'steam:11000013e9d4b99', 'packaged_chicken', 0),
(277, 'steam:11000013e9d4b99', 'WEAPON_KNIFE', 0),
(278, 'steam:11000013e9d4b99', 'bandage', 0),
(279, 'steam:11000013e9d4b99', 'WEAPON_PUMPSHOTGUN', 0),
(280, 'steam:11000013e9d4b99', 'fixtool', 0),
(281, 'steam:11000013e9d4b99', 'stone', 0),
(282, 'steam:11000013e9d4b99', 'fixkit', 0),
(283, 'steam:11000013e9d4b99', 'gold', 0),
(284, 'steam:11000013e9d4b99', 'WEAPON_BAT', 0),
(285, 'steam:11000013e9d4b99', 'wool', 0),
(286, 'steam:11000013e9d4b99', 'essence', 0),
(287, 'steam:11000013e9d4b99', 'cannabis', 0),
(288, 'steam:11000013e9d4b99', 'carokit', 0),
(289, 'steam:11000013e9d4b99', 'washed_stone', 0),
(290, 'steam:11000013e9d4b99', 'water', 0),
(291, 'steam:11000013e9d4b99', 'slaughtered_chicken', 0),
(292, 'steam:11000013e9d4b99', 'medikit', 0),
(293, 'steam:11000013e9d4b99', 'gazbottle', 0),
(294, 'steam:11000013e9d4b99', 'petrol', 0),
(295, 'steam:11000013e9d4b99', '9mm_rounds', 0),
(296, 'steam:11000013e9d4b99', 'carotool', 0),
(297, 'steam:11000013e9d4b99', 'packaged_plank', 0),
(298, 'steam:11000013e9d4b99', 'copper', 0),
(299, 'steam:11000013e9d4b99', 'shotgun_shells', 0),
(300, 'steam:11000013e9d4b99', 'petrol_raffin', 0),
(301, 'steam:11000013e9d4b99', 'diamond', 0),
(302, 'steam:11000013e9d4b99', 'cutted_wood', 0),
(303, 'steam:11000013e9d4b99', 'wood', 0),
(304, 'steam:11000013e9d4b99', 'WEAPON_FLASHLIGHT', 0),
(305, 'steam:11000010a01bdb9', 'cocaine', 0),
(306, 'steam:11000010a01bdb9', 'pcp', 0),
(307, 'steam:11000010a01bdb9', 'beer', 0),
(308, 'steam:11000010a01bdb9', 'heroine', 0),
(309, 'steam:11000010a01bdb9', 'whiskey', 0),
(310, 'steam:11000010a01bdb9', 'vodka', 0),
(311, 'steam:11000010a01bdb9', 'tequila', 0),
(312, 'steam:11000010a01bdb9', 'dabs', 0),
(313, 'steam:11000010a01bdb9', 'narcan', 0),
(314, 'steam:11000010a01bdb9', 'ephedrine', 0),
(315, 'steam:11000010a01bdb9', 'breathalyzer', 0),
(316, 'steam:11000010a01bdb9', 'drugtest', 0),
(317, 'steam:11000010a01bdb9', 'fakepee', 0),
(318, 'steam:11000010a01bdb9', 'opium', 0),
(319, 'steam:11000010a01bdb9', 'meth', 0),
(320, 'steam:11000010a01bdb9', 'ephedra', 0),
(321, 'steam:11000010a01bdb9', 'painkiller', 0),
(322, 'steam:11000010a01bdb9', 'crack', 0),
(323, 'steam:11000010a01bdb9', 'poppy', 0),
(324, 'steam:11000010a01bdb9', 'coca', 0),
(325, 'steam:110000111c332ed', 'pcp', 0),
(326, 'steam:110000111c332ed', 'opium', 0),
(327, 'steam:110000111c332ed', 'vodka', 0),
(328, 'steam:110000111c332ed', 'whiskey', 0),
(329, 'steam:110000111c332ed', 'crack', 0),
(330, 'steam:110000111c332ed', 'tequila', 0),
(331, 'steam:110000111c332ed', 'fakepee', 0),
(332, 'steam:110000111c332ed', 'beer', 0),
(333, 'steam:110000111c332ed', 'poppy', 0),
(334, 'steam:110000111c332ed', 'drugtest', 0),
(335, 'steam:110000111c332ed', 'ephedra', 0),
(336, 'steam:110000111c332ed', 'dabs', 0),
(337, 'steam:110000111c332ed', 'ephedrine', 0),
(338, 'steam:110000111c332ed', 'painkiller', 0),
(339, 'steam:110000111c332ed', 'breathalyzer', 0),
(340, 'steam:110000111c332ed', 'narcan', 0),
(341, 'steam:110000111c332ed', 'meth', 0),
(342, 'steam:110000111c332ed', 'heroine', 0),
(343, 'steam:110000111c332ed', 'coca', 0),
(344, 'steam:110000111c332ed', 'cocaine', 0),
(345, 'steam:110000107941620', 'dabs', 0),
(346, 'steam:110000107941620', 'essence', 0),
(347, 'steam:110000107941620', 'coca', 0),
(348, 'steam:110000107941620', 'narcan', 0),
(349, 'steam:110000107941620', 'cocaine', 0),
(350, 'steam:110000107941620', 'bread', 11),
(351, 'steam:110000107941620', 'petrol', 0),
(352, 'steam:110000107941620', 'iron', 0),
(353, 'steam:110000107941620', 'wool', 0),
(354, 'steam:110000107941620', 'packaged_chicken', 0),
(355, 'steam:110000107941620', 'wood', 0),
(356, 'steam:110000107941620', 'poppy', 0),
(357, 'steam:110000107941620', 'WEAPON_BAT', 0),
(358, 'steam:110000107941620', 'fish', 0),
(359, 'steam:110000107941620', 'packaged_plank', 0),
(360, 'steam:110000107941620', 'whiskey', 0),
(361, 'steam:110000107941620', 'tequila', 0),
(362, 'steam:110000107941620', 'blowpipe', 0),
(363, 'steam:110000107941620', 'water', 14),
(364, 'steam:110000107941620', 'fakepee', 0),
(365, 'steam:110000107941620', 'fixtool', 0),
(366, 'steam:110000107941620', 'opium', 0),
(367, 'steam:110000107941620', 'vodka', 0),
(368, 'steam:110000107941620', 'WEAPON_STUNGUN', 0),
(369, 'steam:110000107941620', 'marijuana', 0),
(370, 'steam:110000107941620', 'cannabis', 0),
(371, 'steam:110000107941620', 'carokit', 0),
(372, 'steam:110000107941620', 'alive_chicken', 0),
(373, 'steam:110000107941620', 'meth', 0),
(374, 'steam:110000107941620', 'drugtest', 0),
(375, 'steam:110000107941620', 'WEAPON_FLASHLIGHT', 0),
(376, 'steam:110000107941620', 'shotgun_shells', 0),
(377, 'steam:110000107941620', 'ephedra', 0),
(378, 'steam:110000107941620', 'stone', 0),
(379, 'steam:110000107941620', 'carotool', 0),
(380, 'steam:110000107941620', 'fixkit', 0),
(381, 'steam:110000107941620', 'medikit', 0),
(382, 'steam:110000107941620', 'gazbottle', 0),
(383, 'steam:110000107941620', 'WEAPON_PUMPSHOTGUN', 0),
(384, 'steam:110000107941620', 'painkiller', 0),
(385, 'steam:110000107941620', 'fabric', 0),
(386, 'steam:110000107941620', 'petrol_raffin', 0),
(387, 'steam:110000107941620', 'heroine', 0),
(388, 'steam:110000107941620', 'gold', 0),
(389, 'steam:110000107941620', 'WEAPON_KNIFE', 0),
(390, 'steam:110000107941620', 'pcp', 0),
(391, 'steam:110000107941620', 'beer', 0),
(392, 'steam:110000107941620', 'WEAPON_PISTOL', 0),
(393, 'steam:110000107941620', 'cutted_wood', 0),
(394, 'steam:110000107941620', 'breathalyzer', 0),
(395, 'steam:110000107941620', '9mm_rounds', 0),
(396, 'steam:110000107941620', 'washed_stone', 0),
(397, 'steam:110000107941620', 'ephedrine', 0),
(398, 'steam:110000107941620', 'clothe', 0),
(399, 'steam:110000107941620', 'slaughtered_chicken', 0),
(400, 'steam:110000107941620', 'copper', 0),
(401, 'steam:110000107941620', 'bandage', 0),
(402, 'steam:110000107941620', 'crack', 0),
(403, 'steam:110000107941620', 'diamond', 0),
(404, 'steam:11000010fe595d0', 'essence', 0),
(405, 'steam:11000010fe595d0', 'cocaine', 0),
(406, 'steam:11000010fe595d0', 'dabs', 0),
(407, 'steam:11000010fe595d0', 'coca', 0),
(408, 'steam:11000010fe595d0', 'narcan', 0),
(409, 'steam:11000010fe595d0', 'bread', 0),
(410, 'steam:11000010fe595d0', 'iron', 0),
(411, 'steam:11000010fe595d0', 'wool', 0),
(412, 'steam:11000010fe595d0', 'packaged_chicken', 0),
(413, 'steam:11000010fe595d0', 'petrol', 0),
(414, 'steam:11000010fe595d0', 'wood', 0),
(415, 'steam:11000010fe595d0', 'poppy', 0),
(416, 'steam:11000010fe595d0', 'WEAPON_BAT', 0),
(417, 'steam:11000010fe595d0', 'packaged_plank', 0),
(418, 'steam:11000010fe595d0', 'fish', 0),
(419, 'steam:11000010fe595d0', 'whiskey', 0),
(420, 'steam:11000010fe595d0', 'fakepee', 0),
(421, 'steam:11000010fe595d0', 'fixtool', 0),
(422, 'steam:11000010fe595d0', 'water', 0),
(423, 'steam:11000010fe595d0', 'blowpipe', 0),
(424, 'steam:11000010fe595d0', 'tequila', 0),
(425, 'steam:11000010fe595d0', 'opium', 0),
(426, 'steam:11000010fe595d0', 'WEAPON_STUNGUN', 0),
(427, 'steam:11000010fe595d0', 'vodka', 0),
(428, 'steam:11000010fe595d0', 'marijuana', 0),
(429, 'steam:11000010fe595d0', 'cannabis', 0),
(430, 'steam:11000010fe595d0', 'meth', 0),
(431, 'steam:11000010fe595d0', 'drugtest', 0),
(432, 'steam:11000010fe595d0', 'carokit', 0),
(433, 'steam:11000010fe595d0', 'WEAPON_FLASHLIGHT', 0),
(434, 'steam:11000010fe595d0', 'alive_chicken', 0),
(435, 'steam:11000010fe595d0', 'shotgun_shells', 0),
(436, 'steam:11000010fe595d0', 'ephedra', 0),
(437, 'steam:11000010fe595d0', 'stone', 0),
(438, 'steam:11000010fe595d0', 'carotool', 0),
(439, 'steam:11000010fe595d0', 'fixkit', 0),
(440, 'steam:11000010fe595d0', 'medikit', 0),
(441, 'steam:11000010fe595d0', 'gazbottle', 0),
(442, 'steam:11000010fe595d0', 'painkiller', 0),
(443, 'steam:11000010fe595d0', 'WEAPON_PUMPSHOTGUN', 0),
(444, 'steam:11000010fe595d0', 'fabric', 0),
(445, 'steam:11000010fe595d0', 'petrol_raffin', 0),
(446, 'steam:11000010fe595d0', 'heroine', 0),
(447, 'steam:11000010fe595d0', 'WEAPON_KNIFE', 0),
(448, 'steam:11000010fe595d0', 'gold', 0),
(449, 'steam:11000010fe595d0', 'pcp', 0),
(450, 'steam:11000010fe595d0', 'beer', 0),
(451, 'steam:11000010fe595d0', 'WEAPON_PISTOL', 0),
(452, 'steam:11000010fe595d0', 'breathalyzer', 0),
(453, 'steam:11000010fe595d0', 'cutted_wood', 0),
(454, 'steam:11000010fe595d0', 'washed_stone', 0),
(455, 'steam:11000010fe595d0', '9mm_rounds', 0),
(456, 'steam:11000010fe595d0', 'ephedrine', 0),
(457, 'steam:11000010fe595d0', 'clothe', 0),
(458, 'steam:11000010fe595d0', 'copper', 0),
(459, 'steam:11000010fe595d0', 'slaughtered_chicken', 0),
(460, 'steam:11000010fe595d0', 'bandage', 0),
(461, 'steam:11000010fe595d0', 'crack', 0),
(462, 'steam:11000010fe595d0', 'diamond', 0),
(463, 'steam:110000111d0b1aa', 'beer', 0),
(464, 'steam:110000111d0b1aa', 'meth', 0),
(465, 'steam:110000111d0b1aa', 'heroine', 0),
(466, 'steam:110000111d0b1aa', 'fakepee', 0),
(467, 'steam:110000111d0b1aa', 'cocaine', 0),
(468, 'steam:110000111d0b1aa', 'tequila', 7),
(469, 'steam:110000111d0b1aa', 'vodka', 0),
(470, 'steam:110000111d0b1aa', 'ephedrine', 0),
(471, 'steam:110000111d0b1aa', 'poppy', 0),
(472, 'steam:110000111d0b1aa', 'drugtest', 0),
(473, 'steam:110000111d0b1aa', 'pcp', 0),
(474, 'steam:110000111d0b1aa', 'painkiller', 0),
(475, 'steam:110000111d0b1aa', 'dabs', 0),
(476, 'steam:110000111d0b1aa', 'ephedra', 0),
(477, 'steam:110000111d0b1aa', 'opium', 0),
(478, 'steam:110000111d0b1aa', 'whiskey', 0),
(479, 'steam:110000111d0b1aa', 'coca', 0),
(480, 'steam:110000111d0b1aa', 'narcan', 0),
(481, 'steam:110000111d0b1aa', 'crack', 0),
(482, 'steam:110000111d0b1aa', 'breathalyzer', 0),
(483, 'steam:11000010e2a62e2', 'carotool', 0),
(484, 'steam:11000010e2a62e2', 'beer', 0),
(485, 'steam:11000010e2a62e2', 'meth', 0),
(486, 'steam:11000010e2a62e2', 'gold', 0),
(487, 'steam:11000010e2a62e2', 'bread', 984),
(488, 'steam:11000010e2a62e2', 'heroine', 0),
(489, 'steam:11000010e2a62e2', 'fixkit', 0),
(490, 'steam:11000010e2a62e2', 'cocaine', 0),
(491, 'steam:11000010e2a62e2', 'WEAPON_BAT', 0),
(492, 'steam:11000010e2a62e2', 'fixtool', 0),
(493, 'steam:11000010e2a62e2', 'diamond', 0),
(494, 'steam:11000010e2a62e2', 'tequila', 0),
(495, 'steam:11000010e2a62e2', 'wool', 0),
(496, 'steam:11000010e2a62e2', 'wood', 0),
(497, 'steam:11000010e2a62e2', 'fakepee', 0),
(498, 'steam:11000010e2a62e2', 'ephedrine', 0),
(499, 'steam:11000010e2a62e2', 'slaughtered_chicken', 0),
(500, 'steam:11000010e2a62e2', 'iron', 0),
(501, 'steam:11000010e2a62e2', 'vodka', 0),
(502, 'steam:11000010e2a62e2', 'stone', 0),
(503, 'steam:11000010e2a62e2', 'WEAPON_KNIFE', 0),
(504, 'steam:11000010e2a62e2', 'shotgun_shells', 0),
(505, 'steam:11000010e2a62e2', 'marijuana', 0),
(506, 'steam:11000010e2a62e2', '9mm_rounds', 0),
(507, 'steam:11000010e2a62e2', 'poppy', 0),
(508, 'steam:11000010e2a62e2', 'petrol_raffin', 0),
(509, 'steam:11000010e2a62e2', 'drugtest', 0),
(510, 'steam:11000010e2a62e2', 'blowpipe', 0),
(511, 'steam:11000010e2a62e2', 'alive_chicken', 0),
(512, 'steam:11000010e2a62e2', 'petrol', 0),
(513, 'steam:11000010e2a62e2', 'fish', 0),
(514, 'steam:11000010e2a62e2', 'pcp', 0),
(515, 'steam:11000010e2a62e2', 'painkiller', 0),
(516, 'steam:11000010e2a62e2', 'carokit', 0),
(517, 'steam:11000010e2a62e2', 'copper', 0),
(518, 'steam:11000010e2a62e2', 'packaged_plank', 0),
(519, 'steam:11000010e2a62e2', 'dabs', 0),
(520, 'steam:11000010e2a62e2', 'ephedra', 0),
(521, 'steam:11000010e2a62e2', 'WEAPON_PUMPSHOTGUN', 0),
(522, 'steam:11000010e2a62e2', 'WEAPON_FLASHLIGHT', 0),
(523, 'steam:11000010e2a62e2', 'opium', 0),
(524, 'steam:11000010e2a62e2', 'narcan', 0),
(525, 'steam:11000010e2a62e2', 'medikit', 0),
(526, 'steam:11000010e2a62e2', 'cannabis', 0),
(527, 'steam:11000010e2a62e2', 'washed_stone', 0),
(528, 'steam:11000010e2a62e2', 'whiskey', 0),
(529, 'steam:11000010e2a62e2', 'fabric', 0),
(530, 'steam:11000010e2a62e2', 'WEAPON_STUNGUN', 0),
(531, 'steam:11000010e2a62e2', 'WEAPON_PISTOL', 0),
(532, 'steam:11000010e2a62e2', 'coca', 0),
(533, 'steam:11000010e2a62e2', 'packaged_chicken', 0),
(534, 'steam:11000010e2a62e2', 'essence', 0),
(535, 'steam:11000010e2a62e2', 'bandage', 0),
(536, 'steam:11000010e2a62e2', 'crack', 0),
(537, 'steam:11000010e2a62e2', 'breathalyzer', 0),
(538, 'steam:11000010e2a62e2', 'clothe', 0),
(539, 'steam:11000010e2a62e2', 'gazbottle', 0),
(540, 'steam:11000010e2a62e2', 'water', 986),
(541, 'steam:11000010e2a62e2', 'cutted_wood', 0),
(542, 'steam:11000010b88b452', 'carotool', 0),
(543, 'steam:11000010b88b452', 'beer', 0),
(544, 'steam:11000010b88b452', 'meth', 0),
(545, 'steam:11000010b88b452', 'gold', 0),
(546, 'steam:11000010b88b452', 'bread', 0),
(547, 'steam:11000010b88b452', 'heroine', 0),
(548, 'steam:11000010b88b452', 'fixkit', 0),
(549, 'steam:11000010b88b452', 'cocaine', 0),
(550, 'steam:11000010b88b452', 'fixtool', 0),
(551, 'steam:11000010b88b452', 'WEAPON_BAT', 0),
(552, 'steam:11000010b88b452', 'diamond', 0),
(553, 'steam:11000010b88b452', 'fakepee', 0),
(554, 'steam:11000010b88b452', 'tequila', 0),
(555, 'steam:11000010b88b452', 'wood', 0),
(556, 'steam:11000010b88b452', 'wool', 0),
(557, 'steam:11000010b88b452', 'ephedrine', 0),
(558, 'steam:11000010b88b452', 'WEAPON_KNIFE', 0),
(559, 'steam:11000010b88b452', 'vodka', 0),
(560, 'steam:11000010b88b452', 'iron', 0),
(561, 'steam:11000010b88b452', 'stone', 0),
(562, 'steam:11000010b88b452', 'slaughtered_chicken', 0),
(563, 'steam:11000010b88b452', 'shotgun_shells', 0),
(564, 'steam:11000010b88b452', 'marijuana', 0),
(565, 'steam:11000010b88b452', '9mm_rounds', 0),
(566, 'steam:11000010b88b452', 'poppy', 0),
(567, 'steam:11000010b88b452', 'petrol_raffin', 0),
(568, 'steam:11000010b88b452', 'drugtest', 0),
(569, 'steam:11000010b88b452', 'blowpipe', 0),
(570, 'steam:11000010b88b452', 'alive_chicken', 0),
(571, 'steam:11000010b88b452', 'petrol', 0),
(572, 'steam:11000010b88b452', 'fish', 0),
(573, 'steam:11000010b88b452', 'pcp', 0),
(574, 'steam:11000010b88b452', 'painkiller', 0),
(575, 'steam:11000010b88b452', 'carokit', 0),
(576, 'steam:11000010b88b452', 'copper', 0),
(577, 'steam:11000010b88b452', 'packaged_plank', 0),
(578, 'steam:11000010b88b452', 'dabs', 0),
(579, 'steam:11000010b88b452', 'ephedra', 0),
(580, 'steam:11000010b88b452', 'WEAPON_PUMPSHOTGUN', 0),
(581, 'steam:11000010b88b452', 'WEAPON_FLASHLIGHT', 0),
(582, 'steam:11000010b88b452', 'opium', 0),
(583, 'steam:11000010b88b452', 'narcan', 0),
(584, 'steam:11000010b88b452', 'medikit', 0),
(585, 'steam:11000010b88b452', 'cannabis', 0),
(586, 'steam:11000010b88b452', 'washed_stone', 0),
(587, 'steam:11000010b88b452', 'whiskey', 0),
(588, 'steam:11000010b88b452', 'fabric', 0),
(589, 'steam:11000010b88b452', 'WEAPON_STUNGUN', 0),
(590, 'steam:11000010b88b452', 'WEAPON_PISTOL', 0),
(591, 'steam:11000010b88b452', 'coca', 0),
(592, 'steam:11000010b88b452', 'packaged_chicken', 0),
(593, 'steam:11000010b88b452', 'essence', 0),
(594, 'steam:11000010b88b452', 'bandage', 0),
(595, 'steam:11000010b88b452', 'gazbottle', 0),
(596, 'steam:11000010b88b452', 'crack', 0),
(597, 'steam:11000010b88b452', 'breathalyzer', 0),
(598, 'steam:11000010b88b452', 'clothe', 0),
(599, 'steam:11000010b88b452', 'water', 0),
(600, 'steam:11000010b88b452', 'cutted_wood', 0),
(601, 'steam:1100001081d1893', 'beer', 0),
(602, 'steam:1100001081d1893', 'meth', 0),
(603, 'steam:1100001081d1893', 'heroine', 0),
(604, 'steam:1100001081d1893', 'cocaine', 0),
(605, 'steam:1100001081d1893', 'fakepee', 0),
(606, 'steam:1100001081d1893', 'tequila', 0),
(607, 'steam:1100001081d1893', 'ephedrine', 0),
(608, 'steam:1100001081d1893', 'vodka', 0),
(609, 'steam:1100001081d1893', 'poppy', 0),
(610, 'steam:1100001081d1893', 'drugtest', 0),
(611, 'steam:1100001081d1893', 'pcp', 0),
(612, 'steam:1100001081d1893', 'painkiller', 0),
(613, 'steam:1100001081d1893', 'dabs', 0),
(614, 'steam:1100001081d1893', 'ephedra', 0),
(615, 'steam:1100001081d1893', 'opium', 0),
(616, 'steam:1100001081d1893', 'narcan', 0),
(617, 'steam:1100001081d1893', 'whiskey', 0),
(618, 'steam:1100001081d1893', 'coca', 0),
(619, 'steam:1100001081d1893', 'crack', 0),
(620, 'steam:1100001081d1893', 'breathalyzer', 0),
(621, 'steam:11000010e2a62e2', 'potato', 0),
(622, 'steam:11000010e2a62e2', 'cheese', 0),
(623, 'steam:11000010e2a62e2', 'lettuce', 0),
(624, 'steam:11000010e2a62e2', 'ctomato', 0),
(625, 'steam:11000010e2a62e2', 'vbread', 0),
(626, 'steam:11000010e2a62e2', 'tomato', 0),
(627, 'steam:11000010e2a62e2', 'nuggets10', 0),
(628, 'steam:11000010e2a62e2', 'clettuce', 0),
(629, 'steam:11000010e2a62e2', 'shamburger', 0),
(630, 'steam:11000010e2a62e2', 'vhamburger', 0),
(631, 'steam:11000010e2a62e2', 'fburger', 0),
(632, 'steam:11000010e2a62e2', 'fvburger', 0),
(633, 'steam:11000010e2a62e2', 'ccheese', 0),
(634, 'steam:11000010e2a62e2', 'nuggets4', 0),
(635, 'steam:11000010e2a62e2', 'nugget', 0),
(636, 'steam:110000105ed368b', 'potato', 0),
(637, 'steam:110000105ed368b', 'fish', 0),
(638, 'steam:110000105ed368b', 'bread', 12),
(639, 'steam:110000105ed368b', 'medikit', 0),
(640, 'steam:110000105ed368b', 'dabs', 0),
(641, 'steam:110000105ed368b', 'stone', 0),
(642, 'steam:110000105ed368b', 'ephedra', 0),
(643, 'steam:110000105ed368b', 'ephedrine', 0),
(644, 'steam:110000105ed368b', 'cocaine', 0),
(645, 'steam:110000105ed368b', 'breathalyzer', 0),
(646, 'steam:110000105ed368b', 'cutted_wood', 0),
(647, 'steam:110000105ed368b', 'WEAPON_FLASHLIGHT', 0),
(648, 'steam:110000105ed368b', 'cheese', 0),
(649, 'steam:110000105ed368b', 'packaged_chicken', 0),
(650, 'steam:110000105ed368b', 'blowpipe', 0),
(651, 'steam:110000105ed368b', '9mm_rounds', 0),
(652, 'steam:110000105ed368b', 'poppy', 0),
(653, 'steam:110000105ed368b', 'petrol_raffin', 0),
(654, 'steam:110000105ed368b', 'fixkit', 0),
(655, 'steam:110000105ed368b', 'lettuce', 0),
(656, 'steam:110000105ed368b', 'washed_stone', 0),
(657, 'steam:110000105ed368b', 'coca', 0),
(658, 'steam:110000105ed368b', 'gazbottle', 0),
(659, 'steam:110000105ed368b', 'fabric', 0),
(660, 'steam:110000105ed368b', 'drugtest', 0),
(661, 'steam:110000105ed368b', 'carokit', 0),
(662, 'steam:110000105ed368b', 'WEAPON_PISTOL', 0),
(663, 'steam:110000105ed368b', 'narcan', 0),
(664, 'steam:110000105ed368b', 'WEAPON_STUNGUN', 0),
(665, 'steam:110000105ed368b', 'fakepee', 0),
(666, 'steam:110000105ed368b', 'copper', 0),
(667, 'steam:110000105ed368b', 'WEAPON_KNIFE', 0),
(668, 'steam:110000105ed368b', 'WEAPON_PUMPSHOTGUN', 0),
(669, 'steam:110000105ed368b', 'crack', 0),
(670, 'steam:110000105ed368b', 'beer', 0),
(671, 'steam:110000105ed368b', 'clothe', 0),
(672, 'steam:110000105ed368b', 'iron', 0),
(673, 'steam:110000105ed368b', 'diamond', 0),
(674, 'steam:110000105ed368b', 'bandage', 0),
(675, 'steam:110000105ed368b', 'marijuana', 0),
(676, 'steam:110000105ed368b', 'heroine', 0),
(677, 'steam:110000105ed368b', 'water', 16),
(678, 'steam:110000105ed368b', 'slaughtered_chicken', 0),
(679, 'steam:110000105ed368b', 'meth', 0),
(680, 'steam:110000105ed368b', 'opium', 0),
(681, 'steam:110000105ed368b', 'vodka', 0),
(682, 'steam:110000105ed368b', 'wool', 0),
(683, 'steam:110000105ed368b', 'tequila', 0),
(684, 'steam:110000105ed368b', 'shotgun_shells', 0),
(685, 'steam:110000105ed368b', 'wood', 0),
(686, 'steam:110000105ed368b', 'pcp', 0),
(687, 'steam:110000105ed368b', 'packaged_plank', 0),
(688, 'steam:110000105ed368b', 'ctomato', 0),
(689, 'steam:110000105ed368b', 'vbread', 0),
(690, 'steam:110000105ed368b', 'tomato', 0),
(691, 'steam:110000105ed368b', 'nuggets10', 0),
(692, 'steam:110000105ed368b', 'alive_chicken', 0),
(693, 'steam:110000105ed368b', 'clettuce', 0),
(694, 'steam:110000105ed368b', 'cannabis', 0),
(695, 'steam:110000105ed368b', 'fixtool', 0),
(696, 'steam:110000105ed368b', 'essence', 0),
(697, 'steam:110000105ed368b', 'shamburger', 0),
(698, 'steam:110000105ed368b', 'painkiller', 0),
(699, 'steam:110000105ed368b', 'vhamburger', 0),
(700, 'steam:110000105ed368b', 'WEAPON_BAT', 0),
(701, 'steam:110000105ed368b', 'fburger', 0),
(702, 'steam:110000105ed368b', 'carotool', 0),
(703, 'steam:110000105ed368b', 'gold', 0),
(704, 'steam:110000105ed368b', 'petrol', 0),
(705, 'steam:110000105ed368b', 'fvburger', 0),
(706, 'steam:110000105ed368b', 'ccheese', 0),
(707, 'steam:110000105ed368b', 'whiskey', 0),
(708, 'steam:110000105ed368b', 'nuggets4', 0),
(709, 'steam:110000105ed368b', 'nugget', 0),
(710, 'steam:11000013e7edea3', 'potato', 0),
(711, 'steam:11000013e7edea3', 'dabs', 0),
(712, 'steam:11000013e7edea3', 'ephedra', 0),
(713, 'steam:11000013e7edea3', 'ephedrine', 0),
(714, 'steam:11000013e7edea3', 'breathalyzer', 0),
(715, 'steam:11000013e7edea3', 'cocaine', 0),
(716, 'steam:11000013e7edea3', 'cheese', 0),
(717, 'steam:11000013e7edea3', 'poppy', 0),
(718, 'steam:11000013e7edea3', 'lettuce', 0),
(719, 'steam:11000013e7edea3', 'coca', 0),
(720, 'steam:11000013e7edea3', 'drugtest', 0),
(721, 'steam:11000013e7edea3', 'narcan', 0),
(722, 'steam:11000013e7edea3', 'fakepee', 0),
(723, 'steam:11000013e7edea3', 'beer', 0),
(724, 'steam:11000013e7edea3', 'crack', 0),
(725, 'steam:11000013e7edea3', 'heroine', 0),
(726, 'steam:11000013e7edea3', 'meth', 0),
(727, 'steam:11000013e7edea3', 'opium', 0),
(728, 'steam:11000013e7edea3', 'vodka', 0),
(729, 'steam:11000013e7edea3', 'tequila', 0),
(730, 'steam:11000013e7edea3', 'pcp', 0),
(731, 'steam:11000013e7edea3', 'ctomato', 0),
(732, 'steam:11000013e7edea3', 'vbread', 0),
(733, 'steam:11000013e7edea3', 'tomato', 0),
(734, 'steam:11000013e7edea3', 'nuggets10', 0),
(735, 'steam:11000013e7edea3', 'clettuce', 0),
(736, 'steam:11000013e7edea3', 'shamburger', 0),
(737, 'steam:11000013e7edea3', 'painkiller', 0),
(738, 'steam:11000013e7edea3', 'vhamburger', 0),
(739, 'steam:11000013e7edea3', 'fburger', 0),
(740, 'steam:11000013e7edea3', 'fvburger', 0),
(741, 'steam:11000013e7edea3', 'ccheese', 0),
(742, 'steam:11000013e7edea3', 'whiskey', 0),
(743, 'steam:11000013e7edea3', 'nuggets4', 0),
(744, 'steam:11000013e7edea3', 'nugget', 0),
(745, 'steam:110000111c332ed', 'potato', 0),
(746, 'steam:110000111c332ed', 'cheese', 0),
(747, 'steam:110000111c332ed', 'lettuce', 0),
(748, 'steam:110000111c332ed', 'ctomato', 0),
(749, 'steam:110000111c332ed', 'vbread', 0),
(750, 'steam:110000111c332ed', 'tomato', 0),
(751, 'steam:110000111c332ed', 'nuggets10', 0),
(752, 'steam:110000111c332ed', 'clettuce', 0),
(753, 'steam:110000111c332ed', 'shamburger', 0),
(754, 'steam:110000111c332ed', 'vhamburger', 0),
(755, 'steam:110000111c332ed', 'fburger', 0),
(756, 'steam:110000111c332ed', 'fvburger', 0),
(757, 'steam:110000111c332ed', 'ccheese', 0),
(758, 'steam:110000111c332ed', 'nuggets4', 0),
(759, 'steam:110000111c332ed', 'nugget', 0),
(760, 'steam:110000107941620', 'clettuce', 0),
(761, 'steam:110000107941620', 'ctomato', 0),
(762, 'steam:110000107941620', 'lettuce', 0),
(763, 'steam:110000107941620', 'fburger', 0),
(764, 'steam:110000107941620', 'shamburger', 0),
(765, 'steam:110000107941620', 'nuggets4', 0),
(766, 'steam:110000107941620', 'vhamburger', 0),
(767, 'steam:110000107941620', 'vbread', 0),
(768, 'steam:110000107941620', 'tomato', 0),
(769, 'steam:110000107941620', 'ccheese', 0),
(770, 'steam:110000107941620', 'fvburger', 0),
(771, 'steam:110000107941620', 'nuggets10', 0),
(772, 'steam:110000107941620', 'cheese', 0),
(773, 'steam:110000107941620', 'nugget', 0),
(774, 'steam:110000107941620', 'potato', 0),
(775, 'steam:110000111d0b1aa', 'clettuce', 0),
(776, 'steam:110000111d0b1aa', 'shamburger', 0),
(777, 'steam:110000111d0b1aa', 'ctomato', 0),
(778, 'steam:110000111d0b1aa', 'lettuce', 0),
(779, 'steam:110000111d0b1aa', 'fburger', 0),
(780, 'steam:110000111d0b1aa', 'nuggets4', 0),
(781, 'steam:110000111d0b1aa', 'vhamburger', 0),
(782, 'steam:110000111d0b1aa', 'vbread', 0),
(783, 'steam:110000111d0b1aa', 'tomato', 0),
(784, 'steam:110000111d0b1aa', 'ccheese', 0),
(785, 'steam:110000111d0b1aa', 'fvburger', 0),
(786, 'steam:110000111d0b1aa', 'nuggets10', 0),
(787, 'steam:110000111d0b1aa', 'cheese', 0),
(788, 'steam:110000111d0b1aa', 'nugget', 0),
(789, 'steam:110000111d0b1aa', 'potato', 0),
(790, 'steam:1100001183c7077', 'vhamburger', 0),
(791, 'steam:1100001183c7077', 'clothe', 0),
(792, 'steam:1100001183c7077', 'packaged_plank', 0),
(793, 'steam:1100001183c7077', 'shamburger', 0),
(794, 'steam:1100001183c7077', 'gold', 0),
(795, 'steam:1100001183c7077', '9mm_rounds', 0),
(796, 'steam:1100001183c7077', 'fish', 0),
(797, 'steam:1100001183c7077', 'diamond', 0),
(798, 'steam:1100001183c7077', 'ctomato', 0),
(799, 'steam:1100001183c7077', 'narcan', 0),
(800, 'steam:1100001183c7077', 'cannabis', 0),
(801, 'steam:1100001183c7077', 'alive_chicken', 0),
(802, 'steam:1100001183c7077', 'drugtest', 0),
(803, 'steam:1100001183c7077', 'poppy', 0),
(804, 'steam:1100001183c7077', 'fabric', 0),
(805, 'steam:1100001183c7077', 'fixtool', 0),
(806, 'steam:1100001183c7077', 'shotgun_shells', 0),
(807, 'steam:1100001183c7077', 'blowpipe', 0),
(808, 'steam:1100001183c7077', 'nuggets4', 0),
(809, 'steam:1100001183c7077', 'bread', 0),
(810, 'steam:1100001183c7077', 'ephedra', 0),
(811, 'steam:1100001183c7077', 'slaughtered_chicken', 0),
(812, 'steam:1100001183c7077', 'fakepee', 0),
(813, 'steam:1100001183c7077', 'lettuce', 0),
(814, 'steam:1100001183c7077', 'whiskey', 0),
(815, 'steam:1100001183c7077', 'packaged_chicken', 0),
(816, 'steam:1100001183c7077', 'tomato', 0),
(817, 'steam:1100001183c7077', 'cocaine', 0),
(818, 'steam:1100001183c7077', 'opium', 0),
(819, 'steam:1100001183c7077', 'crack', 0),
(820, 'steam:1100001183c7077', 'water', 0),
(821, 'steam:1100001183c7077', 'copper', 0),
(822, 'steam:1100001183c7077', 'painkiller', 0),
(823, 'steam:1100001183c7077', 'vodka', 0),
(824, 'steam:1100001183c7077', 'marijuana', 0),
(825, 'steam:1100001183c7077', 'petrol_raffin', 0),
(826, 'steam:1100001183c7077', 'fvburger', 0),
(827, 'steam:1100001183c7077', 'washed_stone', 0),
(828, 'steam:1100001183c7077', 'dabs', 0),
(829, 'steam:1100001183c7077', 'stone', 0),
(830, 'steam:1100001183c7077', 'breathalyzer', 0),
(831, 'steam:1100001183c7077', 'carokit', 0),
(832, 'steam:1100001183c7077', 'WEAPON_BAT', 0),
(833, 'steam:1100001183c7077', 'nugget', 0),
(834, 'steam:1100001183c7077', 'iron', 0),
(835, 'steam:1100001183c7077', 'potato', 0),
(836, 'steam:1100001183c7077', 'WEAPON_FLASHLIGHT', 0),
(837, 'steam:1100001183c7077', 'essence', 0),
(838, 'steam:1100001183c7077', 'wool', 0),
(839, 'steam:1100001183c7077', 'gazbottle', 0),
(840, 'steam:1100001183c7077', 'vbread', 0),
(841, 'steam:1100001183c7077', 'WEAPON_PISTOL', 0),
(842, 'steam:1100001183c7077', 'fixkit', 0),
(843, 'steam:1100001183c7077', 'fburger', 0),
(844, 'steam:1100001183c7077', 'tequila', 0),
(845, 'steam:1100001183c7077', 'cutted_wood', 0),
(846, 'steam:1100001183c7077', 'medikit', 0),
(847, 'steam:1100001183c7077', 'wood', 0),
(848, 'steam:1100001183c7077', 'ephedrine', 0),
(849, 'steam:1100001183c7077', 'cheese', 0),
(850, 'steam:1100001183c7077', 'petrol', 0),
(851, 'steam:1100001183c7077', 'pcp', 0),
(852, 'steam:1100001183c7077', 'ccheese', 0),
(853, 'steam:1100001183c7077', 'beer', 0),
(854, 'steam:1100001183c7077', 'nuggets10', 0),
(855, 'steam:1100001183c7077', 'WEAPON_STUNGUN', 0),
(856, 'steam:1100001183c7077', 'clettuce', 0),
(857, 'steam:1100001183c7077', 'WEAPON_PUMPSHOTGUN', 0),
(858, 'steam:1100001183c7077', 'meth', 0),
(859, 'steam:1100001183c7077', 'carotool', 0),
(860, 'steam:1100001183c7077', 'coca', 0),
(861, 'steam:1100001183c7077', 'heroine', 0),
(862, 'steam:1100001183c7077', 'WEAPON_KNIFE', 0),
(863, 'steam:1100001183c7077', 'bandage', 0),
(864, 'steam:1100001139319c1', 'clettuce', 0),
(865, 'steam:1100001139319c1', 'nuggets4', 0),
(866, 'steam:1100001139319c1', 'drugtest', 0),
(867, 'steam:1100001139319c1', 'cheese', 0),
(868, 'steam:1100001139319c1', 'tomato', 0),
(869, 'steam:1100001139319c1', 'petrol', 0),
(870, 'steam:1100001139319c1', 'nugget', 0),
(871, 'steam:1100001139319c1', 'slaughtered_chicken', 0),
(872, 'steam:1100001139319c1', 'carokit', 0),
(873, 'steam:1100001139319c1', 'lettuce', 0),
(874, 'steam:1100001139319c1', 'packaged_plank', 0),
(875, 'steam:1100001139319c1', 'marijuana', 0),
(876, 'steam:1100001139319c1', 'fburger', 0),
(877, 'steam:1100001139319c1', 'fabric', 0),
(878, 'steam:1100001139319c1', 'opium', 0),
(879, 'steam:1100001139319c1', 'packaged_chicken', 0),
(880, 'steam:1100001139319c1', 'breathalyzer', 0),
(881, 'steam:1100001139319c1', 'water', 20),
(882, 'steam:1100001139319c1', 'vodka', 0),
(883, 'steam:1100001139319c1', 'WEAPON_PUMPSHOTGUN', 0),
(884, 'steam:1100001139319c1', 'cocaine', 0),
(885, 'steam:1100001139319c1', 'dabs', 0),
(886, 'steam:1100001139319c1', 'carotool', 0),
(887, 'steam:1100001139319c1', 'cannabis', 0),
(888, 'steam:1100001139319c1', 'fixkit', 0),
(889, 'steam:1100001139319c1', 'cutted_wood', 0),
(890, 'steam:1100001139319c1', 'iron', 0),
(891, 'steam:1100001139319c1', 'alive_chicken', 0),
(892, 'steam:1100001139319c1', 'gazbottle', 0),
(893, 'steam:1100001139319c1', 'beer', 0),
(894, 'steam:1100001139319c1', 'WEAPON_KNIFE', 0),
(895, 'steam:1100001139319c1', 'wool', 0),
(896, 'steam:1100001139319c1', 'crack', 0),
(897, 'steam:1100001139319c1', 'whiskey', 0),
(898, 'steam:1100001139319c1', 'fakepee', 0),
(899, 'steam:1100001139319c1', 'pcp', 0),
(900, 'steam:1100001139319c1', 'vhamburger', 0),
(901, 'steam:1100001139319c1', 'vbread', 0),
(902, 'steam:1100001139319c1', 'tequila', 0),
(903, 'steam:1100001139319c1', 'poppy', 0),
(904, 'steam:1100001139319c1', 'WEAPON_STUNGUN', 0),
(905, 'steam:1100001139319c1', 'shotgun_shells', 0),
(906, 'steam:1100001139319c1', 'heroine', 0),
(907, 'steam:1100001139319c1', 'ephedra', 0),
(908, 'steam:1100001139319c1', 'bandage', 0),
(909, 'steam:1100001139319c1', 'medikit', 0),
(910, 'steam:1100001139319c1', 'bread', 18),
(911, 'steam:1100001139319c1', 'potato', 0),
(912, 'steam:1100001139319c1', 'petrol_raffin', 0),
(913, 'steam:1100001139319c1', 'clothe', 0),
(914, 'steam:1100001139319c1', 'ephedrine', 0),
(915, 'steam:1100001139319c1', 'fvburger', 0),
(916, 'steam:1100001139319c1', 'coca', 0),
(917, 'steam:1100001139319c1', 'washed_stone', 0),
(918, 'steam:1100001139319c1', 'WEAPON_BAT', 0),
(919, 'steam:1100001139319c1', 'ctomato', 0),
(920, 'steam:1100001139319c1', 'painkiller', 0),
(921, 'steam:1100001139319c1', 'copper', 0),
(922, 'steam:1100001139319c1', 'fish', 0),
(923, 'steam:1100001139319c1', 'ccheese', 0),
(924, 'steam:1100001139319c1', 'WEAPON_FLASHLIGHT', 0),
(925, 'steam:1100001139319c1', 'gold', 0),
(926, 'steam:1100001139319c1', 'WEAPON_PISTOL', 0),
(927, 'steam:1100001139319c1', 'narcan', 0),
(928, 'steam:1100001139319c1', 'meth', 0),
(929, 'steam:1100001139319c1', 'diamond', 0),
(930, 'steam:1100001139319c1', 'wood', 0),
(931, 'steam:1100001139319c1', 'nuggets10', 0),
(932, 'steam:1100001139319c1', 'blowpipe', 0),
(933, 'steam:1100001139319c1', 'fixtool', 0),
(934, 'steam:1100001139319c1', '9mm_rounds', 0),
(935, 'steam:1100001139319c1', 'essence', 0),
(936, 'steam:1100001139319c1', 'stone', 0),
(937, 'steam:1100001139319c1', 'shamburger', 0),
(938, 'steam:110000132580eb0', 'crack', 0),
(939, 'steam:110000132580eb0', 'vodka', 0),
(940, 'steam:110000132580eb0', 'fakepee', 0),
(941, 'steam:110000132580eb0', 'breathalyzer', 0),
(942, 'steam:110000132580eb0', 'cheese', 0),
(943, 'steam:110000132580eb0', 'medikit', 0),
(944, 'steam:110000132580eb0', 'fvburger', 0),
(945, 'steam:110000132580eb0', 'nugget', 0),
(946, 'steam:110000132580eb0', 'shamburger', 0),
(947, 'steam:110000132580eb0', 'stone', 0),
(948, 'steam:110000132580eb0', 'cutted_wood', 0),
(949, 'steam:110000132580eb0', 'vhamburger', 0),
(950, 'steam:110000132580eb0', 'nuggets4', 0),
(951, 'steam:110000132580eb0', 'coca', 0),
(952, 'steam:110000132580eb0', 'cocaine', 0),
(953, 'steam:110000132580eb0', 'heroine', 0),
(954, 'steam:110000132580eb0', 'blowpipe', 0),
(955, 'steam:110000132580eb0', 'opium', 0),
(956, 'steam:110000132580eb0', 'copper', 0),
(957, 'steam:110000132580eb0', 'pcp', 0),
(958, 'steam:110000132580eb0', 'poppy', 0),
(959, 'steam:110000132580eb0', 'ephedra', 0),
(960, 'steam:110000132580eb0', 'wood', 0),
(961, 'steam:110000132580eb0', 'fixkit', 0),
(962, 'steam:110000132580eb0', 'painkiller', 0),
(963, 'steam:110000132580eb0', 'packaged_chicken', 0),
(964, 'steam:110000132580eb0', 'tomato', 0),
(965, 'steam:110000132580eb0', 'carokit', 0),
(966, 'steam:110000132580eb0', '9mm_rounds', 0),
(967, 'steam:110000132580eb0', 'clothe', 0),
(968, 'steam:110000132580eb0', 'packaged_plank', 0),
(969, 'steam:110000132580eb0', 'essence', 0),
(970, 'steam:110000132580eb0', 'ctomato', 0),
(971, 'steam:110000132580eb0', 'WEAPON_FLASHLIGHT', 0),
(972, 'steam:110000132580eb0', 'fburger', 0),
(973, 'steam:110000132580eb0', 'WEAPON_STUNGUN', 0),
(974, 'steam:110000132580eb0', 'alive_chicken', 0),
(975, 'steam:110000132580eb0', 'wool', 0),
(976, 'steam:110000132580eb0', 'vbread', 0),
(977, 'steam:110000132580eb0', 'WEAPON_KNIFE', 0),
(978, 'steam:110000132580eb0', 'water', 17),
(979, 'steam:110000132580eb0', 'potato', 0),
(980, 'steam:110000132580eb0', 'shotgun_shells', 0),
(981, 'steam:110000132580eb0', 'tequila', 0),
(982, 'steam:110000132580eb0', 'narcan', 0),
(983, 'steam:110000132580eb0', 'ephedrine', 0),
(984, 'steam:110000132580eb0', 'slaughtered_chicken', 0),
(985, 'steam:110000132580eb0', 'ccheese', 0),
(986, 'steam:110000132580eb0', 'iron', 0),
(987, 'steam:110000132580eb0', 'washed_stone', 0),
(988, 'steam:110000132580eb0', 'lettuce', 0),
(989, 'steam:110000132580eb0', 'WEAPON_PUMPSHOTGUN', 0),
(990, 'steam:110000132580eb0', 'fabric', 0),
(991, 'steam:110000132580eb0', 'drugtest', 0),
(992, 'steam:110000132580eb0', 'carotool', 0),
(993, 'steam:110000132580eb0', 'petrol_raffin', 0),
(994, 'steam:110000132580eb0', 'diamond', 0),
(995, 'steam:110000132580eb0', 'gazbottle', 0),
(996, 'steam:110000132580eb0', 'petrol', 0),
(997, 'steam:110000132580eb0', 'meth', 0),
(998, 'steam:110000132580eb0', 'whiskey', 0),
(999, 'steam:110000132580eb0', 'WEAPON_BAT', 0),
(1000, 'steam:110000132580eb0', 'nuggets10', 0),
(1001, 'steam:110000132580eb0', 'beer', 0),
(1002, 'steam:110000132580eb0', 'dabs', 0),
(1003, 'steam:110000132580eb0', 'fixtool', 0),
(1004, 'steam:110000132580eb0', 'bread', 87),
(1005, 'steam:110000132580eb0', 'marijuana', 0),
(1006, 'steam:110000132580eb0', 'fish', 0),
(1007, 'steam:110000132580eb0', 'cannabis', 0),
(1008, 'steam:110000132580eb0', 'clettuce', 0),
(1009, 'steam:110000132580eb0', 'bandage', 0),
(1010, 'steam:110000132580eb0', 'WEAPON_PISTOL', 0),
(1011, 'steam:110000132580eb0', 'gold', 0),
(1012, 'steam:1100001081d1893', 'fburger', 0),
(1013, 'steam:1100001081d1893', 'tomato', 0),
(1014, 'steam:1100001081d1893', 'cheese', 0),
(1015, 'steam:1100001081d1893', 'fvburger', 0),
(1016, 'steam:1100001081d1893', 'nuggets4', 0),
(1017, 'steam:1100001081d1893', 'shamburger', 0),
(1018, 'steam:1100001081d1893', 'vhamburger', 0),
(1019, 'steam:1100001081d1893', 'clettuce', 0),
(1020, 'steam:1100001081d1893', 'ctomato', 0),
(1021, 'steam:1100001081d1893', 'nugget', 0),
(1022, 'steam:1100001081d1893', 'potato', 0),
(1023, 'steam:1100001081d1893', 'ccheese', 0),
(1024, 'steam:1100001081d1893', 'vbread', 0),
(1025, 'steam:1100001081d1893', 'nuggets10', 0),
(1026, 'steam:1100001081d1893', 'lettuce', 0),
(1027, 'steam:11000010b49a743', 'tequila', 0),
(1028, 'steam:11000010b49a743', 'clettuce', 0),
(1029, 'steam:11000010b49a743', 'meth', 0),
(1030, 'steam:11000010b49a743', 'ctomato', 0),
(1031, 'steam:11000010b49a743', 'drugtest', 0),
(1032, 'steam:11000010b49a743', 'fakepee', 0),
(1033, 'steam:11000010b49a743', 'cheese', 0),
(1034, 'steam:11000010b49a743', 'ccheese', 0),
(1035, 'steam:11000010b49a743', 'ephedra', 0),
(1036, 'steam:11000010b49a743', 'lettuce', 0),
(1037, 'steam:11000010b49a743', 'fburger', 0),
(1038, 'steam:11000010b49a743', 'fvburger', 0),
(1039, 'steam:11000010b49a743', 'ephedrine', 0),
(1040, 'steam:11000010b49a743', 'breathalyzer', 0),
(1041, 'steam:11000010b49a743', 'painkiller', 0),
(1042, 'steam:11000010b49a743', 'dabs', 0),
(1043, 'steam:11000010b49a743', 'whiskey', 0),
(1044, 'steam:11000010b49a743', 'vodka', 0),
(1045, 'steam:11000010b49a743', 'coca', 0),
(1046, 'steam:11000010b49a743', 'vhamburger', 0),
(1047, 'steam:11000010b49a743', 'beer', 0),
(1048, 'steam:11000010b49a743', 'vbread', 0),
(1049, 'steam:11000010b49a743', 'poppy', 0),
(1050, 'steam:11000010b49a743', 'tomato', 0),
(1051, 'steam:11000010b49a743', 'nugget', 0),
(1052, 'steam:11000010b49a743', 'shamburger', 0),
(1053, 'steam:11000010b49a743', 'crack', 0),
(1054, 'steam:11000010b49a743', 'potato', 0),
(1055, 'steam:11000010b49a743', 'opium', 0),
(1056, 'steam:11000010b49a743', 'pcp', 0),
(1057, 'steam:11000010b49a743', 'nuggets4', 0),
(1058, 'steam:11000010b49a743', 'nuggets10', 0),
(1059, 'steam:11000010b49a743', 'cocaine', 0),
(1060, 'steam:11000010b49a743', 'heroine', 0),
(1061, 'steam:11000010b49a743', 'narcan', 0),
(1062, 'steam:110000119e7384d', 'fish', 0),
(1063, 'steam:110000119e7384d', 'washed_stone', 0),
(1064, 'steam:110000119e7384d', 'tequila', 0),
(1065, 'steam:110000119e7384d', 'WEAPON_KNIFE', 0),
(1066, 'steam:110000119e7384d', 'fabric', 0),
(1067, 'steam:110000119e7384d', 'clettuce', 0),
(1068, 'steam:110000119e7384d', 'diamond', 0),
(1069, 'steam:110000119e7384d', 'petrol_raffin', 0),
(1070, 'steam:110000119e7384d', 'cutted_wood', 0),
(1071, 'steam:110000119e7384d', 'carotool', 0),
(1072, 'steam:110000119e7384d', 'meth', 0),
(1073, 'steam:110000119e7384d', 'gold', 0),
(1074, 'steam:110000119e7384d', 'ctomato', 0),
(1075, 'steam:110000119e7384d', 'blowpipe', 0),
(1076, 'steam:110000119e7384d', 'medikit', 0),
(1077, 'steam:110000119e7384d', 'essence', 0),
(1078, 'steam:110000119e7384d', 'drugtest', 0),
(1079, 'steam:110000119e7384d', 'fakepee', 0),
(1080, 'steam:110000119e7384d', 'cheese', 0),
(1081, 'steam:110000119e7384d', 'fixtool', 0),
(1082, 'steam:110000119e7384d', 'WEAPON_PUMPSHOTGUN', 0),
(1083, 'steam:110000119e7384d', 'ccheese', 0),
(1084, 'steam:110000119e7384d', 'ephedra', 0),
(1085, 'steam:110000119e7384d', 'marijuana', 0),
(1086, 'steam:110000119e7384d', 'water', 0),
(1087, 'steam:110000119e7384d', 'lettuce', 0),
(1088, 'steam:110000119e7384d', 'bread', 0),
(1089, 'steam:110000119e7384d', 'fburger', 0),
(1090, 'steam:110000119e7384d', '9mm_rounds', 0),
(1091, 'steam:110000119e7384d', 'petrol', 0),
(1092, 'steam:110000119e7384d', 'fvburger', 0),
(1093, 'steam:110000119e7384d', 'cannabis', 0),
(1094, 'steam:110000119e7384d', 'ephedrine', 0),
(1095, 'steam:110000119e7384d', 'breathalyzer', 0),
(1096, 'steam:110000119e7384d', 'WEAPON_BAT', 0),
(1097, 'steam:110000119e7384d', 'packaged_chicken', 0),
(1098, 'steam:110000119e7384d', 'painkiller', 0),
(1099, 'steam:110000119e7384d', 'dabs', 0),
(1100, 'steam:110000119e7384d', 'wool', 0),
(1101, 'steam:110000119e7384d', 'WEAPON_PISTOL', 0),
(1102, 'steam:110000119e7384d', 'slaughtered_chicken', 0),
(1103, 'steam:110000119e7384d', 'wood', 0),
(1104, 'steam:110000119e7384d', 'packaged_plank', 0),
(1105, 'steam:110000119e7384d', 'whiskey', 0),
(1106, 'steam:110000119e7384d', 'vodka', 0),
(1107, 'steam:110000119e7384d', 'coca', 0),
(1108, 'steam:110000119e7384d', 'vhamburger', 0),
(1109, 'steam:110000119e7384d', 'beer', 0),
(1110, 'steam:110000119e7384d', 'vbread', 0),
(1111, 'steam:110000119e7384d', 'tomato', 0),
(1112, 'steam:110000119e7384d', 'poppy', 0),
(1113, 'steam:110000119e7384d', 'WEAPON_STUNGUN', 0),
(1114, 'steam:110000119e7384d', 'iron', 0),
(1115, 'steam:110000119e7384d', 'stone', 0),
(1116, 'steam:110000119e7384d', 'carokit', 0),
(1117, 'steam:110000119e7384d', 'nugget', 0),
(1118, 'steam:110000119e7384d', 'shotgun_shells', 0),
(1119, 'steam:110000119e7384d', 'WEAPON_FLASHLIGHT', 0),
(1120, 'steam:110000119e7384d', 'copper', 0),
(1121, 'steam:110000119e7384d', 'shamburger', 0),
(1122, 'steam:110000119e7384d', 'gazbottle', 0),
(1123, 'steam:110000119e7384d', 'crack', 0),
(1124, 'steam:110000119e7384d', 'alive_chicken', 0),
(1125, 'steam:110000119e7384d', 'pcp', 0),
(1126, 'steam:110000119e7384d', 'potato', 0),
(1127, 'steam:110000119e7384d', 'clothe', 0),
(1128, 'steam:110000119e7384d', 'opium', 0),
(1129, 'steam:110000119e7384d', 'nuggets4', 0),
(1130, 'steam:110000119e7384d', 'nuggets10', 0),
(1131, 'steam:110000119e7384d', 'cocaine', 0),
(1132, 'steam:110000119e7384d', 'heroine', 0),
(1133, 'steam:110000119e7384d', 'narcan', 0),
(1134, 'steam:110000119e7384d', 'bandage', 0),
(1135, 'steam:110000119e7384d', 'fixkit', 0),
(1136, 'steam:110000139d9adf9', 'fish', 0);
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(1137, 'steam:110000139d9adf9', 'washed_stone', 0),
(1138, 'steam:110000139d9adf9', 'tequila', 0),
(1139, 'steam:110000139d9adf9', 'WEAPON_KNIFE', 0),
(1140, 'steam:110000139d9adf9', 'fabric', 0),
(1141, 'steam:110000139d9adf9', 'clettuce', 0),
(1142, 'steam:110000139d9adf9', 'diamond', 0),
(1143, 'steam:110000139d9adf9', 'petrol_raffin', 0),
(1144, 'steam:110000139d9adf9', 'cutted_wood', 0),
(1145, 'steam:110000139d9adf9', 'carotool', 0),
(1146, 'steam:110000139d9adf9', 'meth', 0),
(1147, 'steam:110000139d9adf9', 'gold', 0),
(1148, 'steam:110000139d9adf9', 'ctomato', 0),
(1149, 'steam:110000139d9adf9', 'blowpipe', 0),
(1150, 'steam:110000139d9adf9', 'medikit', 0),
(1151, 'steam:110000139d9adf9', 'essence', 0),
(1152, 'steam:110000139d9adf9', 'drugtest', 0),
(1153, 'steam:110000139d9adf9', 'fakepee', 0),
(1154, 'steam:110000139d9adf9', 'cheese', 0),
(1155, 'steam:110000139d9adf9', 'fixtool', 0),
(1156, 'steam:110000139d9adf9', 'WEAPON_PUMPSHOTGUN', 0),
(1157, 'steam:110000139d9adf9', 'ccheese', 0),
(1158, 'steam:110000139d9adf9', 'ephedra', 0),
(1159, 'steam:110000139d9adf9', 'marijuana', 0),
(1160, 'steam:110000139d9adf9', 'water', 0),
(1161, 'steam:110000139d9adf9', 'lettuce', 0),
(1162, 'steam:110000139d9adf9', 'petrol', 0),
(1163, 'steam:110000139d9adf9', 'bread', 0),
(1164, 'steam:110000139d9adf9', 'fburger', 0),
(1165, 'steam:110000139d9adf9', '9mm_rounds', 0),
(1166, 'steam:110000139d9adf9', 'fvburger', 0),
(1167, 'steam:110000139d9adf9', 'cannabis', 0),
(1168, 'steam:110000139d9adf9', 'breathalyzer', 0),
(1169, 'steam:110000139d9adf9', 'ephedrine', 0),
(1170, 'steam:110000139d9adf9', 'WEAPON_BAT', 0),
(1171, 'steam:110000139d9adf9', 'packaged_chicken', 0),
(1172, 'steam:110000139d9adf9', 'painkiller', 0),
(1173, 'steam:110000139d9adf9', 'dabs', 0),
(1174, 'steam:110000139d9adf9', 'wool', 0),
(1175, 'steam:110000139d9adf9', 'WEAPON_PISTOL', 0),
(1176, 'steam:110000139d9adf9', 'slaughtered_chicken', 0),
(1177, 'steam:110000139d9adf9', 'wood', 0),
(1178, 'steam:110000139d9adf9', 'packaged_plank', 0),
(1179, 'steam:110000139d9adf9', 'vodka', 0),
(1180, 'steam:110000139d9adf9', 'whiskey', 0),
(1181, 'steam:110000139d9adf9', 'coca', 0),
(1182, 'steam:110000139d9adf9', 'vhamburger', 0),
(1183, 'steam:110000139d9adf9', 'vbread', 0),
(1184, 'steam:110000139d9adf9', 'beer', 0),
(1185, 'steam:110000139d9adf9', 'tomato', 0),
(1186, 'steam:110000139d9adf9', 'poppy', 0),
(1187, 'steam:110000139d9adf9', 'iron', 0),
(1188, 'steam:110000139d9adf9', 'WEAPON_STUNGUN', 0),
(1189, 'steam:110000139d9adf9', 'stone', 0),
(1190, 'steam:110000139d9adf9', 'carokit', 0),
(1191, 'steam:110000139d9adf9', 'nugget', 0),
(1192, 'steam:110000139d9adf9', 'shotgun_shells', 0),
(1193, 'steam:110000139d9adf9', 'WEAPON_FLASHLIGHT', 0),
(1194, 'steam:110000139d9adf9', 'copper', 0),
(1195, 'steam:110000139d9adf9', 'shamburger', 0),
(1196, 'steam:110000139d9adf9', 'gazbottle', 0),
(1197, 'steam:110000139d9adf9', 'crack', 0),
(1198, 'steam:110000139d9adf9', 'alive_chicken', 0),
(1199, 'steam:110000139d9adf9', 'pcp', 0),
(1200, 'steam:110000139d9adf9', 'potato', 0),
(1201, 'steam:110000139d9adf9', 'clothe', 0),
(1202, 'steam:110000139d9adf9', 'opium', 0),
(1203, 'steam:110000139d9adf9', 'nuggets4', 0),
(1204, 'steam:110000139d9adf9', 'nuggets10', 0),
(1205, 'steam:110000139d9adf9', 'cocaine', 0),
(1206, 'steam:110000139d9adf9', 'heroine', 0),
(1207, 'steam:110000139d9adf9', 'narcan', 0),
(1208, 'steam:110000139d9adf9', 'bandage', 0),
(1209, 'steam:110000139d9adf9', 'fixkit', 0),
(1210, 'steam:11000013eaca305', 'fish', 0),
(1211, 'steam:11000013eaca305', 'washed_stone', 0),
(1212, 'steam:11000013eaca305', 'tequila', 0),
(1213, 'steam:11000013eaca305', 'WEAPON_KNIFE', 0),
(1214, 'steam:11000013eaca305', 'fabric', 0),
(1215, 'steam:11000013eaca305', 'clettuce', 0),
(1216, 'steam:11000013eaca305', 'diamond', 0),
(1217, 'steam:11000013eaca305', 'petrol_raffin', 0),
(1218, 'steam:11000013eaca305', 'cutted_wood', 0),
(1219, 'steam:11000013eaca305', 'carotool', 0),
(1220, 'steam:11000013eaca305', 'meth', 0),
(1221, 'steam:11000013eaca305', 'gold', 0),
(1222, 'steam:11000013eaca305', 'ctomato', 0),
(1223, 'steam:11000013eaca305', 'blowpipe', 0),
(1224, 'steam:11000013eaca305', 'medikit', 0),
(1225, 'steam:11000013eaca305', 'essence', 0),
(1226, 'steam:11000013eaca305', 'drugtest', 0),
(1227, 'steam:11000013eaca305', 'fakepee', 0),
(1228, 'steam:11000013eaca305', 'cheese', 0),
(1229, 'steam:11000013eaca305', 'fixtool', 0),
(1230, 'steam:11000013eaca305', 'WEAPON_PUMPSHOTGUN', 0),
(1231, 'steam:11000013eaca305', 'ccheese', 0),
(1232, 'steam:11000013eaca305', 'ephedra', 0),
(1233, 'steam:11000013eaca305', 'marijuana', 0),
(1234, 'steam:11000013eaca305', 'water', 0),
(1235, 'steam:11000013eaca305', 'lettuce', 0),
(1236, 'steam:11000013eaca305', 'petrol', 0),
(1237, 'steam:11000013eaca305', 'bread', 0),
(1238, 'steam:11000013eaca305', 'fburger', 0),
(1239, 'steam:11000013eaca305', '9mm_rounds', 0),
(1240, 'steam:11000013eaca305', 'fvburger', 0),
(1241, 'steam:11000013eaca305', 'cannabis', 0),
(1242, 'steam:11000013eaca305', 'ephedrine', 0),
(1243, 'steam:11000013eaca305', 'breathalyzer', 0),
(1244, 'steam:11000013eaca305', 'WEAPON_BAT', 0),
(1245, 'steam:11000013eaca305', 'packaged_chicken', 0),
(1246, 'steam:11000013eaca305', 'painkiller', 0),
(1247, 'steam:11000013eaca305', 'dabs', 0),
(1248, 'steam:11000013eaca305', 'wool', 0),
(1249, 'steam:11000013eaca305', 'WEAPON_PISTOL', 0),
(1250, 'steam:11000013eaca305', 'slaughtered_chicken', 0),
(1251, 'steam:11000013eaca305', 'wood', 0),
(1252, 'steam:11000013eaca305', 'packaged_plank', 0),
(1253, 'steam:11000013eaca305', 'whiskey', 0),
(1254, 'steam:11000013eaca305', 'vodka', 0),
(1255, 'steam:11000013eaca305', 'coca', 0),
(1256, 'steam:11000013eaca305', 'vhamburger', 0),
(1257, 'steam:11000013eaca305', 'beer', 0),
(1258, 'steam:11000013eaca305', 'vbread', 0),
(1259, 'steam:11000013eaca305', 'tomato', 0),
(1260, 'steam:11000013eaca305', 'poppy', 0),
(1261, 'steam:11000013eaca305', 'iron', 0),
(1262, 'steam:11000013eaca305', 'WEAPON_STUNGUN', 0),
(1263, 'steam:11000013eaca305', 'stone', 0),
(1264, 'steam:11000013eaca305', 'carokit', 0),
(1265, 'steam:11000013eaca305', 'nugget', 0),
(1266, 'steam:11000013eaca305', 'shotgun_shells', 0),
(1267, 'steam:11000013eaca305', 'WEAPON_FLASHLIGHT', 0),
(1268, 'steam:11000013eaca305', 'copper', 0),
(1269, 'steam:11000013eaca305', 'shamburger', 0),
(1270, 'steam:11000013eaca305', 'gazbottle', 0),
(1271, 'steam:11000013eaca305', 'crack', 0),
(1272, 'steam:11000013eaca305', 'alive_chicken', 0),
(1273, 'steam:11000013eaca305', 'pcp', 0),
(1274, 'steam:11000013eaca305', 'potato', 0),
(1275, 'steam:11000013eaca305', 'clothe', 0),
(1276, 'steam:11000013eaca305', 'opium', 0),
(1277, 'steam:11000013eaca305', 'nuggets10', 0),
(1278, 'steam:11000013eaca305', 'nuggets4', 0),
(1279, 'steam:11000013eaca305', 'cocaine', 0),
(1280, 'steam:11000013eaca305', 'heroine', 0),
(1281, 'steam:11000013eaca305', 'narcan', 0),
(1282, 'steam:11000013eaca305', 'bandage', 0),
(1283, 'steam:11000013eaca305', 'fixkit', 0),
(1284, 'steam:11000013efa68e1', 'fish', 0),
(1285, 'steam:11000013efa68e1', 'washed_stone', 0),
(1286, 'steam:11000013efa68e1', 'tequila', 0),
(1287, 'steam:11000013efa68e1', 'WEAPON_KNIFE', 0),
(1288, 'steam:11000013efa68e1', 'fabric', 0),
(1289, 'steam:11000013efa68e1', 'clettuce', 0),
(1290, 'steam:11000013efa68e1', 'diamond', 0),
(1291, 'steam:11000013efa68e1', 'petrol_raffin', 0),
(1292, 'steam:11000013efa68e1', 'cutted_wood', 0),
(1293, 'steam:11000013efa68e1', 'carotool', 0),
(1294, 'steam:11000013efa68e1', 'meth', 0),
(1295, 'steam:11000013efa68e1', 'gold', 0),
(1296, 'steam:11000013efa68e1', 'ctomato', 0),
(1297, 'steam:11000013efa68e1', 'medikit', 0),
(1298, 'steam:11000013efa68e1', 'blowpipe', 0),
(1299, 'steam:11000013efa68e1', 'essence', 0),
(1300, 'steam:11000013efa68e1', 'drugtest', 0),
(1301, 'steam:11000013efa68e1', 'fakepee', 0),
(1302, 'steam:11000013efa68e1', 'cheese', 0),
(1303, 'steam:11000013efa68e1', 'fixtool', 0),
(1304, 'steam:11000013efa68e1', 'WEAPON_PUMPSHOTGUN', 0),
(1305, 'steam:11000013efa68e1', 'ccheese', 0),
(1306, 'steam:11000013efa68e1', 'ephedra', 0),
(1307, 'steam:11000013efa68e1', 'marijuana', 0),
(1308, 'steam:11000013efa68e1', 'water', 0),
(1309, 'steam:11000013efa68e1', 'lettuce', 0),
(1310, 'steam:11000013efa68e1', 'petrol', 0),
(1311, 'steam:11000013efa68e1', 'bread', 0),
(1312, 'steam:11000013efa68e1', 'fburger', 0),
(1313, 'steam:11000013efa68e1', '9mm_rounds', 0),
(1314, 'steam:11000013efa68e1', 'fvburger', 0),
(1315, 'steam:11000013efa68e1', 'cannabis', 0),
(1316, 'steam:11000013efa68e1', 'ephedrine', 0),
(1317, 'steam:11000013efa68e1', 'breathalyzer', 0),
(1318, 'steam:11000013efa68e1', 'WEAPON_BAT', 0),
(1319, 'steam:11000013efa68e1', 'packaged_chicken', 0),
(1320, 'steam:11000013efa68e1', 'painkiller', 0),
(1321, 'steam:11000013efa68e1', 'wool', 0),
(1322, 'steam:11000013efa68e1', 'dabs', 0),
(1323, 'steam:11000013efa68e1', 'wood', 0),
(1324, 'steam:11000013efa68e1', 'WEAPON_PISTOL', 0),
(1325, 'steam:11000013efa68e1', 'slaughtered_chicken', 0),
(1326, 'steam:11000013efa68e1', 'packaged_plank', 0),
(1327, 'steam:11000013efa68e1', 'whiskey', 0),
(1328, 'steam:11000013efa68e1', 'vodka', 0),
(1329, 'steam:11000013efa68e1', 'coca', 0),
(1330, 'steam:11000013efa68e1', 'vhamburger', 0),
(1331, 'steam:11000013efa68e1', 'beer', 0),
(1332, 'steam:11000013efa68e1', 'vbread', 0),
(1333, 'steam:11000013efa68e1', 'tomato', 0),
(1334, 'steam:11000013efa68e1', 'poppy', 0),
(1335, 'steam:11000013efa68e1', 'iron', 0),
(1336, 'steam:11000013efa68e1', 'WEAPON_STUNGUN', 0),
(1337, 'steam:11000013efa68e1', 'stone', 0),
(1338, 'steam:11000013efa68e1', 'carokit', 0),
(1339, 'steam:11000013efa68e1', 'nugget', 0),
(1340, 'steam:11000013efa68e1', 'shotgun_shells', 0),
(1341, 'steam:11000013efa68e1', 'WEAPON_FLASHLIGHT', 0),
(1342, 'steam:11000013efa68e1', 'copper', 0),
(1343, 'steam:11000013efa68e1', 'shamburger', 0),
(1344, 'steam:11000013efa68e1', 'gazbottle', 0),
(1345, 'steam:11000013efa68e1', 'crack', 0),
(1346, 'steam:11000013efa68e1', 'alive_chicken', 0),
(1347, 'steam:11000013efa68e1', 'pcp', 0),
(1348, 'steam:11000013efa68e1', 'potato', 0),
(1349, 'steam:11000013efa68e1', 'clothe', 0),
(1350, 'steam:11000013efa68e1', 'opium', 0),
(1351, 'steam:11000013efa68e1', 'nuggets4', 0),
(1352, 'steam:11000013efa68e1', 'nuggets10', 0),
(1353, 'steam:11000013efa68e1', 'cocaine', 0),
(1354, 'steam:11000013efa68e1', 'heroine', 0),
(1355, 'steam:11000013efa68e1', 'narcan', 0),
(1356, 'steam:11000013efa68e1', 'bandage', 0),
(1357, 'steam:11000013efa68e1', 'fixkit', 0),
(1358, 'steam:1100001176cddfb', 'WEAPON_PUMPSHOTGUN', 0),
(1359, 'steam:1100001176cddfb', 'copper', 0),
(1360, 'steam:1100001176cddfb', 'WEAPON_FLASHLIGHT', 0),
(1361, 'steam:1100001176cddfb', 'packaged_plank', 0),
(1362, 'steam:1100001176cddfb', 'carokit', 0),
(1363, 'steam:1100001176cddfb', 'ctomato', 0),
(1364, 'steam:1100001176cddfb', 'heroine', 0),
(1365, 'steam:1100001176cddfb', 'whiskey', 0),
(1366, 'steam:1100001176cddfb', 'cutted_wood', 0),
(1367, 'steam:1100001176cddfb', 'wood', 0),
(1368, 'steam:1100001176cddfb', 'bread', 0),
(1369, 'steam:1100001176cddfb', 'alive_chicken', 0),
(1370, 'steam:1100001176cddfb', 'shamburger', 0),
(1371, 'steam:1100001176cddfb', 'WEAPON_KNIFE', 0),
(1372, 'steam:1100001176cddfb', 'vhamburger', 0),
(1373, 'steam:1100001176cddfb', 'marijuana', 0),
(1374, 'steam:1100001176cddfb', 'fvburger', 0),
(1375, 'steam:1100001176cddfb', 'shotgun_shells', 0),
(1376, 'steam:1100001176cddfb', 'breathalyzer', 0),
(1377, 'steam:1100001176cddfb', 'fish', 0),
(1378, 'steam:1100001176cddfb', 'gold', 0),
(1379, 'steam:1100001176cddfb', 'fburger', 0),
(1380, 'steam:1100001176cddfb', 'essence', 0),
(1381, 'steam:1100001176cddfb', 'stone', 0),
(1382, 'steam:1100001176cddfb', 'opium', 0),
(1383, 'steam:1100001176cddfb', 'blowpipe', 0),
(1384, 'steam:1100001176cddfb', 'beer', 0),
(1385, 'steam:1100001176cddfb', 'nuggets4', 0),
(1386, 'steam:1100001176cddfb', 'WEAPON_PISTOL', 0),
(1387, 'steam:1100001176cddfb', 'cheese', 0),
(1388, 'steam:1100001176cddfb', 'meth', 0),
(1389, 'steam:1100001176cddfb', 'fixkit', 0),
(1390, 'steam:1100001176cddfb', 'wool', 0),
(1391, 'steam:1100001176cddfb', 'clothe', 0),
(1392, 'steam:1100001176cddfb', 'fakepee', 0),
(1393, 'steam:1100001176cddfb', 'cannabis', 0),
(1394, 'steam:1100001176cddfb', 'carotool', 0),
(1395, 'steam:1100001176cddfb', 'WEAPON_STUNGUN', 0),
(1396, 'steam:1100001176cddfb', 'fixtool', 0),
(1397, 'steam:1100001176cddfb', 'cocaine', 0),
(1398, 'steam:1100001176cddfb', 'vodka', 0),
(1399, 'steam:1100001176cddfb', 'water', 0),
(1400, 'steam:1100001176cddfb', 'vbread', 0),
(1401, 'steam:1100001176cddfb', 'poppy', 0),
(1402, 'steam:1100001176cddfb', 'petrol_raffin', 0),
(1403, 'steam:1100001176cddfb', 'washed_stone', 0),
(1404, 'steam:1100001176cddfb', 'lettuce', 0),
(1405, 'steam:1100001176cddfb', 'medikit', 0),
(1406, 'steam:1100001176cddfb', 'tequila', 0),
(1407, 'steam:1100001176cddfb', 'bandage', 0),
(1408, 'steam:1100001176cddfb', 'coca', 0),
(1409, 'steam:1100001176cddfb', 'crack', 0),
(1410, 'steam:1100001176cddfb', 'slaughtered_chicken', 0),
(1411, 'steam:1100001176cddfb', 'ephedrine', 0),
(1412, 'steam:1100001176cddfb', 'nugget', 0),
(1413, 'steam:1100001176cddfb', '9mm_rounds', 0),
(1414, 'steam:1100001176cddfb', 'dabs', 0),
(1415, 'steam:1100001176cddfb', 'narcan', 0),
(1416, 'steam:1100001176cddfb', 'ccheese', 0),
(1417, 'steam:1100001176cddfb', 'petrol', 0),
(1418, 'steam:1100001176cddfb', 'packaged_chicken', 0),
(1419, 'steam:1100001176cddfb', 'pcp', 0),
(1420, 'steam:1100001176cddfb', 'gazbottle', 0),
(1421, 'steam:1100001176cddfb', 'WEAPON_BAT', 0),
(1422, 'steam:1100001176cddfb', 'painkiller', 0),
(1423, 'steam:1100001176cddfb', 'diamond', 0),
(1424, 'steam:1100001176cddfb', 'fabric', 0),
(1425, 'steam:1100001176cddfb', 'potato', 0),
(1426, 'steam:1100001176cddfb', 'nuggets10', 0),
(1427, 'steam:1100001176cddfb', 'ephedra', 0),
(1428, 'steam:1100001176cddfb', 'tomato', 0),
(1429, 'steam:1100001176cddfb', 'iron', 0),
(1430, 'steam:1100001176cddfb', 'clettuce', 0),
(1431, 'steam:1100001176cddfb', 'drugtest', 0),
(1432, 'steam:11000010fe595d0', 'ctomato', 0),
(1433, 'steam:11000010fe595d0', 'shamburger', 0),
(1434, 'steam:11000010fe595d0', 'vhamburger', 0),
(1435, 'steam:11000010fe595d0', 'fvburger', 0),
(1436, 'steam:11000010fe595d0', 'fburger', 0),
(1437, 'steam:11000010fe595d0', 'nuggets4', 0),
(1438, 'steam:11000010fe595d0', 'cheese', 0),
(1439, 'steam:11000010fe595d0', 'vbread', 0),
(1440, 'steam:11000010fe595d0', 'lettuce', 0),
(1441, 'steam:11000010fe595d0', 'nugget', 0),
(1442, 'steam:11000010fe595d0', 'ccheese', 0),
(1443, 'steam:11000010fe595d0', 'potato', 0),
(1444, 'steam:11000010fe595d0', 'nuggets10', 0),
(1445, 'steam:11000010fe595d0', 'tomato', 0),
(1446, 'steam:11000010fe595d0', 'clettuce', 0),
(1447, 'steam:11000013f6b85e1', 'WEAPON_PUMPSHOTGUN', 0),
(1448, 'steam:11000013f6b85e1', 'copper', 0),
(1449, 'steam:11000013f6b85e1', 'WEAPON_FLASHLIGHT', 0),
(1450, 'steam:11000013f6b85e1', 'carokit', 0),
(1451, 'steam:11000013f6b85e1', 'packaged_plank', 0),
(1452, 'steam:11000013f6b85e1', 'ctomato', 0),
(1453, 'steam:11000013f6b85e1', 'heroine', 0),
(1454, 'steam:11000013f6b85e1', 'whiskey', 0),
(1455, 'steam:11000013f6b85e1', 'cutted_wood', 0),
(1456, 'steam:11000013f6b85e1', 'wood', 0),
(1457, 'steam:11000013f6b85e1', 'bread', 5),
(1458, 'steam:11000013f6b85e1', 'alive_chicken', 0),
(1459, 'steam:11000013f6b85e1', 'shamburger', 0),
(1460, 'steam:11000013f6b85e1', 'WEAPON_KNIFE', 0),
(1461, 'steam:11000013f6b85e1', 'vhamburger', 0),
(1462, 'steam:11000013f6b85e1', 'marijuana', 0),
(1463, 'steam:11000013f6b85e1', 'fvburger', 0),
(1464, 'steam:11000013f6b85e1', 'shotgun_shells', 0),
(1465, 'steam:11000013f6b85e1', 'breathalyzer', 0),
(1466, 'steam:11000013f6b85e1', 'fish', 0),
(1467, 'steam:11000013f6b85e1', 'gold', 0),
(1468, 'steam:11000013f6b85e1', 'fburger', 0),
(1469, 'steam:11000013f6b85e1', 'essence', 0),
(1470, 'steam:11000013f6b85e1', 'stone', 0),
(1471, 'steam:11000013f6b85e1', 'opium', 0),
(1472, 'steam:11000013f6b85e1', 'blowpipe', 0),
(1473, 'steam:11000013f6b85e1', 'beer', 0),
(1474, 'steam:11000013f6b85e1', 'nuggets4', 0),
(1475, 'steam:11000013f6b85e1', 'WEAPON_PISTOL', 0),
(1476, 'steam:11000013f6b85e1', 'cheese', 0),
(1477, 'steam:11000013f6b85e1', 'meth', 0),
(1478, 'steam:11000013f6b85e1', 'fixkit', 0),
(1479, 'steam:11000013f6b85e1', 'wool', 0),
(1480, 'steam:11000013f6b85e1', 'clothe', 0),
(1481, 'steam:11000013f6b85e1', 'fakepee', 0),
(1482, 'steam:11000013f6b85e1', 'cannabis', 0),
(1483, 'steam:11000013f6b85e1', 'carotool', 0),
(1484, 'steam:11000013f6b85e1', 'WEAPON_STUNGUN', 0),
(1485, 'steam:11000013f6b85e1', 'fixtool', 0),
(1486, 'steam:11000013f6b85e1', 'vodka', 0),
(1487, 'steam:11000013f6b85e1', 'water', 15),
(1488, 'steam:11000013f6b85e1', 'vbread', 0),
(1489, 'steam:11000013f6b85e1', 'cocaine', 0),
(1490, 'steam:11000013f6b85e1', 'petrol_raffin', 0),
(1491, 'steam:11000013f6b85e1', 'washed_stone', 0),
(1492, 'steam:11000013f6b85e1', 'poppy', 0),
(1493, 'steam:11000013f6b85e1', 'lettuce', 0),
(1494, 'steam:11000013f6b85e1', 'medikit', 0),
(1495, 'steam:11000013f6b85e1', 'bandage', 0),
(1496, 'steam:11000013f6b85e1', 'tequila', 0),
(1497, 'steam:11000013f6b85e1', 'coca', 0),
(1498, 'steam:11000013f6b85e1', 'crack', 0),
(1499, 'steam:11000013f6b85e1', 'slaughtered_chicken', 0),
(1500, 'steam:11000013f6b85e1', 'ephedrine', 0),
(1501, 'steam:11000013f6b85e1', 'nugget', 0),
(1502, 'steam:11000013f6b85e1', 'dabs', 0),
(1503, 'steam:11000013f6b85e1', '9mm_rounds', 0),
(1504, 'steam:11000013f6b85e1', 'narcan', 0),
(1505, 'steam:11000013f6b85e1', 'ccheese', 0),
(1506, 'steam:11000013f6b85e1', 'petrol', 0),
(1507, 'steam:11000013f6b85e1', 'packaged_chicken', 0),
(1508, 'steam:11000013f6b85e1', 'gazbottle', 0),
(1509, 'steam:11000013f6b85e1', 'pcp', 0),
(1510, 'steam:11000013f6b85e1', 'WEAPON_BAT', 0),
(1511, 'steam:11000013f6b85e1', 'fabric', 0),
(1512, 'steam:11000013f6b85e1', 'painkiller', 0),
(1513, 'steam:11000013f6b85e1', 'diamond', 0),
(1514, 'steam:11000013f6b85e1', 'potato', 0),
(1515, 'steam:11000013f6b85e1', 'nuggets10', 0),
(1516, 'steam:11000013f6b85e1', 'ephedra', 0),
(1517, 'steam:11000013f6b85e1', 'tomato', 0),
(1518, 'steam:11000013f6b85e1', 'iron', 0),
(1519, 'steam:11000013f6b85e1', 'drugtest', 0),
(1520, 'steam:11000013f6b85e1', 'clettuce', 0),
(1521, 'steam:11000013e33b934', 'WEAPON_PUMPSHOTGUN', 0),
(1522, 'steam:11000013e33b934', 'copper', 0),
(1523, 'steam:11000013e33b934', 'WEAPON_FLASHLIGHT', 0),
(1524, 'steam:11000013e33b934', 'packaged_plank', 0),
(1525, 'steam:11000013e33b934', 'carokit', 0),
(1526, 'steam:11000013e33b934', 'ctomato', 0),
(1527, 'steam:11000013e33b934', 'heroine', 0),
(1528, 'steam:11000013e33b934', 'whiskey', 0),
(1529, 'steam:11000013e33b934', 'cutted_wood', 0),
(1530, 'steam:11000013e33b934', 'wood', 0),
(1531, 'steam:11000013e33b934', 'bread', 0),
(1532, 'steam:11000013e33b934', 'alive_chicken', 0),
(1533, 'steam:11000013e33b934', 'shamburger', 0),
(1534, 'steam:11000013e33b934', 'WEAPON_KNIFE', 0),
(1535, 'steam:11000013e33b934', 'vhamburger', 0),
(1536, 'steam:11000013e33b934', 'marijuana', 0),
(1537, 'steam:11000013e33b934', 'fvburger', 0),
(1538, 'steam:11000013e33b934', 'shotgun_shells', 0),
(1539, 'steam:11000013e33b934', 'breathalyzer', 0),
(1540, 'steam:11000013e33b934', 'fish', 0),
(1541, 'steam:11000013e33b934', 'gold', 0),
(1542, 'steam:11000013e33b934', 'fburger', 0),
(1543, 'steam:11000013e33b934', 'essence', 0),
(1544, 'steam:11000013e33b934', 'stone', 0),
(1545, 'steam:11000013e33b934', 'opium', 0),
(1546, 'steam:11000013e33b934', 'blowpipe', 0),
(1547, 'steam:11000013e33b934', 'beer', 0),
(1548, 'steam:11000013e33b934', 'nuggets4', 0),
(1549, 'steam:11000013e33b934', 'WEAPON_PISTOL', 0),
(1550, 'steam:11000013e33b934', 'cheese', 0),
(1551, 'steam:11000013e33b934', 'meth', 0),
(1552, 'steam:11000013e33b934', 'fixkit', 0),
(1553, 'steam:11000013e33b934', 'wool', 0),
(1554, 'steam:11000013e33b934', 'clothe', 0),
(1555, 'steam:11000013e33b934', 'fakepee', 0),
(1556, 'steam:11000013e33b934', 'cannabis', 0),
(1557, 'steam:11000013e33b934', 'carotool', 0),
(1558, 'steam:11000013e33b934', 'WEAPON_STUNGUN', 0),
(1559, 'steam:11000013e33b934', 'fixtool', 0),
(1560, 'steam:11000013e33b934', 'cocaine', 0),
(1561, 'steam:11000013e33b934', 'water', 0),
(1562, 'steam:11000013e33b934', 'vbread', 0),
(1563, 'steam:11000013e33b934', 'vodka', 0),
(1564, 'steam:11000013e33b934', 'petrol_raffin', 0),
(1565, 'steam:11000013e33b934', 'washed_stone', 0),
(1566, 'steam:11000013e33b934', 'poppy', 0),
(1567, 'steam:11000013e33b934', 'lettuce', 0),
(1568, 'steam:11000013e33b934', 'medikit', 0),
(1569, 'steam:11000013e33b934', 'bandage', 0),
(1570, 'steam:11000013e33b934', 'tequila', 0),
(1571, 'steam:11000013e33b934', 'coca', 0),
(1572, 'steam:11000013e33b934', 'crack', 0),
(1573, 'steam:11000013e33b934', 'slaughtered_chicken', 0),
(1574, 'steam:11000013e33b934', 'ephedrine', 0),
(1575, 'steam:11000013e33b934', 'nugget', 0),
(1576, 'steam:11000013e33b934', '9mm_rounds', 0),
(1577, 'steam:11000013e33b934', 'dabs', 0),
(1578, 'steam:11000013e33b934', 'narcan', 0),
(1579, 'steam:11000013e33b934', 'ccheese', 0),
(1580, 'steam:11000013e33b934', 'petrol', 0),
(1581, 'steam:11000013e33b934', 'packaged_chicken', 0),
(1582, 'steam:11000013e33b934', 'gazbottle', 0),
(1583, 'steam:11000013e33b934', 'pcp', 0),
(1584, 'steam:11000013e33b934', 'WEAPON_BAT', 0),
(1585, 'steam:11000013e33b934', 'fabric', 0),
(1586, 'steam:11000013e33b934', 'painkiller', 0),
(1587, 'steam:11000013e33b934', 'diamond', 0),
(1588, 'steam:11000013e33b934', 'potato', 0),
(1589, 'steam:11000013e33b934', 'nuggets10', 0),
(1590, 'steam:11000013e33b934', 'ephedra', 0),
(1591, 'steam:11000013e33b934', 'tomato', 0),
(1592, 'steam:11000013e33b934', 'iron', 0),
(1593, 'steam:11000013e33b934', 'clettuce', 0),
(1594, 'steam:11000013e33b934', 'drugtest', 0),
(1595, 'steam:11000013517b942', 'WEAPON_PUMPSHOTGUN', 0),
(1596, 'steam:11000013517b942', 'copper', 0),
(1597, 'steam:11000013517b942', 'WEAPON_FLASHLIGHT', 0),
(1598, 'steam:11000013517b942', 'packaged_plank', 0),
(1599, 'steam:11000013517b942', 'carokit', 0),
(1600, 'steam:11000013517b942', 'ctomato', 0),
(1601, 'steam:11000013517b942', 'heroine', 0),
(1602, 'steam:11000013517b942', 'whiskey', 0),
(1603, 'steam:11000013517b942', 'cutted_wood', 0),
(1604, 'steam:11000013517b942', 'wood', 0),
(1605, 'steam:11000013517b942', 'bread', 0),
(1606, 'steam:11000013517b942', 'alive_chicken', 0),
(1607, 'steam:11000013517b942', 'shamburger', 0),
(1608, 'steam:11000013517b942', 'marijuana', 0),
(1609, 'steam:11000013517b942', 'vhamburger', 0),
(1610, 'steam:11000013517b942', 'WEAPON_KNIFE', 0),
(1611, 'steam:11000013517b942', 'shotgun_shells', 0),
(1612, 'steam:11000013517b942', 'fvburger', 0),
(1613, 'steam:11000013517b942', 'breathalyzer', 0),
(1614, 'steam:11000013517b942', 'fish', 0),
(1615, 'steam:11000013517b942', 'gold', 0),
(1616, 'steam:11000013517b942', 'fburger', 0),
(1617, 'steam:11000013517b942', 'essence', 0),
(1618, 'steam:11000013517b942', 'stone', 0),
(1619, 'steam:11000013517b942', 'opium', 0),
(1620, 'steam:11000013517b942', 'blowpipe', 0),
(1621, 'steam:11000013517b942', 'beer', 0),
(1622, 'steam:11000013517b942', 'nuggets4', 0),
(1623, 'steam:11000013517b942', 'WEAPON_PISTOL', 0),
(1624, 'steam:11000013517b942', 'cheese', 0),
(1625, 'steam:11000013517b942', 'meth', 0),
(1626, 'steam:11000013517b942', 'fixkit', 0),
(1627, 'steam:11000013517b942', 'wool', 0),
(1628, 'steam:11000013517b942', 'clothe', 0),
(1629, 'steam:11000013517b942', 'fakepee', 0),
(1630, 'steam:11000013517b942', 'cannabis', 0),
(1631, 'steam:11000013517b942', 'carotool', 0),
(1632, 'steam:11000013517b942', 'WEAPON_STUNGUN', 0),
(1633, 'steam:11000013517b942', 'fixtool', 0),
(1634, 'steam:11000013517b942', 'cocaine', 0),
(1635, 'steam:11000013517b942', 'vodka', 0),
(1636, 'steam:11000013517b942', 'water', 0),
(1637, 'steam:11000013517b942', 'vbread', 0),
(1638, 'steam:11000013517b942', 'petrol_raffin', 0),
(1639, 'steam:11000013517b942', 'washed_stone', 0),
(1640, 'steam:11000013517b942', 'poppy', 0),
(1641, 'steam:11000013517b942', 'lettuce', 0),
(1642, 'steam:11000013517b942', 'medikit', 0),
(1643, 'steam:11000013517b942', 'bandage', 0),
(1644, 'steam:11000013517b942', 'tequila', 0),
(1645, 'steam:11000013517b942', 'coca', 0),
(1646, 'steam:11000013517b942', 'crack', 0),
(1647, 'steam:11000013517b942', 'slaughtered_chicken', 0),
(1648, 'steam:11000013517b942', 'ephedrine', 0),
(1649, 'steam:11000013517b942', 'nugget', 0),
(1650, 'steam:11000013517b942', '9mm_rounds', 0),
(1651, 'steam:11000013517b942', 'dabs', 0),
(1652, 'steam:11000013517b942', 'narcan', 0),
(1653, 'steam:11000013517b942', 'ccheese', 0),
(1654, 'steam:11000013517b942', 'petrol', 0),
(1655, 'steam:11000013517b942', 'packaged_chicken', 0),
(1656, 'steam:11000013517b942', 'pcp', 0),
(1657, 'steam:11000013517b942', 'gazbottle', 0),
(1658, 'steam:11000013517b942', 'WEAPON_BAT', 0),
(1659, 'steam:11000013517b942', 'fabric', 0),
(1660, 'steam:11000013517b942', 'painkiller', 0),
(1661, 'steam:11000013517b942', 'diamond', 0),
(1662, 'steam:11000013517b942', 'potato', 0),
(1663, 'steam:11000013517b942', 'nuggets10', 0),
(1664, 'steam:11000013517b942', 'ephedra', 0),
(1665, 'steam:11000013517b942', 'tomato', 0),
(1666, 'steam:11000013517b942', 'iron', 0),
(1667, 'steam:11000013517b942', 'drugtest', 0),
(1668, 'steam:11000013517b942', 'clettuce', 0),
(1669, 'steam:110000134e3ca1a', 'ephedra', 0),
(1670, 'steam:110000134e3ca1a', 'whiskey', 0),
(1671, 'steam:110000134e3ca1a', 'fish', 0),
(1672, 'steam:110000134e3ca1a', 'painkiller', 0),
(1673, 'steam:110000134e3ca1a', 'nuggets4', 0),
(1674, 'steam:110000134e3ca1a', 'meth', 0),
(1675, 'steam:110000134e3ca1a', 'WEAPON_BAT', 0),
(1676, 'steam:110000134e3ca1a', 'poppy', 0),
(1677, 'steam:110000134e3ca1a', 'iron', 0),
(1678, 'steam:110000134e3ca1a', 'packaged_plank', 0),
(1679, 'steam:110000134e3ca1a', 'cheese', 0),
(1680, 'steam:110000134e3ca1a', 'WEAPON_PUMPSHOTGUN', 0),
(1681, 'steam:110000134e3ca1a', 'fburger', 0),
(1682, 'steam:110000134e3ca1a', 'clettuce', 0),
(1683, 'steam:110000134e3ca1a', 'breathalyzer', 0),
(1684, 'steam:110000134e3ca1a', 'petrol_raffin', 0),
(1685, 'steam:110000134e3ca1a', 'cocaine', 0),
(1686, 'steam:110000134e3ca1a', 'shamburger', 0),
(1687, 'steam:110000134e3ca1a', 'narcan', 0),
(1688, 'steam:110000134e3ca1a', '9mm_rounds', 0),
(1689, 'steam:110000134e3ca1a', 'essence', 0),
(1690, 'steam:110000134e3ca1a', 'fixkit', 0),
(1691, 'steam:110000134e3ca1a', 'tequila', 0),
(1692, 'steam:110000134e3ca1a', 'heroine', 0),
(1693, 'steam:110000134e3ca1a', 'vbread', 0),
(1694, 'steam:110000134e3ca1a', 'fabric', 0),
(1695, 'steam:110000134e3ca1a', 'fakepee', 0),
(1696, 'steam:110000134e3ca1a', 'WEAPON_KNIFE', 0),
(1697, 'steam:110000134e3ca1a', 'WEAPON_FLASHLIGHT', 0),
(1698, 'steam:110000134e3ca1a', 'shotgun_shells', 0),
(1699, 'steam:110000134e3ca1a', 'bread', 6),
(1700, 'steam:110000134e3ca1a', 'drugtest', 0),
(1701, 'steam:110000134e3ca1a', 'cutted_wood', 0),
(1702, 'steam:110000134e3ca1a', 'WEAPON_STUNGUN', 0),
(1703, 'steam:110000134e3ca1a', 'lettuce', 0),
(1704, 'steam:110000134e3ca1a', 'wood', 0),
(1705, 'steam:110000134e3ca1a', 'medikit', 0),
(1706, 'steam:110000134e3ca1a', 'copper', 0),
(1707, 'steam:110000134e3ca1a', 'cannabis', 0),
(1708, 'steam:110000134e3ca1a', 'petrol', 0),
(1709, 'steam:110000134e3ca1a', 'carokit', 0),
(1710, 'steam:110000134e3ca1a', 'pcp', 0),
(1711, 'steam:110000134e3ca1a', 'vhamburger', 0),
(1712, 'steam:110000134e3ca1a', 'fvburger', 0),
(1713, 'steam:110000134e3ca1a', 'water', 6),
(1714, 'steam:110000134e3ca1a', 'ctomato', 0),
(1715, 'steam:110000134e3ca1a', 'vodka', 0),
(1716, 'steam:110000134e3ca1a', 'washed_stone', 0),
(1717, 'steam:110000134e3ca1a', 'wool', 0),
(1718, 'steam:110000134e3ca1a', 'marijuana', 9),
(1719, 'steam:110000134e3ca1a', 'gazbottle', 0),
(1720, 'steam:110000134e3ca1a', 'bandage', 0),
(1721, 'steam:110000134e3ca1a', 'tomato', 0),
(1722, 'steam:110000134e3ca1a', 'stone', 0),
(1723, 'steam:110000134e3ca1a', 'ephedrine', 0),
(1724, 'steam:110000134e3ca1a', 'slaughtered_chicken', 0),
(1725, 'steam:110000134e3ca1a', 'ccheese', 0),
(1726, 'steam:110000134e3ca1a', 'coca', 0),
(1727, 'steam:110000134e3ca1a', 'carotool', 0),
(1728, 'steam:110000134e3ca1a', 'nuggets10', 0),
(1729, 'steam:110000134e3ca1a', 'crack', 0),
(1730, 'steam:110000134e3ca1a', 'potato', 0),
(1731, 'steam:110000134e3ca1a', 'gold', 0),
(1732, 'steam:110000134e3ca1a', 'opium', 0),
(1733, 'steam:110000134e3ca1a', 'packaged_chicken', 0),
(1734, 'steam:110000134e3ca1a', 'dabs', 0),
(1735, 'steam:110000134e3ca1a', 'blowpipe', 0),
(1736, 'steam:110000134e3ca1a', 'fixtool', 0),
(1737, 'steam:110000134e3ca1a', 'WEAPON_PISTOL', 0),
(1738, 'steam:110000134e3ca1a', 'nugget', 0),
(1739, 'steam:110000134e3ca1a', 'beer', 0),
(1740, 'steam:110000134e3ca1a', 'diamond', 0),
(1741, 'steam:110000134e3ca1a', 'clothe', 0),
(1742, 'steam:110000134e3ca1a', 'alive_chicken', 0),
(1743, 'steam:1100001013142e0', 'fixtool', 0),
(1744, 'steam:1100001013142e0', 'marijuana', 0),
(1745, 'steam:1100001013142e0', 'pcp', 0),
(1746, 'steam:1100001013142e0', 'whiskey', 0),
(1747, 'steam:1100001013142e0', 'beer', 0),
(1748, 'steam:1100001013142e0', 'tomato', 0),
(1749, 'steam:1100001013142e0', 'packaged_chicken', 0),
(1750, 'steam:1100001013142e0', 'vhamburger', 0),
(1751, 'steam:1100001013142e0', 'medikit', 0),
(1752, 'steam:1100001013142e0', 'WEAPON_PISTOL', 0),
(1753, 'steam:1100001013142e0', 'petrol_raffin', 0),
(1754, 'steam:1100001013142e0', 'washed_stone', 0),
(1755, 'steam:1100001013142e0', 'cutted_wood', 0),
(1756, 'steam:1100001013142e0', 'wood', 0),
(1757, 'steam:1100001013142e0', 'fakepee', 0),
(1758, 'steam:1100001013142e0', 'fabric', 0),
(1759, 'steam:1100001013142e0', 'WEAPON_KNIFE', 0),
(1760, 'steam:1100001013142e0', 'nuggets10', 0),
(1761, 'steam:1100001013142e0', 'vodka', 0),
(1762, 'steam:1100001013142e0', 'bread', 0),
(1763, 'steam:1100001013142e0', 'essence', 0),
(1764, 'steam:1100001013142e0', 'dabs', 0),
(1765, 'steam:1100001013142e0', 'clothe', 0),
(1766, 'steam:1100001013142e0', 'potato', 0),
(1767, 'steam:1100001013142e0', 'WEAPON_STUNGUN', 0),
(1768, 'steam:1100001013142e0', '9mm_rounds', 0),
(1769, 'steam:1100001013142e0', 'WEAPON_PUMPSHOTGUN', 0),
(1770, 'steam:1100001013142e0', 'narcan', 0),
(1771, 'steam:1100001013142e0', 'coca', 0),
(1772, 'steam:1100001013142e0', 'water', 0),
(1773, 'steam:1100001013142e0', 'poppy', 0),
(1774, 'steam:1100001013142e0', 'petrol', 0),
(1775, 'steam:1100001013142e0', 'gold', 0),
(1776, 'steam:1100001013142e0', 'cheese', 0),
(1777, 'steam:1100001013142e0', 'fixkit', 0),
(1778, 'steam:1100001013142e0', 'painkiller', 0),
(1779, 'steam:1100001013142e0', 'blowpipe', 0),
(1780, 'steam:1100001013142e0', 'wool', 0),
(1781, 'steam:1100001013142e0', 'vbread', 0),
(1782, 'steam:1100001013142e0', 'tequila', 0),
(1783, 'steam:1100001013142e0', 'breathalyzer', 0),
(1784, 'steam:1100001013142e0', 'ephedrine', 0),
(1785, 'steam:1100001013142e0', 'nuggets4', 0),
(1786, 'steam:1100001013142e0', 'drugtest', 0),
(1787, 'steam:1100001013142e0', 'bandage', 0),
(1788, 'steam:1100001013142e0', 'WEAPON_FLASHLIGHT', 0),
(1789, 'steam:1100001013142e0', 'stone', 0),
(1790, 'steam:1100001013142e0', 'shotgun_shells', 0),
(1791, 'steam:1100001013142e0', 'opium', 0),
(1792, 'steam:1100001013142e0', 'slaughtered_chicken', 0),
(1793, 'steam:1100001013142e0', 'carotool', 0),
(1794, 'steam:1100001013142e0', 'ctomato', 0),
(1795, 'steam:1100001013142e0', 'fvburger', 0),
(1796, 'steam:1100001013142e0', 'shamburger', 0),
(1797, 'steam:1100001013142e0', 'copper', 0),
(1798, 'steam:1100001013142e0', 'clettuce', 0),
(1799, 'steam:1100001013142e0', 'ephedra', 0),
(1800, 'steam:1100001013142e0', 'cocaine', 0),
(1801, 'steam:1100001013142e0', 'fburger', 0),
(1802, 'steam:1100001013142e0', 'crack', 0),
(1803, 'steam:1100001013142e0', 'WEAPON_BAT', 0),
(1804, 'steam:1100001013142e0', 'carokit', 0),
(1805, 'steam:1100001013142e0', 'nugget', 0),
(1806, 'steam:1100001013142e0', 'fish', 0),
(1807, 'steam:1100001013142e0', 'packaged_plank', 0),
(1808, 'steam:1100001013142e0', 'iron', 0),
(1809, 'steam:1100001013142e0', 'diamond', 0),
(1810, 'steam:1100001013142e0', 'meth', 0),
(1811, 'steam:1100001013142e0', 'alive_chicken', 0),
(1812, 'steam:1100001013142e0', 'lettuce', 0),
(1813, 'steam:1100001013142e0', 'heroine', 0),
(1814, 'steam:1100001013142e0', 'gazbottle', 0),
(1815, 'steam:1100001013142e0', 'cannabis', 0),
(1816, 'steam:1100001013142e0', 'ccheese', 0),
(1817, 'steam:110000133d93ea2', 'meth', 0),
(1818, 'steam:110000133d93ea2', 'heroine', 0),
(1819, 'steam:110000133d93ea2', 'WEAPON_KNIFE', 0),
(1820, 'steam:110000133d93ea2', 'stone', 0),
(1821, 'steam:110000133d93ea2', 'crack', 0),
(1822, 'steam:110000133d93ea2', 'gold', 0),
(1823, 'steam:110000133d93ea2', 'ctomato', 0),
(1824, 'steam:110000133d93ea2', 'fvburger', 0),
(1825, 'steam:110000133d93ea2', 'WEAPON_BAT', 0),
(1826, 'steam:110000133d93ea2', 'washed_stone', 0),
(1827, 'steam:110000133d93ea2', 'ephedrine', 0),
(1828, 'steam:110000133d93ea2', 'dabs', 0),
(1829, 'steam:110000133d93ea2', 'drugtest', 0),
(1830, 'steam:110000133d93ea2', 'slaughtered_chicken', 0),
(1831, 'steam:110000133d93ea2', 'cheese', 0),
(1832, 'steam:110000133d93ea2', 'narcan', 0),
(1833, 'steam:110000133d93ea2', 'packaged_chicken', 0),
(1834, 'steam:110000133d93ea2', 'breathalyzer', 0),
(1835, 'steam:110000133d93ea2', 'lettuce', 0),
(1836, 'steam:110000133d93ea2', 'beer', 0),
(1837, 'steam:110000133d93ea2', 'carotool', 0),
(1838, 'steam:110000133d93ea2', 'carokit', 0),
(1839, 'steam:110000133d93ea2', 'vhamburger', 0),
(1840, 'steam:110000133d93ea2', 'WEAPON_FLASHLIGHT', 0),
(1841, 'steam:110000133d93ea2', 'fish', 0),
(1842, 'steam:110000133d93ea2', 'blowpipe', 0),
(1843, 'steam:110000133d93ea2', 'bandage', 0),
(1844, 'steam:110000133d93ea2', 'WEAPON_PUMPSHOTGUN', 0),
(1845, 'steam:110000133d93ea2', 'clettuce', 0),
(1846, 'steam:110000133d93ea2', 'fixtool', 0),
(1847, 'steam:110000133d93ea2', 'essence', 0),
(1848, 'steam:110000133d93ea2', 'fabric', 0),
(1849, 'steam:110000133d93ea2', 'ccheese', 0),
(1850, 'steam:110000133d93ea2', 'tomato', 0),
(1851, 'steam:110000133d93ea2', 'poppy', 0),
(1852, 'steam:110000133d93ea2', 'coca', 0),
(1853, 'steam:110000133d93ea2', 'fburger', 0),
(1854, 'steam:110000133d93ea2', 'opium', 0),
(1855, 'steam:110000133d93ea2', 'cutted_wood', 0),
(1856, 'steam:110000133d93ea2', 'diamond', 0),
(1857, 'steam:110000133d93ea2', 'WEAPON_STUNGUN', 0),
(1858, 'steam:110000133d93ea2', 'alive_chicken', 0),
(1859, 'steam:110000133d93ea2', 'nuggets10', 0),
(1860, 'steam:110000133d93ea2', 'wool', 0),
(1861, 'steam:110000133d93ea2', 'clothe', 0),
(1862, 'steam:110000133d93ea2', 'copper', 0),
(1863, 'steam:110000133d93ea2', 'medikit', 0),
(1864, 'steam:110000133d93ea2', 'cocaine', 0),
(1865, 'steam:110000133d93ea2', '9mm_rounds', 0),
(1866, 'steam:110000133d93ea2', 'petrol_raffin', 0),
(1867, 'steam:110000133d93ea2', 'ephedra', 0),
(1868, 'steam:110000133d93ea2', 'wood', 0),
(1869, 'steam:110000133d93ea2', 'nugget', 0),
(1870, 'steam:110000133d93ea2', 'packaged_plank', 0),
(1871, 'steam:110000133d93ea2', 'whiskey', 0),
(1872, 'steam:110000133d93ea2', 'water', 99),
(1873, 'steam:110000133d93ea2', 'petrol', 0),
(1874, 'steam:110000133d93ea2', 'vbread', 0),
(1875, 'steam:110000133d93ea2', 'tequila', 0),
(1876, 'steam:110000133d93ea2', 'shotgun_shells', 0),
(1877, 'steam:110000133d93ea2', 'vodka', 0),
(1878, 'steam:110000133d93ea2', 'nuggets4', 0),
(1879, 'steam:110000133d93ea2', 'marijuana', 0),
(1880, 'steam:110000133d93ea2', 'pcp', 0),
(1881, 'steam:110000133d93ea2', 'shamburger', 0),
(1882, 'steam:110000133d93ea2', 'gazbottle', 0),
(1883, 'steam:110000133d93ea2', 'potato', 0),
(1884, 'steam:110000133d93ea2', 'painkiller', 0),
(1885, 'steam:110000133d93ea2', 'iron', 0),
(1886, 'steam:110000133d93ea2', 'WEAPON_PISTOL', 0),
(1887, 'steam:110000133d93ea2', 'cannabis', 0),
(1888, 'steam:110000133d93ea2', 'fixkit', 0),
(1889, 'steam:110000133d93ea2', 'bread', 248),
(1890, 'steam:110000133d93ea2', 'fakepee', 0),
(1891, 'steam:11000011531f6b5', 'drugtest', 0),
(1892, 'steam:11000011531f6b5', 'cocaine', 0),
(1893, 'steam:11000011531f6b5', 'cannabis', 29),
(1894, 'steam:11000011531f6b5', 'fakepee', 0),
(1895, 'steam:11000011531f6b5', 'shamburger', 0),
(1896, 'steam:11000011531f6b5', 'vhamburger', 0),
(1897, 'steam:11000011531f6b5', 'potato', 0),
(1898, 'steam:11000011531f6b5', 'diamond', 0),
(1899, 'steam:11000011531f6b5', 'bread', 0),
(1900, 'steam:11000011531f6b5', 'WEAPON_BAT', 0),
(1901, 'steam:11000011531f6b5', 'lettuce', 0),
(1902, 'steam:11000011531f6b5', 'washed_stone', 0),
(1903, 'steam:11000011531f6b5', 'pcp', 0),
(1904, 'steam:11000011531f6b5', 'water', 0),
(1905, 'steam:11000011531f6b5', 'breathalyzer', 0),
(1906, 'steam:11000011531f6b5', 'cheese', 0),
(1907, 'steam:11000011531f6b5', 'vodka', 0),
(1908, 'steam:11000011531f6b5', 'cutted_wood', 0),
(1909, 'steam:11000011531f6b5', 'fish', 0),
(1910, 'steam:11000011531f6b5', 'gazbottle', 0),
(1911, 'steam:11000011531f6b5', 'ephedra', 0),
(1912, 'steam:11000011531f6b5', 'nuggets10', 0),
(1913, 'steam:11000011531f6b5', 'WEAPON_STUNGUN', 0),
(1914, 'steam:11000011531f6b5', 'fabric', 0),
(1915, 'steam:11000011531f6b5', 'blowpipe', 0),
(1916, 'steam:11000011531f6b5', 'essence', 0),
(1917, 'steam:11000011531f6b5', 'meth', 0),
(1918, 'steam:11000011531f6b5', 'clettuce', 0),
(1919, 'steam:11000011531f6b5', 'fixtool', 0),
(1920, 'steam:11000011531f6b5', 'WEAPON_KNIFE', 0),
(1921, 'steam:11000011531f6b5', 'carokit', 0),
(1922, 'steam:11000011531f6b5', 'clothe', 0),
(1923, 'steam:11000011531f6b5', 'nugget', 0),
(1924, 'steam:11000011531f6b5', 'shotgun_shells', 0),
(1925, 'steam:11000011531f6b5', 'wood', 0),
(1926, 'steam:11000011531f6b5', 'tequila', 0),
(1927, 'steam:11000011531f6b5', 'iron', 0),
(1928, 'steam:11000011531f6b5', 'gold', 0),
(1929, 'steam:11000011531f6b5', 'carotool', 0),
(1930, 'steam:11000011531f6b5', 'wool', 0),
(1931, 'steam:11000011531f6b5', 'stone', 0),
(1932, 'steam:11000011531f6b5', 'WEAPON_PUMPSHOTGUN', 0),
(1933, 'steam:11000011531f6b5', 'beer', 0),
(1934, 'steam:11000011531f6b5', 'whiskey', 0),
(1935, 'steam:11000011531f6b5', 'vbread', 0),
(1936, 'steam:11000011531f6b5', 'WEAPON_PISTOL', 0),
(1937, 'steam:11000011531f6b5', 'tomato', 0),
(1938, 'steam:11000011531f6b5', 'bandage', 0),
(1939, 'steam:11000011531f6b5', 'crack', 0),
(1940, 'steam:11000011531f6b5', 'fvburger', 0),
(1941, 'steam:11000011531f6b5', 'copper', 0),
(1942, 'steam:11000011531f6b5', 'ephedrine', 23),
(1943, 'steam:11000011531f6b5', 'slaughtered_chicken', 0),
(1944, 'steam:11000011531f6b5', 'WEAPON_FLASHLIGHT', 0),
(1945, 'steam:11000011531f6b5', 'coca', 0),
(1946, 'steam:11000011531f6b5', 'fburger', 0),
(1947, 'steam:11000011531f6b5', 'poppy', 0),
(1948, 'steam:11000011531f6b5', 'petrol_raffin', 0),
(1949, 'steam:11000011531f6b5', 'ccheese', 0),
(1950, 'steam:11000011531f6b5', 'packaged_chicken', 0),
(1951, 'steam:11000011531f6b5', 'petrol', 0),
(1952, 'steam:11000011531f6b5', 'packaged_plank', 0),
(1953, 'steam:11000011531f6b5', 'painkiller', 0),
(1954, 'steam:11000011531f6b5', 'dabs', 0),
(1955, 'steam:11000011531f6b5', 'alive_chicken', 0),
(1956, 'steam:11000011531f6b5', 'narcan', 0),
(1957, 'steam:11000011531f6b5', 'nuggets4', 0),
(1958, 'steam:11000011531f6b5', 'ctomato', 0),
(1959, 'steam:11000011531f6b5', 'medikit', 0),
(1960, 'steam:11000011531f6b5', 'opium', 0),
(1961, 'steam:11000011531f6b5', 'marijuana', 0),
(1962, 'steam:11000011531f6b5', 'heroine', 0),
(1963, 'steam:11000011531f6b5', 'fixkit', 0),
(1964, 'steam:11000011531f6b5', '9mm_rounds', 0),
(1965, 'steam:11000010cfb10c1', '9mm_rounds', 0),
(1966, 'steam:11000010cfb10c1', 'shamburger', 0),
(1967, 'steam:11000010cfb10c1', 'alive_chicken', 0),
(1968, 'steam:11000010cfb10c1', 'fabric', 0),
(1969, 'steam:11000010cfb10c1', 'vhamburger', 0),
(1970, 'steam:11000010cfb10c1', 'washed_stone', 0),
(1971, 'steam:11000010cfb10c1', 'wool', 0),
(1972, 'steam:11000010cfb10c1', 'petrol_raffin', 0),
(1973, 'steam:11000010cfb10c1', 'slaughtered_chicken', 0),
(1974, 'steam:11000010cfb10c1', 'marijuana', 0),
(1975, 'steam:11000010cfb10c1', 'fvburger', 0),
(1976, 'steam:11000010cfb10c1', 'fixtool', 0),
(1977, 'steam:11000010cfb10c1', 'vbread', 0),
(1978, 'steam:11000010cfb10c1', 'essence', 0),
(1979, 'steam:11000010cfb10c1', 'gold', 0),
(1980, 'steam:11000010cfb10c1', 'fakepee', 0),
(1981, 'steam:11000010cfb10c1', 'petrol', 0),
(1982, 'steam:11000010cfb10c1', 'lettuce', 0),
(1983, 'steam:11000010cfb10c1', 'meth', 0),
(1984, 'steam:11000010cfb10c1', 'fixkit', 0),
(1985, 'steam:11000010cfb10c1', 'cutted_wood', 0),
(1986, 'steam:11000010cfb10c1', 'crack', 0),
(1987, 'steam:11000010cfb10c1', 'bread', 0),
(1988, 'steam:11000010cfb10c1', 'ccheese', 0),
(1989, 'steam:11000010cfb10c1', 'wood', 0),
(1990, 'steam:11000010cfb10c1', 'cannabis', 0),
(1991, 'steam:11000010cfb10c1', 'cheese', 0),
(1992, 'steam:11000010cfb10c1', 'clettuce', 0),
(1993, 'steam:11000010cfb10c1', 'ephedrine', 0),
(1994, 'steam:11000010cfb10c1', 'beer', 0),
(1995, 'steam:11000010cfb10c1', 'bandage', 0),
(1996, 'steam:11000010cfb10c1', 'WEAPON_FLASHLIGHT', 0),
(1997, 'steam:11000010cfb10c1', 'WEAPON_STUNGUN', 0),
(1998, 'steam:11000010cfb10c1', 'ctomato', 0),
(1999, 'steam:11000010cfb10c1', 'carokit', 0),
(2000, 'steam:11000010cfb10c1', 'copper', 0),
(2001, 'steam:11000010cfb10c1', 'nuggets10', 0),
(2002, 'steam:11000010cfb10c1', 'clothe', 0),
(2003, 'steam:11000010cfb10c1', 'fburger', 0),
(2004, 'steam:11000010cfb10c1', 'water', 2),
(2005, 'steam:11000010cfb10c1', 'fish', 0),
(2006, 'steam:11000010cfb10c1', 'drugtest', 0),
(2007, 'steam:11000010cfb10c1', 'vodka', 0),
(2008, 'steam:11000010cfb10c1', 'WEAPON_KNIFE', 0),
(2009, 'steam:11000010cfb10c1', 'painkiller', 0),
(2010, 'steam:11000010cfb10c1', 'pcp', 0),
(2011, 'steam:11000010cfb10c1', 'stone', 0),
(2012, 'steam:11000010cfb10c1', 'carotool', 0),
(2013, 'steam:11000010cfb10c1', 'dabs', 0),
(2014, 'steam:11000010cfb10c1', 'opium', 0),
(2015, 'steam:11000010cfb10c1', 'coca', 0),
(2016, 'steam:11000010cfb10c1', 'potato', 0),
(2017, 'steam:11000010cfb10c1', 'ephedra', 0),
(2018, 'steam:11000010cfb10c1', 'cocaine', 0),
(2019, 'steam:11000010cfb10c1', 'tomato', 0),
(2020, 'steam:11000010cfb10c1', 'shotgun_shells', 0),
(2021, 'steam:11000010cfb10c1', 'diamond', 0),
(2022, 'steam:11000010cfb10c1', 'heroine', 0),
(2023, 'steam:11000010cfb10c1', 'poppy', 0),
(2024, 'steam:11000010cfb10c1', 'WEAPON_PISTOL', 0),
(2025, 'steam:11000010cfb10c1', 'WEAPON_BAT', 0),
(2026, 'steam:11000010cfb10c1', 'nuggets4', 0),
(2027, 'steam:11000010cfb10c1', 'WEAPON_PUMPSHOTGUN', 0),
(2028, 'steam:11000010cfb10c1', 'tequila', 0),
(2029, 'steam:11000010cfb10c1', 'packaged_plank', 0),
(2030, 'steam:11000010cfb10c1', 'whiskey', 0),
(2031, 'steam:11000010cfb10c1', 'packaged_chicken', 0),
(2032, 'steam:11000010cfb10c1', 'breathalyzer', 0),
(2033, 'steam:11000010cfb10c1', 'medikit', 0),
(2034, 'steam:11000010cfb10c1', 'nugget', 0),
(2035, 'steam:11000010cfb10c1', 'gazbottle', 0),
(2036, 'steam:11000010cfb10c1', 'narcan', 0),
(2037, 'steam:11000010cfb10c1', 'blowpipe', 0),
(2038, 'steam:11000010cfb10c1', 'iron', 0),
(2039, 'steam:110000135e316b5', 'medikit', 0),
(2040, 'steam:110000135e316b5', 'opium', 0),
(2041, 'steam:110000135e316b5', 'coca', 0),
(2042, 'steam:110000135e316b5', 'fvburger', 0),
(2043, 'steam:110000135e316b5', 'stone', 0),
(2044, 'steam:110000135e316b5', 'cocaine', 0),
(2045, 'steam:110000135e316b5', 'fish', 0),
(2046, 'steam:110000135e316b5', 'petrol_raffin', 0),
(2047, 'steam:110000135e316b5', 'copper', 0),
(2048, 'steam:110000135e316b5', 'fabric', 0),
(2049, 'steam:110000135e316b5', 'gazbottle', 0),
(2050, 'steam:110000135e316b5', 'meth', 0),
(2051, 'steam:110000135e316b5', 'potato', 0),
(2052, 'steam:110000135e316b5', 'carokit', 0),
(2053, 'steam:110000135e316b5', 'pcp', 0),
(2054, 'steam:110000135e316b5', 'gold', 0),
(2055, 'steam:110000135e316b5', 'WEAPON_KNIFE', 0),
(2056, 'steam:110000135e316b5', 'fakepee', 0),
(2057, 'steam:110000135e316b5', 'marijuana', 0),
(2058, 'steam:110000135e316b5', '9mm_rounds', 0),
(2059, 'steam:110000135e316b5', 'WEAPON_PISTOL', 0),
(2060, 'steam:110000135e316b5', 'alive_chicken', 0),
(2061, 'steam:110000135e316b5', 'blowpipe', 0),
(2062, 'steam:110000135e316b5', 'vbread', 0),
(2063, 'steam:110000135e316b5', 'bandage', 0),
(2064, 'steam:110000135e316b5', 'water', 0),
(2065, 'steam:110000135e316b5', 'WEAPON_STUNGUN', 0),
(2066, 'steam:110000135e316b5', 'WEAPON_PUMPSHOTGUN', 0),
(2067, 'steam:110000135e316b5', 'carotool', 0),
(2068, 'steam:110000135e316b5', 'wood', 0),
(2069, 'steam:110000135e316b5', 'WEAPON_FLASHLIGHT', 0),
(2070, 'steam:110000135e316b5', 'shamburger', 0),
(2071, 'steam:110000135e316b5', 'vhamburger', 0),
(2072, 'steam:110000135e316b5', 'packaged_plank', 0),
(2073, 'steam:110000135e316b5', 'ccheese', 0),
(2074, 'steam:110000135e316b5', 'petrol', 0),
(2075, 'steam:110000135e316b5', 'ephedrine', 0),
(2076, 'steam:110000135e316b5', 'dabs', 0),
(2077, 'steam:110000135e316b5', 'essence', 0),
(2078, 'steam:110000135e316b5', 'crack', 0),
(2079, 'steam:110000135e316b5', 'fixtool', 0),
(2080, 'steam:110000135e316b5', 'fixkit', 0),
(2081, 'steam:110000135e316b5', 'drugtest', 0),
(2082, 'steam:110000135e316b5', 'poppy', 0),
(2083, 'steam:110000135e316b5', 'clothe', 0),
(2084, 'steam:110000135e316b5', 'whiskey', 0),
(2085, 'steam:110000135e316b5', 'nuggets10', 0),
(2086, 'steam:110000135e316b5', 'lettuce', 0),
(2087, 'steam:110000135e316b5', 'cannabis', 0),
(2088, 'steam:110000135e316b5', 'wool', 0),
(2089, 'steam:110000135e316b5', 'narcan', 0),
(2090, 'steam:110000135e316b5', 'heroine', 0),
(2091, 'steam:110000135e316b5', 'ctomato', 0),
(2092, 'steam:110000135e316b5', 'breathalyzer', 0),
(2093, 'steam:110000135e316b5', 'tomato', 0),
(2094, 'steam:110000135e316b5', 'slaughtered_chicken', 0),
(2095, 'steam:110000135e316b5', 'diamond', 0),
(2096, 'steam:110000135e316b5', 'WEAPON_BAT', 0),
(2097, 'steam:110000135e316b5', 'packaged_chicken', 0),
(2098, 'steam:110000135e316b5', 'vodka', 0),
(2099, 'steam:110000135e316b5', 'cutted_wood', 0),
(2100, 'steam:110000135e316b5', 'tequila', 0),
(2101, 'steam:110000135e316b5', 'painkiller', 0),
(2102, 'steam:110000135e316b5', 'nuggets4', 0),
(2103, 'steam:110000135e316b5', 'washed_stone', 0),
(2104, 'steam:110000135e316b5', 'clettuce', 0),
(2105, 'steam:110000135e316b5', 'nugget', 0),
(2106, 'steam:110000135e316b5', 'cheese', 0),
(2107, 'steam:110000135e316b5', 'iron', 0),
(2108, 'steam:110000135e316b5', 'shotgun_shells', 0),
(2109, 'steam:110000135e316b5', 'fburger', 0),
(2110, 'steam:110000135e316b5', 'ephedra', 0),
(2111, 'steam:110000135e316b5', 'bread', 0),
(2112, 'steam:110000135e316b5', 'beer', 0),
(2113, 'steam:1100001156cfac9', 'nugget', 0),
(2114, 'steam:1100001156cfac9', 'bread', 8),
(2115, 'steam:1100001156cfac9', 'shamburger', 0),
(2116, 'steam:1100001156cfac9', 'WEAPON_KNIFE', 0),
(2117, 'steam:1100001156cfac9', 'wool', 0),
(2118, 'steam:1100001156cfac9', 'vhamburger', 0),
(2119, 'steam:1100001156cfac9', 'marijuana', 0),
(2120, 'steam:1100001156cfac9', 'essence', 0),
(2121, 'steam:1100001156cfac9', 'cannabis', 0),
(2122, 'steam:1100001156cfac9', 'painkiller', 0),
(2123, 'steam:1100001156cfac9', 'cocaine', 0),
(2124, 'steam:1100001156cfac9', 'medikit', 0),
(2125, 'steam:1100001156cfac9', 'heroine', 0),
(2126, 'steam:1100001156cfac9', 'pcp', 0),
(2127, 'steam:1100001156cfac9', 'diamond', 0),
(2128, 'steam:1100001156cfac9', 'WEAPON_BAT', 0),
(2129, 'steam:1100001156cfac9', 'gazbottle', 0),
(2130, 'steam:1100001156cfac9', 'cheese', 0),
(2131, 'steam:1100001156cfac9', 'fixkit', 0),
(2132, 'steam:1100001156cfac9', 'clettuce', 0),
(2133, 'steam:1100001156cfac9', 'fburger', 0),
(2134, 'steam:1100001156cfac9', 'fvburger', 0),
(2135, 'steam:1100001156cfac9', 'potato', 0),
(2136, 'steam:1100001156cfac9', 'vbread', 0),
(2137, 'steam:1100001156cfac9', 'meth', 0),
(2138, 'steam:1100001156cfac9', 'nuggets4', 0),
(2139, 'steam:1100001156cfac9', 'packaged_chicken', 0),
(2140, 'steam:1100001156cfac9', 'WEAPON_PISTOL', 0),
(2141, 'steam:1100001156cfac9', 'iron', 0),
(2142, 'steam:1100001156cfac9', 'fish', 0),
(2143, 'steam:1100001156cfac9', 'carotool', 0),
(2144, 'steam:1100001156cfac9', 'opium', 0),
(2145, 'steam:1100001156cfac9', 'ctomato', 0),
(2146, 'steam:1100001156cfac9', 'clothe', 0),
(2147, 'steam:1100001156cfac9', 'breathalyzer', 0),
(2148, 'steam:1100001156cfac9', 'fabric', 0),
(2149, 'steam:1100001156cfac9', 'shotgun_shells', 0),
(2150, 'steam:1100001156cfac9', 'ephedra', 0),
(2151, 'steam:1100001156cfac9', 'gold', 0),
(2152, 'steam:1100001156cfac9', 'drugtest', 0),
(2153, 'steam:1100001156cfac9', '9mm_rounds', 0),
(2154, 'steam:1100001156cfac9', 'water', 0),
(2155, 'steam:1100001156cfac9', 'whiskey', 0),
(2156, 'steam:1100001156cfac9', 'petrol_raffin', 0),
(2157, 'steam:1100001156cfac9', 'tequila', 0),
(2158, 'steam:1100001156cfac9', 'WEAPON_STUNGUN', 0),
(2159, 'steam:1100001156cfac9', 'carokit', 0),
(2160, 'steam:1100001156cfac9', 'petrol', 0),
(2161, 'steam:1100001156cfac9', 'fakepee', 0),
(2162, 'steam:1100001156cfac9', 'wood', 0),
(2163, 'steam:1100001156cfac9', 'WEAPON_FLASHLIGHT', 0),
(2164, 'steam:1100001156cfac9', 'dabs', 0),
(2165, 'steam:1100001156cfac9', 'packaged_plank', 0),
(2166, 'steam:1100001156cfac9', 'tomato', 0),
(2167, 'steam:1100001156cfac9', 'copper', 0),
(2168, 'steam:1100001156cfac9', 'vodka', 0),
(2169, 'steam:1100001156cfac9', 'alive_chicken', 0),
(2170, 'steam:1100001156cfac9', 'lettuce', 0),
(2171, 'steam:1100001156cfac9', 'beer', 0),
(2172, 'steam:1100001156cfac9', 'slaughtered_chicken', 0),
(2173, 'steam:1100001156cfac9', 'ccheese', 0),
(2174, 'steam:1100001156cfac9', 'ephedrine', 0),
(2175, 'steam:1100001156cfac9', 'coca', 0),
(2176, 'steam:1100001156cfac9', 'blowpipe', 0),
(2177, 'steam:1100001156cfac9', 'poppy', 0),
(2178, 'steam:1100001156cfac9', 'cutted_wood', 0),
(2179, 'steam:1100001156cfac9', 'nuggets10', 0),
(2180, 'steam:1100001156cfac9', 'fixtool', 0),
(2181, 'steam:1100001156cfac9', 'narcan', 0),
(2182, 'steam:1100001156cfac9', 'WEAPON_PUMPSHOTGUN', 0),
(2183, 'steam:1100001156cfac9', 'washed_stone', 0),
(2184, 'steam:1100001156cfac9', 'bandage', 0),
(2185, 'steam:1100001156cfac9', 'crack', 0),
(2186, 'steam:1100001156cfac9', 'stone', 0),
(2187, 'steam:110000136177a4e', 'bread', 0),
(2188, 'steam:110000136177a4e', 'nugget', 0),
(2189, 'steam:110000136177a4e', 'shamburger', 0),
(2190, 'steam:110000136177a4e', 'WEAPON_KNIFE', 0),
(2191, 'steam:110000136177a4e', 'wool', 0),
(2192, 'steam:110000136177a4e', 'vhamburger', 0),
(2193, 'steam:110000136177a4e', 'marijuana', 0),
(2194, 'steam:110000136177a4e', 'essence', 0),
(2195, 'steam:110000136177a4e', 'painkiller', 0),
(2196, 'steam:110000136177a4e', 'cannabis', 0),
(2197, 'steam:110000136177a4e', 'cocaine', 0),
(2198, 'steam:110000136177a4e', 'medikit', 0),
(2199, 'steam:110000136177a4e', 'heroine', 0),
(2200, 'steam:110000136177a4e', 'pcp', 0),
(2201, 'steam:110000136177a4e', 'diamond', 0),
(2202, 'steam:110000136177a4e', 'gazbottle', 0),
(2203, 'steam:110000136177a4e', 'WEAPON_BAT', 0),
(2204, 'steam:110000136177a4e', 'cheese', 0),
(2205, 'steam:110000136177a4e', 'fixkit', 0),
(2206, 'steam:110000136177a4e', 'clettuce', 0),
(2207, 'steam:110000136177a4e', 'fburger', 0),
(2208, 'steam:110000136177a4e', 'fvburger', 0),
(2209, 'steam:110000136177a4e', 'potato', 0),
(2210, 'steam:110000136177a4e', 'meth', 0),
(2211, 'steam:110000136177a4e', 'vbread', 0),
(2212, 'steam:110000136177a4e', 'nuggets4', 0),
(2213, 'steam:110000136177a4e', 'packaged_chicken', 0),
(2214, 'steam:110000136177a4e', 'WEAPON_PISTOL', 0),
(2215, 'steam:110000136177a4e', 'iron', 0),
(2216, 'steam:110000136177a4e', 'fish', 0),
(2217, 'steam:110000136177a4e', 'carotool', 0),
(2218, 'steam:110000136177a4e', 'opium', 0),
(2219, 'steam:110000136177a4e', 'clothe', 0),
(2220, 'steam:110000136177a4e', 'ctomato', 0),
(2221, 'steam:110000136177a4e', 'breathalyzer', 0);
INSERT INTO `user_inventory` (`id`, `identifier`, `item`, `count`) VALUES
(2222, 'steam:110000136177a4e', 'fabric', 0),
(2223, 'steam:110000136177a4e', 'shotgun_shells', 0),
(2224, 'steam:110000136177a4e', 'ephedra', 0),
(2225, 'steam:110000136177a4e', 'gold', 0),
(2226, 'steam:110000136177a4e', 'drugtest', 0),
(2227, 'steam:110000136177a4e', '9mm_rounds', 0),
(2228, 'steam:110000136177a4e', 'water', 0),
(2229, 'steam:110000136177a4e', 'whiskey', 0),
(2230, 'steam:110000136177a4e', 'petrol_raffin', 0),
(2231, 'steam:110000136177a4e', 'tequila', 0),
(2232, 'steam:110000136177a4e', 'WEAPON_STUNGUN', 0),
(2233, 'steam:110000136177a4e', 'carokit', 0),
(2234, 'steam:110000136177a4e', 'petrol', 0),
(2235, 'steam:110000136177a4e', 'fakepee', 0),
(2236, 'steam:110000136177a4e', 'wood', 0),
(2237, 'steam:110000136177a4e', 'WEAPON_FLASHLIGHT', 0),
(2238, 'steam:110000136177a4e', 'dabs', 0),
(2239, 'steam:110000136177a4e', 'packaged_plank', 0),
(2240, 'steam:110000136177a4e', 'tomato', 0),
(2241, 'steam:110000136177a4e', 'vodka', 0),
(2242, 'steam:110000136177a4e', 'copper', 0),
(2243, 'steam:110000136177a4e', 'alive_chicken', 0),
(2244, 'steam:110000136177a4e', 'lettuce', 0),
(2245, 'steam:110000136177a4e', 'beer', 0),
(2246, 'steam:110000136177a4e', 'slaughtered_chicken', 0),
(2247, 'steam:110000136177a4e', 'ccheese', 0),
(2248, 'steam:110000136177a4e', 'ephedrine', 0),
(2249, 'steam:110000136177a4e', 'coca', 0),
(2250, 'steam:110000136177a4e', 'blowpipe', 0),
(2251, 'steam:110000136177a4e', 'poppy', 0),
(2252, 'steam:110000136177a4e', 'cutted_wood', 0),
(2253, 'steam:110000136177a4e', 'fixtool', 0),
(2254, 'steam:110000136177a4e', 'nuggets10', 0),
(2255, 'steam:110000136177a4e', 'narcan', 0),
(2256, 'steam:110000136177a4e', 'WEAPON_PUMPSHOTGUN', 0),
(2257, 'steam:110000136177a4e', 'washed_stone', 0),
(2258, 'steam:110000136177a4e', 'bandage', 0),
(2259, 'steam:110000136177a4e', 'crack', 0),
(2260, 'steam:110000136177a4e', 'stone', 0),
(2261, 'steam:110000116e48a9b', 'bread', 83),
(2262, 'steam:110000116e48a9b', 'wool', 0),
(2263, 'steam:110000116e48a9b', 'nugget', 0),
(2264, 'steam:110000116e48a9b', 'WEAPON_KNIFE', 0),
(2265, 'steam:110000116e48a9b', 'shamburger', 0),
(2266, 'steam:110000116e48a9b', 'vhamburger', 0),
(2267, 'steam:110000116e48a9b', 'marijuana', 0),
(2268, 'steam:110000116e48a9b', 'painkiller', 0),
(2269, 'steam:110000116e48a9b', 'essence', 0),
(2270, 'steam:110000116e48a9b', 'cannabis', 0),
(2271, 'steam:110000116e48a9b', 'cocaine', 0),
(2272, 'steam:110000116e48a9b', 'medikit', 0),
(2273, 'steam:110000116e48a9b', 'pcp', 0),
(2274, 'steam:110000116e48a9b', 'diamond', 0),
(2275, 'steam:110000116e48a9b', 'WEAPON_BAT', 0),
(2276, 'steam:110000116e48a9b', 'gazbottle', 0),
(2277, 'steam:110000116e48a9b', 'heroine', 0),
(2278, 'steam:110000116e48a9b', 'cheese', 0),
(2279, 'steam:110000116e48a9b', 'fixkit', 0),
(2280, 'steam:110000116e48a9b', 'clettuce', 0),
(2281, 'steam:110000116e48a9b', 'fburger', 0),
(2282, 'steam:110000116e48a9b', 'fvburger', 0),
(2283, 'steam:110000116e48a9b', 'vbread', 0),
(2284, 'steam:110000116e48a9b', 'potato', 0),
(2285, 'steam:110000116e48a9b', 'meth', 0),
(2286, 'steam:110000116e48a9b', 'nuggets4', 0),
(2287, 'steam:110000116e48a9b', 'packaged_chicken', 0),
(2288, 'steam:110000116e48a9b', 'WEAPON_PISTOL', 0),
(2289, 'steam:110000116e48a9b', 'iron', 0),
(2290, 'steam:110000116e48a9b', 'fish', 0),
(2291, 'steam:110000116e48a9b', 'carotool', 0),
(2292, 'steam:110000116e48a9b', 'opium', 0),
(2293, 'steam:110000116e48a9b', 'ctomato', 0),
(2294, 'steam:110000116e48a9b', 'clothe', 0),
(2295, 'steam:110000116e48a9b', 'fabric', 0),
(2296, 'steam:110000116e48a9b', 'breathalyzer', 0),
(2297, 'steam:110000116e48a9b', 'shotgun_shells', 0),
(2298, 'steam:110000116e48a9b', 'ephedra', 0),
(2299, 'steam:110000116e48a9b', 'gold', 0),
(2300, 'steam:110000116e48a9b', '9mm_rounds', 0),
(2301, 'steam:110000116e48a9b', 'water', 86),
(2302, 'steam:110000116e48a9b', 'drugtest', 0),
(2303, 'steam:110000116e48a9b', 'whiskey', 0),
(2304, 'steam:110000116e48a9b', 'petrol_raffin', 0),
(2305, 'steam:110000116e48a9b', 'WEAPON_STUNGUN', 0),
(2306, 'steam:110000116e48a9b', 'tequila', 0),
(2307, 'steam:110000116e48a9b', 'carokit', 0),
(2308, 'steam:110000116e48a9b', 'petrol', 0),
(2309, 'steam:110000116e48a9b', 'fakepee', 0),
(2310, 'steam:110000116e48a9b', 'dabs', 0),
(2311, 'steam:110000116e48a9b', 'wood', 0),
(2312, 'steam:110000116e48a9b', 'WEAPON_FLASHLIGHT', 0),
(2313, 'steam:110000116e48a9b', 'tomato', 0),
(2314, 'steam:110000116e48a9b', 'vodka', 0),
(2315, 'steam:110000116e48a9b', 'packaged_plank', 0),
(2316, 'steam:110000116e48a9b', 'copper', 0),
(2317, 'steam:110000116e48a9b', 'alive_chicken', 0),
(2318, 'steam:110000116e48a9b', 'lettuce', 0),
(2319, 'steam:110000116e48a9b', 'beer', 0),
(2320, 'steam:110000116e48a9b', 'slaughtered_chicken', 0),
(2321, 'steam:110000116e48a9b', 'ephedrine', 0),
(2322, 'steam:110000116e48a9b', 'ccheese', 0),
(2323, 'steam:110000116e48a9b', 'coca', 0),
(2324, 'steam:110000116e48a9b', 'blowpipe', 0),
(2325, 'steam:110000116e48a9b', 'poppy', 0),
(2326, 'steam:110000116e48a9b', 'cutted_wood', 0),
(2327, 'steam:110000116e48a9b', 'fixtool', 0),
(2328, 'steam:110000116e48a9b', 'nuggets10', 0),
(2329, 'steam:110000116e48a9b', 'narcan', 0),
(2330, 'steam:110000116e48a9b', 'WEAPON_PUMPSHOTGUN', 0),
(2331, 'steam:110000116e48a9b', 'washed_stone', 0),
(2332, 'steam:110000116e48a9b', 'bandage', 0),
(2333, 'steam:110000116e48a9b', 'crack', 0),
(2334, 'steam:110000116e48a9b', 'stone', 0),
(2335, 'steam:110000106197c1a', 'pcp', 0),
(2336, 'steam:110000106197c1a', 'gold', 0),
(2337, 'steam:110000106197c1a', 'painkiller', 0),
(2338, 'steam:110000106197c1a', 'clettuce', 0),
(2339, 'steam:110000106197c1a', 'petrol', 0),
(2340, 'steam:110000106197c1a', 'cheese', 0),
(2341, 'steam:110000106197c1a', 'meth', 0),
(2342, 'steam:110000106197c1a', 'ctomato', 0),
(2343, 'steam:110000106197c1a', 'carotool', 0),
(2344, 'steam:110000106197c1a', 'diamond', 0),
(2345, 'steam:110000106197c1a', 'bread', 0),
(2346, 'steam:110000106197c1a', 'medikit', 0),
(2347, 'steam:110000106197c1a', 'WEAPON_PISTOL', 0),
(2348, 'steam:110000106197c1a', 'cannabis', 0),
(2349, 'steam:110000106197c1a', 'clothe', 0),
(2350, 'steam:110000106197c1a', 'ephedra', 0),
(2351, 'steam:110000106197c1a', 'fvburger', 0),
(2352, 'steam:110000106197c1a', 'copper', 0),
(2353, 'steam:110000106197c1a', 'iron', 0),
(2354, 'steam:110000106197c1a', 'bandage', 0),
(2355, 'steam:110000106197c1a', 'WEAPON_FLASHLIGHT', 0),
(2356, 'steam:110000106197c1a', 'stone', 0),
(2357, 'steam:110000106197c1a', 'water', 0),
(2358, 'steam:110000106197c1a', 'gazbottle', 0),
(2359, 'steam:110000106197c1a', 'essence', 0),
(2360, 'steam:110000106197c1a', 'beer', 0),
(2361, 'steam:110000106197c1a', 'blowpipe', 0),
(2362, 'steam:110000106197c1a', 'nuggets4', 0),
(2363, 'steam:110000106197c1a', 'tomato', 0),
(2364, 'steam:110000106197c1a', 'potato', 0),
(2365, 'steam:110000106197c1a', 'shotgun_shells', 0),
(2366, 'steam:110000106197c1a', 'ccheese', 0),
(2367, 'steam:110000106197c1a', 'lettuce', 0),
(2368, 'steam:110000106197c1a', 'carokit', 0),
(2369, 'steam:110000106197c1a', 'wool', 0),
(2370, 'steam:110000106197c1a', 'heroine', 0),
(2371, 'steam:110000106197c1a', 'marijuana', 0),
(2372, 'steam:110000106197c1a', 'WEAPON_BAT', 0),
(2373, 'steam:110000106197c1a', 'fabric', 0),
(2374, 'steam:110000106197c1a', 'crack', 0),
(2375, 'steam:110000106197c1a', 'fakepee', 0),
(2376, 'steam:110000106197c1a', 'fburger', 0),
(2377, 'steam:110000106197c1a', 'cutted_wood', 0),
(2378, 'steam:110000106197c1a', 'WEAPON_STUNGUN', 0),
(2379, 'steam:110000106197c1a', 'fish', 0),
(2380, 'steam:110000106197c1a', 'wood', 0),
(2381, 'steam:110000106197c1a', 'coca', 0),
(2382, 'steam:110000106197c1a', 'whiskey', 0),
(2383, 'steam:110000106197c1a', 'tequila', 0),
(2384, 'steam:110000106197c1a', 'washed_stone', 0),
(2385, 'steam:110000106197c1a', 'packaged_chicken', 0),
(2386, 'steam:110000106197c1a', 'ephedrine', 0),
(2387, 'steam:110000106197c1a', 'vhamburger', 0),
(2388, 'steam:110000106197c1a', 'vbread', 0),
(2389, 'steam:110000106197c1a', 'slaughtered_chicken', 0),
(2390, 'steam:110000106197c1a', 'fixtool', 0),
(2391, 'steam:110000106197c1a', 'breathalyzer', 0),
(2392, 'steam:110000106197c1a', 'packaged_plank', 0),
(2393, 'steam:110000106197c1a', 'alive_chicken', 0),
(2394, 'steam:110000106197c1a', 'shamburger', 0),
(2395, 'steam:110000106197c1a', 'WEAPON_PUMPSHOTGUN', 0),
(2396, 'steam:110000106197c1a', 'poppy', 0),
(2397, 'steam:110000106197c1a', 'vodka', 0),
(2398, 'steam:110000106197c1a', 'fixkit', 0),
(2399, 'steam:110000106197c1a', 'cocaine', 0),
(2400, 'steam:110000106197c1a', 'petrol_raffin', 0),
(2401, 'steam:110000106197c1a', 'opium', 0),
(2402, 'steam:110000106197c1a', 'dabs', 0),
(2403, 'steam:110000106197c1a', 'nuggets10', 0),
(2404, 'steam:110000106197c1a', 'narcan', 0),
(2405, 'steam:110000106197c1a', 'nugget', 0),
(2406, 'steam:110000106197c1a', '9mm_rounds', 0),
(2407, 'steam:110000106197c1a', 'drugtest', 0),
(2408, 'steam:110000106197c1a', 'WEAPON_KNIFE', 0),
(2409, 'steam:11000010a01bdb9', 'potato', 0),
(2410, 'steam:11000010a01bdb9', 'nugget', 0),
(2411, 'steam:11000010a01bdb9', 'fburger', 0),
(2412, 'steam:11000010a01bdb9', 'clettuce', 0),
(2413, 'steam:11000010a01bdb9', 'fvburger', 0),
(2414, 'steam:11000010a01bdb9', 'ccheese', 0),
(2415, 'steam:11000010a01bdb9', 'lettuce', 0),
(2416, 'steam:11000010a01bdb9', 'tomato', 0),
(2417, 'steam:11000010a01bdb9', 'nuggets4', 0),
(2418, 'steam:11000010a01bdb9', 'cheese', 0),
(2419, 'steam:11000010a01bdb9', 'ctomato', 0),
(2420, 'steam:11000010a01bdb9', 'nuggets10', 0),
(2421, 'steam:11000010a01bdb9', 'vbread', 0),
(2422, 'steam:11000010a01bdb9', 'vhamburger', 0),
(2423, 'steam:11000010a01bdb9', 'shamburger', 0),
(2424, 'steam:1100001081d1893', 'weed_pooch', 0),
(2425, 'steam:1100001081d1893', 'meth_pooch', 0),
(2426, 'steam:1100001081d1893', 'coke_pooch', 0),
(2427, 'steam:1100001081d1893', 'opium_pooch', 0),
(2428, 'steam:1100001081d1893', 'weed', 0),
(2429, 'steam:1100001081d1893', 'coke', 0),
(2430, 'steam:11000010b49a743', 'meth_pooch', 0),
(2431, 'steam:11000010b49a743', 'weed', 0),
(2432, 'steam:11000010b49a743', 'opium_pooch', 0),
(2433, 'steam:11000010b49a743', 'weed_pooch', 0),
(2434, 'steam:11000010b49a743', 'coke_pooch', 0),
(2435, 'steam:11000010b49a743', 'coke', 0),
(2436, 'steam:11000010e2a62e2', 'weed', 0),
(2437, 'steam:11000010e2a62e2', 'opium_pooch', 0),
(2438, 'steam:11000010e2a62e2', 'weed_pooch', 0),
(2439, 'steam:11000010e2a62e2', 'meth_pooch', 0),
(2440, 'steam:11000010e2a62e2', 'coke_pooch', 0),
(2441, 'steam:11000010e2a62e2', 'coke', 0),
(2442, 'steam:11000010a01bdb9', 'coke_pooch', 0),
(2443, 'steam:11000010a01bdb9', 'loka', 0),
(2444, 'steam:11000010a01bdb9', 'chips', 0),
(2445, 'steam:11000010a01bdb9', 'burger', 0),
(2446, 'steam:11000010a01bdb9', 'coke', 0),
(2447, 'steam:11000010a01bdb9', 'cigarett', 0),
(2448, 'steam:11000010a01bdb9', 'pizza', 0),
(2449, 'steam:11000010a01bdb9', 'cheesebows', 0),
(2450, 'steam:11000010a01bdb9', 'lighter', 0),
(2451, 'steam:11000010a01bdb9', 'weed_pooch', 1),
(2452, 'steam:11000010a01bdb9', 'lotteryticket', 0),
(2453, 'steam:11000010a01bdb9', 'weed', 0),
(2454, 'steam:11000010a01bdb9', 'meth_pooch', 0),
(2455, 'steam:11000010a01bdb9', 'sprite', 0),
(2456, 'steam:11000010a01bdb9', 'fanta', 0),
(2457, 'steam:11000010a01bdb9', 'pastacarbonara', 0),
(2458, 'steam:11000010a01bdb9', 'opium_pooch', 0),
(2459, 'steam:11000010a01bdb9', 'marabou', 0),
(2460, 'steam:11000010a01bdb9', 'macka', 0),
(2461, 'steam:11000010a01bdb9', 'cocacola', 1),
(2462, 'steam:1100001081d1893', 'chips', 0),
(2463, 'steam:1100001081d1893', 'lotteryticket', 0),
(2464, 'steam:1100001081d1893', 'lighter', 0),
(2465, 'steam:1100001081d1893', 'fanta', 0),
(2466, 'steam:1100001081d1893', 'macka', 0),
(2467, 'steam:1100001081d1893', 'pastacarbonara', 0),
(2468, 'steam:1100001081d1893', 'sprite', 0),
(2469, 'steam:1100001081d1893', 'pizza', 0),
(2470, 'steam:1100001081d1893', 'cigarett', 0),
(2471, 'steam:1100001081d1893', 'cheesebows', 0),
(2472, 'steam:1100001081d1893', 'cocacola', 0),
(2473, 'steam:1100001081d1893', 'marabou', 0),
(2474, 'steam:1100001081d1893', 'loka', 0),
(2475, 'steam:1100001081d1893', 'burger', 0),
(2476, 'steam:1100001081d1893', 'radio', 1),
(2477, 'steam:11000010a01bdb9', 'radio', 1),
(2478, 'steam:11000010e2a62e2', 'lighter', 0),
(2479, 'steam:11000010e2a62e2', 'pastacarbonara', 0),
(2480, 'steam:11000010e2a62e2', 'cheesebows', 0),
(2481, 'steam:11000010e2a62e2', 'macka', 0),
(2482, 'steam:11000010e2a62e2', 'cocacola', 0),
(2483, 'steam:11000010e2a62e2', 'loka', 0),
(2484, 'steam:11000010e2a62e2', 'radio', 1),
(2485, 'steam:11000010e2a62e2', 'cigarett', 0),
(2486, 'steam:11000010e2a62e2', 'sprite', 0),
(2487, 'steam:11000010e2a62e2', 'pizza', 0),
(2488, 'steam:11000010e2a62e2', 'chips', 0),
(2489, 'steam:11000010e2a62e2', 'lotteryticket', 0),
(2490, 'steam:11000010e2a62e2', 'marabou', 0),
(2491, 'steam:11000010e2a62e2', 'fanta', 0),
(2492, 'steam:11000010e2a62e2', 'burger', 0),
(2493, 'steam:110000111c332ed', 'lighter', 0),
(2494, 'steam:110000111c332ed', 'pastacarbonara', 0),
(2495, 'steam:110000111c332ed', 'cheesebows', 0),
(2496, 'steam:110000111c332ed', 'macka', 0),
(2497, 'steam:110000111c332ed', 'coke_pooch', 0),
(2498, 'steam:110000111c332ed', 'opium_pooch', 0),
(2499, 'steam:110000111c332ed', 'loka', 0),
(2500, 'steam:110000111c332ed', 'cocacola', 0),
(2501, 'steam:110000111c332ed', 'meth_pooch', 0),
(2502, 'steam:110000111c332ed', 'weed', 0),
(2503, 'steam:110000111c332ed', 'radio', 1),
(2504, 'steam:110000111c332ed', 'weed_pooch', 0),
(2505, 'steam:110000111c332ed', 'cigarett', 0),
(2506, 'steam:110000111c332ed', 'sprite', 0),
(2507, 'steam:110000111c332ed', 'pizza', 0),
(2508, 'steam:110000111c332ed', 'chips', 0),
(2509, 'steam:110000111c332ed', 'lotteryticket', 0),
(2510, 'steam:110000111c332ed', 'marabou', 0),
(2511, 'steam:110000111c332ed', 'fanta', 0),
(2512, 'steam:110000111c332ed', 'burger', 0),
(2513, 'steam:110000111c332ed', 'coke', 0),
(2514, 'steam:11000013e7edea3', 'lighter', 0),
(2515, 'steam:11000013e7edea3', 'coke_pooch', 0),
(2516, 'steam:11000013e7edea3', 'pastacarbonara', 0),
(2517, 'steam:11000013e7edea3', 'cheesebows', 0),
(2518, 'steam:11000013e7edea3', 'macka', 0),
(2519, 'steam:11000013e7edea3', 'cocacola', 2),
(2520, 'steam:11000013e7edea3', 'opium_pooch', 0),
(2521, 'steam:11000013e7edea3', 'weed', 0),
(2522, 'steam:11000013e7edea3', 'loka', 0),
(2523, 'steam:11000013e7edea3', 'meth_pooch', 0),
(2524, 'steam:11000013e7edea3', 'radio', 1),
(2525, 'steam:11000013e7edea3', 'weed_pooch', 0),
(2526, 'steam:11000013e7edea3', 'sprite', 0),
(2527, 'steam:11000013e7edea3', 'cigarett', 0),
(2528, 'steam:11000013e7edea3', 'pizza', 0),
(2529, 'steam:11000013e7edea3', 'chips', 0),
(2530, 'steam:11000013e7edea3', 'marabou', 0),
(2531, 'steam:11000013e7edea3', 'lotteryticket', 0),
(2532, 'steam:11000013e7edea3', 'fanta', 0),
(2533, 'steam:11000013e7edea3', 'burger', 0),
(2534, 'steam:11000013e7edea3', 'coke', 0),
(2535, 'steam:1100001156cfac9', 'lighter', 0),
(2536, 'steam:1100001156cfac9', 'coke_pooch', 0),
(2537, 'steam:1100001156cfac9', 'pastacarbonara', 0),
(2538, 'steam:1100001156cfac9', 'cheesebows', 0),
(2539, 'steam:1100001156cfac9', 'macka', 0),
(2540, 'steam:1100001156cfac9', 'cocacola', 0),
(2541, 'steam:1100001156cfac9', 'meth_pooch', 0),
(2542, 'steam:1100001156cfac9', 'opium_pooch', 0),
(2543, 'steam:1100001156cfac9', 'radio', 1),
(2544, 'steam:1100001156cfac9', 'loka', 0),
(2545, 'steam:1100001156cfac9', 'weed_pooch', 0),
(2546, 'steam:1100001156cfac9', 'cigarett', 0),
(2547, 'steam:1100001156cfac9', 'weed', 0),
(2548, 'steam:1100001156cfac9', 'sprite', 0),
(2549, 'steam:1100001156cfac9', 'chips', 0),
(2550, 'steam:1100001156cfac9', 'lotteryticket', 0),
(2551, 'steam:1100001156cfac9', 'marabou', 0),
(2552, 'steam:1100001156cfac9', 'pizza', 0),
(2553, 'steam:1100001156cfac9', 'fanta', 0),
(2554, 'steam:1100001156cfac9', 'burger', 0),
(2555, 'steam:1100001156cfac9', 'coke', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_licenses`
--

CREATE TABLE `user_licenses` (
  `id` int(11) NOT NULL,
  `type` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `owner` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `user_licenses`
--

INSERT INTO `user_licenses` (`id`, `type`, `owner`) VALUES
(1, 'dmv', 'steam:110000132580eb0');

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL,
  `category` varchar(60) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`name`, `model`, `price`, `category`) VALUES
('Dodge Charger', '16charger', 35000, 'super'),
('Lambo Huracan', '18performante', 250000, 'super'),
('Ferrari', '599gtox', 155000, 'super'),
('Akuma', 'AKUMA', 7500, 'motorcycles'),
('Adder', 'adder', 1100000, 'super'),
('Alpha', 'alpha', 60000, 'sports'),
('Ardent', 'ardent', 1150000, 'sportsclassics'),
('Asea', 'asea', 5500, 'sedans'),
('Autarch', 'autarch', 1955000, 'super'),
('Avarus', 'avarus', 18000, 'motorcycles'),
('Bagger', 'bagger', 13500, 'motorcycles'),
('Baller', 'baller2', 40000, 'suvs'),
('Baller Sport', 'baller3', 60000, 'suvs'),
('Banshee', 'banshee', 70000, 'sports'),
('Banshee 900R', 'banshee2', 255000, 'super'),
('Bati 801', 'bati', 12000, 'motorcycles'),
('Bati 801RR', 'bati2', 19000, 'motorcycles'),
('Bestia GTS', 'bestiagts', 55000, 'sports'),
('BF400', 'bf400', 6500, 'motorcycles'),
('Bf Injection', 'bfinjection', 16000, 'offroad'),
('Bifta', 'bifta', 12000, 'offroad'),
('Bison', 'bison', 45000, 'vans'),
('Blade', 'blade', 15000, 'muscle'),
('Blazer', 'blazer', 6500, 'offroad'),
('Blazer Sport', 'blazer4', 8500, 'offroad'),
('blazer5', 'blazer5', 1755600, 'offroad'),
('Blista', 'blista', 8000, 'compacts'),
('BMX (velo)', 'bmx', 160, 'motorcycles'),
('Bobcat XL', 'bobcatxl', 32000, 'vans'),
('Brawler', 'brawler', 45000, 'offroad'),
('Brioso R/A', 'brioso', 18000, 'compacts'),
('Btype', 'btype', 62000, 'sportsclassics'),
('Btype Hotroad', 'btype2', 155000, 'sportsclassics'),
('Btype Luxe', 'btype3', 85000, 'sportsclassics'),
('Buccaneer', 'buccaneer', 18000, 'muscle'),
('Buccaneer Rider', 'buccaneer2', 24000, 'muscle'),
('Buffalo', 'buffalo', 12000, 'sports'),
('Buffalo S', 'buffalo2', 20000, 'sports'),
('Bullet', 'bullet', 90000, 'super'),
('Burrito', 'burrito3', 19000, 'vans'),
('Camper', 'camper', 42000, 'vans'),
('Carbonizzare', 'carbonizzare', 75000, 'sports'),
('Carbon RS', 'carbonrs', 18000, 'motorcycles'),
('Casco', 'casco', 30000, 'sportsclassics'),
('Cavalcade', 'cavalcade2', 55000, 'suvs'),
('Cheetah', 'cheetah', 375000, 'super'),
('Chimera', 'chimera', 38000, 'motorcycles'),
('Chino', 'chino', 15000, 'muscle'),
('Chino Luxe', 'chino2', 19000, 'muscle'),
('Cliffhanger', 'cliffhanger', 9500, 'motorcycles'),
('Cognoscenti Cabrio', 'cogcabrio', 55000, 'coupes'),
('Cognoscenti', 'cognoscenti', 55000, 'sedans'),
('Comet', 'comet2', 65000, 'sports'),
('Comet 5', 'comet5', 1145000, 'sports'),
('Contender', 'contender', 70000, 'suvs'),
('Coquette', 'coquette', 65000, 'sports'),
('Coquette Classic', 'coquette2', 40000, 'sportsclassics'),
('Coquette BlackFin', 'coquette3', 55000, 'muscle'),
('Corvette', 'corvettezr1', 75000, 'super'),
('Cruiser (velo)', 'cruiser', 510, 'motorcycles'),
('Cyclone', 'cyclone', 1890000, 'super'),
('Daemon', 'daemon', 11500, 'motorcycles'),
('Daemon High', 'daemon2', 13500, 'motorcycles'),
('Defiler', 'defiler', 9800, 'motorcycles'),
('Deluxo', 'deluxo', 4721500, 'sportsclassics'),
('Dominator', 'dominator', 35000, 'muscle'),
('Double T', 'double', 28000, 'motorcycles'),
('Dubsta', 'dubsta', 45000, 'suvs'),
('Dubsta Luxuary', 'dubsta2', 60000, 'suvs'),
('Bubsta 6x6', 'dubsta3', 120000, 'offroad'),
('Dukes', 'dukes', 28000, 'muscle'),
('Dune Buggy', 'dune', 8000, 'offroad'),
('Elegy', 'elegy2', 38500, 'sports'),
('Emperor', 'emperor', 8500, 'sedans'),
('Enduro', 'enduro', 5500, 'motorcycles'),
('Entity XF', 'entityxf', 425000, 'super'),
('Esskey', 'esskey', 4200, 'motorcycles'),
('Exemplar', 'exemplar', 32000, 'coupes'),
('F620', 'f620', 40000, 'coupes'),
('Faction', 'faction', 20000, 'muscle'),
('Faction Rider', 'faction2', 30000, 'muscle'),
('Faction XL', 'faction3', 40000, 'muscle'),
('Faggio', 'faggio', 1900, 'motorcycles'),
('Vespa', 'faggio2', 2800, 'motorcycles'),
('Felon', 'felon', 42000, 'coupes'),
('Felon GT', 'felon2', 55000, 'coupes'),
('Feltzer', 'feltzer2', 55000, 'sports'),
('Stirling GT', 'feltzer3', 65000, 'sportsclassics'),
('Fixter (velo)', 'fixter', 225, 'motorcycles'),
('FMJ', 'fmj', 185000, 'super'),
('Fhantom', 'fq2', 17000, 'suvs'),
('Fugitive', 'fugitive', 12000, 'sedans'),
('Furore GT', 'furoregt', 45000, 'sports'),
('Fusilade', 'fusilade', 40000, 'sports'),
('Gargoyle', 'gargoyle', 16500, 'motorcycles'),
('Gauntlet', 'gauntlet', 30000, 'muscle'),
('Gang Burrito', 'gburrito', 45000, 'vans'),
('Burrito', 'gburrito2', 29000, 'vans'),
('Glendale', 'glendale', 6500, 'sedans'),
('Grabger', 'granger', 50000, 'suvs'),
('Gresley', 'gresley', 47500, 'suvs'),
('GT 500', 'gt500', 785000, 'sportsclassics'),
('Guardian', 'guardian', 45000, 'offroad'),
('Hakuchou', 'hakuchou', 31000, 'motorcycles'),
('Hakuchou Sport', 'hakuchou2', 55000, 'motorcycles'),
('Hermes', 'hermes', 535000, 'muscle'),
('Hexer', 'hexer', 12000, 'motorcycles'),
('Hotknife', 'hotknife', 125000, 'muscle'),
('Huntley S', 'huntley', 40000, 'suvs'),
('Hustler', 'hustler', 625000, 'muscle'),
('Infernus', 'infernus', 180000, 'super'),
('Innovation', 'innovation', 23500, 'motorcycles'),
('Intruder', 'intruder', 7500, 'sedans'),
('Issi', 'issi2', 10000, 'compacts'),
('Jackal', 'jackal', 38000, 'coupes'),
('Jester', 'jester', 65000, 'sports'),
('Jester(Racecar)', 'jester2', 135000, 'sports'),
('Journey', 'journey', 6500, 'vans'),
('Kamacho', 'kamacho', 345000, 'offroad'),
('Khamelion', 'khamelion', 38000, 'sports'),
('Kuruma', 'kuruma', 30000, 'sports'),
('Lambo Svj', 'lamboavj', 400000, 'super'),
('Landstalker', 'landstalker', 35000, 'suvs'),
('Rmodlb750sv', 'lb750sv', 400000, 'super'),
('RE-7B', 'le7b', 325000, 'super'),
('Lynx', 'lynx', 40000, 'sports'),
('Mamba', 'mamba', 70000, 'sports'),
('Manana', 'manana', 12800, 'sportsclassics'),
('Manchez', 'manchez', 5300, 'motorcycles'),
('Massacro', 'massacro', 65000, 'sports'),
('Massacro(Racecar)', 'massacro2', 130000, 'sports'),
('Mesa', 'mesa', 16000, 'suvs'),
('Mesa Trail', 'mesa3', 40000, 'suvs'),
('Minivan', 'minivan', 13000, 'vans'),
('Monroe', 'monroe', 55000, 'sportsclassics'),
('The Liberator', 'monster', 210000, 'offroad'),
('Moonbeam', 'moonbeam', 18000, 'vans'),
('Moonbeam Rider', 'moonbeam2', 35000, 'vans'),
('Nemesis', 'nemesis', 5800, 'motorcycles'),
('Neon', 'neon', 1500000, 'sports'),
('Nightblade', 'nightblade', 35000, 'motorcycles'),
('Nightshade', 'nightshade', 65000, 'muscle'),
('9F', 'ninef', 65000, 'sports'),
('9F Cabrio', 'ninef2', 80000, 'sports'),
('Omnis', 'omnis', 35000, 'sports'),
('Oppressor', 'oppressor', 3524500, 'super'),
('Oracle XS', 'oracle2', 35000, 'coupes'),
('Osiris', 'osiris', 160000, 'super'),
('Panto', 'panto', 10000, 'compacts'),
('Paradise', 'paradise', 19000, 'vans'),
('Pariah', 'pariah', 1420000, 'sports'),
('Patriot', 'patriot', 55000, 'suvs'),
('PCJ-600', 'pcj', 6200, 'motorcycles'),
('Penumbra', 'penumbra', 28000, 'sports'),
('Pfister', 'pfister811', 85000, 'super'),
('Phoenix', 'phoenix', 12500, 'muscle'),
('Picador', 'picador', 18000, 'muscle'),
('Pigalle', 'pigalle', 20000, 'sportsclassics'),
('Prairie', 'prairie', 12000, 'compacts'),
('Premier', 'premier', 8000, 'sedans'),
('Primo Custom', 'primo2', 14000, 'sedans'),
('X80 Proto', 'prototipo', 2500000, 'super'),
('Radius', 'radi', 29000, 'suvs'),
('raiden', 'raiden', 1375000, 'sports'),
('Dodge Ram', 'ram2500', 30000, 'super'),
('Rapid GT', 'rapidgt', 35000, 'sports'),
('Rapid GT Convertible', 'rapidgt2', 45000, 'sports'),
('Rapid GT3', 'rapidgt3', 885000, 'sportsclassics'),
('Reaper', 'reaper', 150000, 'super'),
('Rebel', 'rebel2', 35000, 'offroad'),
('Regina', 'regina', 5000, 'sedans'),
('Retinue', 'retinue', 615000, 'sportsclassics'),
('Revolter', 'revolter', 1610000, 'sports'),
('riata', 'riata', 380000, 'offroad'),
('Amg C63', 'rmodamgc63', 56000, 'super'),
('Amg GT63', 'rmodgt63', 65000, 'super'),
('Ford Mustang ', 'rmodmustang', 40000, 'super'),
('Rocoto', 'rocoto', 45000, 'suvs'),
('Ruffian', 'ruffian', 6800, 'motorcycles'),
('Ruiner 2', 'ruiner2', 5745600, 'muscle'),
('Rumpo', 'rumpo', 15000, 'vans'),
('Rumpo Trail', 'rumpo3', 19500, 'vans'),
('Sabre Turbo', 'sabregt', 20000, 'muscle'),
('Sabre GT', 'sabregt2', 25000, 'muscle'),
('Sanchez', 'sanchez', 5300, 'motorcycles'),
('Sanchez Sport', 'sanchez2', 5300, 'motorcycles'),
('Sanctus', 'sanctus', 25000, 'motorcycles'),
('Sandking', 'sandking', 55000, 'offroad'),
('Savestra', 'savestra', 990000, 'sportsclassics'),
('SC 1', 'sc1', 1603000, 'super'),
('Schafter', 'schafter2', 25000, 'sedans'),
('Schafter V12', 'schafter3', 50000, 'sports'),
('Scorcher (velo)', 'scorcher', 280, 'motorcycles'),
('Seminole', 'seminole', 25000, 'suvs'),
('Sentinel', 'sentinel', 32000, 'coupes'),
('Sentinel XS', 'sentinel2', 40000, 'coupes'),
('Sentinel3', 'sentinel3', 650000, 'sports'),
('Seven 70', 'seven70', 39500, 'sports'),
('ETR1', 'sheava', 220000, 'super'),
('Shotaro Concept', 'shotaro', 320000, 'motorcycles'),
('Slam Van', 'slamvan3', 11500, 'muscle'),
('Sovereign', 'sovereign', 22000, 'motorcycles'),
('Stinger', 'stinger', 80000, 'sportsclassics'),
('Stinger GT', 'stingergt', 75000, 'sportsclassics'),
('Streiter', 'streiter', 500000, 'sports'),
('Stretch', 'stretch', 90000, 'sedans'),
('Stromberg', 'stromberg', 3185350, 'sports'),
('Sultan', 'sultan', 15000, 'sports'),
('Sultan RS', 'sultanrs', 65000, 'super'),
('Super Diamond', 'superd', 130000, 'sedans'),
('Surano', 'surano', 50000, 'sports'),
('Surfer', 'surfer', 12000, 'vans'),
('T20', 't20', 300000, 'super'),
('Tailgater', 'tailgater', 30000, 'sedans'),
('Tampa', 'tampa', 16000, 'muscle'),
('Drift Tampa', 'tampa2', 80000, 'sports'),
('Thrust', 'thrust', 24000, 'motorcycles'),
('Tri bike (velo)', 'tribike3', 520, 'motorcycles'),
('Trophy Truck', 'trophytruck', 60000, 'offroad'),
('Trophy Truck Limited', 'trophytruck2', 80000, 'offroad'),
('Tropos', 'tropos', 40000, 'sports'),
('Turismo R', 'turismor', 350000, 'super'),
('Tyrus', 'tyrus', 600000, 'super'),
('Vacca', 'vacca', 120000, 'super'),
('Vader', 'vader', 7200, 'motorcycles'),
('Verlierer', 'verlierer2', 70000, 'sports'),
('Vigero', 'vigero', 12500, 'muscle'),
('Virgo', 'virgo', 14000, 'muscle'),
('Viseris', 'viseris', 875000, 'sportsclassics'),
('Visione', 'visione', 2250000, 'super'),
('Voltic', 'voltic', 90000, 'super'),
('Voltic 2', 'voltic2', 3830400, 'super'),
('Voodoo', 'voodoo', 7200, 'muscle'),
('Vortex', 'vortex', 9800, 'motorcycles'),
('Warrener', 'warrener', 4000, 'sedans'),
('Washington', 'washington', 9000, 'sedans'),
('Windsor', 'windsor', 95000, 'coupes'),
('Windsor Drop', 'windsor2', 125000, 'coupes'),
('Woflsbane', 'wolfsbane', 9000, 'motorcycles'),
('XLS', 'xls', 32000, 'suvs'),
('Yosemite', 'yosemite', 485000, 'muscle'),
('Youga', 'youga', 10800, 'vans'),
('Youga Luxuary', 'youga2', 14500, 'vans'),
('Z190', 'z190', 900000, 'sportsclassics'),
('Zentorno', 'zentorno', 1500000, 'super'),
('Zion', 'zion', 36000, 'coupes'),
('Zion Cabrio', 'zion2', 45000, 'coupes'),
('Zombie', 'zombiea', 9500, 'motorcycles'),
('Zombie Luxuary', 'zombieb', 12000, 'motorcycles'),
('Z-Type', 'ztype', 220000, 'sportsclassics');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_categories`
--

CREATE TABLE `vehicle_categories` (
  `name` varchar(60) COLLATE utf8mb4_bin NOT NULL,
  `label` varchar(60) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `vehicle_categories`
--

INSERT INTO `vehicle_categories` (`name`, `label`) VALUES
('compacts', 'Compacts'),
('coupes', 'Coupes'),
('motorcycles', 'Motos'),
('muscle', 'Muscle'),
('offroad', 'Off Road'),
('sedans', 'Sedans'),
('sports', 'Sports'),
('sportsclassics', 'Sports Classics'),
('super', 'Super'),
('suvs', 'SUVs'),
('vans', 'Vans');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_sold`
--

CREATE TABLE `vehicle_sold` (
  `client` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `model` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `plate` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `soldby` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `date` varchar(50) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Table structure for table `weashops`
--

CREATE TABLE `weashops` (
  `id` int(11) NOT NULL,
  `zone` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `item` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `weashops`
--

INSERT INTO `weashops` (`id`, `zone`, `item`, `price`) VALUES
(1, 'GunShop', 'WEAPON_PISTOL', 300),
(2, 'BlackWeashop', 'WEAPON_PISTOL', 500),
(3, 'GunShop', 'WEAPON_FLASHLIGHT', 60),
(4, 'BlackWeashop', 'WEAPON_FLASHLIGHT', 70),
(5, 'GunShop', 'WEAPON_MACHETE', 90),
(6, 'BlackWeashop', 'WEAPON_MACHETE', 110),
(7, 'GunShop', 'WEAPON_NIGHTSTICK', 150),
(8, 'BlackWeashop', 'WEAPON_NIGHTSTICK', 150),
(9, 'GunShop', 'WEAPON_BAT', 100),
(10, 'BlackWeashop', 'WEAPON_BAT', 100),
(11, 'GunShop', 'WEAPON_STUNGUN', 50),
(12, 'BlackWeashop', 'WEAPON_STUNGUN', 50),
(13, 'GunShop', 'WEAPON_MICROSMG', 1400),
(14, 'BlackWeashop', 'WEAPON_MICROSMG', 1700),
(15, 'GunShop', 'WEAPON_PUMPSHOTGUN', 3400),
(16, 'BlackWeashop', 'WEAPON_PUMPSHOTGUN', 3500),
(17, 'GunShop', 'WEAPON_ASSAULTRIFLE', 10000),
(18, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 11000),
(19, 'GunShop', 'WEAPON_SPECIALCARBINE', 15000),
(20, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 16500),
(21, 'GunShop', 'WEAPON_SNIPERRIFLE', 22000),
(22, 'BlackWeashop', 'WEAPON_SNIPERRIFLE', 24000),
(23, 'GunShop', 'WEAPON_FIREWORK', 18000),
(24, 'BlackWeashop', 'WEAPON_FIREWORK', 20000),
(25, 'GunShop', 'WEAPON_GRENADE', 500),
(26, 'BlackWeashop', 'WEAPON_GRENADE', 650),
(27, 'GunShop', 'WEAPON_BZGAS', 200),
(28, 'BlackWeashop', 'WEAPON_BZGAS', 350),
(29, 'GunShop', 'WEAPON_FIREEXTINGUISHER', 100),
(30, 'BlackWeashop', 'WEAPON_FIREEXTINGUISHER', 100),
(31, 'GunShop', 'WEAPON_BALL', 50),
(32, 'BlackWeashop', 'WEAPON_BALL', 50),
(33, 'GunShop', 'WEAPON_SMOKEGRENADE', 100),
(34, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 100),
(35, 'BlackWeashop', 'WEAPON_APPISTOL', 1100),
(36, 'BlackWeashop', 'WEAPON_CARBINERIFLE', 12000),
(37, 'BlackWeashop', 'WEAPON_HEAVYSNIPER', 30000),
(38, 'BlackWeashop', 'WEAPON_MINIGUN', 45000),
(39, 'BlackWeashop', 'WEAPON_RAILGUN', 50000),
(40, 'BlackWeashop', 'WEAPON_STICKYBOMB', 500);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addon_account`
--
ALTER TABLE `addon_account`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_addon_account_data_account_name_owner` (`account_name`,`owner`),
  ADD KEY `index_addon_account_data_account_name` (`account_name`);

--
-- Indexes for table `addon_inventory`
--
ALTER TABLE `addon_inventory`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_addon_inventory_items_inventory_name_name` (`inventory_name`,`name`),
  ADD KEY `index_addon_inventory_items_inventory_name_name_owner` (`inventory_name`,`name`,`owner`),
  ADD KEY `index_addon_inventory_inventory_name` (`inventory_name`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `datastore`
--
ALTER TABLE `datastore`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `datastore_data`
--
ALTER TABLE `datastore_data`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `index_datastore_data_name_owner` (`name`,`owner`),
  ADD KEY `index_datastore_data_name` (`name`);

--
-- Indexes for table `fine_types`
--
ALTER TABLE `fine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `job_grades`
--
ALTER TABLE `job_grades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licenses`
--
ALTER TABLE `licenses`
  ADD PRIMARY KEY (`type`);

--
-- Indexes for table `old_owned_vehicles`
--
ALTER TABLE `old_owned_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `owned_properties`
--
ALTER TABLE `owned_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owned_vehicles`
--
ALTER TABLE `owned_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_calls`
--
ALTER TABLE `phone_calls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_messages`
--
ALTER TABLE `phone_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rented_vehicles`
--
ALTER TABLE `rented_vehicles`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plate` (`plate`);

--
-- Indexes for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_likes_twitter_accounts` (`authorId`),
  ADD KEY `FK_twitter_likes_twitter_tweets` (`tweetId`);

--
-- Indexes for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_twitter_tweets_twitter_accounts` (`authorId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`identifier`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_licenses`
--
ALTER TABLE `user_licenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `vehicle_categories`
--
ALTER TABLE `vehicle_categories`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `vehicle_sold`
--
ALTER TABLE `vehicle_sold`
  ADD PRIMARY KEY (`plate`);

--
-- Indexes for table `weashops`
--
ALTER TABLE `weashops`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addon_account_data`
--
ALTER TABLE `addon_account_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `addon_inventory_items`
--
ALTER TABLE `addon_inventory_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cardealer_vehicles`
--
ALTER TABLE `cardealer_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `datastore_data`
--
ALTER TABLE `datastore_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `fine_types`
--
ALTER TABLE `fine_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `glovebox_inventory`
--
ALTER TABLE `glovebox_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `job_grades`
--
ALTER TABLE `job_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `owned_properties`
--
ALTER TABLE `owned_properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `phone_app_chat`
--
ALTER TABLE `phone_app_chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `phone_calls`
--
ALTER TABLE `phone_calls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `phone_messages`
--
ALTER TABLE `phone_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `phone_users_contacts`
--
ALTER TABLE `phone_users_contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `society_moneywash`
--
ALTER TABLE `society_moneywash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trunk_inventory`
--
ALTER TABLE `trunk_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `twitter_accounts`
--
ALTER TABLE `twitter_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `user_inventory`
--
ALTER TABLE `user_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2556;

--
-- AUTO_INCREMENT for table `user_licenses`
--
ALTER TABLE `user_licenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `weashops`
--
ALTER TABLE `weashops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `twitter_likes`
--
ALTER TABLE `twitter_likes`
  ADD CONSTRAINT `FK_twitter_likes_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`),
  ADD CONSTRAINT `FK_twitter_likes_twitter_tweets` FOREIGN KEY (`tweetId`) REFERENCES `twitter_tweets` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `twitter_tweets`
--
ALTER TABLE `twitter_tweets`
  ADD CONSTRAINT `FK_twitter_tweets_twitter_accounts` FOREIGN KEY (`authorId`) REFERENCES `twitter_accounts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
